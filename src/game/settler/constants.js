import { Specificity } from "../../constants.js";

export class GameSpecificity extends Specificity {
    static AREA = 5;
    static PATH = 10;
    static TERRAIN_FEATURE = 20;
    static SETTLER = 50;
    static BUILDING = 55;
}

export class Goods {
    static Wood = 1;
    static Stone = 2;
    static Coal = 10;
    static IronOre = 11;
    static GoldOre = 12;
    static Axe = 20;
    static Pickaxe = 21;
}
