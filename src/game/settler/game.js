import { Point } from "../../point.js";
import { TextBlock } from "../../text.js"
import { getRandomInt, shuffle, extend, objectFlip } from "../../helper/helper.js";
import { Perlin } from "../../helper/noise.js";
import { MatrixCanvas } from "../../matrix-canvas.js";
import { MurkelCanvas } from "../../murkel-canvas.js";
import { GameSpecificity, Goods } from "./constants.js";
import {
    Terrain, Water, Land, Mountain, River, Path,
    Depletable, Growing, Forest, Rock, Swamp, Iron, Gold, Coal,
    Civilization, CivArea,
} from "./game-object/all.js";
import { getRenderer } from "./renderer.js";
import { AI } from "./ai.js";

// for debugging
import { Marker } from "../../matrix/matrix-object.js";

// For re-use in map generator demo
export {
    Terrain, Water, Land, Mountain, River,
    Forest, Rock, Swamp, Iron, Gold, Coal,
    getRenderer,
};


export class SettlerGame extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        cursor: false,
        getRenderer: getRenderer,
        zoom: true,
        zoomSteps: 10,
        initialZoom: 300,

        // gridSize is only used for noise generation
        gridSize: 2,
        // …we only used every Nth point from the originally generated noise,
        // which results in a `gridSize = gridSize * coarseMatrix`;
        coarseMatrix: 5,

        // Percentage value between 0 and 1 that indicates terrain height
        waterLevel: 0.45, // up to
        // …everything in between is land
        mountainLevel: 0.8, // starting from

        textBlockOptions: {
            size: 14,
            textAlign: "left",
            background: "rgba(255,255,255,.8)",
            padding: 16,
        },

        removeAutoDisplayAfter: 15000,

        computerPlayerCount: 10,
        playerColors: [
            "#0000FF", "#8A2BE2", "#A52A2A", "#7FFF00", "#FF7F50", "#DC143C", "#00FFFF", "#008B8B", "#B8860B", "#FFFF00",
            "#006400", "#00CED1", "#FF1493", "#B22222", "#FF00FF", "#FFD700", "#ADFF2F", "#4B0082", "#FF00FF", "#800000",
            "#BA55D3", "#9370DB", "#3CB371", "#7B68EE", "#C71585", "#191970", "#FF4500", "#4169E1", "#87CEEB", "#6A5ACD", 
        ],
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        // Internal storage to check map validity
        this.hasTerrain = [];
        this.hasTerrain[Water.name] = false;
        this.hasTerrain[Land.name] = false;
        this.hasTerrain[Mountain.name] = false;
        this.hasTerrain[River.name] = false;

        let perlin = new Perlin(this.cols, this.rows, (n) => {
            let terrainConstructor;
            if(n < this.options.waterLevel) {
                terrainConstructor = Water;
                this.hasTerrain[Water.name] = true;
            } else if(n < this.options.mountainLevel) {
                terrainConstructor = Land;
                this.hasTerrain[Land.name] = true;
            } else {
                terrainConstructor = Mountain;
                this.hasTerrain[Mountain.name] = true;
            }
            return new terrainConstructor(n);
        });

        perlin.getNoise2D().forEach((row, y) => {
            row.forEach((terrain, x) => {
                let position = new Point(x, y);
                this.matrix.setValue(position, terrain);

                // Some tiles may have a random feature added
                if(terrain instanceof Land) {
                    if(terrain.height > 0.7) {
                        // Add a forest to many land tiles with higher elevation
                        if(Math.random() > 0.2) {
                            this.matrix.addValue(position, new Forest(getRandomInt(69, 100)));
                        }

                    } else if(Math.random() > terrain.height) {
                        // And some random object to some other fields 
                        let terrainFeature;
                        let featureFactor = Math.random();
                        if(featureFactor < 0.1) {
                            terrainFeature = new Swamp();
                        } else if(featureFactor < 0.25) {
                            terrainFeature = new Rock(getRandomInt(69, 100));
                        } else if(featureFactor < 0.75) {
                            terrainFeature = new Forest(getRandomInt(69, 100));
                        }

                        if(terrainFeature) {
                            this.matrix.addValue(
                                position,
                                terrainFeature,
                            );
                        }
                    }
                } else if(terrain instanceof Mountain) {
                    // Add some ore to a mountain
                    if(Math.random() < 0.5) {
                        let ore;
                        let oreFactor = Math.random();
                        if(oreFactor < 0.1) {
                            ore = new Gold(getRandomInt(25, 100));
                        } else if(oreFactor < 0.5) {
                            ore = new Iron(getRandomInt(59, 100));
                        } else {
                            ore = new Coal(getRandomInt(59, 100));
                        }

                        this.matrix.addValue(position, ore);
                    }
                }
            });
        });

        this.matrix.coarse(this.options.coarseMatrix);
        this.setRowsCols(this.matrix.rows, this.matrix.cols);
        this.updateVisiblePortion();

        // Smoothen map
        this.matrix.forEach((point, values) => {
            for(let i in values) {
                if(values[i] instanceof Terrain) {
                    values[i].setNeighbours(point, this);
                }
            }
        });

        let playerPositions = this.findPossiblePlayerPositions();
        if(playerPositions.length == 0) {
            throw new Error("No possible free player positions.");
        }

        if(this.options.debug) {
            // Draw all possible player positions
            playerPositions.forEach(pp => this.matrix.addValue(pp, new Marker("rgba(255, 0, 0, 0.5)")));
        }

        // Add computer players
        if(playerPositions.length < this.options.computerPlayerCount) {
            throw new Error("Generated map does not provide enough room for " + this.options.computerPlayerCount + " player start positions.");
        }

        let playerCount = this.options.computerPlayerCount;
        let AISpeed = 2000;
        this.computerPlayer = [];
        for(let i = 0; i < playerCount; i++) {
            let newCiv = new Civilization(this, playerPositions[i], {
                color: this.options.playerColors[i],
            });

            // Setup AI to control this civilization
            this.computerPlayer[newCiv.id] = new AI(newCiv, {
                speed: AISpeed,
            });

            // Each new AI is one second slower in thinking than the previous
            AISpeed = AISpeed + 500;

            // Add civilization to matrix
            this.matrix.addValue(playerPositions[i], newCiv);
        }

        // Start at a given zoom level (in percent; e.g. in options: `{initialZoom: 300}`)
        if(this.options.initialZoom) {
            this.zoom(this.options.initialZoom);
        }

        this.lastClickedTile = false;
        this.lastClickedTileUpdate = performance.now();
    }

    onPointerDown(pointer) {
        super.onPointerDown(pointer);

        this.lastClickedTile = this.matrixPointFromPointer(pointer);
        this.lastClickedTileUpdate = performance.now();
    }

    findPossiblePlayerPositions(minPlayerDistance = 9) {
        let playerPosCandidates = [];
        this.matrix.forEach((point, values) => {
            let terrain;
            for(let i = values.length - 1; i >= 0; i--) {
                if(values[i] instanceof Iron) {
                    let waterNearIron = this.matrix.findNearestWithType(point, Water);
                    let midpoint = point.midpoint(waterNearIron).midpoint(waterNearIron);
                    midpoint.x = Math.floor(midpoint.x);
                    midpoint.y = Math.floor(midpoint.y);

                    if(this.matrix.hasValue(midpoint, Land)) {
                        playerPosCandidates.push(midpoint);
                    }
                }
            }

        });

        if(playerPosCandidates.length == 0) {
            throw new Error("No possible player positions candidates.");
        }

        // Pick three random positions that are not too close to each other
        let possiblePos = [];

        shuffle(playerPosCandidates).forEach(ppp => {
            // This exact point was already added; ignore duplicate
            if(ppp.inList(possiblePos)) {
                return;
            }

            let minDistanceToOtherPlayer = Number.MAX_SAFE_INTEGER;
            possiblePos.forEach(pp => {
                let distanceToOtherPlayer = ppp.distanceTo(pp);
                if(distanceToOtherPlayer < minDistanceToOtherPlayer) {
                    minDistanceToOtherPlayer = distanceToOtherPlayer;
                }
            });

            if(minDistanceToOtherPlayer > minPlayerDistance) {
                possiblePos.push(ppp);
            }
        });

        return possiblePos;
    }

    /**
     * Overload MatrixCanvas.content() _without_ call to super.
     *
     * The main difference in this version is, that it does not run the Matrix-/GameObjects,
     * but relies on those objects having their own run loop.
     * 
     * @todo Add possibility to MatrixCanvas to not run each object per main loop execution.
     *       We still rely on the original version in MatrixCanvas for e.g. the Boulder Game.
     */
    content(removeDirection = true) {
        // Call grandparent super, which is MurkelCanvas (not MatrixCanvas)
        MurkelCanvas.prototype.content.call(this);

        /* ##### code copied from MatrixCanvas minus `matrixObject.run()` ##### */

        // Move cursor if the user is pressing a key during this paint cycle
        if(this.direction !== null) {
            this.moveCursor(this.direction);
            if(removeDirection === true) {
                this.direction = null;
            }
        }

        for(let x = this.visibleFrom.x; x <= this.visibleTo.x; x++) {
            for(let y = this.visibleFrom.y; y <= this.visibleTo.y; y++) {
                let point = new Point(x, y);
                let values = this.matrix.getValues(point);
                // Add to layer
                values.forEach(matrixObject => {
                    this.layers.layer(matrixObject.specificity).addContent(() => {
                        // Render the current object
                        this.renderer.render(matrixObject, point.x, point.y);
                    });
                });
            }
        }

        if(this.options.debug) {
            // Display cursor coordinates.
            this.layers.layer(GameSpecificity.FOREGROUND).addContent(() => this.drawPosition());
        }


        /* ##### SettlerGame specific code ##### */

        // If the user had clicked somewhere, display infos about the values from the corresponding matrix field.
        if(this.lastClickedTile) {
            // Ensure to remove (not add again) the message after some time
            if(performance.now() < this.lastClickedTileUpdate + this.options.removeAutoDisplayAfter) {
                let namedLayers = objectFlip(GameSpecificity);
                let namedGoods = objectFlip(Goods);

                this.layers.layer(GameSpecificity.FOREGROUND).addContent(() => {
                    let msg = new TextBlock(this.context, this.gridSizeX, this.gridSizeY, this.options.textBlockOptions);
                    msg.heading(`Objects at position x: ${this.lastClickedTile.x}, y: ${this.lastClickedTile.y}`);
                    msg.text(""); // empty line
                    this.matrix.getValues(this.lastClickedTile).forEach(val => {
                        let line = `– ${val.constructor.name} (`;
                        line += (namedLayers[val.specificity] ? namedLayers[val.specificity].toLowerCase() : "layer " + val.specificity);
                        line += val.passable ? ", passable" : ", not passable";
                        if(val instanceof Terrain) {
                            line += ", " + val.constructor.name;
                        } else if(val instanceof Depletable || val instanceof Growing) {
                            line += ", amount " + val.amount + "%";
                        } else if(val instanceof Path) {
                            line += ", " + JSON.stringify(val.directions);
                        } else if(val instanceof CivArea && val.isEdge == true) {
                            line += ", edge";
                        }
                        line += ")";
                        msg.text(line);
                    });
                    msg.draw();
                });

                // Civ display
                if(this.matrix.hasValue(this.lastClickedTile, CivArea)) {
                    let civilization = this.matrix.getValue(this.lastClickedTile, CivArea).kontor.civilization;
                    this.layers.layer(GameSpecificity.FOREGROUND).addContent(() => {
                        let msg = new TextBlock(this.context, this.gridSizeX, this.height / 2, this.options.textBlockOptions);
                        msg.heading(`Civilization`, { background: civilization.color });
                        msg.text("ID: " + civilization.id);
                        msg.text("Color: " + civilization.color);
                        msg.text("███████████████", { color: civilization.color });
                        msg.text("");

                        msg.heading("Civ Storage");
                        for(let article in civilization.storage.storage) {
                            msg.text(" – " + namedGoods[article] + ": " + civilization.storage.storage[article]);
                        }
                        msg.text("");

                        msg.heading("Computer player");
                        msg.text("Speed: " + this.computerPlayer[civilization.id].options.speed);
                        let actions = [];
                        this.computerPlayer[civilization.id].actions.actions.forEach(aga => actions.push(aga.callback.name));
                        msg.text("Actions: " + actions.join(", "));

                        msg.draw();
                    });
                }
            }
        }
    }

    /**
     * Paint cursor crosshair;
     * This layer gets special treament, as it is the only one rendered when the main loop is paused.
     */
    pointerLayer() {
        if (this.canvas.matches(':hover')) {
            this.layers.layer(GameSpecificity.POINTER).addContent(() => {
                this.context.lineWidth = 2;
                this.context.fillStyle = "rgba(255, 255, 255, 0.8)";
                this.context.strokeStyle = "rgba(30, 30, 30, 0.5)";
                let crosshairLength = 30 / 2;
                let crosshairWidth = 4 / 2;

                this.context.beginPath();
                this.context.moveTo(this.pointer.x - crosshairLength, this.pointer.y + crosshairWidth);
                this.context.lineTo(this.pointer.x - crosshairLength, this.pointer.y - crosshairWidth);
                this.context.lineTo(this.pointer.x - crosshairWidth, this.pointer.y - crosshairWidth);
                this.context.lineTo(this.pointer.x - crosshairWidth, this.pointer.y - crosshairLength);
                this.context.lineTo(this.pointer.x + crosshairWidth, this.pointer.y - crosshairLength);
                this.context.lineTo(this.pointer.x + crosshairWidth, this.pointer.y - crosshairWidth);
                this.context.lineTo(this.pointer.x + crosshairLength, this.pointer.y - crosshairWidth);
                this.context.lineTo(this.pointer.x + crosshairLength, this.pointer.y + crosshairWidth);

                this.context.lineTo(this.pointer.x + crosshairWidth, this.pointer.y + crosshairWidth);
                this.context.lineTo(this.pointer.x + crosshairWidth, this.pointer.y + crosshairLength);
                this.context.lineTo(this.pointer.x - crosshairWidth, this.pointer.y + crosshairLength);
                this.context.lineTo(this.pointer.x - crosshairWidth, this.pointer.y + crosshairWidth);
                this.context.closePath();
                this.context.fill();
                this.context.stroke();
            });
        }
    }
}
