import { Direction } from "../../constants.js";
import { getTempCanvas } from "../../helper/helper.js";
import { Renderer } from "../../render/renderer.js";
import { AntSquare, LangtonsAnt } from "./langtons-ant-objects.js";

function getAntTile(width, height, direction = Direction.UP, color = "#930") {
    let tileCanvas = getTempCanvas(width, height);
    let tileContext = tileCanvas.getContext('2d');

    // Rotate the image according to direction
    let angle;
    switch(direction) {
        case Direction.UP:
            angle = 0
            break;

        case Direction.RIGHT:
            angle = 90
            break;

        case Direction.DOWN:
            angle = 180
            break;

        case Direction.LEFT:
            angle = 270
            break;
    }

    let centerX = width / 2;
    let centerY = height / 2;
    let radians = angle * Math.PI / 180;

    tileContext.translate(centerX, centerY);
    tileContext.rotate(radians);
    tileContext.translate(centerX * -1, centerY * -1);


    // Draw the ant
    let drawWidth = width / 10;
    let drawHeight = height / 10;
    let border = width / 30;

    tileContext.fillStyle = color;
    tileContext.strokeStyle = "rgba(60, 60, 60, 0.85)";

    tileContext.lineWidth = border / 2;
    tileContext.lineCap = "round";
    tileContext.lineJoin = "round";


    // Legs
    tileContext.beginPath();
    // Left 1
    tileContext.moveTo(drawWidth * 4.6, drawHeight * 4.25);
    tileContext.lineTo(drawWidth * 3.5, drawHeight * 2.5);
    tileContext.lineTo(drawWidth * 2, drawHeight);
    // Left 2
    tileContext.moveTo(drawWidth * 4.5, drawHeight * 5);
    tileContext.lineTo(drawWidth * 3, drawHeight * 5);
    tileContext.lineTo(drawWidth, drawHeight * 5.5);
    // Left 3
    tileContext.moveTo(drawWidth * 4.5, drawHeight * 5.5);
    tileContext.lineTo(drawWidth * 3, drawHeight * 7);
    tileContext.lineTo(drawWidth * 2, drawHeight * 10);

    // Right 1
    tileContext.moveTo(drawWidth * 5.4, drawHeight * 4.25);
    tileContext.lineTo(drawWidth * 6.55, drawHeight * 2.5);
    tileContext.lineTo(drawWidth * 8, drawHeight);
    // Right 2
    tileContext.moveTo(drawWidth * 5.5, drawHeight * 5);
    tileContext.lineTo(drawWidth * 7, drawHeight * 5);
    tileContext.lineTo(drawWidth * 9, drawHeight * 5.5);
    // Right 3
    tileContext.moveTo(drawWidth * 5.5, drawHeight * 5.5);
    tileContext.lineTo(drawWidth * 7, drawHeight * 7);
    tileContext.lineTo(drawWidth * 8, drawHeight * 10);
    tileContext.stroke();


    // Antennae
    tileContext.beginPath();
    // Left
    tileContext.moveTo(drawWidth * 4.5, drawHeight * 1.2);
    tileContext.lineTo(drawWidth * 3.5, drawHeight);
    tileContext.lineTo(drawWidth * 3.5, 0);
    // Right
    tileContext.moveTo(drawWidth * 5.5, drawHeight * 1.2);
    tileContext.lineTo(drawWidth * 6.5, drawHeight);
    tileContext.lineTo(drawWidth * 6.5, 0);
    tileContext.stroke();

    // Head
    tileContext.beginPath();
    tileContext.arc(drawWidth * 5, drawHeight * 2, drawWidth, Math.PI, Math.PI * 2);
    tileContext.roundRect(drawWidth * 4, drawHeight * 2, drawWidth * 2, drawHeight / 2, [0, 0, border, border]);
    tileContext.stroke();
    tileContext.fill();

    // Body
    tileContext.beginPath();
    tileContext.ellipse(drawWidth * 5, drawHeight * 5, drawWidth / 2, drawHeight * 1.25, 0, 0, Math.PI * 2);
    tileContext.stroke();
    tileContext.fill();

    // Breast
    tileContext.beginPath();
    tileContext.ellipse(drawWidth * 5, drawHeight * 3.9, drawWidth / 1.5, drawHeight * 1.25, 0, Math.PI, Math.PI * 2);
    tileContext.closePath();
    tileContext.stroke();
    tileContext.fill();

    // Butt
    tileContext.beginPath();
    tileContext.arc(drawWidth * 5, drawHeight * 7.5, drawWidth, Math.PI * 2, Math.PI);
    tileContext.roundRect(drawWidth * 4, drawHeight * 6, drawWidth * 2, drawHeight * 1.5, [drawWidth / 2, drawWidth / 2, 0, 0]);
    tileContext.stroke();
    tileContext.fill();

    return tileCanvas;
}

export function getRenderer(canvasObject) {
    let renderer = new Renderer(canvasObject);

    renderer.register(AntSquare.name, (context, x, y, width, height, antTileObject) => {
        context.fillStyle = antTileObject.color;
        context.fillRect(x, y, width, height);
    });

    let antIcons = {};
    renderer.register(LangtonsAnt.name, (context, x, y, width, height, antObject) => {
        if(!antIcons[antObject.direction]) {
            antIcons[antObject.direction] = getAntTile(width, height, antObject.direction, antObject.color);
        }
        context.drawImage(antIcons[antObject.direction], x, y);
    });

    return renderer;
}
