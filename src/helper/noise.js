import { Point } from "../point.js";
import { shuffle } from "./helper.js";

/**
 * A noise generator using an implementation of the Perlin algorithm.
 * 
 * Based on code from:
 * @author Copyright (c) 2022 Raouf Touti
 * @license MIT License
 */
export class Perlin {
    constructor(cols, rows, setter = false, frequency = 0.005) {
        this.cols = cols;
        this.rows = rows;
        this.setter = setter || (n => n);
        this.frequency = frequency;

        this.permutation = Perlin.makePermutation();
    }

    getNoise2D() {
        let noiseMatrix = [];

        for(let y = 0; y < this.rows; y++) {
            if(!noiseMatrix[y]) {
                noiseMatrix[y] = [];
            }

            for(let x = 0; x < this.cols; x++) {

                //  Create heightmap with fractal brownian motion
                let n = 0.0,
                    a = 1.0,
                    f = this.frequency;

                for(let o = 0; o < 8; o++) {
                    let v = a * this.getNoise2DPixel(x * f, y * f);
                    n += v;
                    
                    a *= 0.5;
                    f *= 2.0;
                }
                
                n += 1.0;
                n *= 0.5;

                noiseMatrix[y][x] = this.setter(n);
            }
        }

        return noiseMatrix;
    }

    getNoise2DPixel(x, y) {
        const X = Math.floor(x) & 255;
        const Y = Math.floor(y) & 255;

        const xf = x-Math.floor(x);
        const yf = y-Math.floor(y);

        const topRight = new Point(xf-1.0, yf-1.0);
        const topLeft = new Point(xf, yf-1.0);
        const bottomRight = new Point(xf-1.0, yf);
        const bottomLeft = new Point(xf, yf);
        
        // Select a value from the permutation array for each of the 4 corners
        const valueTopRight = this.permutation[this.permutation[X+1]+Y+1];
        const valueTopLeft = this.permutation[this.permutation[X]+Y+1];
        const valueBottomRight = this.permutation[this.permutation[X+1]+Y];
        const valueBottomLeft = this.permutation[this.permutation[X]+Y];
        
        const dotTopRight = topRight.dotProduct(Perlin.getConstantVector(valueTopRight));
        const dotTopLeft = topLeft.dotProduct(Perlin.getConstantVector(valueTopLeft));
        const dotBottomRight = bottomRight.dotProduct(Perlin.getConstantVector(valueBottomRight));
        const dotBottomLeft = bottomLeft.dotProduct(Perlin.getConstantVector(valueBottomLeft));
        
        const u = Perlin.fade(xf);
        const v = Perlin.fade(yf);
        
        return Perlin.lerp(u,
            Perlin.lerp(v, dotBottomLeft, dotTopLeft),
            Perlin.lerp(v, dotBottomRight, dotTopRight)
        );
    }

    static makePermutation() {
        let permutation = [];
        for(let i = 0; i < 256; i++) {
            permutation.push(i);
        }

        permutation = shuffle(permutation);
        
        for(let i = 0; i < 256; i++) {
            permutation.push(permutation[i]);
        }
        
        return permutation;
    }

    /**
     * @param {Number} v is a value from the permutation table
     */
    static getConstantVector(v) {
        const h = v & 3;
        if(h == 0)
            return new Point(1.0, 1.0);
        else if(h == 1)
            return new Point(-1.0, 1.0);
        else if(h == 2)
            return new Point(-1.0, -1.0);
        else
            return new Point(1.0, -1.0);
    }

    static fade(t) {
        return ((6*t - 15)*t + 10)*t*t*t;
    }

    static lerp(t, a1, a2) {
        return a1 + t*(a2-a1);
    }
}
