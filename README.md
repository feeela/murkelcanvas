# Murkel Canvas – A rudimentary 2D game engine for a HTML canvas

See MurkelCanvas, Matrix & Malkasten in action at [spiele.imstadtpark.de](http://spiele.imstadtpark.de/).

## [MurkelCanvas](src/murkel-canvas.js)

* Provides an internal grid, based on given rows & columns
* Has layer to stack drawn contents and keep render ordering for any drawn objects (like in Photoshop, but on canvas)
* Render loop can be paused
* Provides a base function to draw the mouse/pointer cursor
* Canvas size is set from CSS sizing
* Scales according to `window.devicePixelRatio`

### Options

* **gridSize**: Defines the width and height of a grid tile; determines how many of it will fit onto the canvas using canvas width and height; rounds down. (overrides `options.rows` and `options.cols`; default: `false`).
* **rows** and **cols**: Defines into how many rows and columns the canvas should be divided, given the current canvas width and height. (default: `{ rows: 24, cols: 80 }`)
* **backgroundColor**: The whole canvas is filled with this color first on each paint. (default: `false`)
* **paintSpeed**: App tick; the time in microseconds to wait before each step in the main render loop. (default: `50`)
* **debug**: Print computed frames per second (FPS) at the lower right corner of the canvas

##### Example

<strong>MurkelCanvas is not meant to be instantiated directly</strong>, but overloaded by your own class definitions. 

```javascript
import { MurkelCanvas } from "../js/lib-murkel-canvas.js";

class MurkelCanvasDemo extends MurkelCanvas {
    content() {
        super.content();

        this.layers.layer().addContent(() => {
            this.context.fillStyle = "palevioletred";

            // x & y are starting at zero in the grid
            let x = 7 * this.gridSizeX;
            let y = 6 * this.gridSizeY;
            this.context.fillRect(x, y, this.gridSizeX, this.gridSizeY);
        });
    }
}

let murkelCanvas = new MurkelCanvasDemo(document.querySelector('#my-canvas'), {
    rows: 10,
    cols: 10,
    backgroundColor: "#aac",
    debug: true,
});
murkelCanvas.run();
```

## [Matrix](src/matrix/matrix.js)

Extends MurkelCanvas and uses an internal 2D array of fields (tiles) in the grid.

* Has a **cursor property** to mark the current controllable position
	* Is a [Point](src/point.js)
	* Can be moved using the keyboard arrow keys
	* Has collision detection (can not move onto tiles, that hold at least one object that is not passable)
* Each **field can hold multiple objects**
* **Matrix objects** are objects that can be placed on any field in the matrix. They must extend [MatrixObject](src/matrix/objects.js), which defines some properties:
	* `passable`: Determines if a field with such an object on it is passable (by e.g. the cursor or player)
	* `movable`: Determines if this object can be moved from one field to the other
	* `unique`: For behavior and rendering, unique objects must be instantiated each time, while non-unique objects may be reused by reference. (e.g. a "wall" object is reused by reference to draw many walls all over the map while a "monster" or "foe" object should be unique, to allow independent rendering of the same class multiple times in a map)
* **Has a renderer** which is responsible on how to [actually draw](src/matrix/default-renderer.js) each matrix object
	* The renderer may be overridden via `options.getRenderer` or can be extended in overloaded classes by using `this.renderer.register("object-class-name", (context, x, y, width, height) => { … });`; Example renderers are:
		* [Boulder game renderer](src/game/boulder/boulder-renderer.js) which reuses [tile patterns](src/game/pattern.js) that are drawn onto a temporary canvas with the dimensions of a single field in the matrix
		* [Minesweeper renderer](src/game/minesweeper/minesweeper-renderer.js)
* Provides utility functions to:
	* check `Point`'s (if they are still within the matrix or are passable)
	* get objects (identified by their class) from a field
	* remove objects (identified by their class) from a field
	* move objects from one field to the other
	* find `Point`'s for neighbouring fields

### Options

Matrix has all the options of MurkelCanvas plus:

* **cursor**: Defines if a keyboard controllable cursor is used and if yes, which matrix object type should be used to represent it in the matrix.
	* **not set / `undefined`**: A cursor will be used, the Matrix constructor uses `new Cursor()` as the object (this is the **default**)
	* **`false`**: Do not use a cursor
	* **class name**: A cursor will be used, but with a specific matrix object representing it
* **getRenderer**: A function that must return an instance of [Renderer](src/matrix/renderer.js).
	* [Boulder game example](src/game/boulder/boulder-renderer.js)
* **debug**: Print the current cursor position at the lower left corner of the canvas.

##### Example

A plain Matrix object with a keyboard movable cursor and you can place objects on it.

```javascript
import { Matrix, Wall } from "../js/lib-matrix.js";
import { Point } from "../js/lib-murkel-canvas.js";

let matrix = new Matrix(document.querySelector('#my-canvas'), {
	rows: 10,
	cols: 10,
    backgroundColor: "#aac",
    debug: true,
});
matrix.run();

matrix.addToPoint(new Point(4, 5), new Wall());
matrix.addToPoint(new Point(4, 6), new Wall());
matrix.addToPoint(new Point(5, 7), new Wall());
matrix.addToPoint(new Point(5, 8), new Wall());
```

## A* path finding algorithm implementation for Matrix

The [Astar class](src/matrix/astar.js) is an implementation of the [A* path finding algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm) on the Matrix object, using the `passable` property of the objects on each field to find a path from a start `Point` to an end `Point`.

##### Example

```javascript
import { Matrix } from "src/matrix/matrix.js";
import { Cursor, Wall } from "src/matrix/objects.js";
import { Point } from "src/point.js"
import { Astar } from "src/matrix/astar.js";

let matrix = new Matrix(document.querySelector('#my-canvas'), {
    rows: 10,
    cols: 10,
    cursor: false,
});
matrix.run();
matrix.addToPoint(new Point(3, 2), new Wall());
matrix.addToPoint(new Point(3, 3), new Wall());
matrix.addToPoint(new Point(3, 4), new Wall());
matrix.addToPoint(new Point(3, 5), new Wall());
matrix.addToPoint(new Point(3, 6), new Wall());

let astar = new Astar(matrix);
let path = astar.search(new Point(2, 4), new Point(7, 4));

// console.log(path);
// > [WeightedPoint, …, WeightedPoint]

path.forEach(point => {
	// `new Cursor` here is just a random MatrixObject to display the found path,
	// could be any other MatrixObject and has nothing to do with the internal
	// keyboard cursor (which is not used via `options.cursor = false` above).
    matrix.addToPoint(point, new Cursor());
});
```


## Malkasten

Malkasten is distinct from MurkelCanvas or Matrix and does not feature a grid; besides that it has a similar functionality to MurkelCanvas.

* API works with percentage coordinates (i.e. `new Point(50, 50)` is in the center of the canvas)
* Has layer to stack drawn contents and keep render ordering for any drawn objects
* Render loop can be paused
* Provides a base function to draw the mouse/pointer cursor
* Canvas size is set from CSS sizing
* Scales according to `window.devicePixelRatio`

### Options

* **backgroundColor**: The whole canvas is filled with this color first on each paint. (default: `false`)
* **pointer**: A function to draw a custom pointer cursor (i.e. mouse cursor).
* **speed**: App tick; the time in microseconds to wait before each step in the main render loop. (default: `50`)
* **debug**: Print computed frames per second (FPS) at the lower right corner of the canvas

### Malkasten Tools

You can add `CanvasObjects` to the Malkasten, which tick in their own timing (that also can be paused), have a layer specificity and behaviors which may be added to each object.

#### `CanvasObject` base class

##### Options

* **lineWidth**: The stroke width in percentage (default: `0.25`)
* **speed**: Object tick in milliseconds (defaul: `50`)
* **specificity**: Set the layer to paint this object on (default: `50`); see layer.js

##### `CanvasObject` API

* **setCenter(Point: point)** / **getCenter() : Point**: Each object has one internal position, which should be the center; this point is either the actual object position (e.g. for `Circle`) or a computed value (e.g. for `Rect` or `Polygon`)
* **stroke(context, drawCallback, color)** / **fill(context, drawCallback, color)**: Helper methods to draw paths and either stroke or fill them.
* **addBehavior(Function: behavior)**: Behavior callbacks may be used to animate an object.<br> Each `CanvasObject` may have several behaviors added, which are callback functions that are executed in the objects run-loop in the order they were added. `behavior` is a callback that is executed with the current `CanvasObject` as `this` and a single argument `context`.<br>*Do not use arrow syntax, since arrow functions don't have a `this`*.
	* May return another callback which is executed during paint and can be used to directly draw onto the canvas from within a behavior.
	* Example behaviors can be found in [malkasten/objects/behavior.js](src/malkasten/objects/behavior.js)

```javascript
canvasObject.addBehavior(function(context) {
	// manipulate `this` (which is the CanvasObject this callback was added to)
	// or apply transforms to the context;

	let center = this.getCenter();
	center.x++;
	this.setCenter(center);

	// optional:
	return function(context) {
		// draw something onto context
	};
});
```


#### `Gradient` helper class

Not a CanvasObject, but a helper class to create gradients from a center point and an angle (in degree). Can be animated. The `Gradient` class has a method `fillStyle()` to get a value to be used in `canvas.context.fillStyle`.

##### Example


```javascript
const malkasten = new Malkasten(document.querySelector('#my-canvas'));
let gradient = new Gradient(malkasten, new Point(25, 25), ["red", "blue", "yellow"], 45);
malkasten.context.fillStyle = gradient.fillStyle();

```

#### Implemented basic tools

The [`malkasten/objects/geometry.js`](src/malkasten/objects/geometry.js) contains some basic objects to be placed on the canvas.

* **Circle**: `new Circle(malkasten, position, options)`
* **Rect**: `new Rect(malkasten, position, options)`
* **Line**: `new Line(malkasten, from, to, options)`
* **CubicBezier**: `new CubicBezier(malkasten, from, cp1, cp2, to, options)`
* **Polygon**: `new Line(malkasten, points, options)`

Each of those inherits the `intersect()` method to check for intersection between two objects. The following combinations are implemented:

* **from**: Line **to**: Line, Polygon
* **from**: Circle **to**: Circle, Rect, Polygon
* **from**: Rect **to**: Circle, Rect, Polygon
* **from**: Polygon **to**: Circle, Rect, Line, Polygon

##### Example

```javascript
const malkasten = new Malkasten(document.querySelector('#my-canvas'));

malkasten.objects.add(new Circle(malkasten, new Point(75, 75)));

let line1 = new Line(malkasten, new Point(10, 20), new Point(20, 10), {
    color: "green",
});
malkasten.objects.add(line1);

let line2 = new Line(malkasten, new Point(10, 10), new Point(20, 20), {
    color: "blue",
    lineWidth: 1,
});
malkasten.objects.add(line2);

console.log( line1.intersects(line2) );
```
