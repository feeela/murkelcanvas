import { extend } from "../../../helper/helper.js";
import { ActionGenerator } from "../../../helper/action-generator.js";
import { MatrixObject } from "../../../matrix/matrix-object.js";
import { Direction } from "../../../constants.js";
import { GameSpecificity } from "../constants.js";

/**
 * Base class for any in-game objects, that are displayed in the matrix.
 */
export class GameObject extends MatrixObject {
    static defaultOptions = {
        debug: false,
    };

    constructor(passable = true, movable = false, options = {}, specificity = GameSpecificity.DEFAULT) {
        super(passable, movable, specificity);

        this.options = extend(this.constructor.defaultOptions, options);
    }
}

export class GameObjectWithActions extends GameObject {
    static defaultOptions = {
        speed: 500,
    };

    /**
     * @param {SettlerGame} game
     */
    constructor(game, passable = true, movable = false, options = {}, specificity = GameSpecificity.DEFAULT) {
        super(passable, movable, options, specificity);

        this.game = game;
        this.paused = false;
        this.actions = new ActionGenerator();
        this.previousTimestamp = false;

        this.run();
    }

    addAction(callback) {
        this.actions.addAction(callback);
    }

    removeAction(callback) {
        this.actions.removeAction(callback);
    }

    /**
     * Is called once every game tick and executes the object's actions.
     */
    run() {
        setTimeout(() => {
            this.run();
        }, this.options.speed);

        if(this.paused || this.game.isPaused) {
            return;
        }

        this.actions.runAction(this);
    }
}

/**
 * CivArea is used by Civilization to mark the own territory.
 * The class is defined here, because of its usage in builing.js,
 * which would result in circular imports if the CivArea class
 * were to reside in civilization.js..
 */
export class CivArea extends GameObject {
    /**
     * @param {Kontor} kontor A CivArea knows to which Kontor and thus to which Civilization it belongs.
     * @param {Boolean} isEdge Indicates that this CivArea is on the edge onf the current territory.
     * @param {Direction[]} direction Orientation directions
     */
    constructor(kontor, isEdge = false, directions = []) {
        super(true, false, {}, GameSpecificity.AREA);

        this.kontor = kontor;
        this.isEdge = isEdge;
        this.directions = directions;
    }
}

export class Settler extends GameObject {
	constructor(color, direction = Direction.DOWN) {
		super(true, false, {}, GameSpecificity.SETTLER);

        this.color = color;
        this.direction = direction;
	}
}
