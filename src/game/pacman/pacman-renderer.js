import * as GameObjects from "../matrix/objects/game.js";
import { Renderer } from "./matrix-renderer.js";
import { getMonsterTile } from "./pattern.js";

/**
 * @var {2DRenderContext} 
 */
export function getRenderer(canvasObject) {
 	let renderer = new Renderer(canvasObject);

    const icons = {};

    /* PacMan */

    // Differently colored ghosts
    const renderGhost = (context, x, y, width, height, ghostObject) => {
        if(!icons["ghost" + ghostObject.color]) {
            icons["ghost" + ghostObject.color] = getMonsterTile(width, height, ghostObject.color);
        }
        context.drawImage(icons["ghost" + ghostObject.color], x, y);
    };

    renderer.register(GameObjects.Ghost.name, renderGhost);
    renderer.register(GameObjects.Blinky.name, renderGhost);
    renderer.register(GameObjects.Pinky.name, renderGhost);
    renderer.register(GameObjects.Inky.name, renderGhost);
    renderer.register(GameObjects.Clyde.name, renderGhost);

    // Pebble or pill
    renderer.register(GameObjects.Pebble.name, (context, x, y, width, height) => {
        context.beginPath();
        context.fillStyle = "#99c";
        context.arc(x + width / 2, y + height / 2, width / 3, 0, 2 * Math.PI);
        context.fill();
    });

    return renderer;
}
