import { Point } from "../point.js";
import { getRandomInt } from "../helper/helper.js";
import { Direction, Specificity } from "../constants.js";
import { Behavior } from "../matrix/matrix-object-behavior.js";
import { Astar } from "../matrix/astar.js";


export class WaitBehavior extends Behavior {
    constructor() {
        super();
    }
}


export class ManhattanBehavior extends Behavior {
    constructor(steps = 3) {
        super();
        this.direction = null;
        this.steps = steps;
        this.currentStep = 1;
    }

    run(matrixCanvas, currentPoint, self) {
        if(this.direction !== null) {
            // move towards target
            if(this.currentStep <= this.steps) {
                let targetPoint = currentPoint.move(this.direction);
                /** @todo matrixCanvas.matrix.cursor is undefined in rare case; investigate */
                if(matrixCanvas.matrix.isValidPoint(targetPoint)
                    && (matrixCanvas.matrix.isPassable(targetPoint) || matrixCanvas.matrix.cursor.equals(targetPoint))) {
                    matrixCanvas.matrix.moveValue(currentPoint, targetPoint, self.constructor);
                    this.currentStep++;
                }
            } else {
                this.currentStep = 1;
                this.direction = null;
            }
        } else {
            // find a direction
            let possibleDirections = this.findDirection(matrixCanvas, currentPoint, this.steps);

            if(possibleDirections !== false) {
                this.direction = possibleDirections[getRandomInt(0, possibleDirections.length - 1)];
            }
        }
    }

    /**
     * Look "steps" fields ahead in all directions to determine, which directions are open to go to.
     */
    findDirection(matrixCanvas, point, steps) {
        let possibleDirections = [];

        for(let direction in Direction) {
            let currentPoint = point;
            let stepsCounted = 0;

            for(let i = 0; i < steps; i++) {
                let nextField = currentPoint.move(Direction[direction]);

                if(!matrixCanvas.matrix.isValidPoint(nextField)
                    || (nextField.x == currentPoint.x && nextField.y == currentPoint.y)
                    || (!matrixCanvas.matrix.isPassable(nextField)) && !matrixCanvas.cursor.equals(nextField)) {
                    break;
                }

                currentPoint = nextField;
                stepsCounted++;
            }

            if(stepsCounted == steps) {
                possibleDirections.push(Direction[direction]);
            }
        }

        return possibleDirections.length > 0 ? possibleDirections : false;
    }
}

export class ChaiseCursorBehavior extends Behavior {
    constructor() {
        super();

        this.path = [];
    }

    reset() {
        this.lastCursor = new Point(-1, -1);
    }

    run(matrixCanvas, currentPoint, self) {
        if(!this.astar) {
            // Initialize path searching object
            this.astar = new Astar(matrixCanvas.matrix);
        }

        if(!matrixCanvas.cursor.equals(this.lastCursor)) {
            this.path = this.astar.search(currentPoint, matrixCanvas.cursor);
            this.lastCursor = matrixCanvas.cursor;
        }

        if(this.path.length > 0) {
            let nextStep = this.path.shift();

            if(matrixCanvas.matrix.isValidPoint(nextStep)
                && (matrixCanvas.matrix.isPassable(nextStep) || matrixCanvas.cursor.equals(nextStep))) {
                matrixCanvas.matrix.moveValue(currentPoint, nextStep, self.constructor);
            }
        }
    }
}
