import { extend } from "../../helper/helper.js";
import { MatrixCanvas } from "../../matrix-canvas.js";
import { getRenderer } from "./conway-renderer.js";
import { ConwayCell } from "./conway-objects.js";

export { ConwayCell };

export class ConwayCanvas extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
    	speed: 250,
        gridSize: 25,
        cursor: false,
        getRenderer: getRenderer,
        fillConstructor: ConwayCell,
        fillDistinct: true,
    });

    constructor(canvas, options = {}) {
		/* Set speed from options as speed for MatrixObject (ConwayCell) */
		options.fillConstructorArguments = [false, options.speed];

		/* Decrease paintSpeed for low values of speed */
		if(options.speed < options.paintSpeed) {
        	options.paintSpeed = options.speed;
        }

        super(canvas, options);
    }

	// User controllable ConwaySquare state
	onPointerDown(pointer) {
		super.onPointerDown(pointer);

		if(!this.isPaused) {
			/* this.lastClickedTile is updated in super.onPointerDown() */
			let cell = this.matrix.getValue(this.lastClickedTile, ConwayCell);
			cell.flip();
		}
	}
}
