import { Direction } from "../constants.js";
import { Point } from "../point.js";
import { getTempCanvas } from "../helper/helper.js";
import { toRGB, saturation, light, rgbValuesAsColorString } from "../helper/color.js";

/**
 * Draw something that looks like a stilized brick in a wall
 */
export function drawSingleBrick(context, x, y, width, height, border = 4) {
    context.beginPath();
    context.fillStyle = "#d00";
    context.moveTo(x, y);
    context.lineTo(x + width, y);
    context.lineTo(x, y + height);
    context.fill();

    context.beginPath();
    context.fillStyle = "#b00";
    context.moveTo(x + width, y);
    context.lineTo(x + width, y + height);
    context.lineTo(x, y + height);
    context.fill();

    context.strokeStyle = "#bdd";
    context.lineWidth = border;
    context.strokeRect(x, y, width, height);
}

/**
 * Draw bricks as repeatable pattern
 */
export function getBricksTile(tileWidth, tileHeight) {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    // set brick- and border-size according to tile size
    let brickWidth = tileWidth / 3;
    let brickHeight = tileHeight / 4;
    let border = tileWidth / 30;

    // line 1
    drawSingleBrick(tileContext, 0, 0, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth, 0, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth * 2, 0, brickWidth, brickHeight, border);

    // line 2
    drawSingleBrick(tileContext, 0 - brickWidth / 2, brickHeight, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth / 2, brickHeight, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth + brickWidth / 2, brickHeight, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth * 2 + brickWidth / 2, brickHeight, brickWidth, brickHeight, border);

    // line 3
    drawSingleBrick(tileContext, 0, brickHeight * 2, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth, brickHeight * 2, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth * 2, brickHeight * 2, brickWidth, brickHeight, border);

    // line 4
    drawSingleBrick(tileContext, 0 - brickWidth / 2, brickHeight * 3, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth / 2, brickHeight * 3, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth + brickWidth / 2, brickHeight * 3, brickWidth, brickHeight, border);
    drawSingleBrick(tileContext, brickWidth * 2 + brickWidth / 2, brickHeight * 3, brickWidth, brickHeight, border);

    return tileCanvas;
}

export function getDoorTile(tileWidth, tileHeight, locked = false) {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 6;
    let drawHeight = tileHeight / 6;
    let border = tileWidth / 30;
    let halfBorder = border / 2;

    tileContext.lineWidth = border;
    tileContext.strokeStyle = "brown";

    // planks
    tileContext.beginPath();

    let gradient1 = tileContext.createLinearGradient(0, 0, 0 + tileWidth, 0);
    let glth = border / tileWidth / 2;
    gradient1.addColorStop(0, "#ca6");
    gradient1.addColorStop(0.25 - glth, "#ca6");
    gradient1.addColorStop(0.25 - glth, "#666");
    gradient1.addColorStop(0.25 + glth, "#666");
    gradient1.addColorStop(0.25 + glth, "#ca6");
    gradient1.addColorStop(0.5 - glth, "#ca6");
    gradient1.addColorStop(0.5 - glth, "#666");
    gradient1.addColorStop(0.5 + glth, "#666");
    gradient1.addColorStop(0.5 + glth, "#ca6");
    gradient1.addColorStop(0.75 - glth, "#ca6");
    gradient1.addColorStop(0.75 - glth, "#666");
    gradient1.addColorStop(0.75 + glth, "#666");
    gradient1.addColorStop(0.75 + glth, "#ca6");
    gradient1.addColorStop(0.90, "#ca6");
    gradient1.addColorStop(1, "#ca6");
    tileContext.fillStyle = gradient1;

    tileContext.roundRect(halfBorder, halfBorder, tileWidth - border, tileHeight * 2 - halfBorder, tileWidth / 2);
    // fill round rect for plank-gradient
    tileContext.fill();
    // stroke round rect for border
    tileContext.stroke();

    // door knob
    tileContext.beginPath();
    tileContext.fillStyle = "#669";
    tileContext.arc(tileWidth / 4 * 3, tileHeight / 5 * 3, border * 2.5, 0, 2 * Math.PI);
    tileContext.fill();

    if(locked === true) {
        tileContext.beginPath();
        tileContext.lineWidth = border * 3;
        tileContext.strokeStyle = "#333";
        tileContext.fillStyle = "#333";
        tileContext.roundRect(drawWidth * 1.5, drawHeight, drawWidth * 3, drawHeight * 4, drawWidth * 1.5);
        tileContext.stroke();
        tileContext.fillRect(drawWidth, drawHeight * 3, drawWidth * 4, drawHeight * 2.5)
    }

    return tileCanvas;
}

export function getFloorTile(tileWidth, tileHeight, color = "#e4e4e4") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');
    
    let drawWidth = tileWidth / 4;
    let drawHeight = tileHeight / 4;
    let border = tileWidth / 30;

    tileContext.strokeStyle = color;
    tileContext.lineWidth = border;
    tileContext.lineCap = "square";

    tileContext.beginPath();

    tileContext.moveTo(0, 0);
    tileContext.lineTo(drawWidth * 2, drawHeight * 2);
    tileContext.moveTo(drawWidth * 2, 0);
    tileContext.lineTo(drawWidth * 4, drawHeight * 2);
    tileContext.moveTo(drawWidth * 4, 0);
    tileContext.lineTo(drawWidth * 5, drawHeight); // a tiny edge in the upper right corner

    tileContext.moveTo(0, drawHeight * 2);
    tileContext.lineTo(drawWidth, drawHeight);
    tileContext.moveTo(0, drawHeight * 4);
    tileContext.lineTo(drawWidth * 3, drawHeight);
    tileContext.moveTo(drawWidth * 2, drawHeight * 4);
    tileContext.lineTo(drawWidth * 4, drawHeight * 2);

    tileContext.moveTo(drawWidth, drawHeight * 3);
    tileContext.lineTo(drawWidth * 2, drawHeight * 4);
    tileContext.moveTo(drawWidth * 3, drawHeight * 3);
    tileContext.lineTo(drawWidth * 4, drawHeight * 4);

    tileContext.stroke();

    return tileCanvas;
}

export function getBoxTile(tileWidth, tileHeight, color = "#33f") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 6;
    let drawHeight = tileHeight / 6;
    let border = tileWidth / 30;

    let rgbValues = toRGB(color);

    // left front
    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(saturation(rgbValues, 80), -10));
    tileContext.moveTo(0, drawHeight * 2);
    tileContext.lineTo(drawWidth * 3, drawHeight * 3);
    tileContext.lineTo(drawWidth * 3, drawHeight * 6);
    tileContext.lineTo(0, drawHeight * 5);
    tileContext.fill();

    // right front
    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(saturation(rgbValues, 80), 40));
    tileContext.moveTo(drawWidth * 3, drawHeight * 3);
    tileContext.lineTo(drawWidth * 6, drawHeight * 2);
    tileContext.lineTo(drawWidth * 6, drawHeight * 5);
    tileContext.lineTo(drawWidth * 3, drawHeight * 6);
    tileContext.fill();

    // overlap of lid
    let overlap = drawHeight / 3 * 2;

    // overlap left
    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, -20));
    tileContext.moveTo(0, drawHeight * 2);
    tileContext.lineTo(drawWidth * 3, drawHeight * 3);
    tileContext.lineTo(drawWidth * 3, drawHeight * 3 + overlap);
    tileContext.lineTo(0, drawHeight * 2 + overlap);
    tileContext.fill();
    // overlap right
    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, 30));
    tileContext.moveTo(drawWidth * 3, drawHeight * 3);
    tileContext.lineTo(drawWidth * 6, drawHeight * 2);
    tileContext.lineTo(drawWidth * 6, drawHeight * 2 + overlap);
    tileContext.lineTo(drawWidth * 3, drawHeight * 3 + overlap);
    tileContext.fill();

    // lid
    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.moveTo(0, drawHeight * 2);
    tileContext.lineTo(drawWidth * 3, drawHeight);
    tileContext.lineTo(drawWidth * 6, drawHeight * 2);
    tileContext.lineTo(drawWidth * 3, drawHeight * 3);
    tileContext.fill();

    // ribbon
    // TODO

    // tie
    tileContext.strokeStyle = "#a00";
    tileContext.lineWidth = border;
    // left loop
    tileContext.beginPath();
    tileContext.ellipse(drawWidth * 2.4, drawHeight, drawWidth * 0.5, drawHeight * 0.8, Math.PI / 8 * -1, 0, 2 * Math.PI);
    tileContext.stroke();
    // right loop
    tileContext.beginPath();
    tileContext.ellipse(drawWidth * 4.1, drawHeight * 1.1, drawWidth * 0.7, drawHeight * 1.1, Math.PI / 4, 0, 2 * Math.PI);
    tileContext.stroke();
    // knot
    tileContext.beginPath();
    tileContext.fillStyle = "#a00";
    tileContext.arc(drawWidth * 3, drawHeight * 2, border * 2.5, 0, 2 * Math.PI);
    tileContext.fill();

    return tileCanvas;
}


export function getPlayerTile(tileWidth, tileHeight, direction = Direction.DOWN, color = "#a3a") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 6;
    let drawHeight = tileHeight / 6;
    let border = tileWidth / 30;

    tileContext.lineWidth = border;
    tileContext.lineCap = "square";

    // legs
    tileContext.beginPath();
    tileContext.fillStyle = "#999";
    tileContext.fillRect(drawWidth * 2.55, drawHeight * 5, drawWidth * 0.25, drawHeight / 2);
    tileContext.fillRect(drawWidth * 3.25, drawHeight * 5, drawWidth * 0.25, drawHeight / 2);

    // feet
    tileContext.beginPath();
    tileContext.fillStyle = "#666";
    tileContext.fillRect(drawWidth * 2.15, drawHeight * 5.5, drawWidth * 0.66, drawHeight / 2);
    tileContext.fillRect(drawWidth * 3.25, drawHeight * 5.5, drawWidth * 0.66, drawHeight / 2);

    // body
    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.moveTo(drawWidth * 2.75, drawHeight * 2.5);
    tileContext.lineTo(drawWidth * 3.25, drawHeight * 2.5);
    tileContext.lineTo(drawWidth * 4, drawHeight * 5);
    tileContext.lineTo(drawWidth * 2, drawHeight * 5);
    tileContext.fill();

    // head
    tileContext.beginPath();
    tileContext.fillStyle = "#ccc";
    tileContext.strokeStyle = "#666";
    tileContext.lineWidth = border / 2;
    tileContext.arc(drawWidth * 3, drawHeight * 2, drawWidth * 0.75, 0, 2 * Math.PI);
    tileContext.fill();
    tileContext.stroke();

    switch(direction) {
        case Direction.UP:
            // hair
            tileContext.beginPath();
            tileContext.fillStyle = "#673c1d";
            tileContext.arc(drawWidth * 3, drawHeight * 2, drawWidth * 0.75, Math.PI, 2 * Math.PI);

            tileContext.moveTo(drawWidth * 2.25, drawHeight * 2);
            tileContext.lineTo(drawWidth * 3.75, drawHeight * 2);
            tileContext.lineTo(drawWidth * 3.5, drawHeight * 2.75);
            tileContext.lineTo(drawWidth * 3.25, drawHeight * 2.5);
            tileContext.lineTo(drawWidth * 3, drawHeight * 3);
            tileContext.lineTo(drawWidth * 2.75, drawHeight * 2.5);
            tileContext.lineTo(drawWidth * 2.5, drawHeight * 2.75);
            tileContext.fill();

            break;

        case Direction.RIGHT:
            // hand
            tileContext.beginPath();
            tileContext.fillStyle = "#666";
            tileContext.fillRect(drawWidth * 3.5, drawHeight * 3.5, drawWidth * 0.5, drawHeight / 2);

            // eye
            tileContext.beginPath();
            tileContext.fillStyle = "#666";
            tileContext.arc(drawWidth * 3.3, drawHeight * 1.9, border * 0.75, 0, 2 * Math.PI);
            tileContext.fill();

            break;

        case Direction.DOWN:
            // hands
            tileContext.beginPath();
            tileContext.fillStyle = "#666";
            tileContext.fillRect(drawWidth * 2, drawHeight * 3.75, drawWidth * 0.5, drawHeight / 2);
            tileContext.fillRect(drawWidth * 3.5, drawHeight * 3.75, drawWidth * 0.5, drawHeight / 2);

            // eyes
            tileContext.beginPath();
            tileContext.fillStyle = "#666";
            tileContext.arc(drawWidth * 2.75, drawHeight * 1.9, border * 0.75, 0, 2 * Math.PI);
            tileContext.arc(drawWidth * 3.25, drawHeight * 1.9, border * 0.75, 0, 2 * Math.PI);
            tileContext.fill();

            break;

        case Direction.LEFT:
            // hand
            tileContext.beginPath();
            tileContext.fillStyle = "#666";
            tileContext.fillRect(drawWidth * 2, drawHeight * 3.5, drawWidth * 0.5, drawHeight / 2);

            // eye
            tileContext.beginPath();
            tileContext.fillStyle = "#666";
            tileContext.arc(drawWidth * 2.7, drawHeight * 1.9, border * 0.75, 0, 2 * Math.PI);
            tileContext.fill();

            break;
    }

    return tileCanvas;
}

export function getKeyTile(tileWidth, tileHeight, color = "#ddbc00") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 10;
    let drawHeight = tileHeight / 10;
    let border = tileWidth / 30;

    let rgbValues = toRGB(color);

    // beard
    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.moveTo(0, drawHeight * 10);
    tileContext.lineTo(0, drawHeight * 8);
    tileContext.lineTo(drawWidth * 5, drawHeight * 3);
    tileContext.lineTo(drawWidth * 7, drawHeight * 5);

    tileContext.lineTo(drawWidth * 5, drawHeight * 7);
    tileContext.lineTo(drawWidth * 4, drawHeight * 7);
    tileContext.lineTo(drawWidth * 4, drawHeight * 8);
    tileContext.lineTo(drawWidth * 3, drawHeight * 8);
    tileContext.lineTo(drawWidth * 3, drawHeight * 9);
    tileContext.lineTo(drawWidth * 2, drawHeight * 9);
    tileContext.lineTo(drawWidth * 2, drawHeight * 10);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.lineWidth = border;
    tileContext.strokeStyle = rgbValuesAsColorString(light(rgbValues, -15));;
    tileContext.moveTo(0, drawHeight * 9);
    tileContext.lineTo(drawWidth * 5, drawHeight * 4);
    tileContext.stroke();

    // plate
    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.arc(drawWidth * 7, drawHeight * 3, drawWidth * 3, 0, 2 * Math.PI);
    tileContext.arc(drawWidth * 7.75, drawHeight * 2.25, drawWidth * 0.75, 0, 2 * Math.PI, true);
    tileContext.fill();

    return tileCanvas;
}

export function getBoulderTile(tileWidth, tileHeight, color = "#787") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 12;
    let drawHeight = tileHeight / 12;
    let border = tileWidth / 30;

    let rgbValues = toRGB(color);

    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, 20));
    tileContext.moveTo(0, drawHeight * 6);
    tileContext.lineTo(drawWidth, drawHeight * 6);
    tileContext.lineTo(drawWidth * 2, drawHeight * 8);
    tileContext.lineTo(drawWidth * 2, drawHeight * 10);
    tileContext.lineTo(drawWidth, drawHeight * 9.8);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, 10));
    tileContext.moveTo(drawWidth * 2, drawHeight * 10);
    tileContext.lineTo(drawWidth * 2, drawHeight * 8);
    tileContext.lineTo(drawWidth * 9, drawHeight * 7);
    tileContext.lineTo(drawWidth * 8, drawHeight * 11);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, -10));
    tileContext.moveTo(drawWidth * 8, drawHeight * 11);
    tileContext.lineTo(drawWidth * 9, drawHeight * 6);
    tileContext.lineTo(drawWidth * 10, drawHeight * 5);
    tileContext.lineTo(drawWidth * 12, drawHeight * 10);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, -20));
    tileContext.moveTo(drawWidth * 12, drawHeight * 10);
    tileContext.lineTo(drawWidth * 10, drawHeight * 5);
    tileContext.lineTo(drawWidth * 9, drawHeight * 1);
    tileContext.lineTo(drawWidth * 12, drawHeight * 5);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, 10));
    tileContext.moveTo(drawWidth * 10, drawHeight * 5);
    tileContext.lineTo(drawWidth * 9, drawHeight * 6);
    tileContext.lineTo(drawWidth * 6, drawHeight * 1);
    tileContext.lineTo(drawWidth * 9, drawHeight * 1);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.moveTo(drawWidth * 6, drawHeight * 1);
    tileContext.lineTo(drawWidth * 9, drawHeight * 6);
    tileContext.lineTo(drawWidth * 8.8, drawHeight * 7.1);
    tileContext.lineTo(drawWidth * 2, drawHeight * 8);
    tileContext.lineTo(drawWidth, drawHeight * 6);
    tileContext.fill();

    tileContext.beginPath();
    tileContext.fillStyle = rgbValuesAsColorString(light(rgbValues, 10));
    tileContext.moveTo(drawWidth * 6, drawHeight * 1);
    tileContext.lineTo(drawWidth, drawHeight * 6);
    tileContext.lineTo(0, drawHeight * 6);
    tileContext.fill();

    return tileCanvas;
}

export function getMonsterTile(tileWidth, tileHeight, color = "#393") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 12;
    let drawHeight = tileHeight / 12;
    let border = tileWidth / 50;

    let rgbValues = toRGB(color);

    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.moveTo(0, drawHeight * 10);
    // left half
    tileContext.bezierCurveTo(drawWidth * 2, drawHeight * 9, 0, drawHeight, drawWidth * 6, drawHeight);
    // right half
    tileContext.bezierCurveTo(drawWidth * 13, drawHeight, drawWidth * 8, drawHeight * 11, drawWidth * 12, drawHeight * 11);
    // bottom
    tileContext.bezierCurveTo(drawWidth * 9, drawHeight * 13.5, drawWidth * 10, drawHeight * 10, drawWidth * 8, drawHeight * 11);
    tileContext.bezierCurveTo(drawWidth * 6, drawHeight * 12, drawWidth * 6, drawHeight * 10, drawWidth * 4, drawHeight * 11);
    tileContext.bezierCurveTo(drawWidth * 2, drawHeight * 12, drawWidth * 2, drawHeight * 10, 0, drawHeight * 10);
    tileContext.fill();

    // eye
    tileContext.lineWidth = border;
    tileContext.beginPath();
    tileContext.strokeStyle = "#333";
    tileContext.fillStyle = rgbValuesAsColorString(light(saturation(rgbValues, 25), 100));
    tileContext.arc(drawWidth * 6, drawHeight * 3 + border, drawWidth * 3, 0, 2 * Math.PI);
    tileContext.fill();
    tileContext.stroke();
    tileContext.beginPath();
    tileContext.fillStyle = "#333";
    tileContext.arc(drawWidth * 6, drawHeight * 3 + border, drawWidth * 1.5, 0, 2 * Math.PI);
    tileContext.fill();
    // gloss
    tileContext.beginPath();
    tileContext.fillStyle = "#ddd";
    tileContext.ellipse(drawWidth * 6.75, drawHeight * 3.25 + border, drawWidth * 0.6, drawHeight * 0.3, Math.PI / 3 * -1, 0, 2 * Math.PI);
    tileContext.fill();
    // eye-lid
    tileContext.beginPath();
    tileContext.strokeStyle = "#333";
    tileContext.fillStyle = rgbValuesAsColorString(light(saturation(rgbValues, 50), 50));
    tileContext.arc(drawWidth * 6, drawHeight * 3 + border, drawWidth * 3, Math.PI + Math.PI / 15, 0 - Math.PI / 15);
    tileContext.closePath();
    tileContext.fill();
    tileContext.stroke();

    // mouth
    tileContext.beginPath();
    tileContext.fillStyle = "#333";
    tileContext.moveTo(drawWidth * 3, drawHeight * 7);
    tileContext.bezierCurveTo(drawWidth * 5, drawHeight * 8, drawWidth * 7, drawHeight * 8, drawWidth * 9, drawHeight * 6);
    tileContext.bezierCurveTo(drawWidth * 8, drawHeight * 9, drawWidth * 5, drawHeight * 9, drawWidth * 3, drawHeight * 7);
    tileContext.fill();
    return tileCanvas;
}

export function getMineTile(tileWidth, tileHeight, color = "#336") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 10;
    let drawHeight = tileHeight / 10;
    let border = tileWidth / 20;

    let rgbValues = toRGB(color);

    tileContext.fillStyle = color;
    tileContext.beginPath();
    tileContext.arc(drawWidth * 5, drawHeight * 7, drawWidth * 4, Math.PI, Math.PI * 2);
    tileContext.ellipse(drawWidth * 5, drawHeight * 8, drawWidth * 4, drawHeight, 2 * Math.PI, 0, Math.PI);    
    tileContext.closePath();
    tileContext.fill();

    tileContext.fillStyle = rgbValuesAsColorString(light(saturation(rgbValues, 50), 150));
    tileContext.beginPath();
    tileContext.arc(drawWidth * 2.5, drawHeight * 5, drawWidth / 1.5, 0, 2 * Math.PI);
    tileContext.fill();
    tileContext.beginPath();
    tileContext.arc(drawWidth * 5, drawHeight * 5.5, drawWidth / 1.5, 0, 2 * Math.PI);
    tileContext.fill();
    tileContext.beginPath();
    tileContext.arc(drawWidth * 7.5, drawHeight * 5, drawWidth / 1.5, 0, 2 * Math.PI);
    tileContext.fill();

    tileContext.lineWidth = border;
    tileContext.lineCap = "round";
    tileContext.strokeStyle = rgbValuesAsColorString(light(rgbValues, 50));
    tileContext.beginPath();
    tileContext.moveTo(drawWidth * 2.5, drawHeight * 5);
    tileContext.lineTo(border, drawHeight * 2.5);
    tileContext.stroke();
    tileContext.beginPath();
    tileContext.moveTo(drawWidth * 5, drawHeight * 5.5);
    tileContext.lineTo(drawWidth * 5, drawHeight * 2.5);
    tileContext.stroke();
    tileContext.beginPath();
    tileContext.moveTo(drawWidth * 7.5, drawHeight * 5);
    tileContext.lineTo(drawWidth * 10 - border, drawHeight * 2.5);
    tileContext.stroke();

    return tileCanvas;
}

export function getForestTile(tileWidth, tileHeight, amount, color = "#006400", borderColor = "rgba(47, 79, 79, 0.75)") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let featureSize = tileWidth / 10;
    tileContext.fillStyle = color;
    tileContext.strokeStyle = borderColor;
    tileContext.lineWidth = 1;
    tileContext.beginPath();

    if(amount > 90) {
        tileContext.moveTo(featureSize * 7.5, featureSize * 7);
        tileContext.lineTo(featureSize * 8, featureSize * 9);
        tileContext.lineTo(featureSize * 7, featureSize * 9);
        tileContext.closePath();
    }

    if(amount > 80) {
        tileContext.moveTo(featureSize * 1.5, featureSize * 6);
        tileContext.lineTo(featureSize * 2, featureSize * 8);
        tileContext.lineTo(featureSize, featureSize * 8);
        tileContext.closePath();
    }

    if(amount > 70) {
        tileContext.moveTo(featureSize * 8, featureSize * 3);
        tileContext.lineTo(featureSize * 9, featureSize * 6);
        tileContext.lineTo(featureSize * 7, featureSize * 6);
        tileContext.closePath();
    }

    if(amount > 50) {
        tileContext.moveTo(featureSize * 3, featureSize);
        tileContext.lineTo(featureSize * 4, featureSize * 4);
        tileContext.lineTo(featureSize * 2, featureSize * 4);
        tileContext.closePath();
    }

    if(amount > 30) {
        tileContext.moveTo(featureSize * 6.5, featureSize * 2);
        tileContext.lineTo(featureSize * 7, featureSize * 4);
        tileContext.lineTo(featureSize * 6, featureSize * 4);
        tileContext.closePath();
    }

    tileContext.moveTo(featureSize * 5, featureSize * 5);
    tileContext.lineTo(featureSize * 6, featureSize * 8);
    tileContext.lineTo(featureSize * 4, featureSize * 8);
    tileContext.closePath();

    tileContext.fill();
    tileContext.stroke();

    return tileCanvas;
}

export function getOreTile(tileWidth, tileHeight, amount, color = "#B0C4DE") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let unitX = tileWidth / 10;
    let unitY = tileHeight / 10;

    tileContext.fillStyle = color;
    tileContext.beginPath();

    if(amount > 90) {
        tileContext.moveTo(unitX * 2, unitY * 2);
        tileContext.arc(unitX * 2, unitY * 2, unitX / 2, 0, 2 * Math.PI);
    }

    if(amount > 80) {
        tileContext.moveTo(unitX * 8, unitY * 4);
        tileContext.arc(unitX * 8, unitY * 4, unitX / 2, 0, 2 * Math.PI);
    }

    if(amount > 60) {
        tileContext.moveTo(unitX * 4, unitY * 3);
        tileContext.arc(unitX * 4, unitY * 3, unitX, 0, 2 * Math.PI);
    }

    if(amount > 40) {
        tileContext.moveTo(unitX * 5, unitY * 8);
        tileContext.arc(unitX * 5, unitY * 8, unitX, 0, 2 * Math.PI);
    }

    if(amount > 25) {
        tileContext.moveTo(unitX * 8, unitY * 4);
        tileContext.arc(unitX * 3, unitY * 6, unitX / 1.25, 0, 2 * Math.PI);
    }

    tileContext.moveTo(unitX * 6, unitY * 5);
    tileContext.arc(unitX * 6, unitY * 5, unitX, 0, 2 * Math.PI);

    tileContext.fill();

    return tileCanvas;
}

export function getBorderPostTile(width, height, directions = [], color = "#090") {
    let tileCanvas = getTempCanvas(width, height);
    let tileContext = tileCanvas.getContext('2d');

    tileContext.lineWidth = width / 5;
    tileContext.strokeStyle = color;
    tileContext.fillStyle = color;

    const dottedLine = (from, to, dotCount, direction) => {
        let step = from.distanceTo(to) / dotCount;
        let radius = Math.floor(step / 3);
        if(from.x == to.x) {
            // vertical
            let x;
            if(direction == Direction.UP) {
                x = from.x - radius * 2;
            } else {
                x = from.x + radius;
            }

            for(let y = from.y + radius * 1.75; y < to.y; y = y + step) {
                tileContext.beginPath();
                tileContext.arc(x, y, radius, 0, 2 * Math.PI);
                tileContext.fill();
            }
        } else if(from.y == to.y) {
            // horizontal
            let y;
            if(direction == Direction.LEFT) {
                y = from.y - radius * 2;
            } else {
                y = from.y + radius;
            }

            for(let x = from.x + radius * 1.75; x < to.x; x = x + step) {
                tileContext.beginPath();
                tileContext.arc(x, y + radius, radius, 0, 2 * Math.PI);
                tileContext.fill();
            }
        }
    }

    let dotAmount = 3;
    directions.forEach(direction => {
        switch(direction) {
            case Direction.UP:
                tileContext.moveTo(0, 0);
                tileContext.lineTo(width, 0);
                //dottedLine(new Point(0, 0), new Point(width, 0), dotAmount, direction);
                break;
            case Direction.RIGHT:
                tileContext.moveTo(width, 0);
                tileContext.lineTo(width, height);
                //dottedLine(new Point(width, 0), new Point(width, height), dotAmount, direction);
                break;
            case Direction.DOWN:
                tileContext.moveTo(0, height);
                tileContext.lineTo(width, height);
                //dottedLine(new Point(0, height), new Point(width, height), dotAmount, direction);
                break;
            case Direction.LEFT:
                tileContext.moveTo(0, 0);
                tileContext.lineTo(0, height);
                //dottedLine(new Point(0, 0), new Point(0, height), dotAmount, direction);
                break;
        }
        tileContext.stroke();
    });

    return tileCanvas;
}


export function getCivAreaTile(tileWidth, tileHeight, color = "transparent") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let originalColor = toRGB(color);
    originalColor.a = 0.5;
    originalColor = rgbValuesAsColorString(originalColor);

    let transColor = toRGB(color);
    transColor.a = 0;
    transColor = rgbValuesAsColorString(transColor);

    let gradient = tileContext.createLinearGradient(0, 0, tileWidth, tileHeight);
    let step = 0.05;

    gradient.addColorStop(0, transColor);
    let currentColor = originalColor;
    for(let i = step; i <= 1; i = i + step) {
        if(currentColor != originalColor) {
            gradient.addColorStop(i, originalColor);
            gradient.addColorStop(i, transColor);
            currentColor = originalColor;
        } else {
            gradient.addColorStop(i, transColor);
            gradient.addColorStop(i, originalColor);
            currentColor = transColor;
        }

    }

    tileContext.beginPath();
    tileContext.fillStyle = gradient;
    tileContext.rect(0, 0, tileWidth, tileHeight);
    tileContext.fill();

    return tileCanvas;
}

export function getWoodCutterTile(tileWidth, tileHeight, color = "#8B4513") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = tileWidth / 10;
    let drawHeight = tileHeight / 10;

    tileContext.beginPath();
    tileContext.fillStyle = color;

    // hut outer
    tileContext.moveTo(0, drawHeight * 3);
    tileContext.lineTo(drawWidth * 5, 0);
    tileContext.lineTo(drawWidth * 10, drawHeight * 3);
    tileContext.lineTo(drawWidth * 9, drawHeight * 3);
    tileContext.lineTo(drawWidth * 9, drawHeight * 10);
    tileContext.lineTo(drawWidth, drawHeight * 10);
    tileContext.lineTo(drawWidth, drawHeight * 3);
    tileContext.closePath();

    // window
    tileContext.moveTo(drawWidth * 3, drawHeight * 3);
    tileContext.lineTo(drawWidth * 3, drawHeight * 6);
    tileContext.lineTo(drawWidth * 7, drawHeight * 6);
    tileContext.lineTo(drawWidth * 7, drawHeight * 3);
    tileContext.closePath();

    tileContext.fill();

    return tileCanvas;
}

function stalk(context, x, startY, height) {
    context.moveTo(x, startY);
    context.lineTo(x, startY - height);
}

/**
 * 
 * @param {Number} height Between (and including) 1 and 10
 */
export function getWheatTile(tileWidth, tileHeight, height = 4, color = "#FFD700") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let featureSize = tileWidth / 20;

    // field
    let gradient = tileContext.createLinearGradient(0, tileHeight, tileWidth, 0);
    let step = 0.05;

    let fieldColorA = "#800000";
    let fieldColorB = "transparent"; // "#A52A2A";
    gradient.addColorStop(0, fieldColorA);
    let currentColor = fieldColorB;
    for(let i = step; i <= 1; i = i + step) {
        if(currentColor != fieldColorB) {
            gradient.addColorStop(i, fieldColorB);
            gradient.addColorStop(i, fieldColorA);
            currentColor = fieldColorB;
            i = i + step;
        } else {
            gradient.addColorStop(i, fieldColorA);
            gradient.addColorStop(i, fieldColorB);
            currentColor = fieldColorA;
        }

    }

    tileContext.beginPath();
    tileContext.fillStyle = gradient;
    tileContext.fillRect(featureSize, featureSize, featureSize * 19, featureSize * 19);

    // stalks
    tileContext.lineWidth = tileWidth / 30;

    height = featureSize * height;

    if(height <= featureSize * 3) {
        tileContext.strokeStyle = "#00FF7F";
    } else {
        tileContext.strokeStyle = color;
    }

    for(let x = 17; x < 19; x++) {
        stalk(tileContext, featureSize * x + tileContext.lineWidth, featureSize * (x - 15), height);
    }

    for(let x = 12; x < 20; x++) {
        stalk(tileContext, featureSize * x, featureSize * (x - 10), height);
    }

    for(let x = 6; x < 19; x++) {
        stalk(tileContext, featureSize * x + tileContext.lineWidth, featureSize * (x - 4), height);
    }

    for(let x = 2; x < 18; x++) {
        stalk(tileContext, featureSize * x, featureSize * (x + 2), height);
    }

    for(let x = 2; x < 12; x++) {
        stalk(tileContext, featureSize * x + tileContext.lineWidth, featureSize * (x + 8), height);
    }

    for(let x = 2; x < 6; x++) {
        stalk(tileContext, featureSize * x, featureSize * (x + 14), height);
    }

    tileContext.stroke();

    return tileCanvas;
}

export function getTowerTile(tileWidth, tileHeight, color = "#ccc", border = "rgba(60, 60, 60, 0.5)") {
    let tileCanvas = getTempCanvas(tileWidth, tileHeight);
    let tileContext = tileCanvas.getContext('2d');

    let centerX = tileWidth / 2;
    let centerY = tileHeight / 2;

    let unitX = tileWidth / 6;
    let unitY = tileHeight / 6;

    tileContext.beginPath();
    tileContext.fillStyle = color;

    tileContext.translate(centerX, centerY);
    tileContext.rotate(0.785398);
    tileContext.translate(centerX * -1, centerY * -1);

    tileContext.beginPath();
    tileContext.fillStyle = color;
    tileContext.rect(unitX, unitY, unitX * 4, unitY * 4);
    tileContext.fill();

    if(border !== false) {
        tileContext.lineWidth = tileWidth / 20;
        tileContext.strokeStyle = border;
        tileContext.stroke();
    }

    return tileCanvas;
}
