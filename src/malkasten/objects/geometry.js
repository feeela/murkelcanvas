/**
 * Canvas objects – basic geometry
 */

import { extend } from "../../helper/helper.js";
import { toRGB, saturation, light, rgbValuesAsColorString } from "../../helper/color.js";
import { Point } from "../../point.js";
import { ObjectState, CanvasObject } from "./basic.js";

export class DimensionDescriptior {
    constructor(name, calculator, multiplyWith = "width") {
        this.name = name;
        this.calculator = calculator;
        this.multiplyWith = ["width", "height"].includes(multiplyWith) ? multiplyWith : "width";
    }
}

export class Geometry extends CanvasObject {

    constructor(malkasten, position, options) {
        super(malkasten, position, options);

        this.intersectionMethodMap = {
            "Line": {
                "Line": this.lineIntersectsLine,
                "Polygon": this.lineIntersectsPolygon,
                // TODO
            },
            "Circle": {
                "Circle": this.circleIntersectsCircle,
                "Rect": this.circleIntersectsRect,
                "Polygon": this.circleIntersectsPolygon,
            },
            "Rect": {
                "Circle": (A, B) => this.circleIntersectsRect(B, A),
                "Rect": this.rectIntersectsRect,
                "Polygon": this.rectIntersectsRect,
            },
            "Polygon": {
                "Circle": (A, B) => this.circleIntersectsPolygon(B, A),
                "Rect": this.rectIntersectsRect,
                "Line": (A, B) => this.lineIntersectsPolygon(B, A),
                "Polygon": this.polygonIntersectsPolygon,
            },
        };
    }

    getDimensionDescriptors() {
        return [
            new DimensionDescriptior("x", () => this.position.x, "width"),
            new DimensionDescriptior("y", () => this.position.y, "height"),

            // Each object should define the top-left (x1, y1) and bottom-right (x2, y2) corners
            new DimensionDescriptior("x1", () => this.position.x, "width"),
            new DimensionDescriptior("y1", () => this.position.y, "height"),
            new DimensionDescriptior("x2", () => this.position.x, "width"),
            new DimensionDescriptior("y2", () => this.position.y, "height"),
        ]
    }

    getDimensions(asPixel = false) {
        let dimensions = {};
        this.getDimensionDescriptors().forEach(dimension => {
            if(asPixel === true) {
                dimensions[dimension.name] = this.malkasten.percentageToPixel(dimension.calculator.call(this), dimension.multiplyWith);
            } else {
                dimensions[dimension.name] = dimension.calculator.call(this);
            }
        });
        return dimensions;
    }

    /**
     * @param {Point} point
     * @return {Boolean}
     */
    isPointPartOfObject(point) {
        let {x1, y1, x2, y2} = this.getDimensions();
        return point.x >= x1 && point.y >= y1
            && point.x <= x2 && point.y <= y2;
    }

    intersects(otherObject) {
        if(this.intersectionIsImplemented(this, otherObject)) {
            return this.intersectionMethodMap[this.constructor.name][otherObject.constructor.name](this, otherObject);
        } else {
            throw new Error(`Intersection computing of ${this.constructor.name} to ${otherObject.constructor.name} is not implemented.`);
        }
    }

    intersectionIsImplemented(A, B) {
        return this.intersectionMethodMap[A.constructor.name] && this.intersectionMethodMap[A.constructor.name][B.constructor.name];
    }

    lineIntersectsLine(lineA, lineB) {
        let { x1, y1, x2, y2 } = lineA.getDimensions();
        let { x1: x3, y1: y3, x2: x4, y2: y4 } = lineB.getDimensions();

        let ua, ub, denom = (y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1);
        if (denom == 0) {
            return null;
        }
        ua = ((x4 - x3)*(y1 - y3) - (y4 - y3)*(x1 - x3))/denom;
        ub = ((x2 - x1)*(y1 - y3) - (y2 - y1)*(x1 - x3))/denom;

        let at = new Point(x1 + ua * (x2 - x1), y1 + ua * (y2 - y1));
        return (ua >= 0 && ua <= 1) && (ub >= 0 && ub <= 1);
    }

    /**
     * @param {Circle} circleA
     * @param {Circle} circleB
     */
    circleIntersectsCircle(circleA, circleB) {
        let minDistance = circleA.options.radius + circleB.options.radius;
        return Math.abs(circleA.position.x - circleB.position.x) > minDistance
            && Math.abs(circleA.position.y - circleB.position.y) > minDistance;
    }

    /**
     * @param {Circle} circle
     * @param {Rect} rect
     */
    circleIntersectsRect(circle, rect) {
        let R = circle.options.radius;
        let Xc = circle.position.x;
        let Yc = circle.position.y;
        let {x1, y1, x2, y2} = rect.getDimensions();

        // Find the nearest point on the 
        // rectangle to the center of 
        // the circle
        let Xn = Math.max(x1, Math.min(Xc, x2));
        let Yn = Math.max(y1, Math.min(Yc, y2));

        // Find the distance between the 
        // nearest point and the center 
        // of the circle
        // Distance between 2 points, 
        // (x1, y1) & (x2, y2) in 
        // 2D Euclidean space is
        // ((x1-x2)**2 + (y1-y2)**2)**0.5
        let Dx = Xn - Xc;
        let Dy = Yn - Yc;
        return (Dx * Dx + Dy * Dy) <= R * R;
    }

    /**
     * @param {Geometry} rectA Any object that defines x1/y1 & x2/y2 pairs in getDimensionDescriptors()
     * @param {Geometry} rectB Any object that defines x1/y1 & x2/y2 pairs in getDimensionDescriptors()
     */
    rectIntersectsRect(rectA, rectB) {
        let ownDim = rectA.getDimensions();
        let otherDim = rectB.getDimensions();

        // Top-left corner of `this` is inside `rectB`
        if(ownDim.x1 >= otherDim.x1 && ownDim.x1 <= otherDim.x2
            && ownDim.y1 >= otherDim.y1 && ownDim.y1 <= otherDim.y2) {
            return true;
        }

        // Top-right corner of `this` is inside `rectB`
        if(ownDim.x2 >= otherDim.x1 && ownDim.x2 <= otherDim.x2
            && ownDim.y1 >= otherDim.y1 && ownDim.y1 <= otherDim.y2) {
            return true;
        }

        // Bottom-right corner of `this` is inside `rectB`
        if(ownDim.x2 >= otherDim.x1 && ownDim.x2 <= otherDim.x2
            && ownDim.y2 >= otherDim.y1 && ownDim.y2 <= otherDim.y2) {
            return true;
        }

        // Bottom-left corner of `this` is inside `rectB`
        if(ownDim.x1 >= otherDim.x1 && ownDim.x1 <= otherDim.x2
            && ownDim.y2 >= otherDim.y1 && ownDim.y2 <= otherDim.y2) {
            return true;
        }

        // Special case: `rectB` is smaller than `this` and contained within `this`; 
        if(ownDim.x1 <= otherDim.x1 && ownDim.y1 <= otherDim.y1
            && ownDim.x2 >= otherDim.x2 && ownDim.y2 >= otherDim.y2) {
            return true;
        }

        return false;
    }

    /**
     * `returnIntersectionCount = true` is used in `Polygon.isPointPartOfObject()` for a simple ray casting implementation
     * 
     * @param {Line} line
     * @param {Polygon} polygon
     * @param {Boolean} returnIntersectionCount Set to `true` to get the count of intersections, insted of boolean result.
     * @return {Boolean|Number}
     */
    lineIntersectsPolygon(line, polygon, returnIntersectionCount = false) {
        let intersections = [];
        polygon.getEdges().forEach(polyLine => {
            intersections.push(polyLine.intersects(line));
        });

        if(returnIntersectionCount === true) {
            return intersections.reduce((accumulator, currentValue) => {
                if(currentValue) {
                    accumulator++;
                }
                return accumulator;
            }, 0);
        } else {
            return intersections.some(intRes => intRes);
        }
    }

    polygonIntersectsPolygon(polygonA, polygonB) {
        let intersections = [];
        polygonA.getEdges().forEach(polyLineA => {
            polygonB.getEdges().forEach(polyLineB => {
                intersections.push(polyLineA.intersects(polyLineB));
            });
        });

        return intersections.some(intRes => intRes);
    }

    circleIntersectsPolygon(circle, polygon) {
        return polygon.isPointPartOfObject(circle.position);
    }
}

export class Circle extends Geometry {

    static defaultOptions = extend(super.defaultOptions, {
        radius: 5,
        start: 0,
        end: 2 * Math.PI,
        color: "#000",
        focusColor: false,
        activeColor: false,
        method: "stroke", // "stroke" or "fill"
    });

    constructor(malkasten, position, options) {
        super(malkasten, position, options);

        this.color = this.options.color;
        this.focusColor = this.options.focusColor || this.color;
        this.activeColor = this.options.activeColor || this.focusColor;
    }

    draw(context) {
        super.draw(context);

        let {x, y, radius} = this.getDimensions(true);
        let color;
        switch(this.state) {
            case ObjectState.idle:
                color = this.color;
                break;
            case ObjectState.focus:
                color = this.focusColor;
                break;
            case ObjectState.active:
                color = this.activeColor;
                break;
        }

        this[this.options.method](context, () => {
            context.arc(x, y, radius, this.options.start, this.options.end);
        }, color);

        if(this.growPhase === true) {
            this.end = this.end + 0.2;

            if(this.end > 2 * Math.PI) {
                this.start = 0;
                this.end = 2 * Math.PI;
                this.growPhase = false;
            }
        } else {
            this.start = this.start + 0.2;

            if(this.start > 2 * Math.PI) {
                this.start = 0;
                this.end = 0;
                this.growPhase = true;
            }
        }
    }

    getDimensionDescriptors() {
        return [
            ...super.getDimensionDescriptors(),
            new DimensionDescriptior("x1", () => this.position.x - this.options.radius, "width"),
            new DimensionDescriptior("y1", () => this.position.y - this.options.radius, "height"),
            new DimensionDescriptior("x2", () => this.position.x + this.options.radius, "width"),
            new DimensionDescriptior("y2", () => this.position.y + this.options.radius, "height"),
            new DimensionDescriptior("width", () => this.options.radius * 2, "width"),
            new DimensionDescriptior("height", () => this.options.radius * 2, "height"),
            new DimensionDescriptior("radius", () => this.options.radius, "width"),
        ]
    }

    isPointPartOfObject(point) {
        // Use Pythagorean theorem to calculate distance of given point from circle center;
        // If this distance is small than the radius, the given point is inside the circle.
        let a = point.x - this.position.x;
        let b = point.y - this.position.y;
        let distance = Math.sqrt(a * a + b * b);
        return distance <= this.options.radius;
    }
}

export class Rect extends Geometry {

    static defaultOptions = extend(super.defaultOptions, {
        width: 5,
        height: 5,
        color: "#000",
        method: "stroke", // "stroke" or "fill"
    });

    constructor(malkasten, position, options) {
        super(malkasten, position, options);

        this.color = toRGB(this.options.color);
    }

    draw(context) {
        super.draw(context);

        let {x1, y1, width, height} = this.getDimensions(true);
        let color = rgbValuesAsColorString(this.state == ObjectState.active ? light(saturation(this.color, 75), 35) : this.color);

        this[this.options.method](context, () => {
            context.rect(x1, y1, width, height);
        }, color);
    }

    getDimensionDescriptors() {
        return [
            ...super.getDimensionDescriptors(),
            new DimensionDescriptior("x1", () => this.position.x - this.options.width / 2, "width"),
            new DimensionDescriptior("y1", () => this.position.y - this.options.height / 2, "height"),
            new DimensionDescriptior("x2", () => this.position.x + this.options.width / 2, "width"),
            new DimensionDescriptior("y2", () => this.position.y + this.options.height / 2, "height"),
            new DimensionDescriptior("width", () => this.options.width, "width"),
            new DimensionDescriptior("height", () => this.options.height, "height"),
        ]
    }
}

export class Line extends Geometry {

    static defaultOptions = extend(super.defaultOptions, {
        color: "#000",
        focusColor: false,
        activeColor: false,
        method: "stroke", // "stroke" or "fill"
    });

    constructor(malkasten, from, to, options) {
        super(malkasten, from, options);

        this.to = to;

        this.color = this.options.color;
        this.focusColor = this.options.focusColor || this.color;
        this.activeColor = this.options.activeColor || this.focusColor;
    }

    draw(context) {
        let {x1, y1, x2, y2} = this.getDimensions(true);

        let color;
        switch(this.state) {
            case ObjectState.idle:
                color = this.color;
                break;
            case ObjectState.focus:
                color = this.focusColor;
                break;
            case ObjectState.active:
                color = this.activeColor;
                break;
        }

        this[this.options.method](context, () => {
            context.moveTo(x1, y1);
            context.lineTo(x2, y2);
        }, color);
    }

    getCenter() {
        let center = new Point(this.position.x, this.position.y);

        if(this.to.x != this.position.x) {
            let diffX = this.to.x - this.position.x;
            center.x = center.x + diffX / 2;
        }
        if(this.to.y != this.position.y) {
            let diffY = this.to.y - this.position.y;
            center.y = center.y + diffY / 2;
        }

        return center;
    }

    setCenter(point) {
        let center = this.getCenter();
        let diffX = point.x - center.x;
        let diffY = point.y - center.y;

        this.position.x = this.position.x + diffX;
        this.position.y = this.position.y + diffY;
        this.to.x = this.to.x + diffX;
        this.to.y = this.to.y + diffY;

    }

    getDimensionDescriptors() {
        let width = this.to.x - this.position.x;
        let height = this.to.y - this.position.y;

        return [
            ...super.getDimensionDescriptors(),
            new DimensionDescriptior("x1", () => this.position.x, "width"),
            new DimensionDescriptior("y1", () => this.position.y, "height"),
            new DimensionDescriptior("x2", () => this.to.x, "width"),
            new DimensionDescriptior("y2", () => this.to.y, "height"),
            new DimensionDescriptior("width", () => width, "width"),
            new DimensionDescriptior("height", () => height, "height"),
        ]
    }
}

export class CubicBezier extends Line {

    constructor(malkasten, from, cp1, cp2, to, options) {
        super(malkasten, from, to, options);

        this.cp1 = cp1;
        this.cp2 = cp2;

        this.color = this.options.color;
        this.focusColor = this.options.focusColor || this.color;
        this.activeColor = this.options.activeColor || this.focusColor;
    }

    draw(context) {
        let {x1, y1, cp1X, cp1Y, cp2X, cp2Y, x2, y2} = this.getDimensions(true);
        
        let color;
        switch(this.state) {
            case ObjectState.idle:
                color = this.color;
                break;
            case ObjectState.focus:
                color = this.focusColor;
                break;
            case ObjectState.active:
                color = this.activeColor;
                break;
        }

        this[this.options.method](context, () => {
            context.moveTo(x1, y1);
            context.bezierCurveTo(cp1X, cp1Y, cp2X, cp2Y, x2, y2);
        }, color);
    }

    setCenter(point) {
        super.setCenter(point);

        let center = this.getCenter();
        let diffX = point.x - center.x;
        let diffY = point.y - center.y;

        this.cp1.x = this.cp1.x + diffX;
        this.cp1.y = this.cp1.y + diffY;
        this.cp2.x = this.cp2.x + diffX;
        this.cp2.y = this.cp2.y + diffY;
    }

    getDimensionDescriptors() {
        return [
            ...super.getDimensionDescriptors(),
            new DimensionDescriptior("cp1X", () => this.cp1.x, "width"),
            new DimensionDescriptior("cp1Y", () => this.cp1.y, "height"),
            new DimensionDescriptior("cp2X", () => this.cp2.x, "width"),
            new DimensionDescriptior("cp2Y", () => this.cp2.y, "height"),
        ]
    }
}

export class Polygon extends Geometry {

    static defaultOptions = extend(super.defaultOptions, {
        color: "#000",
        focusColor: false,
        activeColor: false,
        method: "stroke", // "stroke" or "fill"
    });

    constructor(malkasten, points, options) {
        super(malkasten, new Point(), options);

        this.points = points;

        this.color = this.options.color;
        this.focusColor = this.options.focusColor || this.color;
        this.activeColor = this.options.activeColor || this.focusColor;
    }

    draw(context) {
        super.draw(context);

        let color;
        switch(this.state) {
            case ObjectState.idle:
                color = this.color;
                break;
            case ObjectState.focus:
                color = this.focusColor;
                break;
            case ObjectState.active:
                color = this.activeColor;
                break;
        }

        context.lineJoin = "round";
        this[this.options.method](context, () => {
            this.points.forEach((point, index) => {
                let pixelPoint = this.malkasten.percentagePointToPixelPoint(point);

                if(index == 0) {
                    context.moveTo(pixelPoint.x, pixelPoint.y);
                } else {
                    context.lineTo(pixelPoint.x, pixelPoint.y);
                }
            });
            context.closePath();
        }, color);
    }

    /**
     * Return all the edges of this polygon as `Line` objects
     * 
     * @return {Line[]}
     */
    getEdges() {
        let edges = [];
        let points = [...this.points];
        let firstPoint = points.shift();
        let previousPoint = firstPoint;

        points.forEach((point, index) => {
            edges.push(new Line(this.malkasten, previousPoint, point));

            previousPoint = point;
        });

        // Close the path back to the start point
        edges.push(new Line(this.malkasten, previousPoint, firstPoint));

        return edges;
    }

    getMinMaxPoints() {
        let min = new Point(100, 100);
        let max = new Point(0, 0);
        this.points.forEach(point => {
            if(point.x < min.x) {
                min.x = point.x;
            }
            if(point.x > max.x) {
                max.x = point.x;
            }

            if(point.y < min.y) {
                min.y = point.y;
            }
            if(point.y > max.y) {
                max.y = point.y;
            }
        });
        return { min, max };
    }

    getCenter() {
        let { min, max } = this.getMinMaxPoints();
        return min.midpoint(max);
    }

    setCenter(point) {
        throw new Error("Polygon.setCenter() is not (yet) implemented");
    }

    getDimensionDescriptors() {
        let { min, max } = this.getMinMaxPoints();
        let center = this.getCenter();

        return [
            ...super.getDimensionDescriptors(),
            new DimensionDescriptior("x", () => center.x, "width"),
            new DimensionDescriptior("y", () => center.y, "height"),
            new DimensionDescriptior("x1", () => min.x, "width"),
            new DimensionDescriptior("y1", () => min.y, "height"),
            new DimensionDescriptior("x2", () => max.x, "width"),
            new DimensionDescriptior("y2", () => max.y, "height"),
            new DimensionDescriptior("width", () => Math.abs(max.x - min.x), "width"),
            new DimensionDescriptior("height", () => Math.abs(max.y - min.y), "height"),
        ]
    }

    /**
     * Use ray casting to find out if an object is inside a polygon
     * 
     * @param {Point} point
     * @return {Boolean}
     */
    isPointPartOfObject(point) {
        let ray = new Line(this.malkasten, new Point(-1, point.y / 2), point);
        return !!(this.lineIntersectsPolygon(ray, this, true) % 2);
    }
}
