import { Renderer } from "../../render/renderer.js";
import {
    getSnakeHeadTile,
    getSnakeBodyTile,
    getSnakeTailTile,
    getAppleTile,
} from "./snake-pattern.js";
import {
    SnakeHead,
    SnakeBody,
    SnakeTail,
    Apple,
} from "./snake-game-objects.js";

/**
 * @param {String} snakeColor Set the color of the snake
 */
export function getRendererFunction(snakeColor = "#090") {
    /**
     * @param {2DRenderContext} context
     * @param {Number} tileWidth
     * @param {Number} tileHeight
     */
    return function getRenderer(canvasObject) {
        let renderer = new Renderer(canvasObject);

        const icons = {};

        renderer.register(SnakeHead.name, (context, x, y, width, height, snakeHeadObject) => {
            let iconKey = "head" + snakeHeadObject.direction;
            if(!icons[iconKey]) {
                icons[iconKey] = getSnakeHeadTile(width, height, snakeHeadObject.direction, snakeColor);
            }
            context.drawImage(icons[iconKey], x, y);
        });

        renderer.register(SnakeBody.name, (context, x, y, width, height, snakeBodyObject) => {
            let iconKey = "body" + snakeBodyObject.from + snakeBodyObject.to;

            if(!icons[iconKey]) {
                icons[iconKey] = getSnakeBodyTile(width, height, snakeBodyObject.from, snakeBodyObject.to, snakeColor);
            }
            context.drawImage(icons[iconKey], x, y);
        });

        renderer.register(SnakeTail.name, (context, x, y, width, height, snakeTailObject) => {
            let iconKey = "tail" + snakeTailObject.direction;
            if(!icons[iconKey]) {
                icons[iconKey] = getSnakeTailTile(width, height, snakeTailObject.direction, snakeColor);
            }
            context.drawImage(icons[iconKey], x, y);
        });

        renderer.register(Apple.name, (context, x, y, width, height, appleObject) => {
            if(!icons.apple) {
                icons.apple = getAppleTile(width, height);
            }
            context.drawImage(icons.apple, x, y);
        });

        return renderer;
    }

}
