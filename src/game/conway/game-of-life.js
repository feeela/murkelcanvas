import { ActionRunner } from "../../helper/action-runner.js";
import { MatrixObject } from "../../matrix/matrix-object.js";

export class GameOfLife extends ActionRunner {

    constructor(speed, matrix) {
        super(speed, true);

        this.matrix = matrix;

        this.iteration = 0;
        this.population = 0;
        this.maxPopulation = 0;

        this.addAction(this.update);
    }

    update() {
        this.iteration++;
        this.population = 0;

        /* Test for number of neighbouring active cells */
        this.matrix.forEach((point, values) => {
            values[0].activeNeighbourCount = this.getActiveNeighbourCount(this.matrix, point);
        });

        /* Update the cells based on previously determined count */
        this.matrix.forEach((point, values) => {
            if(values[0].alife == true) {
                if(values[0].activeNeighbourCount < 2 || values[0].activeNeighbourCount > 3) {
                    values[0].alife = false;
                } else {
                    values[0].alife = true;
                    this.population++;
                }
            } else {
                if(values[0].activeNeighbourCount == 3) {
                    values[0].alife = true;
                    this.population++;
                }
            }
        });

        if(this.population > this.maxPopulation) {
            this.maxPopulation = this.population;
        }
    }

    getActiveNeighbourCount(matrix, position) {
        let allNeighbours = matrix.getNeighbours(position, true, true);
        let activeNeighbours = allNeighbours.filter(neighbourPoint => {
            return matrix.getValue(neighbourPoint, GameOfLifeCell).alife == true;
        });

        let allNeighbourCount = allNeighbours.length;
        let currentCell = matrix.getValue(position, GameOfLifeCell);
        if(currentCell.alife == true && allNeighbourCount < 8) {
            /* If the current living cell borders the canvas bounds, substract the number of cells that would be outside the screen;
             * This is required to remove object near the edges or let gliders fade out at the edges; */
            return activeNeighbours.length - (8 - allNeighbourCount);
        } else {
            return activeNeighbours.length;
        }
    }
}

export class GameOfLifeCell extends MatrixObject {

    static COLOR = {
        BLACK: "#444",
        WHITE: "transparent",
    }

    /**
     * @param {Boolean} alife
     */
    constructor(alife = false) {
        super(true, false);

        this.alife = alife;
        this.activeNeighbourCount = 0;
    }


    getColor() {
        if(this.alife === true) {
            return GameOfLifeCell.COLOR.BLACK;
        } else {
            return GameOfLifeCell.COLOR.WHITE;
        }
    }

    flip() {
        this.alife = !this.alife;
    }
}
