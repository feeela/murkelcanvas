import { extend, getRandomInt } from "../../helper/helper.js";
import { Point } from "../../point.js";
import { ObjectState, CanvasObject, Gradient } from "./basic.js";

export class Bacteria extends CanvasObject {

    static colors = [
        "darkblue","darkcyan","darkgoldenrod","darkgreen","darkkhaki","darkmagenta","darkolivegreen",
        "darkorange","darkorchid","darkred","darksalmon","darkseagreen","darkslateblue","darkturquoise","darkviolet",
    ];

    constructor(malkasten, position, options) {
        super(malkasten, position, options);

        this.color = this.options.color || Bacteria.colors[getRandomInt(0, Bacteria.colors.length - 1)];

        this.points = {};
        this.points[position.x] = {};
        this.points[position.x][position.y] = 0;

        // Use behavior to grow
        this.stepSize = this.options.stepSize || 0.5;
        this.addBehavior(this.grow);

        // Draw helper and infos
        this.paintAs = this.options.paintAs || "Rect"; // One of: "Rect", "Circle", "Star", "Line"
    }

    grow(context) {
        if(Math.random() > 0.5) {
            for(let x in this.points) {
                for(let y in this.points[x]) {
                    if(this.points[x][y] < 4) {
                        let xi = parseFloat(x);
                        let yi = parseFloat(y);
                        let newPoints = [
                            new Point(xi - this.stepSize, yi),
                            new Point(xi + this.stepSize, yi),
                            new Point(xi, yi - this.stepSize),
                            new Point(xi, yi + this.stepSize),
                        ];
                        let newPoint = newPoints[getRandomInt(0, 3)];

                        if(newPoint.x < 0 || newPoint.x > 100
                            || newPoint.y < 0 || newPoint.y > 100) {
                            continue;
                        }

                        if(!this.points[newPoint.x]) {
                            this.points[newPoint.x] = {};
                        }

                        let increaseBy = getRandomInt(1, 2);
                        if(!this.points[newPoint.x][newPoint.y]) {
                            this.points[newPoint.x][newPoint.y] = 0;
                        } else {
                            this.points[newPoint.x][newPoint.y] = this.points[newPoint.x][newPoint.y] + increaseBy;
                        }

                        this.points[x][y] = this.points[x][y] + increaseBy;
                    }
                }
            }
        }
    }

    paintAsRect(context, x, y, tileSize) {
        context.beginPath();
        context.fillRect(x - tileSize / 2, y - tileSize / 2, tileSize, tileSize);
        context.fill();
    }

    paintAsCircle(context, x, y, tileSize) {
        context.beginPath();
        context.arc(x, y, tileSize / 2, 0, 2 * Math.PI);
        context.fill();
    }

    paintAsStar(context, x, y, tileSize) {        
        let firstPoint = this.malkasten.percentagePointToPixelPoint(this.position);

        context.beginPath();
        context.moveTo(firstPoint.x, firstPoint.y);
        context.lineTo(x, y);
        context.stroke();
    }

    paintAsLine(context, x, y, tileSize) {
        if(!this.previousPoint) {
            this.previousPoint = this.malkasten.percentagePointToPixelPoint(this.position);
        }

        context.lineWidth = tileSize / 2;
        context.beginPath();
        context.moveTo(this.previousPoint.x, this.previousPoint.y);
        context.lineTo(x, y);
        context.stroke();

        this.previousPoint = new Point(x, y);
    }

    draw(context) {
        super.draw(context);

        context.fillStyle = this.color;
        context.strokeStyle = this.color;

        let paintAsMethod = `paintAs${this.paintAs}`;

        for(let x in this.points) {
            for(let y in this.points[x]) {
                this[paintAsMethod](
                    context,
                    this.malkasten.percentageToPixel(x, "width"),
                    this.malkasten.percentageToPixel(y, "height"),
                    this.malkasten.percentageToPixel(this.stepSize, "width")
                );
            }
        }
    }
}

export class ColonyEvent extends Event {
    static type = "malkasten-colony";

    constructor(object, status) {
        super(ColonyEvent.type);
        this.object = object;
        this.status = status;
    }
}

export class Colony extends CanvasObject {

    static paintAsMethod = {
        rect: "paintAsRect",
        circle: "paintAsCircle",
        star: "paintAsStar",
        line: "paintAsLine",
    };

    constructor(malkasten, position, options) {
        super(malkasten, position, options);

        this.color = this.options.color || `rgba(${getRandomInt(0, 255)}, ${getRandomInt(0, 255)}, ${getRandomInt(0, 255)}, 0.5)`;

        // Setup list of points to draw
        this.position.age = 1;
        this.position.cannotGrowAnymore = false;
        this.points = [this.position];
        this.pausedPointCount = 0;

        // Use behavior to grow
        this.stepSize = this.options.stepSize || 1;
        this.doNotStepOn = this.options.doNotStepOn || [];
        this.addBehavior(this.grow);

        this.drawSize = this.options.drawSize || this.stepSize;

        // Draw method
        this.paintAs = this.options.paintAs || this.constructor.paintAsMethod.rect;
    }

    grow(context) {
        this.points.forEach(point => {
            // Do not compute neighbours, we already now, there are no more free to grow to
            if(point.cannotGrowAnymore == true) {
                return;
            }

            let possibleNeighbours = Point.withoutDuplicates(point.getNeighbours(this.stepSize), [...this.points, ...this.doNotStepOn]);

            // Filter out any points outside the canvas – we don't go there
            possibleNeighbours = possibleNeighbours.filter(
                neighbour => !(neighbour.x < 0 || neighbour.x > 100 || neighbour.y < 0 || neighbour.y > 100)
            );

            // No more free neighbouring points
            if(possibleNeighbours.length == 0) {
                point.cannotGrowAnymore = true;
                this.pausedPointCount++;

                if(this.pausedPointCount == this.points.length) {
                    this.malkasten.canvas.dispatchEvent(new ColonyEvent(this, "finished"));
                    this.isPaused = true;
                }
                return;
            }

            // Add a new neighbour to the list of points
            if(Math.random() <= 1 / point.age) {
                let newPoint = possibleNeighbours[getRandomInt(0, possibleNeighbours.length - 1)];
                newPoint.cannotGrowAnymore = false;

                if(Math.random() < 0.25) {
                    newPoint.age = 1;
                } else {
                    newPoint.age = point.age;
                }

                this.points.push(newPoint);
            }

            point.age = point.age + getRandomInt(1, 5);
        });
    }

    paintAsRect(context, x, y, tileSize) {
        context.beginPath();
        context.fillRect(x - tileSize / 2, y - tileSize / 2, tileSize, tileSize);
        context.fill();
    }

    paintAsCircle(context, x, y, tileSize) {
        context.beginPath();
        context.arc(x, y, tileSize / 2, 0, 2 * Math.PI);
        context.fill();
    }

    paintAsStar(context, x, y, tileSize) {        
        let firstPoint = this.malkasten.percentagePointToPixelPoint(this.position);

        context.beginPath();
        context.moveTo(firstPoint.x, firstPoint.y);
        context.lineTo(x, y);
        context.stroke();
    }

    paintAsLine(context, x, y, tileSize) {
        if(!this.previousPoint) {
            this.previousPoint = this.malkasten.percentagePointToPixelPoint(this.position);
        }

        context.lineWidth = tileSize / 2;
        context.beginPath();
        context.moveTo(this.previousPoint.x, this.previousPoint.y);
        context.lineTo(x, y);
        context.stroke();

        this.previousPoint = new Point(x, y);
    }

    draw(context) {
        super.draw(context);

        context.fillStyle = this.color;
        context.strokeStyle = this.color;

        this.points.forEach(point => {
            this[this.paintAs](
                context,
                this.malkasten.percentageToPixel(point.x, "width"),
                this.malkasten.percentageToPixel(point.y, "height"),
                this.malkasten.percentageToPixel(this.drawSize, "width")
            );
        });
    }
}


export class Ravel extends CanvasObject {

    static colors = [
        "darkblue","darkcyan","darkgoldenrod","darkgreen","darkkhaki","darkmagenta","darkolivegreen",
        "darkorange","darkorchid","darkred","darksalmon","darkseagreen","darkslateblue","darkturquoise","darkviolet",
    ];

    constructor(malkasten, position, options = {}) {
        options.lineWidth = 0.75;

        super(malkasten, position, options);

        this.color = this.options.color || Ravel.colors[getRandomInt(0, Ravel.colors.length - 1)];

        this.points = [this.position];

        this.stepSize = getRandomInt(3, 6);
        this.addBehavior(this.grow);
    }

    grow(context) {
        let previousPoint = this.points[this.points.length - 1];
        let newPoint, newPoints;

        do {
            newPoints = [
                new Point(previousPoint.x - this.stepSize, previousPoint.y),
                new Point(previousPoint.x + this.stepSize, previousPoint.y),
                new Point(previousPoint.x, previousPoint.y - this.stepSize),
                new Point(previousPoint.x, previousPoint.y + this.stepSize),
            ];
            newPoint = newPoints[getRandomInt(0, 3)];
        } while(newPoint.x < 0 || newPoint.x > 100 || newPoint.y < 0 || newPoint.y > 100);

        this.points.push(newPoint);

        this.stepSize = getRandomInt(3, 6);

        // Spawn a new object
        if(Math.random() < 0.005) {
            this.malkasten.objects.add(new this.constructor(this.malkasten, newPoint));
        }
    }

    draw(context) {
        super.draw(context);

        context.beginPath();
        context.strokeStyle = this.color;
        context.lineWidth = this.malkasten.percentageToPixel(this.options.lineWidth, "width");
        context.lineCap = "round";
        context.lineJoin = "round";

        let points = [...this.points];
        let firstPoint = points.shift();

        context.moveTo(
            this.malkasten.percentageToPixel(firstPoint.x),
            this.malkasten.percentageToPixel(firstPoint.y)
        );

        this.points.forEach(point => {
            context.lineTo(
                this.malkasten.percentageToPixel(point.x, "width"),
                this.malkasten.percentageToPixel(point.y, "height")
            );
        });
        context.stroke();
    }
}

export class Cryptic extends CanvasObject {

    static defaultOptions = extend(super.defaultOptions, {
        lineWidth: 0.75,
        stepMin: 3,
        stepMax: 6,
        color: "#333",
    });

    constructor(malkasten, position, options = {}) {
        super(malkasten, position, options);

        this.points = [this.position];

        this.addBehavior(this.grow);
    }

    grow(context) {
        let previousPoint = this.points[this.points.length - 1];

        // How far to travel on this iteration
        let stepSize = getRandomInt(this.options.stepMin, this.options.stepMax);

        // Get a new random point, that is still on the canvas
        let newPoint, newPoints;
        do {
            newPoints = [
                new Point(previousPoint.x - stepSize, previousPoint.y),
                new Point(previousPoint.x + stepSize, previousPoint.y),
                new Point(previousPoint.x, previousPoint.y - stepSize),
                new Point(previousPoint.x, previousPoint.y + stepSize),
            ];
            newPoint = newPoints[getRandomInt(0, 3)];
        } while(newPoint.x < 0 || newPoint.x > 100 || newPoint.y < 0 || newPoint.y > 100);

        this.points.push(newPoint);

        // Spawn a new object
        if(Math.random() < 0.15) {
            // Stop growing the current object
            this.removeBehavior(this.grow);
            this.isPaused = true;

            // Add a new object to the canvas
            let newX = this.position.x + 15;
            let newY = this.position.y;
            if(newX > 91) {
                newX = 10;
                newY = newY + 15;
            }
            if(newY <= 90) {
                let nextSymbolAt = new Point(newX, newY);
                this.malkasten.objects.add(new this.constructor(this.malkasten, nextSymbolAt, this.options));
            }
        }
    }

    draw(context) {
        super.draw(context);

        context.beginPath();
        context.strokeStyle = this.options.color;
        context.lineWidth = this.malkasten.percentageToPixel(this.options.lineWidth, "width");
        context.lineCap = "round";
        context.lineJoin = "round";

        let points = [...this.points];
        let firstPoint = points.shift();

        context.moveTo(
            this.malkasten.percentageToPixel(firstPoint.x),
            this.malkasten.percentageToPixel(firstPoint.y)
        );

        this.points.forEach(point => {
            context.lineTo(
                this.malkasten.percentageToPixel(point.x, "width"),
                this.malkasten.percentageToPixel(point.y, "height")
            );
        });
        context.stroke();
    }
}


export class Sigil extends CanvasObject {

    static defaultOptions = extend(super.defaultOptions, {
        lineWidth: 0.5,
        stepMin: 2,
        stepMax: 5,
        color: "#333",
    });

    constructor(malkasten, position, options = {}) {
        super(malkasten, position, options);

        this.points = [this.position];

        this.addBehavior(this.grow);
    }

    grow(context) {
        let previousPoint = this.points[this.points.length - 1];

        // How far to travel on this iteration
        let stepSize = getRandomInt(this.options.stepMin, this.options.stepMax);

        // Get a new random point, that is still on the canvas
        let newPoint, newPoints;
        do {
            newPoints = [
                new Point(previousPoint.x - stepSize, previousPoint.y),
                new Point(previousPoint.x + stepSize, previousPoint.y),
                new Point(previousPoint.x, previousPoint.y - stepSize),
                new Point(previousPoint.x, previousPoint.y + stepSize),
                new Point(previousPoint.x - stepSize, previousPoint.y - stepSize),
                new Point(previousPoint.x + stepSize, previousPoint.y - stepSize),
                new Point(previousPoint.x + stepSize, previousPoint.y + stepSize),
                new Point(previousPoint.x - stepSize, previousPoint.y + stepSize),
            ];
            newPoint = newPoints[getRandomInt(0, newPoints.length - 1)];
        } while(newPoint.x < 0 || newPoint.x > 100 || newPoint.y < 0 || newPoint.y > 100);

        let pointTypeRandomFactor = Math.random();
        if(pointTypeRandomFactor < 0.7) {
            newPoint.type = "line";
        } else if(pointTypeRandomFactor < 0.9) {
            newPoint.type = "circle";
        } else {
            newPoint.type = "gap";
        }
        //newPoint.type = Sigil.pointTypes[getRandomInt(0, Sigil.pointTypes.length - 1)];
        this.points.push(newPoint);

        // Spawn a new object
        if(Math.random() < 0.2) {
            // Stop growing the current object
            this.removeBehavior(this.grow);
            this.isPaused = true;

            // Add a new object to the canvas
            let newX = this.position.x + 15;
            let newY = this.position.y;
            if(newX > 91) {
                newX = 10;
                newY = newY + 15;
            }
            if(newY <= 90) {
                let nextSymbolAt = new Point(newX, newY);
                this.malkasten.objects.add(new this.constructor(this.malkasten, nextSymbolAt, this.options));
            }
        }
    }

    draw(context) {
        super.draw(context);

        context.beginPath();
        context.strokeStyle = this.options.color;
        context.lineWidth = this.malkasten.percentageToPixel(this.options.lineWidth, "width");
        context.lineCap = "round";
        context.lineJoin = "round";

        let partials = {

            line: (from, to) => {
                context.moveTo(from.x, from.y);
                context.lineTo(to.x, to.y);
                context.stroke();
            },

            circle: (from, to) => {
                let radius = from.distanceTo(to) / 2;
                let center = new Point( (from.x + to.x) / 2, (from.y + to.y) / 2 );

                context.arc(center.x, center.y, radius, 0, 2 * Math.PI);
                context.stroke();
                context.moveTo(to.x, to.y);
            },

            gap: (from, to) => {
                context.moveTo(to.x, to.y);
            },
        };

        let points = [...this.points];
        let previousPoint = points.shift();

        this.points.forEach(point => {
            context.beginPath();
            partials[point.type || "line"](
                this.malkasten.percentagePointToPixelPoint(previousPoint),
                this.malkasten.percentagePointToPixelPoint(point)
            );

            previousPoint = point;
        });
    }
}


export class StaticSigil extends CanvasObject {

    static defaultOptions = extend(super.defaultOptions, {
        lineWidth: 0.5,
        stepMin: 2,
        stepMax: 5,
        color: "#333",
        maxLength: 10,
    });

    constructor(malkasten, position, options = {}) {
        super(malkasten, position, options);

        this.points = [this.position];

        this.addBehavior(this.grow);
    }

    grow(context) {
        let previousPoint = this.points[this.points.length - 1];

        // How far to travel on this iteration
        let stepSize = getRandomInt(this.options.stepMin, this.options.stepMax);

        // Get a new random point, that is still on the canvas
        let newPoint, newPoints;
        do {
            newPoints = [
                new Point(previousPoint.x - stepSize, previousPoint.y),
                new Point(previousPoint.x + stepSize, previousPoint.y),
                new Point(previousPoint.x, previousPoint.y - stepSize),
                new Point(previousPoint.x, previousPoint.y + stepSize),
                new Point(previousPoint.x - stepSize, previousPoint.y - stepSize),
                new Point(previousPoint.x + stepSize, previousPoint.y - stepSize),
                new Point(previousPoint.x + stepSize, previousPoint.y + stepSize),
                new Point(previousPoint.x - stepSize, previousPoint.y + stepSize),
            ];
            newPoint = newPoints[getRandomInt(0, newPoints.length - 1)];
        } while(newPoint.x < 0 || newPoint.x > 100 || newPoint.y < 0 || newPoint.y > 100);

        let pointTypeRandomFactor = Math.random();
        if(pointTypeRandomFactor < 0.7) {
            newPoint.type = "line";
        } else if(pointTypeRandomFactor < 0.9) {
            newPoint.type = "circle";
        } else {
            newPoint.type = "gap";
        }

        this.points.push(newPoint);

        // Stop growing the current object
        if(this.points.length == this.options.maxLength) {
            this.removeBehavior(this.grow);
            this.isPaused = true;
        }
    }

    draw(context) {
        super.draw(context);

        context.beginPath();
        context.strokeStyle = this.options.color;
        context.lineWidth = this.malkasten.percentageToPixel(this.options.lineWidth, "width");
        context.lineCap = "round";
        context.lineJoin = "round";

        let partials = {

            line: (from, to) => {
                context.moveTo(from.x, from.y);
                context.lineTo(to.x, to.y);
                context.stroke();
            },

            circle: (from, to) => {
                let radius = from.distanceTo(to) / 2;
                let center = new Point( (from.x + to.x) / 2, (from.y + to.y) / 2 );

                context.arc(center.x, center.y, radius, 0, 2 * Math.PI);
                context.stroke();
                context.moveTo(to.x, to.y);
            },

            gap: (from, to) => {
                context.moveTo(to.x, to.y);
            },
        };

        let points = [...this.points];
        let previousPoint = points.shift();

        this.points.forEach(point => {
            context.beginPath();
            partials[point.type || "line"](
                this.malkasten.percentagePointToPixelPoint(previousPoint),
                this.malkasten.percentagePointToPixelPoint(point)
            );

            previousPoint = point;
        });
    }
}

export class FullCanvasGradient extends CanvasObject {

    static defaultOptions = extend(super.defaultOptions, {
        colors: ["transparent", "cyan", "rgba(0, 255, 0, 0.5)"],
        angle: 0,
    });

    constructor(malkasten, position, options = {}) {
        super(malkasten, position, options);

        this.angle = this.options.angle;
        this.colors = this.options.colors;
    }

    draw(context) {
        super.draw(context);

        let gradient1 = new Gradient(
            this.malkasten,
            this.position,
            this.colors,
            this.angle,
        );

        // Full canvas gradient    
        let canvasBottomRight = this.malkasten.percentagePointToPixelPoint(new Point(100, 100));
        context.fillStyle = gradient1.fillStyle();
        context.fillRect(0, 0, canvasBottomRight.x, canvasBottomRight.y);
    }
}
