import { extend } from "./helper/helper.js";
import { Layers } from "./layer.js";
import { Text } from "./text.js";
import { Point } from "./point.js";
import { Specificity } from "./constants.js";
import { CanvasBase } from "./canvas-base.js";

/**
 * Base constructor for canvas objects
 */
export class MurkelCanvas extends CanvasBase {

    static defaultOptions = {
        // app tick iteration speed in milliseconds
        paintSpeed: 50,
        // larger = more smoothing
        smoothing: 0.5,

        // Dimensions
        gridSize: false,
        cols: 30,
        rows: 15,

        // Enable zoom an pan
        zoom: false,
        minZoomPercent: 100,
        maxZoomPercent: 1000,
        // The amount in percent to reduce or increase zoom percentage
        zoomSteps: 5,
        // Time in milliseconds after which to remove the zoom percentage display
        zoomRemoveDisplayAfter: 1000,

        // The whole canvas is filled with this color first on each paint
        backgroundColor: "#fff",

        // Set to `true` to print a background grid and some useful infos
        debug: false,
    };

    /**
     * 
     * @var {HTMLCanvasElement|string} canvas A canvas DOM element or a CSS selector to find one in the current page
     */
    constructor(canvas, options = {}, autoRun = true) {
        super(canvas);

        this.options = extend(this.constructor.defaultOptions, options);

        // Use either gridSize or cols & rows
        if(this.options.gridSize !== false) {
            this.setRowsColsFromgridSize(this.options.gridSize);
        } else {
            this.setRowsCols(this.options.rows, this.options.cols);
        }

        this.layers = new Layers();
        this.staticRendering = false;

        // Variables for FPS calculation and loop-tick speed
        this.fps = {
            last: 0,
            lastTimestamp: 0,
        };
        this.previousTimestamp = false;

        // Pause handling
        this.isInitialPaint = true;
        this.paintBeforePause = true;
        this.lastFrameCapture = null;

        // Pointer (mouse or other pointer device)
        let canvasDim = this.canvas.getBoundingClientRect();
        this.courseCorrection = { x: canvasDim.left, y: canvasDim.top };
        this.pointer = new Point();
        this.canvas.style.cursor = "none";

        // Update pointer position
        this.canvas.addEventListener("pointermove", event => this.onPointerMove(this.getPointerFromEvent(event)));
        this.canvas.addEventListener("pointerdown", event => this.onPointerDown(this.getPointerFromEvent(event)));
        this.canvas.addEventListener("pointerup", event => this.onPointerUp(this.getPointerFromEvent(event)));
        this.canvas.addEventListener("pointerleave", event => this.onPointerUp(this.getPointerFromEvent(event)));

        // Zoom
        this.zoomPercent = 100;
        this.gridSizeX100Percent = this.gridSizeX;
        this.gridSizeY100Percent = this.gridSizeY;
        this.lastZoomUpdate = performance.now();
        if(this.options.zoom == true) {
            this.canvas.addEventListener("wheel", event => this.onWheel(event));
        }

        // Listen for user input
        this.canvas.addEventListener("keydown", event => this.onKeyPress(event));
        this.canvas.addEventListener("keyup", event => this.onKeyUp(event));

        this.offsetX = 0;
        this.offsetY = 0;
        this.pointerBeforeDown = null;
        this.offsetXBeforeDown = 0;
        this.offsetYBeforeDown = 0;

        // Go!
        if(autoRun === true) {
            this.run();
        }
    }

    getPointerFromEvent(pointerEvent) {
        return new Point(
            event.x - this.courseCorrection.x,
            event.y - this.courseCorrection.y
        );
    }

    enforceMinMaxOffset() {
        let maxOffsetX = this.width - this.gridSizeX * this.cols;
        if(this.offsetX > 0) {
            this.offsetX = 0;
        } else if(this.offsetX < maxOffsetX) {
            this.offsetX = maxOffsetX;
        }

        let maxOffsetY = this.height - this.gridSizeY * this.rows;
        if(this.offsetY > 0) {
            this.offsetY = 0;
        } else if(this.offsetY < maxOffsetY) {
            this.offsetY = maxOffsetY;
        }
    }

    onPointerMove(pointer) {
        this.pointer = pointer;

        if(this.pointerBeforeDown !== null && this.options.zoom == true) {
            let newOffsetX = this.offsetXBeforeDown + (this.pointer.x - this.pointerBeforeDown.x);
            let newOffsetY = this.offsetYBeforeDown + (this.pointer.y - this.pointerBeforeDown.y);

            this.offsetX = Math.abs(newOffsetX) <= this.gridSizeX ? 0 : newOffsetX;
            this.offsetY = Math.abs(newOffsetY) <= this.gridSizeY ? 0 : newOffsetY;

            if(this.options.minZoomPercent >= 100) {
                this.enforceMinMaxOffset();
            }
        }
    }

    onPointerDown(pointer) {
        this.pointerBeforeDown = pointer;
        this.offsetXBeforeDown = this.offsetX;
        this.offsetYBeforeDown = this.offsetY;
    }

    onPointerUp(pointer) {
        this.pointerBeforeDown = null;
        this.offsetXBeforeDown = 0;
        this.offsetYBeforeDown = 0;
    }

    onWheel(event) {
        if(!this.isPaused) {
            // Do not scroll in page
            event.preventDefault();

            // …but instead zoom into canvas
            let zoomPercent;
            if(event.deltaY > 0) {
                // Zoom out
                this.zoom(this.zoomPercent - this.options.zoomSteps);
            } else {
                // Zoom into
                this.zoom(this.zoomPercent + this.options.zoomSteps);
            }
        }
    }

    zoom(percent) {
        // Enforce minimum & maximum zoom
        if(percent < this.options.minZoomPercent || percent > this.options.maxZoomPercent) {
            return false;
        }

        // Store center for offset calculation
        let centerPixelPoint = new Point(Math.round(this.width / 2), Math.round(this.height / 2));
        let gridCenter = new Point(
            Math.round((Math.abs(this.offsetX) + centerPixelPoint.x) / this.gridSizeX),
            Math.round((Math.abs(this.offsetY) + centerPixelPoint.y) / this.gridSizeY),
        );

        this.gridSizeX = this.gridSizeX100Percent / 100 * percent;
        this.gridSizeY = this.gridSizeY100Percent / 100 * percent;

        // Set new offset
        let newOffset = new Point(
            gridCenter.x * this.gridSizeX - this.width / 2,
            gridCenter.y * this.gridSizeY - this.height / 2,
        );
        this.offsetX = newOffset.x * -1;
        this.offsetY = newOffset.y * -1;

        // Store percentage internally for other methods
        this.zoomPercent = percent;

        if(this.options.minZoomPercent >= 100) {
            this.enforceMinMaxOffset();
        }

        // The timestamp is stored for display only, which hides after a certain time (`options.zoomRemoveDisplayAfter`)
        this.lastZoomUpdate = performance.now();
    }

    /**
     * What to do, when a keyboard key was pressed.
     * The base version here enables pausing the main loop.
     */
    onKeyPress(event) {
        switch(event.key) {
            case " ":
                if(this.isPaused == true) {
                    this.isPaused = false;
                } else {
                    this.isPaused = true;
                }
                break;
            case "+":
                if(event.ctrlKey == true) {
                    event.preventDefault();
                    this.zoom(this.zoomPercent + this.options.zoomSteps);
                }
                break;
            case "-":
                if(event.ctrlKey == true) {
                    event.preventDefault();
                    this.zoom(this.zoomPercent - this.options.zoomSteps);
                }
                break;
        }
    }

    onKeyUp(event) {
        // void base method; overload to add keyboard features
    }

    setRowsCols(rows, cols) {
        this.rows = rows;
        this.cols = cols;

        this.gridSizeY = Math.round(this.height / this.rows);
        this.gridSizeX = Math.round(this.width / this.cols);

        this.gridSizeX100Percent = this.gridSizeX;
        this.gridSizeY100Percent = this.gridSizeY;
    }

    setRowsColsFromgridSize(gridSize) {
        this.gridSizeY = gridSize;
        this.gridSizeX = gridSize;
        this.rows = Math.floor(this.height / this.gridSizeY);
        this.cols = Math.floor(this.width / this.gridSizeX);

        // Fit canvas size to rounded grid- * tile-sizes
        this.height = this.rows * this.gridSizeY;
        this.width = this.cols * this.gridSizeX;

        this.gridSizeX100Percent = this.gridSizeX;
        this.gridSizeY100Percent = this.gridSizeY;
    }

    /**
     * The canvas paint loop
     * 
     * @param {DOMHighResTimeStamp} timestamp
     */
    run(timestamp = null) {
        if(timestamp === null) {
            timestamp = performance.now();
        }

        if(this.options.debug) {
            // FPS computing
            this.fps.last = (this.fps.last * this.options.smoothing) + ((timestamp - this.fps.lastTimestamp) * (1.0 - this.options.smoothing));
            this.fps.lastTimestamp = timestamp;
        }

        // Wait for app tick
        if(this.previousTimestamp !== false && timestamp < (this.previousTimestamp + this.options.paintSpeed)) {
            requestAnimationFrame(timestamp => { this.run(timestamp); });
            return;
        }

        // Start over with no contents; this does not influence already rendered static layers.
        this.layers.reset();

        // Pause
        // TODO Bug on retina devices: due to scaling only a part of the canvas is stored in lastFrameCaptured
        if(this.isPaused) {
            if(this.paintBeforePause) {
                this.paintBeforePause = false;

                // gather current loop contents
                this.content();
                this.layers.layer(Specificity.POINTER).reset();

                // draw layers to canvas
                this.draw();

                this.lastFrameCapture = this.context.getImageData(0, 0, this.width, this.height);
            } else {
                // gather current loop contents
                this.context.putImageData(this.lastFrameCapture, 0, 0);
                this.pointerLayer();

                // draw cursor layer to canvas
                this.drawLayer(Specificity.POINTER);
            }
        } else {
            this.paintBeforePause = true;

            // gather current loop contents
            this.content();

            // draw layers to canvas
            this.draw();
        }

        // on to next paint cycle
        this.previousTimestamp = timestamp;

        requestAnimationFrame(timestamp => { this.run(timestamp); });
    }

    /**
     * Actually paint all layers on the screen.
     */
    draw() {
        // Render static layers only once
        if(!this.staticRendering) {
            this.layers.each(layer => {
                // Render each static layer and afterwards save the image to re-use later
                layer.contents.forEach(callback => callback());

                // Cannot add any more content to this layer afterwards
                layer.addContent = () => {};
            }, Layers.STATIC);

            this.staticRendering = this.context.getImageData(0, 0, this.width, this.height);
        } else {
            // Reuse staticRendering
            this.context.putImageData(this.staticRendering, 0, 0);
        }

        // Draw dynamic layers
        this.layers.each(layer => {
            layer.contents.forEach(callback => callback());
        }, Layers.NON_STATIC);
    }

    /**
     * Paint only a single layer onto the screen. Silent error mode.
     */
    drawLayer(specificity) {
        if(!this.layers.layers[specificity]) {
            return;
        }

        this.layers.layers[specificity].contents.forEach(callback => callback());
    }

    content() {
        this.layers.layer(Specificity.BACKGROUND).addContent(() => {
            // re-draw background
            this.context.fillStyle = this.options.backgroundColor;
            this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        });

        if(this.options.debug) {
            this.layers.layer(Specificity.FOREGROUND - 2).addContent(() => this.drawGrid());
            this.layers.layer(Specificity.FOREGROUND).addContent(() => this.drawFps());
        }

        if(this.options.zoom == true && performance.now() < this.lastZoomUpdate + this.options.zoomRemoveDisplayAfter) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                let text = new Text(
                    this.context,
                    `zoom: ${Math.round(this.zoomPercent)}%`,
                    this.width / 2,
                    this.height - 24,
                    {
                        textAlign: "center",
                        background: this.options.backgroundColor,
                    }
                );
                text.draw();
            });
        }

        this.pointerLayer();
    }

    /**
     * Paint cursor crosshair;
     * This layer gets special treament, as it is the only one rendered when the main loop is paused.
     */
    pointerLayer() {
        if (this.canvas.matches(':hover')) {
            this.layers.layer(Specificity.POINTER).addContent(() => {
                this.context.lineWidth = 2;
                this.context.strokeStyle = "#666";
                let crosshairLength = 10;

                this.context.beginPath();
                this.context.moveTo(this.pointer.x - crosshairLength, this.pointer.y);
                this.context.lineTo(this.pointer.x + crosshairLength, this.pointer.y);
                this.context.stroke();

                this.context.beginPath();
                this.context.moveTo(this.pointer.x, this.pointer.y - crosshairLength);
                this.context.lineTo(this.pointer.x, this.pointer.y + crosshairLength);
                this.context.stroke();
            });
        }
    }

    /**
     * For debug purposes: draw the matrix as a grid
     */
    drawGrid() {
        this.context.strokeStyle = "rgba(128, 128, 128, 0.5)";
        this.context.lineWidth = 1;
        for (var i = this.cols - 1; i > 0; i--) {
            this.context.beginPath();
            this.context.moveTo(i * this.gridSizeX + this.offsetX, this.offsetY);
            this.context.lineTo(i * this.gridSizeX + this.offsetX, this.rows * this.gridSizeY + this.offsetY);
            this.context.stroke();
        }
        for (var i = this.rows - 1; i > 0; i--) {
            this.context.beginPath();
            this.context.moveTo(this.offsetX, i * this.gridSizeY + this.offsetY);
            this.context.lineTo(this.cols * this.gridSizeX + this.offsetX, i * this.gridSizeY + this.offsetY);
            this.context.stroke();
        }
    }

    /**
     * For debug purposes: draw the computed frames-per-second
     */
    drawFps() {
        let text = new Text(
            this.context,
            `${Math.round(this.fps.last)} fps`,
            this.width - 10,
            this.height - 24,
            {textAlign: "right"}
        );
        text.draw();
    }

}
