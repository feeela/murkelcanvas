import { extend, getRandomInt } from "../../../helper/helper.js";
import { Direction } from "../../../constants.js";
import { EventStack } from "../../../helper/event-stack.js";
import { Astar } from "../../../matrix/astar.js";
import { GameSpecificity, Goods } from "../constants.js";
import { Storage } from "../storage.js";
import { GameObject, CivArea } from "./game-object.js";
import { Water, Land, Mountain } from "./terrain.js";
import { TerrainFeature, Path, TreeNursery } from "./terrain-feature.js";
import { Building, Kontor, Tower } from "./building.js";

// for debugging
import { Marker } from "../../../matrix/matrix-object.js";

export class Civilization extends GameObject {

    static defaultOptions = {
        color: "#a6a",
        kontorBorderRadius: 4,
        startGoods: {
            // Default civilization storage items at game start
            [Goods.Axe]: 2,
            [Goods.Pickaxe]: 3,

            // 30 Wood & 20 Stone for first/initial Kontor are used up immediately on initialization
            [Goods.Wood]: 30,
            [Goods.Stone]: 30, // + 10 Stone extra for a tower, in case the initial territory has no stone
        },
        noDrawnPathTo: [Kontor.name], // Must be an array; can be empty
    };

    constructor(game, position, options = {}) {
        super(true, false, options, options.specificity || GameSpecificity.DEFAULT);

        // Each civilization has an internal ID to identify it
        this.id = this.constructor.name + "_" + Math.random().toString(16).slice(2);

        this.game = game;
        this.position = position;
        this.color = this.options.color;
        this.astar = new Astar(game.matrix);

        this.events = new EventStack();

        // A list of Points for the overall area occupied by this civilization
        this.area = [];

        // A list of all buildings
        this.buildings = {};

        // Initiaize this civilizatinos storage for goods, tools, fruit, …
        this.storage = new Storage();
        this.storage.fillKeys(Object.values(Goods));
        if(this.options.startGoods) {
            for(let article in this.options.startGoods) {
                this.storage.add(article, this.options.startGoods[article]);
            }
        }

        // Initial Kontor at civ-center
        this.addKontor(this.position);
    }

    /**
     * This method wotks on `this.area` – the internal list of points from the own territory.
     * Stops at first found item with given `type`.
     * 
     * @param {Point} center Sort territory points by distance to given point
     * @param {Number} minRadius Start looking at minimum radius from center
     * @param {Number} maxRadius Search up to a maximum radius from center
     * @return {Point|false} Returns the nearest matching point or `false` if not found,
     */
    findFreeField(center, minRadius = 0, maxRadius = 100) {
        let area = center.sortListByDistance(this.area);
        for(let i in area) {
            let distance = Math.round(center.distanceTo(area[i]));
            if(distance >= minRadius && distance <= maxRadius
                && !this.game.matrix.hasValue(area[i], TerrainFeature)
                && !this.game.matrix.hasValue(area[i], Building)
                && !this.game.matrix.hasValue(area[i], Water)
                && !this.game.matrix.hasValue(area[i], Mountain)) {
                return area[i];
            }
        }
        return false;
    }

    /**
     * This method wotks on `this.area` – the internal list of points from the own territory.
     * 
     * @param {Point} center Sort territory points by distance to given point
     * @param {Number} minRadius Start looking at minimum radius from center
     * @param {Number} maxRadius Search up to a maximum radius from center
     */
    findFreeFields(center, minRadius = 0, maxRadius = 100) {
        let area = center.sortListByDistance(this.area);
        return area.filter(fbp => {
            let distance = center.distanceTo(fbp);
            return distance >= minRadius && distance <= maxRadius
                && !this.game.matrix.hasValue(fbp, TerrainFeature)
                && !this.game.matrix.hasValue(fbp, Building)
                && !this.game.matrix.hasValue(fbp, Water)
                && !this.game.matrix.hasValue(fbp, Mountain);
        });
    }

    /**
     * This method wotks on `this.area` – the internal list of points from the own territory.
     * Stops at first found item with given `type`.
     * 
     * @param {Function|string} type Some GameObject's constructor or class name
     * @param {Point} center Sort territory points by distance to given point
     * @param {Number} minRadius Start looking at minimum radius from center
     * @param {Number} maxRadius Search up to a maximum radius from center
     * @return {Point|false} Returns the nearest matching point or `false` if not found,
     */
    findTypeField(type, center, minRadius = 0, maxRadius = 100) {
        let area = center.sortListByDistance(this.area);
        for(let i in area) {
            let distance = Math.round(center.distanceTo(area[i]));
            if(distance >= minRadius && distance <= maxRadius
                && this.game.matrix.hasValue(area[i], type)) {
                return area[i];
            }
        }
        return false;
    }

    /**
     * This method wotks on `this.area` – the internal list of points from the own territory.
     * 
     * @param {Function|string} type Some GameObject's constructor or class name
     * @param {Point} center Sort territory points by distance to given point
     * @param {Number} minRadius Start looking at minimum radius from center
     * @param {Number} maxRadius Search up to a maximum radius from center
     * @return {Point[]} Returns a list of Points or an empty list if `type` was not found.
     */
    findTypeFields(type, center, minRadius = 0, maxRadius = 100) {
        let area = center.sortListByDistance(this.area);
        return area.filter(fbp => {
            let distance = Math.round(center.distanceTo(fbp));
            return distance >= minRadius && distance <= maxRadius
                && this.game.matrix.hasValue(fbp, type);
        });
    }

    addBuilding(position, buildingConstructor, buildingOptions = {}) {
        // Is there enough materials in the storage to build this building?
        if(!this.storage.check(buildingConstructor.requires)) {
            return false;
        }

        // Take required material from the storage
        for(let article in buildingConstructor.requires) {
            this.storage.take(article, buildingConstructor.requires[article]);
        }

        // Pass on debug flag from SettlerGame
        buildingOptions.debug = this.game.options.debug;

        let building = new buildingConstructor(
            this,
            (buildingConstructor == Tower || buildingConstructor == Kontor) ? false : this.getClosestKontor(position),
            position,
            buildingOptions
        );

        if(!this.buildings[building.constructor.name]) {
            this.buildings[building.constructor.name] = [];
        }

        this.buildings[building.constructor.name].push(building);
        this.game.matrix.addValue(position, building);

        // The path drawn here is computed in each building (except Kontors)
        if(building.pathTo[Kontor.name] && !this.options.noDrawnPathTo.includes(building.constructor.name)) {
            // Add Path objects to the found path (which actually draw this path on the map)
            let pathToNewBuilding = building.pathTo[Kontor.name];
            let previousPoint = building.position;
            for(let i = 0; i < pathToNewBuilding.length - 1; i++) {
                let pp = pathToNewBuilding[i];

                let d1 = Direction.angleToDirection(pp.angleTo(previousPoint));
                let d2 = Direction.angleToDirection(pp.angleTo(pathToNewBuilding[i + 1]));

                let existingPath = this.game.matrix.getValue(pp, Path);
                if(existingPath) {
                    existingPath.addDirection(d1, d2);
                } else {
                    let path = new Path([d1, d2]);
                    this.game.matrix.addValue(pp, path);
                }

                previousPoint = pp;
            }
        }

        return true;
    }

    removeBuilding(building) {
        building.cleanup();
        this.game.matrix.removeValue(building.position, building.constructor);
    }

    /**
     * Add a list of points to the area of this civilization.
     * 
     * Checks constraints:
     * – Are the points even valid?
     * – Is this a point that already belongs to another civilization?
     * 
     * @param {Point[]} points
     */
    addArea(points, kontor = false) {
        if(!kontor) {
            kontor = this.buildings[Kontor.name][0];
        }

        // Filter given points
        let newPoints = points.filter(point => {
            if(!this.game.matrix.isValidPoint(point)) {
                return false;
            }

            // Reset old edges of own CivAreas
            let existingCivArea = this.game.matrix.getValue(point, CivArea);
            if(existingCivArea != false && existingCivArea.kontor.civilization.id == this.id) {
                existingCivArea.isEdge = false;
            }

            return existingCivArea == false;
            //return this.game.matrix.isValidPoint(point) && !this.game.matrix.hasValue(point, CivArea);
        });

        // Add CivArea to those points
        newPoints.forEach(point => {
            this.game.matrix.addValue(point, new CivArea(kontor, false, kontor.position.angleTo(point)));
        });

        // Merge list of points
        this.area = [...this.area, ...newPoints];

        // Edge detection (used to render borders)
        this.area.forEach(point => {
            let ownCivNeighbours = this.game.matrix.getNeighboursWithType(point, CivArea, false).filter(neighbour => {
                let civArea = this.game.matrix.getValue(neighbour, CivArea);
                if(civArea.kontor.civilization.id == this.id) {
                    // One of ours, reset isEdge
                    //civArea.isEdge = false;
                    return true;
                } else {
                    return false;
                }
            });

            let civArea = this.game.matrix.getValue(point, CivArea);

            // If this field has at least one empty or foreign neighbouring field, it is a border field.
            if(ownCivNeighbours.length != 4) {
                civArea.isEdge = true;

                let ownDirections = [];
                ownCivNeighbours.forEach(ocn => {
                    ownDirections.push(Direction.angleToDirection(point.angleTo(ocn)));
                });

                civArea.directions = [Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT].filter(x => !ownDirections.includes(x));
            }
        });
    }

    addKontor(position) {
        this.game.matrix.removeValues(position, TerrainFeature);

        return this.addBuilding(position, Kontor, {
            borderRadius: this.options.kontorBorderRadius,
        });
    }

    getClosestKontor(point) {
        let closestKontor;
        let closestKontorDistance = Number.MAX_SAFE_INTEGER;
        this.buildings[Kontor.name].forEach(kontor => {
            let kontorDistance = point.distanceTo(kontor.position);
            if(kontorDistance < closestKontorDistance) {
                closestKontorDistance = kontorDistance;
                closestKontor = kontor;
            }
        });
        return closestKontor;
    }

    isPointWithinBorder(point) {
        let buildingsWithArea;
        if(this.buildings[Tower.name]) {
            buildingsWithArea = [...this.buildings[Kontor.name], ...this.buildings[Tower.name]];
        } else {
            buildingsWithArea = this.buildings[Kontor.name];
        }

        for(let i in buildingsWithArea) {
            let distance = Math.floor(buildingsWithArea[i].position.distanceTo(point));
            if(distance <= this.options.kontorBorderRadius) {
                return true;
            }
        }
        return false;
    }

    findPathWithinTerritory(from , to) {
        // Both, source and target must be within own borders
        if(!this.isPointWithinBorder(from) || !this.isPointWithinBorder(to)) {
            return false;
        }

        return this.astar.search(from, to, point => {
            return this.game.matrix.getNeighboursWithType(point, Land)
                .filter(neighbour => {
                    // Make sure to only step on own territory
                    let civArea = this.game.matrix.getValue(neighbour, CivArea);
                    return civArea !== false && civArea.kontor.civilization.id == this.id;
                });
        });
    }

    on(eventType, callback) {
        this.events.on(eventType, callback);
    }

    dispatchEvent(event) {
        this.events.dispatchEvent(event);
    }
}
