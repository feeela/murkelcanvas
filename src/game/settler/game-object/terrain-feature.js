import { extend } from "../../../helper/helper.js";
import { GameSpecificity } from "../constants.js";
import { GameObject, GameObjectWithActions } from "./game-object.js";
import { ActionGenerator } from "../../../helper/action-generator.js";

export class TerrainFeature extends GameObject {
    constructor(specificity = GameSpecificity.TERRAIN_FEATURE) {
        super(true, false, {}, specificity);
    }
}

export class Path extends TerrainFeature {
    constructor(directions = [Direction.LEFT, Direction.RIGHT]) {
        super(GameSpecificity.PATH);

        this.directions = [... new Set(directions)]; // get unique values via Set;
    }

    addDirection(...directions) {
        directions.forEach(direction => this.directions.push(direction));

        this.directions = [... new Set(this.directions)]; // get unique values via Set
    }
}

/**
 * A base class for an object where somehting can be taken from.
 */
export class Depletable extends TerrainFeature {
    /**
     * @param {Number} amount The fill level in percent.
     */
    constructor(amount = 100) {
        super();

        this.amount = amount;
    }

    take(amount = 5) {
        if(this.amount >= amount) {
            this.amount = this.amount - amount;
        } else {
            amount = this.amount;
            this.amount = 0;
        }
        return amount;
    }
}

// for land

export class Forest extends Depletable {
}

/**
 * Growing objects have their own loop implemented, because they should extend TerrainFeature
 * to be detected as such.
 * 
 * The amount is meant to be a percentage value.
 */
export class Growing extends TerrainFeature {
    static defaultOptions = {
        speed: 1000,
        growBy: 1,
    };

    constructor(game, position, options = {}) {
        super();

        this.game = game;
        this.position = position;
        this.options = extend(this.constructor.defaultOptions, options);

        this.amount = 0;

        this.actions = new ActionGenerator();
        this.actions.addAction(this.grow);

        this.run();
    }

    grow() {
        if(this.amount < 100) {
            this.amount = this.amount + this.options.growBy;

            if(this.amount >= 100) {
                this.amount = 100;

                this.actions.removeAction(this.grow);
                this.paused = true;
            }
        }
        return true;
    }

    run() {
        setTimeout(() => {
            this.run();
        }, this.options.speed);

        if(this.game.isPaused == true) {
            return;
        }

        this.actions.runAction(this);
    }
}

export class TreeNursery extends Growing {
    grow() {
        super.grow();

        // Replace with Forest at 100%
        if(this.amount >= 100) {
            // Replace TreeNursery with Forest in matrix
            this.game.matrix.removeValue(this.position, TreeNursery);
            this.game.matrix.addValue(this.position, new Forest());
        }

        return true;
    }
}

export class Wheat extends Growing {
    static defaultOptions = extend(super.defaultOptions, {
        growBy: 1,
    });
}

export class Rock extends Depletable {
}

export class Swamp extends TerrainFeature {
    constructor() {
        super();

        this.passable = false;
    }
}

// for mountains

export class Ore extends Depletable {
}

export class Iron extends Ore {
}

export class Gold extends Ore {
}

export class Coal extends Ore {
}
