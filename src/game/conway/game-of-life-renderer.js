import { Renderer } from "../../render/renderer.js";
import { GameOfLifeCell } from "./game-of-life.js";

export function getRenderer(canvasObject) {
    let renderer = new Renderer(canvasObject);

    renderer.register(GameOfLifeCell.name, (context, x, y, width, height, gameOfLifeCellObject) => {
        context.fillStyle = gameOfLifeCellObject.getColor();
        context.fillRect(x, y, width, height);
    });

    return renderer;
}
