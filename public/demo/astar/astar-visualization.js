import { Point } from '../../js/lib-point.js';
import { extend, shuffle } from "../../js/helper.js"
import { MatrixObject, Cursor } from '../../js/matrix.js';
import { MatrixCanvas } from "../../js/matrix-canvas.js";
import { MazeBuilder } from "../../maze/maze-builder.js";
import { Astar, BinaryHeap, WeightedPoint } from "../../js/astar.js";

export class Marker extends MatrixObject {
    constructor(importance = 0, parent = null, color = "rgba(255, 0, 255, 0.33)") {
        super();
        this.color = color;
        this.importance = importance;
        this.parent = parent;
    }
}

export class PathTile extends MatrixObject {
    constructor(color = "#00a") {
        super();
        this.color = color;
    }
}

export class Door extends MatrixObject {
    constructor(color = "#3c3") {
        super();
        this.color = color;
    }
}

export class AstarMatrixVisualization extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        paintSpeed: 25,
        cursor: false,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        // generate maze
        let mazeBuilder = new MazeBuilder(this.matrix, options);

        let cornerTiles = shuffle([
            new Point(1, 1),
            new Point(this.cols - 2, 1),
            new Point(1, this.rows - 2),
            new Point(this.cols - 2, this.rows - 2),
        ]);

        this.cursor = cornerTiles[0];
        // Add cursor object to that field
        // (The original cursor from Matrix has been overwritten while generating the maze.)
        this.entrance = this.cursor;
        this.matrix.addValue(this.cursor, new Cursor());

        // Add exit
        this.exit = cornerTiles[1];
        this.matrix.addValue(cornerTiles[1], new Door());

        // Path finding variables
        this.possiblePathPoints = [];
        this.dirtyNodes = [];
        this.path = [];
        this.pathTotalLength = 0;
    }

    resetRenderer() {
        super.resetRenderer();

        // Add the matrix object Marker to the renderer
        this.renderer.register(Marker.name, (context, x, y, width, height, markerObject) => {
            context.fillStyle = markerObject.color;
            let drawWidth = width / 6;
            let drawHeight = height / 6;

            context.fillRect(x + drawWidth, y + drawHeight, drawWidth * 4, drawHeight * 4);

            context.font = `${drawHeight * 2}px sans-serif`;
            context.textAlign = "center";
            context.textBaseline = "middle";
            context.fillStyle = "#333";
            context.fillText(markerObject.importance, x + width / 2, y + height / 1.8);

            if(markerObject.parent !== null) {
                context.beginPath();
                context.lineWidth = width / 20;
                context.strokeStyle = "#999";
                let parentX = markerObject.parent.x * width;
                let parentY = markerObject.parent.y * height;

                if(parentX < x) {
                    context.moveTo(x + drawWidth, y + drawHeight * 3);
                    context.lineTo(markerObject.parent.x * width + drawWidth * 5, markerObject.parent.y * height + drawHeight * 3);
                } else if(parentX > x) {
                    context.moveTo(x + drawWidth * 5, y + drawHeight * 3);
                    context.lineTo(markerObject.parent.x * width + drawWidth, markerObject.parent.y * height + drawHeight * 3);
                } else if(parentY < y) {
                    context.moveTo(x + drawWidth * 3, y + drawHeight);
                    context.lineTo(markerObject.parent.x * width + drawWidth * 3, markerObject.parent.y * height + drawHeight * 5);
                } else if(parentY > y) {
                    context.moveTo(x + drawWidth * 3, y + drawHeight * 5);
                    context.lineTo(markerObject.parent.x * width + drawWidth * 3, markerObject.parent.y * height + drawHeight);
                }
                context.stroke();
            }
        });

        // Add the matrix object PathTile to the renderer
        this.renderer.register(PathTile.name, (context, x, y, width, height, pathTileObject) => {
            context.fillStyle = pathTileObject.color;
            context.strokeStyle = "#fff";
            let drawWidth = width / 6;
            let drawHeight = height / 6;
            context.fillRect(x + drawWidth * 2, y + drawHeight * 2, drawWidth * 2, drawHeight * 2);
            context.strokeRect(x + drawWidth * 2, y + drawHeight * 2, drawWidth * 2, drawHeight * 2);
        });

        this.renderer.register(Door.name, (context, x, y, width, height, doorObject) => {
            context.fillStyle = doorObject.color;
            context.fillRect(x, y, width, height);
        });
    }

    moveCursor(direction, newCursor = null) {
        // Do nothing
    }

    content(removeDirection = true) {
        super.content();

        let searchStep = this.searchStep(this.entrance, this.exit);
        if(typeof searchStep !== "undefined") {
            if(searchStep instanceof Point) {
                this.matrix.addValue(searchStep, new Marker(searchStep.weight, searchStep.parent))
            } else {
                // Is final result array
                this.path = searchStep;
                this.pathTotalLength = searchStep.length;
            }
        }

        // Draw found path, one pebble per Matrix.run() iteration
        if(this.path.length > 0) {
            let wayPoint = this.path.pop();
            this.matrix.addValue(wayPoint, new PathTile());
        }
    }


    /**
    * Perform an A* Search on a graph given a start and end node.
    *
    * @param {Point} start
    * @param {Point} end
    */
    *searchGenerator(start, end) {
        this.cleanDirty();

        //let heuristic = options.heuristic || astar.heuristics.manhattan
        let heuristic = Astar.heuristics.manhattan
        //let closest = options.closest || false;
        let closest = false;

        let path = [];
        let openHeap = this.getHeap();
        let closestNode = start;

        start = new WeightedPoint(start.x, start.y);
        end = new WeightedPoint(end.x, end.y);

        start.h = heuristic(start, end);

        openHeap.push(start);

        while(openHeap.size() > 0) {
            // Grab the lowest f(x) to process next.  Heap keeps this sorted for us.
            let currentNode = openHeap.pop();

            // End case -- result has been found, return the traced path.
            if(currentNode.equals(end)) {
                return this.pathTo(currentNode);
            }

            // Normal case -- move currentNode from open to closed, process each of its neighbours.
            currentNode.closed = true;

            // Find all passable neighbours for the current node.
            let neighbours = this.getNeighbours(currentNode);

            for(let i = neighbours.length - 1; i >= 0; i--) {
                let neighbour = neighbours[i];

                if (neighbour.closed === true) {
                    // Not a valid node to process, skip to next neighbour.
                    continue;
                }

                // The g score is the shortest distance from start to current node.
                // We need to check if the path we have arrived at this neighbour is the shortest one we have seen yet.
                let gScore = currentNode.g + neighbour.getCost(currentNode);
                let beenVisited = neighbour.visited;

                if (!beenVisited || gScore < neighbour.g) {
                    // Found an optimal (so far) path to this node.  Take score for node to see how good it is.
                    neighbour.visited = true;
                    neighbour.parent = currentNode;
                    neighbour.h = neighbour.h || heuristic(neighbour, end);
                    neighbour.g = gScore;
                    neighbour.f = neighbour.g + neighbour.h;
                    neighbour.weight = heuristic(neighbour, end);
                    this.markDirty(neighbour);
                    if (closest) {
                        // If the neighbour is closer than the current closestNode or if it's equally close but has
                        // a cheaper path than the current closest node then it becomes the closest node
                        if (neighbour.h < closestNode.h || (neighbour.h === closestNode.h && neighbour.g < closestNode.g)) {
                            closestNode = neighbour;
                        }
                    }

                    if (!beenVisited) {
                        // Pushing to heap will put it in proper place based on the 'f' value.
                        openHeap.push(neighbour);
                    }
                    else {
                        // Already seen the node, but since it has been rescored we need to reorder it in the heap
                        openHeap.rescoreElement(neighbour);
                    }
                }

                yield neighbour;
            }
        }

        if (closest) {
            return this.pathTo(closestNode);
        }

        // No result was found - empty array signifies failure to find path.
        return [];
    }

    getSearchGenerator(start, end) {
        if(!this.generator) {
            this.generator = this.searchGenerator(start, end);
        }
        return this.generator;
    }

    searchStep(start, end) {
        return this.getSearchGenerator(start, end).next().value;
    }


    /**
     * Return the valid neighbouring fields of the given point as list of WeightedPoint's
     */
    getNeighbours(point) {
        let neighbours = [];

        // Iterate over possible neighbour candidates
        [
            new Point(point.x - 1, point.y),
            new Point(point.x + 1, point.y),
            new Point(point.x, point.y - 1),
            new Point(point.x, point.y + 1),
        ].forEach(candidate => {
            // Skip invalid or not passable points
            if(!this.matrix.isValidPoint(candidate) || !this.matrix.isPassable(candidate)) {
                return;
            }

            if(!this.possiblePathPoints[candidate.y]
                || !this.possiblePathPoints[candidate.y][candidate.x]) {
                // Create new one for the list
                if(!this.possiblePathPoints[candidate.y]) {
                    this.possiblePathPoints[candidate.y] = [];
                }
                this.possiblePathPoints[candidate.y][candidate.x] = new WeightedPoint(candidate.x, candidate.y, );
            }

            neighbours.push(this.possiblePathPoints[candidate.y][candidate.x]);
        });

        return neighbours;
    }

    pathTo(node){
        var curr = node,
            path = [];
        while(curr.parent) {
            path.unshift(curr);
            curr = curr.parent;
        }
        return path;
    }

    getHeap() {
        return new BinaryHeap(function(node) {
            return node.f;
        });
    }

    cleanDirty() {
        for (let i = this.dirtyNodes.length - 1; i >= 0; i--) {
            this.cleanNode(this.dirtyNodes[i]);
        }
        this.dirtyNodes = [];
    }

    markDirty(node) {
        this.dirtyNodes.push(node);
    }

    cleanNode(point){
        point.f = 0;
        point.g = 0;
        point.h = 0;
        point.visited = false;
        point.closed = false;
        point.parent = null;
        return point;
    }
}
