import { ActionGenerator } from "./action-generator.js";

export class ActionRunner {
    /**
     * @param {Number|false} speed The time in milliseconds from one iteration to the next; if set to false; run() must be called manually
     * @param {Boolean} paused Pauses the action execution (though loop keeps running)
     */
    constructor(speed = 250, paused = false) {
        this.speed = speed;
        this.paused = paused;

        this.actions = new ActionGenerator();

        this.currentTimeOut = false;
        if(this.speed !== false) {
            this.run();
        }
    }

    addAction(callback) {
        this.actions.addAction(callback);
    }

    removeAction(callback) {
        this.actions.removeAction(callback);
    }

    removeAll() {
        this.actions.removeAll();
    }

    /**
     * Is called once every game tick and executes the object's actions.
     */
    run() {
        if(this.speed !== false) {
            this.currentTimeOut = setTimeout(() => {
                this.run();
            }, this.speed);
        }

        if(this.paused) {
            return;
        }

        this.actions.runAction(this);
    }

    /**
     * Stop this action runner by canceling the curent timeout and setting speed to false.
     */
    stop() {
        if(this.currentTimeOut) {
            clearTimeout(this.currentTimeOut);
        }
        this.speed = false;
    }
}
