
export class GameEvent {
    constructor() {
    }
}

export class ResourcesDepletedEvent extends GameEvent {
    constructor(harvester) {
        super();

        this.harvester = harvester;
    }
}
