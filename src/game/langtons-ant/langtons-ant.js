import { Point } from "../../point.js";
import { extend } from "../../helper/helper.js";
import { Direction } from "../../constants.js";
import { MatrixCanvas } from "../../matrix-canvas.js";
import { getRenderer } from "./langtons-ant-renderer.js";
import { AntSquare, LangtonsAnt } from "./langtons-ant-objects.js";

export { AntSquare, LangtonsAnt };

export class LangtonsAntCanvas extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
    	antSpeed: 250,
    	antPosition: null,
    	autoRunAnt: true,
    	antColor: "#930",
        gridSize: 25,
        cursor: false,
        getRenderer: getRenderer,
        fillConstructor: AntSquare,
        fillDistinct: true,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        if(this.options.antSpeed < this.options.paintSpeed) {
        	this.options.paintSpeed = this.options.antSpeed;
        }

        this.setup();

	    // User controllable AntSquare color
	    this.canvas.addEventListener("pointerdown", event => {
	        if(!this.isPaused) {
	            let matrixPoint = new Point(Math.floor(this.pointer.x / this.gridSizeX), Math.floor(this.pointer.y / this.gridSizeY));
	            let square = this.matrix.getValue(matrixPoint, AntSquare);
	            square.flip();
	        }
	    });
    }

    setup() {
	    // Create an ant and add to the center
	    this.ant = new LangtonsAnt(this.options.antColor, Direction.UP, this.options.antSpeed);
	    if(this.options.autoRunAnt == false) {
	    	this.ant.isPaused = true;
	    }

	    let antPosition = this.options.antPosition || new Point(Math.floor((this.cols - 1) / 2), Math.floor((this.rows - 1) / 2));
	    this.cursor = antPosition;
	    this.matrix.addValue(
	        antPosition,
	        this.ant
	    );
    }
}
