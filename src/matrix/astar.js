import { Point } from "../point.js";
import { BinaryHeap } from "../helper/binary-heap.js"

export { BinaryHeap };

export class WeightedPoint extends Point {
    constructor(x, y) {
        super(x, y);

        this.weight = 1;

        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.visited = false;
        this.closed = false;
        this.parent = null;
    }

    getCost(fromNeighbour) {
        // Take diagonal weight into consideration.
        if (fromNeighbour && fromNeighbour.x != this.x && fromNeighbour.y != this.y) {
            return this.weight * 1.41421;
        }
        return this.weight;
    }

    toString() {
        return JSON.stringify({
            x: this.x,
            y: this.y,
            weight: this.weight,
            f: this.f,
            g: this.g,
            h: this.h,
            visited: this.visited,
            closed: this.closed,
            parent: this.parent,
        });
    }
}

/**
 * An A* path finding algorithm implementation on the Matrix object.
 */
export class Astar {
    static heuristics = {
        manhattan: function(pos0, pos1) {
            var d1 = Math.abs(pos1.x - pos0.x);
            var d2 = Math.abs(pos1.y - pos0.y);
            return d1 + d2;
        },
        diagonal: function(pos0, pos1) {
            var D = 1;
            var D2 = Math.sqrt(2);
            var d1 = Math.abs(pos1.x - pos0.x);
            var d2 = Math.abs(pos1.y - pos0.y);
            return (D * (d1 + d2)) + ((D2 - (2 * D)) * Math.min(d1, d2));
        }
    };

    /**
     * @param {Matrix} matrix This algorithm uses `Matrix.getNeighbours()` to find passable fields.
     */
    constructor(matrix) {
        this.matrix = matrix;

        // Lookup list for `this.getNeighbours()`
        this.possiblePathPoints = [];

        this.dirtyNodes = [];
    }

    /**
     * Perform an A* Search on a graph given a start and end node.
     *
     * @param {Point} start
     * @param {Point} end
     * @return {WeightedPoint[]} Returns an empty array if no path was found
     */
    search(start, end, getNeighboursFunction = false) {
        this.cleanDirty();
        this.possiblePathPoints = [];

        //let heuristic = options.heuristic || astar.heuristics.manhattan
        let heuristic = Astar.heuristics.manhattan
        //let closest = options.closest || false;
        let closest = false;

        if(getNeighboursFunction == false) {
            getNeighboursFunction = point => this.matrix.getNeighbours(point);
        }

        let path = [];
        let openHeap = this.getHeap();
        let closestNode = start;

        start = new WeightedPoint(start.x, start.y);
        end = new WeightedPoint(end.x, end.y);

        start.h = heuristic(start, end);

        openHeap.push(start);

        while(openHeap.size() > 0) {
            // Grab the lowest f(x) to process next.  Heap keeps this sorted for us.
            let currentNode = openHeap.pop();

            // End case -- result has been found, return the traced path.
            if(currentNode.equals(end)) {
                return this.pathTo(currentNode);
            }

            // Normal case -- move currentNode from open to closed, process each of its neighbours.
            currentNode.closed = true;

            // Find all passable neighbours for the current node.
            let neighbours = this.getNeighbours(currentNode, getNeighboursFunction);

            neighbours.forEach(neighbour => {
                if (neighbour.closed === true) {
                    // Not a valid node to process, skip to next neighbour.
                    return;
                }
                // The g score is the shortest distance from start to current node.
                // We need to check if the path we have arrived at this neighbour is the shortest one we have seen yet.
                let gScore = currentNode.g + neighbour.getCost(currentNode);
                let beenVisited = neighbour.visited;

                if (!beenVisited || gScore < neighbour.g) {
                    // Found an optimal (so far) path to this node.  Take score for node to see how good it is.
                    neighbour.visited = true;
                    neighbour.parent = currentNode;
                    neighbour.h = neighbour.h || heuristic(neighbour, end);
                    neighbour.g = gScore;
                    neighbour.f = neighbour.g + neighbour.h;
                    this.markDirty(neighbour);
                    if (closest) {
                        // If the neighbour is closer than the current closestNode or if it's equally close but has
                        // a cheaper path than the current closest node then it becomes the closest node
                        if (neighbour.h < closestNode.h || (neighbour.h === closestNode.h && neighbour.g < closestNode.g)) {
                            closestNode = neighbour;
                        }
                    }

                    if (!beenVisited) {
                        // Pushing to heap will put it in proper place based on the 'f' value.
                        openHeap.push(neighbour);
                    }
                    else {
                        // Already seen the node, but since it has been rescored we need to reorder it in the heap
                        openHeap.rescoreElement(neighbour);
                    }
                }
            });
        }

        if (closest) {
            return this.pathTo(closestNode);
        }

        // No result was found - empty array signifies failure to find path.
        return [];
    }


    /**
     * Return the valid neighbouring fields of the given point as list of WeightedPoint's
     */
    getNeighbours(point, getNeighboursFunction) {
        return getNeighboursFunction(point)
            .filter(candidate => {
                // Remove duplicates, using a lookup list `this.possiblePathPoints`
                if(candidate.inList(this.possiblePathPoints)) {
                    return false;
                } else {
                    this.possiblePathPoints.push(candidate);
                    return true;
                }
            })
            .map(newNeighbour => {
                // Replace each Point with a corresponding WeightedPoint
                return new WeightedPoint(newNeighbour.x, newNeighbour.y);
            });
    }

    pathTo(node){
        var curr = node,
            path = [];
        while(curr.parent) {
            path.unshift(curr);
            curr = curr.parent;
        }
        return path;
    }

    getHeap() {
        return new BinaryHeap(function(node) {
            return node.f;
        });
    }

    cleanDirty() {
        for (let i = this.dirtyNodes.length - 1; i >= 0; i--) {
            this.cleanNode(this.dirtyNodes[i]);
        }
        this.dirtyNodes = [];
    }

    markDirty(node) {
        this.dirtyNodes.push(node);
    }

    cleanNode(point){
        point.f = 0;
        point.g = 0;
        point.h = 0;
        point.visited = false;
        point.closed = false;
        point.parent = null;
        return point;
    }

    targetPointValuesTemporaryPassable(target, callback) {
        let nonPassableTmp = [];
        this.matrix.getValues(target).forEach(value => {
            if(value.passable == false) {
                value.passable = true;
                nonPassableTmp.push(value);
            }
        });

        callback();

        nonPassableTmp.forEach(npt => npt.passable = false);
    }
}
