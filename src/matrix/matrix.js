import { getRandomInt } from "../helper/helper.js";
import { Point } from "../point.js";
import { Space, Marker } from "./matrix-object.js";

export * from "./matrix-object.js";
export * from "./matrix-object-behavior.js";
export * from "./terrain.js";

/**
 * A 2D matrix to be used as data storage
 *
 *  - matrix: a 2D array containing rows which contain columns which contain fields with values
 *  - field: a point in the matrix designated by X & Y coordinates
 *  - field value: any field in the matrix contains a list of objects, which are currently placed on this field
 *  - cursor: the active point in the matrix, can be controlled with keyboard (e.g. the player position)
 *  - pointer: the current mouse (or other pointer device) position
 */
export class Matrix {

    constructor(cols, rows, fillConstructor = Space, fillDistinct = false) {
        this.cols = cols;
        this.rows = rows;
        this.matrix = Matrix.generateEmptyMatrix(cols, rows, fillConstructor, fillDistinct);
    }

    /**
     * Export static matrix data, where each matrix object is replaced by the name of its constructor class.
     * This is meant to statically save an existing Matrix.
     * 
     * @return {Array}
     */
    export() {
        let staticMatrix = [];
        this.matrix.forEach((column, y) => {
            staticMatrix[y] = [];
            column.forEach((field, x) => {
                staticMatrix[y][x] = [];
                field.forEach((matrixObject, i) => {
                    staticMatrix[y][x][i] = matrixObject.constructor.name;
                });
            });
        });

        return staticMatrix;
    }

    /**
     * Pretty print the data from this.export() as nicely indented JSON
     */
    toString() {
        return JSON.stringify(this.export(), null, 2)
            .replaceAll("[\n      ", "[")
            .replaceAll("\n    ]", "]")
            .replaceAll(",\n    ", ",")
            .replaceAll("[\n    ", "[")
            .replaceAll(",\n  [", ",\n  [")
            .replaceAll("\n  ]", "]")
            ;
    }

    /**
     * Generate a 2D array with the given rows & cols.
     * 
     * @param {Number} cols
     * @param {Number} rows
     * @param {MatrixObject.constructor} fillConstructor
     * @param {Boolean} fillDistinct By default each field is filled with the same object by reference;
     *                               Set to `true` for a new object instance on each field;
     * @return {Array}
     * @nosideeffects
     */
    static generateEmptyMatrix(cols, rows, fillConstructor, fillDistinct = false) {
        if(cols < 1) {
            cols = 1;
        }
        if(rows < 1) {
            rows = 1;
        }

        // Create nested array with Array.from (instead of Array.fill) to create distinct
        // value arrays for each field, which are not linked via reference to each other.
        if(fillDistinct == true) {
            // A new object instance for each field
            return Array.from({length: rows}, el => Array.from({length: cols}, el => [new fillConstructor()]));
        } else {
            // The same space object is reused via reference on each field
            let fill = new fillConstructor();
            return Array.from({length: rows}, el => Array.from({length: cols}, el => [fill]));
        }
    }


    /* ********** Coordinates / Points ********** */


    /**
     * Check if the given point is still inside the matrix
     *
     * @param {Point} point
     * @return {Boolean}
     */
    isValidPoint(point) {
        if(point.y < 0 || point.x < 0
            || point.y > this.rows - 1 || point.x > this.cols - 1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Can you walk on this point in the matrix?
     * 
     * @param {Point} point
     * @nosideeffects
     */
    isPassable(point) {
        let isPassable = true;
        for(let i = this.matrix[point.y][point.x].length - 1; i >= 0; i--) {
            if(this.matrix[point.y][point.x][i].passable == false) {
                isPassable = false;
                break;
            }
        }
        return isPassable;
    }

    /**
     * Return the neighbouring points of the given point checking the if these points are valid and passable.
     * By default looks at the four vertical and horizontal neighbours only.
     * 
     * @param {Point} point
     * @param {Boolean} onlyPassable Set to FALSE to return all neighbours, no matter if they are passable
     * @param {Boolean} diagonal Set to TRUE to also find diagonal neighbours (up to eight)
     */
    getNeighbours(point, onlyPassable = true, diagonal = false) {
        return point.getNeighbours(diagonal).filter(neighbour => {
            return this.isValidPoint(neighbour) && (!onlyPassable || this.isPassable(neighbour));
        });
    }


    /* ********** Basic CRUD opertions for matrix points/fields ********** */


    /**
     * Set one ore more MatrixObjects to a point in the matrix.
     * Overwrites existing content of that field.
     * 
     * @param {Point} point
     * @param {MatrixObject} values One ore more MatrixObjects as single arguments
     * @return {Boolean} Returns `false` if point is not within matrix
     */
    setValue(point, ...values) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        this.matrix[point.y][point.x] = values;
        return true;
    }

    /**
     * Return latest added object of a certain point and maybe a given type too.
     * 
     * @param {Point} point
     * @param {null|string|Function} type A constructor or class name to indicate the the MatrixObject type to return
     * @return {MatrixObject|false} Returns `false` if point is not within matrix or no object (of type) in field 
     */
    getValue(point, type = null) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        if(this.matrix[point.y][point.x].length == 0) {
            return false;
        }

        if(type === null) {
            return this.matrix[point.y][point.x][this.matrix[point.y][point.x].length - 1];
        } else {
            for(let i = this.matrix[point.y][point.x].length - 1; i >= 0; i--) {
                if(this.matrix[point.y][point.x][i] instanceof type) {
                    return this.matrix[point.y][point.x][i];
                }
            }
            return false;
        }
    }

    /**
     * Return all objects of a certain point.
     * 
     * @param {Point} point
     * @param {null|string|Function} type An object or constructor or class name to indicate the the MatrixObject type to return
     * @return {MatrixObject[]|false} Returns `false` if point is not within matrix; Empty array when no object (of type) in field 
     */
    getValues(point, type = null) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        if(this.matrix[point.y][point.x].length == 0) {
            return [];
        }

        if(type === null) {
            return this.matrix[point.y][point.x];
        } else {
            let values = [];
            for(let i = this.matrix[point.y][point.x].length - 1; i >= 0; i--) {
                if(this.matrix[point.y][point.x][i] instanceof type) {
                    values.push(this.matrix[point.y][point.x][i]);
                }
            }
            return values.length > 0 ? values : false;
        }
    }

    /**
     * Add an object by appending to a matrix field.
     * Keeps existing content of that field.
     * 
     * @param {Point} point
     * @param {MatrixObject} value
     * @return {Boolean} Returns `false` if point is not within matrix
     */
    addValue(point, value) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        this.matrix[point.y][point.x].push(value);
        return true;
    }

    /**
     * Remove top-most object of a given type from a field and return that object (or FALSE if not found).
     * 
     * @param {Point} point
     * @param {string|Function} type
     * @return {MatrixObject|false} Returns `false` if point is not within matrix
     */
    removeValue(point, type) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        let removedObject = false;

        for(let i = this.matrix[point.y][point.x].length - 1; i >= 0; i--) {
            if(this.matrix[point.y][point.x][i] instanceof type) {
                removedObject = this.matrix[point.y][point.x][i];
                // remove this item from the array
                this.matrix[point.y][point.x].splice(i, 1);
                break;
            }
        }

        return removedObject;
    }

    /**
     * Remove specific instance from a field and return that object (or FALSE if not found).
     * 
     * @param {Point} point
     * @param {MatrixObject} value
     * @return {MatrixObject|false} Returns `false` if point is not within matrix
     */
    removeValueEqualTo(point, value) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        let removedObject = false;

        for(let i = this.matrix[point.y][point.x].length - 1; i >= 0; i--) {
            if(this.matrix[point.y][point.x][i] === value) {
                removedObject = this.matrix[point.y][point.x][i];
                // remove this item from the array
                this.matrix[point.y][point.x].splice(i, 1);
                break;
            }
        }

        return removedObject;
    }

    /**
     * Remove all objects of a given type from a field and return those objects (or FALSE if not found).
     * 
     * @param {Point} point
     * @param {string|Function} type
     * @return {MatrixObject[]|Boolean} Returns `false` if point is not within matrix
     */
    removeValues(point, type) {
        if(!this.isValidPoint(point)) {
            return false;
        }

        let removedObjects = [];

        for(let i = this.matrix[point.y][point.x].length - 1; i >= 0; i--) {
            if(this.matrix[point.y][point.x][i] instanceof type) {
                removedObjects.push(this.matrix[point.y][point.x][i]);
                // remove this item from the array
                this.matrix[point.y][point.x].splice(i, 1);
            }
        }

        return removedObjects.length > 0 ? removedObjects : false;
    }

    /**
     * @param {Point} point
     * @param {string|Function} type
     */
    hasValue(point, type) {
        let valueObject = this.getValue(point, type);
        return valueObject === false ? false : true;
    }

    /**
     * Find specific values / óbjects from a field using a callback
     * 
     * Examples:
     *
     *  // Find all movable objects on a given point
     *  matrix.findValue(point, mo => mo.movable == true);
     *
     *  // Find all non-passable objects on a given point
     *  matrix.findValue(point, mo => mo.passable == false);
     *
     * @param {Point} point
     * @param {Function} callback
     * @return {MatrixObject[]|false} 
     */
    findValue(point, callback) {
        let candiates = this.getValues(point);
        if(candiates === false) {
            return false;
        }

        let result = candiates.filter(callback);
        return result.length > 0 ? result : false;
    }

    /**
     * Move one object matching the given type from `point` to `newPoint`.
     * 
     * @param {Point} point
     * @param {Point} newPoint
     * @param {string|MatrixObject} type
     * @return {Boolean} Returns `false` if `point` had no objects of `type`
     */
    moveValue(point, newPoint, type) {
        let valueObject = this.removeValue(point, type);
        if(valueObject == false) {
            return false;
        }

        this.addValue(newPoint, valueObject);
        return true;
    }

    /**
     * Move all objects matching the given type from `point` to `newPoint`.
     * 
     * @param {Point} point
     * @param {Point} newPoint
     * @param {string|MatrixObject} type
     * @return {Boolean} Returns `false` if `point` had no objects of `type`
     */
    moveValues(point, newPoint, type) {
        let valueObjects = this.removeValues(point, type);
        if(valueObjects == false) {
            return false;
        }

        valueObjects.forEach(valueObject => {
            this.addValue(newPoint, valueObject);
        });
        return true;
    }


    /* ********** Find points in Matrix (helper methods) ********** */


    /**
     * @return {Point} Definitly inside the matrix
     */
    getRandomPoint() {
        return new Point(getRandomInt(0, this.cols - 1), getRandomInt(0, this.rows - 1));
    }

    /**
     * Find a Point using a callback.
     *
     * @param {Function} callback Retrieves a MatrixObject as single argument and should return `true` if a point with such an object is favorable
     * @return {Point|false} Returns `false` if the callback could not be satisfied on any point in the matrix
     */
    findRandomPoint(callback) {
        let candidates = [];

        this.forEach((point, values) => {
            values.forEach(value => {
                if(callback(value)) {
                    candidates.push(point);
                }
            });
        });

        if(candidates.length == 0) {
            return false;
        } else {
            return candidates[getRandomInt(0, candidates.length - 1)]
        }
    }

    /**
     *
     * @param {string|MatrixObject} type
     * @param {Number} Maximum repetation to try and find a random point with the given type
     * @return {Point|false} Returns `false` if no point of `type` could be found after `maxTries`
     */
    findRandomTypePoint(type, maxTries = 500) {
        let randomPoint = false;
        let terrainObject;

        // TODO maybe re-implement to 1.) search for all fields with an object of type
        // and 2.) choose a random one from that result
        do {
            randomPoint = new Point(getRandomInt(0, this.cols - 1), getRandomInt(0, this.rows - 1));
            terrainObject = this.getValue(randomPoint, type);
            maxTries--;
        } while(maxTries > 0 && terrainObject === false);

        return randomPoint;
    }

    /**
     * Get neighbours that match a given MatrixObject type
     * 
     * @param {Point} point
     * @param {string|MatrixObject} type
     * @param {Boolean} onlyPassable Set to FALSE to return all neighbours, no matter if they are passable
     * @param {Boolean} diagonal Set to TRUE to also find diagonal neighbours (up to eight)
     * @return {Point[]} Returns empty list of none are found
     */
    getNeighboursWithType(point, type, onlyPassable = true, diagonal = false) {
        let neighbours = this.getNeighbours(point, onlyPassable, diagonal);

        return neighbours.filter(neighbourPoint => this.hasValue(neighbourPoint, type));
    }

    /**
     * Get neighbours that do not match a given MatrixObject type
     * 
     * @param {Point} point
     * @param {string|MatrixObject} type
     * @param {Boolean} onlyPassable Set to FALSE to return all neighbours, no matter if they are passable
     * @param {Boolean} diagonal Set to TRUE to also find diagonal neighbours (up to eight)
     * @return {Point[]} Returns empty list of none are found
     */
    getNeighboursWithoutType(point, type, onlyPassable = true, diagonal = false) {
        let neighbours = this.getNeighbours(point, onlyPassable, diagonal);

        return neighbours.filter(neighbourPoint => !this.hasValue(neighbourPoint, type));
    }

    /**
     * Find a point with a MatrixObject of given type and return that point,
     * 
     * This method works recursively up to the canvas edges and
     * increases `radius` by one on each iteration.
     *
     * @param {Point} point
     * @param {string|MatrixObject} type
     * @param {Number} radius The start radius
     * @return {MatrixObject|false}
     */
    findNearestWithType(point, type, radius = 1, useFilledCircle = false, maxRadius = false, filterCallback = false) {
        // Find points on orbit around given point and filter out
        // invalid objects – those that are one of either:
        //  - outside the matrix
        //  - are non-passable
        //  - are not matching the given type
        let foundTypeObjects = point.midpointCircle(radius, useFilledCircle).filter(cp => {
            if(!this.isValidPoint(cp) || !this.isPassable(cp)) {
                return false;
            }

            let cpObject = this.getValue(cp, type);
            return cpObject !== false;
        });

        if(filterCallback != false) {
            foundTypeObjects = foundTypeObjects.filter(filterCallback);
        }

        let nearestTypePoint;
        if(foundTypeObjects.length > 0) {

            // Found what we were looking for, return a random tile from the serach results
            return foundTypeObjects[getRandomInt(0, foundTypeObjects.length - 1)];

        } else {
            // Recurse and increase radius each time

            if(!maxRadius) {
                // If no maximum radius was given, the distance to the farthest canvas corner is the `maxRadius`.
                let farthestCorner = point.farthest([
                    new Point(0, 0),
                    new Point(this.cols - 1, 0),
                    new Point(this.cols - 1, this.rows - 1),
                    new Point(0, this.rows - 1),
                ]);
                maxRadius = point.distanceTo(farthestCorner);
            }

            if(radius + 1 > maxRadius) {
                return false;
            } else {
                return this.findNearestWithType(point, type, radius + 1, useFilledCircle, filterCallback);
            }
        }
    }

    findNearestWithTypeBAK(point, type, radius = 1) {
        let stepSize = 3 * radius;

        // Calculate points on orbit around given point
        let circlePoints = point.getOrbitPoints(360 / stepSize, radius, radius).map(orbitPoint => {
            // Round orbit point
            orbitPoint.x = Math.round(orbitPoint.x);
            orbitPoint.y = Math.round(orbitPoint.y);
            return orbitPoint;
        });

        // Filter out invalid objects – those that are one of either:
        //  - outside the matrix
        //  - are non-passable
        //  - are not matching the given type
        let foundTypeObjects = circlePoints.filter(cp => {
            if(!this.isValidPoint(cp) || !this.isPassable(cp)) {
                return false;
            }

            let cpObject = this.getValue(cp, type);
            return cpObject !== false;
        });

        let nearestTypePoint;
        if(foundTypeObjects.length > 0) {

            // Found what we were looking for, return a random tile from the serach results
            return foundTypeObjects[getRandomInt(0, foundTypeObjects.length - 1)];

        } else {

            // Recurse and increase radius each time
            let farthestCorner = point.farthest([
                new Point(0, 0),
                new Point(this.cols - 1, 0),
                new Point(this.cols - 1, this.rows - 1),
                new Point(0, this.rows - 1),
            ]);
            let maxRadius = point.distanceTo(farthestCorner);

            if(radius + 1 > maxRadius) {
                return false;
            } else {
                return this.findNearestWithType(point, type, radius + 1);
            }
        }
    }


    /* ********** Iterate over Matrix Points ********** */


    /**
     * Iterate over all fields in the matrix and pass point and list of MatrixObjects to callback
     * 
     * Example:
     *
     *  this.matrix.forEach((point, values) => {
     *      console.log(
     *          point,      // The current Point
     *          values,     // An array of MatrixObjects on the current point/field
     *      );
     *  });
     * 
     * @param {Function} callback
     */
    forEach(callback) {
        this.matrix.forEach((row, y) => {
            row.forEach((field, x) => {
                let currentPoint = new Point(x, y);
                callback(currentPoint, this.getValues(currentPoint));
            });
        });
    }

    /**
     * Iterable protocol
     * 
     * Iterate over all fields in the matrix and return values (i.e. array of MatrixObjects)
     * 
     * Example:
     * 
     *  for(let fieldValues of matrixInstance) {
     *      console.log(fieldValues); // An array of MatrixObjects for the current point
     *  }
     */
    *[Symbol.iterator]() {
        for(let y in this.matrix) {
            for(let x in this.matrix[y]) {
                yield this.matrix[y][x];
            }
        }
    }

    coarse(useEvery = 2) {
        let newMatrix = [];
        let newMatrixCols = Math.floor(this.cols / useEvery);
        let newMatrixRows = Math.floor(this.rows / useEvery);

        for(let y = 0; y < newMatrixRows; y++) {
            if(!newMatrix[y]) {
                newMatrix[y] = [];
            }
            for(let x = 0; x < newMatrixCols; x++) {
                newMatrix[y][x] = this.matrix[y * useEvery][x * useEvery];
            }
        }

        this.matrix = newMatrix;
        this.cols = newMatrixCols;
        this.rows = newMatrixRows;
    }
}
