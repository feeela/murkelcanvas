import { Renderer } from "../../render/renderer.js";
import { getMineTile } from "../pattern.js";
import * as GameObjects from "./minesweeper-game-objects.js";

/**
 * @var {2DRenderContext} 
 */
export function getRenderer(canvasObject) {
 	let renderer = new Renderer(canvasObject);

    const icons = {};

    renderer.register(GameObjects.Mine.name, (context, x, y, width, height) => {
        if(!icons.mine) {
            icons.mine = getMineTile(width, height);
        }
        let scaleX = width / 8;
        let scaleY = height / 8;
        context.drawImage(icons.mine, x + scaleX, y + scaleY, width - scaleX * 2, height - scaleY * 2);
    });

    renderer.register(GameObjects.Cover.name, (context, x, y, width, height, coverObject) => {
        if(coverObject.isOpen == true) {
            if(coverObject.mineCount > 0) {
                context.font = `${height / 8 * 4}px sans-serif`;
                context.textAlign = "center";
                context.textBaseline = "middle";
                context.fillStyle = "#333";
                context.fillText(coverObject.mineCount, x + width / 2, y + height / 1.8);
            }
        } else {
            let border = width / 15;
            context.fillStyle = "#a4aaa4";
            context.fillRect(x, y, width, height);

            context.lineWidth = border;
            context.lineJoin = "";
            let offset = border / 2;

            context.beginPath();
            context.strokeStyle = "#ccc";
            context.moveTo(x + offset, y + height - offset);
            context.lineTo(x + offset, y + offset);
            context.lineTo(x + width - offset, y + offset);
            context.stroke();

            context.beginPath();
            context.strokeStyle = "#999";
            context.moveTo(x + width - offset, y + offset);
            context.lineTo(x + width - offset, y + height - offset);
            context.lineTo(x + offset, y + height - offset);
            context.stroke();

            if(coverObject.isMarked == true) {
                context.beginPath();
                context.strokeStyle = "#666";
                context.moveTo(x + width / 2, y + height / 4);
                context.lineTo(x + width / 2, y + height / 4 * 3);
                context.stroke();

                context.beginPath();
                context.fillStyle = "#090";
                context.moveTo(x + width / 2 - offset, y + height / 4);
                context.lineTo(x + width - width / 4, y + height / 4 * 1.5);
                context.lineTo(x + width / 2 - offset, y + height / 4 * 2);
                context.fill();
            }
        }
    });

    return renderer;
}
