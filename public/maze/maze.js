import { Point } from '../js/lib-point.js';
import { shuffle } from "../js/helper.js"
import { MatrixObject, Cursor } from '../js/matrix.js';
import { MatrixCanvas } from "../js/matrix-canvas.js";
import { MazeBuilder } from "./maze-builder.js";

export class Exit extends MatrixObject {
    constructor() {
        super();
    }
}

/**
 * Example usage for Matrix & MazeBuilder
 */
export class Maze extends MatrixCanvas {
    constructor(canvas, options = {}) {
        super(canvas, options);

        // generate maze
        let mazeBuilder = new MazeBuilder(this.matrix, options);

        let cornerTiles = shuffle([
            new Point(1, 1),
            new Point(this.cols - 2, 1),
            new Point(1, this.rows - 2),
            new Point(this.cols - 2, this.rows - 2),
        ]);

        this.cursor = cornerTiles[0];
        // Add cursor object to that field
        // (The original cursor from Matrix has been overwritten while generating the maze.)
        this.matrix.addValue(this.cursor, new Cursor());

        // Add exit
        this.matrix.addValue(cornerTiles[1], new Exit());
    }

    resetRenderer() {
        super.resetRenderer();

        // Extend renderer
        this.renderer.register(Exit.name, (context, x, y, width, height) => {
            context.fillStyle = "#05c";
            context.fillRect(x + (width / 10), y + (height / 10), width / 10 * 8, height / 10 * 8);
        });
    }
}
