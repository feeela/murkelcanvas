import { Direction } from "./constants.js";

/**
 * Data-structure for a point in a two dimensional space.
 * 
 * Has `x` and `y` and some helper methods.
 */
export class Point {

    /**
     * @param {Number} x The X coordinate (usually horizontal from left to right)
     * @param {Number} y The Y coordinate (usually vertically from top to bottom)
     */
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    /**
     * Test if the given point has the same coordinates as `this`.
     * 
     * @param {Point} point
     * @return {Boolean}
     */
    equals(point) {
        return this.x == point.x && this.y == point.y; 
    }

    /**
     * Returns the Euclidean (Pythagorean) distance between `this` and a given point.
     * 
     * @param {Point} point
     * @return {Number}
     */
    distanceTo(point) {
        return Math.sqrt( Math.pow(this.x - point.x, 2) + Math.pow(this.y - point.y, 2) );
    }

    /**
     * @param {Point} point
     * @return {Number} The angle between the two points in degree
     */
    angleTo(point) {
        let angle = Math.atan2(point.y - this.y, point.x - this.x) * 180 / Math.PI;
        if(angle < 0) {
            angle = 360 + angle;
        }
        return angle;
    }

    /**
     * Find the midpoint between `this` and another point.
     *
     * @param {Point} point
     * @return {Point}
     */
    midpoint(point) {
        return new Point(
            this.x + (point.x - this.x) / 2,
            this.y + (point.y - this.y) / 2
        );
    }

    /**
     * Compute the dot product
     *
     * @param {Point} point
     * @return {Number}
     */
    dotProduct(point) {
        return this.x * point.x + this.y * point.y;
    }

    /**
     * Return the neighbouring points of the given point with the given distance.
     * 
     * 
     * @param {Boolean} diagonal Set to `false` to ignore diagonal neighbours
     * @param {Number} distance The distance to neighbouring points
     * @return {Point[]} Returns 8 (diagonal = true) or 4 (diagonal = false) points around `this` 
     */
    getNeighbours(diagonal = true, distance = 1) {
        let neighbours;
        if(diagonal === true) {
            // Look at all eight neighbour fields
            neighbours = [
                new Point(this.x, this.y - distance),
                new Point(this.x + distance, this.y - distance),
                new Point(this.x + distance, this.y),
                new Point(this.x + distance, this.y + distance),
                new Point(this.x, this.y + distance),
                new Point(this.x - distance, this.y + distance),
                new Point(this.x - distance, this.y),
                new Point(this.x - distance, this.y - distance),
            ];
        } else {
            // Only look at vertical and horizontal neighbours
            neighbours = [
                new Point(this.x - distance, this.y),
                new Point(this.x + distance, this.y),
                new Point(this.x, this.y - distance),
                new Point(this.x, this.y + distance),
            ];
        }

        return neighbours;
    }

    /**
     * Compute a point in an orbit around `this` with an angle in degree.
     * 
     * If `orbitRadiusX` and `orbitRadiusY` differ, the returned points is on an ellipsis around `this`.
     * 
     * @param {Number} degree
     * @param {Number} orbitRadiusX
     * @param {Number} orbitRadiusY
     * @param {Number} orbitSpeed A fraction of PI
     */
    getOrbitPoint(degree, orbitRadiusX = 10, orbitRadiusY = 10, orbitSpeed = Math.PI / 180) {
        let radian = orbitSpeed * degree;

        return new Point(
            this.x + orbitRadiusX * Math.cos(radian),
            this.y + orbitRadiusY * Math.sin(radian)
        );
    }

    /**
     * Return a list of orbit points.
     * 
     * If `orbitRadiusX` and `orbitRadiusY` differ, the returned points is on an ellipsis around `this`.
     * 
     * @param {Number} degree
     * @param {Number} orbitRadiusX
     * @param {Number} orbitRadiusY
     * @param {Number} orbitSpeed A fraction of PI
     */
    getOrbitPoints(stepSize = 1, orbitRadiusX = 1, orbitRadiusY = 1, orbitSpeed = Math.PI / 180) {
        let circlePoints = [];
        for(let degree = 0; degree < 360; degree = degree + stepSize) {
            circlePoints.push(this.getOrbitPoint(degree, orbitRadiusX, orbitRadiusY, orbitSpeed));
        }
        return circlePoints;
    }

    /**
     * Integer points on a circle using the midpoint circle algorithm
     * 
     * see: https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
     */
    midpointCircle(radius, fill = false) {
        let points = [];

        let determinant = (5 - radius * 4) / 4;
        let x = 0;
        let y = radius;

        const pushTopAndBottomPoints = (top, bottom) => {
            if(fill == true) {
                for(let y = top.y; y <= bottom.y; y++) {
                    let candiate = new Point(top.x, y);
                    if(!candiate.inList(points)) {
                        points.push(candiate);
                    }
                }
            } else {
                if(!top.inList(points)) {
                    points.push(top);
                }
                if(!bottom.inList(points)) {
                    points.push(bottom);
                }
            }
        }

        do {
            pushTopAndBottomPoints(
                new Point(this.x + x, this.y - y),  // top right
                new Point(this.x + x, this.y + y)   // bottom right
            );

            pushTopAndBottomPoints(
                new Point(this.x - x, this.y - y),  // top left
                new Point(this.x - x, this.y + y)   // bottom left
            );

            pushTopAndBottomPoints(
                new Point(this.x - y, this.y - x),  // left top
                new Point(this.x - y, this.y + x)   // left bottom
            );

            pushTopAndBottomPoints(
                new Point(this.x + y, this.y - x),  // right top
                new Point(this.x + y, this.y + x)   // right bottom
            );

            if (determinant < 0) {
                determinant += 2 * x + 1;
            } else {
                determinant += 2 * (x - y) + 1;
                y--;
            }
            x++;
        } while (x <= y);

        return points;
    }

    /**
     * @param {Points[]} points A list of points
     * @return {Point} Returns the Point from the input list that is closest to `this`
     */
    closest(points) {
        let closest;
        let minX = Number.MAX_SAFE_INTEGER;
        let minY = Number.MAX_SAFE_INTEGER;

        points.forEach(cp => {
            let distX = Math.abs(this.x - cp.x);
            let distY = Math.abs(this.y - cp.y);

            if(distX < minX && distY < minY) {
                minX = distX;
                minY = distY;
                closest = cp;
            }
        });

        return closest;
    }

    /**
     * @param {Points[]} points A list of points
     * @return {Point} Returns the Point from the input list that is closest to `this`
     */
    farthest(points) {
        let farthest;
        let maxX = 0;
        let maxY = 0;

        points.forEach(cp => {
            let distX = Math.abs(this.x - cp.x);
            let distY = Math.abs(this.y - cp.y);

            if(distX > maxX && distY > maxY) {
                maxX = distX;
                maxY = distY;
                farthest = cp;
            }
        });

        return farthest;
    }

    /**
     * Move the coordinates into "direction" and return a Point instance.
     *
     * @param {Direction} direction
     * @param {Number} amount The amout to add or substract from coordinates
     * @nosideeffects
     */
    move(direction, amount = 1) {
        let point = new Point(this.x, this.y);
        switch(direction) {
            case Direction.UP:
                point.y = this.y - amount;
                break;
            case Direction.RIGHT:
                point.x = this.x + amount;
                break;
            case Direction.DOWN:
                point.y = this.y + amount;
                break;
            case Direction.LEFT:
                point.x = this.x - amount;
                break;
        }
        return point;
    }

    /**
     * JSON representation of the coordinates
     * 
     * @return {string}
     */
    toString() {
        return JSON.stringify({ x: this.x, y: this.y });
    }

    /**
     * @param {Point[]} pointsToCheck
     * @return {Boolean}
     */
    inList(pointsToCheck) {
        return typeof pointsToCheck.find(checkPoint => {
            return this.equals(checkPoint);
        }) != "undefined";
    }

    sortListByDistance(pointsToSort) {
        return [...pointsToSort].sort((a, b) => {
            let aDistance = this.distanceTo(a);
            let bDistance = this.distanceTo(b);

            if (aDistance < bDistance) {
                return -1;
            } else if (aDistance > bDistance) {
                return 1;
            }
            // a must be equal to b
            return 0;
        });
    }

    /**
     * Remove all points from list A that are included in list B.
     * 
     * @param {Point[]} pointsToCheck
     * @param {Point[]} pointsToRemove
     * @return {Point[]}
     */
    static withoutDuplicates(pointsToCheck, pointsToRemove) {
        return pointsToCheck.filter(checkPoint => {
            return typeof pointsToRemove.find(someOriginal => {
                return someOriginal.equals(checkPoint);
            }) === "undefined";
        });
    }
}
