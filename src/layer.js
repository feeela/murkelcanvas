/**
 * Layers – like in Potoshop, but for a Canvas
 */

import { Specificity } from "./constants.js";


/**
 * A single layer in a graphic, that has a specificity and a list of callback functions,
 * which should actually draw something onto the canvas.
 */
export class Layer {
    /**
     * @var {Number} specificity Lower numbers are in background, higher numbers are in foreground
     */
    constructor(specificity, isStatic = false) {
        this.specificity = specificity;
        this.static = isStatic;
        this.contents = [];
    }

    addContent(drawCallback) {
        this.contents.push(drawCallback);
    }

    reset() {
        this.contents = [];
    }
}

/**
 * A sorted stack of Layer objects.
 */
export class Layers {

    static STATIC = 1;
    static NON_STATIC = 2;

    constructor() {

        /* 
         * Numeric indizes are sorted by default in JS objects (no matter when added):
         * Automagically sorted layers!
         *
         * let layers = {0:"bg", 50:"default", 100:"fg"};
         * layers[75] = "player";
         *
         * -> {"0":"bg", "50":"default", "75":"player", "100":"fg"}
         *
         * See: https://exploringjs.com/es6/ch_oop-besides-classes.html#_traversal-order-of-properties
         */
        this.layers = {};
    }

    addLayer(specificity, isStatic = false) {
        this.layers[specificity] = new Layer(specificity, isStatic);

        return this.layers[specificity];
    }

    /**
     * Return the Layer for the given specificity; creates one if not existing.
     * 
     * @var {Number} specificity Layers are identified by their specificity
     */
    layer(specificity = Specificity.DEFAULT) {
        let layer;
        if(!this.layers[specificity]) {
            layer = this.addLayer(specificity);
        } else {
            layer = this.layers[specificity];
        }
        return layer;
    }

    each(callback, retStat = Layers.STATIC | Layers.NON_STATIC) {
        for(let specificity in this.layers) {
            if((retStat == Layers.STATIC && this.layers[specificity].static == true)
                || (retStat == Layers.NON_STATIC && this.layers[specificity].static == false)) {
                callback(this.layers[specificity]);
            } else if(retStat == (Layers.STATIC | Layers.NON_STATIC)) {
                callback(this.layers[specificity]);
            }
        }
    }

    reset() {
        for(let specificity in this.layers) {
            this.layers[specificity].reset();
        }
    }
}
