import { extend, getRandomInt } from "../../helper/helper.js";
import { Point } from "../../point.js";
import { Text, TextBlock } from "../../text.js"
import { MatrixCanvas } from "../../matrix-canvas.js";
import { GameStatus } from "../../game/event/game-events.js";
import { Direction, Specificity } from "../../constants.js";
import {
    SnakeHead,
    SnakeBody,
    SnakeTail,
    Apple,
} from "./snake-game-objects.js";
import { getRendererFunction } from "./snake-renderer.js";

// for use in demo/pattern.htm only
export * from "./snake-pattern.js";

/**
 * A snake game implementation using the Matrix as base.
 */
export class Snake extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        // The paintSpeed will be reduced by one for each eaten apple
        paintSpeed: 200,
        paintSpeedDecrease: 4,
        cursor: false,
        initialLength: 4,
    });

    static colors = ["aqua", "chocolate", "cornflowerblue", "crimson", "darkcyan", "darkgreen",
        "darkmagenta", "darkolivegreen", "deepskyblue", "forestgreen", "goldenrod", "indianred", "mediumseagreen",
        "olivedrab", "orangered", "palevioletred", "saddlebrown", "seagreen", "sienna", "teal", "tomato", "yellowgreen"];

    constructor(canvas, options = {}, autoRun = false) {
        // Set snake color
        //let color = options.color || Snake.colors[getRandomInt(0, Snake.colors.length - 1)];
        let color = options.color || "forestgreen";
        options.getRenderer = getRendererFunction(color);

        super(canvas, options, autoRun);

        // Setup snake head
        this.cursor = new Point(
            getRandomInt(0, this.cols - 1),
            getRandomInt(Math.floor(this.rows / 2), this.rows - this.options.initialLength - 1)
        );
        this.head = new SnakeHead(Direction.UP);
        this.matrix.addValue(this.cursor, this.head);

        // Create initial points for snake body/tail parts
        this.parts = []
        for(let i = 1; i <= this.options.initialLength - 1; i++) {
            this.parts.push(new Point(this.cursor.x, this.cursor.y + i));
        }

        // Add snake MatrixObjects to the matrix; last part differs as it is the tail
        this.parts.forEach((partPos, index) => {
            if(index < this.parts.length - 1) {
                let newBodyPart = new SnakeBody(Direction.UP, Direction.DOWN);
                newBodyPart.direction = Direction.UP;
                this.matrix.addValue(partPos, newBodyPart);
            } else {
                this.tail = new SnakeTail(Direction.DOWN);
                this.matrix.addValue(partPos, this.tail);
            }
        });

        this.applesCollected = 0;
        this.placeApple();

        this.direction = Direction.UP;
        this.status = GameStatus.statusStart;
        this.isPaused = true;
    }

    /**
     * Overloaded method to prevent going into opposite directions
     */
    onKeyPress(event) {
        switch(event.key) {
            case "ArrowUp":
                if(this.direction != Direction.DOWN) {
                    this.direction = Direction.UP;
                }
                break;
            case "ArrowRight":
                if(this.direction != Direction.LEFT) {
                    this.direction = Direction.RIGHT;
                }
                break;
            case "ArrowDown":
                if(this.direction != Direction.UP) {
                    this.direction = Direction.DOWN;
                }
                break;
            case "ArrowLeft":
                if(this.direction != Direction.RIGHT) {
                    this.direction = Direction.LEFT;
                }
                break;
            default:
                super.onKeyPress(event);
        }
    }

    /**
     * Update direction in SnakeHead object
     */
    moveCursor(direction) {
        // Do not move the cursor of player has lost
        if(this.status == GameStatus.statusLost) {
            return false;
        }

        let previousHeadPos = this.cursor;
        let previousHeadDirection = this.head.direction;

        if(!super.moveCursor(direction)) {
            this.status = GameStatus.statusLost;
            let oldHighscore = this.getHighscore();
            if(this.applesCollected > oldHighscore) {
                this.setHighscore(this.applesCollected);
            }
            return false;
        }

        // Set current direction to player object
        this.head.direction = direction;

        // Move body by prepending a new SnakeBody the the `previousHeadPos`
        // and by removing the last point and move the tail object to he second-last point;
        let newBodyPart = new SnakeBody(direction, Direction.opposite(previousHeadDirection));
        newBodyPart.direction = previousHeadDirection;
        this.parts.unshift(previousHeadPos);
        this.matrix.addValue(previousHeadPos, newBodyPart);

        // Collect apples, if there are any
        if(this.matrix.hasValue(this.cursor, Apple)) {
            // The player is on a field with an apple; collect it
            this.applesCollected++;
            this.options.paintSpeed = this.options.paintSpeed - this.options.paintSpeedDecrease;

            // Remove this Apple object
            this.matrix.removeValue(this.cursor, Apple);
            this.placeApple();
        } else {
            // Move tail
            let lastPointInList = this.parts.pop();
            let removedBodyPart = this.matrix.removeValue(this.parts[this.parts.length - 1], SnakeBody);
            this.matrix.moveValue(lastPointInList, this.parts[this.parts.length - 1], SnakeTail);
            this.tail.direction = Direction.opposite(removedBodyPart.from);
        }
    }

    content() {
        super.content(false);

        // Reuse text styles
        let textBlockOptions = {
            size: this.height / 35,
            textAlign: "center",
            background: "rgba(222,255,222,.75)",
            padding: this.height / 40,
            borderRadius: this.height / 50,
        };

        // Show number of eaten apples
        this.layers.layer(Specificity.FOREGROUND).addContent(() => {
            let text = new Text(this.context, `${this.applesCollected} apples eaten`, this.gridSizeX / 3, this.gridSizeY / 6, {
                size: textBlockOptions.size,
                background: textBlockOptions.background,
            });
            text.draw();
        });

        // Paused message
        if(this.isPaused == true) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                let msg = new TextBlock(this.context, this.width / 2, this.height / 4, textBlockOptions);

                msg.heading("Snake Game");
                msg.text("");
                msg.text("Eat the apples.");
                msg.text("For each eaten apple the snake gets");
                msg.text("longer and the game speed increases.");
                msg.text("Control with arrow-keys.");
                msg.text("");
                msg.text(`Highscore: ${this.getHighscore()} apples`);
                msg.text("");
                msg.heading("Paused");
                if(!this.isTouchDevice) {
                    msg.text("Press Space to pause / resume");
                }

                msg.draw();
            });
            return;
        }

        switch(this.status) {

            // Level won message
            case GameStatus.statusWon:
            case GameStatus.statusEnd:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You've won!");
                    msg.text("");
                    msg.text("Reload this page to try again.");
                    msg.draw();
                });
                break;

            case GameStatus.statusLost:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You've lost!");
                    msg.text("");
                    msg.text("Reload this page to try again.");
                    msg.draw();
                });
                break;
        }
    }

    placeApple() {
        let newApplePoint;
        do {
            newApplePoint = new Point(getRandomInt(0, this.cols - 1), getRandomInt(0, this.rows - 1));
        } while(newApplePoint.equals(this.cursor) || typeof this.parts.find(part => newApplePoint.equals(part)) !== "undefined");

        this.matrix.addValue(newApplePoint, new Apple());
    }

    setHighscore(highscore) {
        window.localStorage.setItem("snake-highscore", highscore);
    }

    getHighscore() {
        let highscore = window.localStorage.getItem("snake-highscore");
        if(!highscore) {
            highscore = 0;
        }
        return highscore;
    }
}
