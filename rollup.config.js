import del from 'rollup-plugin-delete';
//import copy from 'rollup-plugin-copy';
//import path from 'path';

export default {
  input: {
    // Basics
    helper: 'src/helper/all.js',
    text: 'src/text.js',
    astar: 'src/matrix/astar.js',

    // Main objects
    'canvas-base': 'src/canvas-base.js',
    'murkel-canvas': 'src/murkel-canvas.js',
    matrix: 'src/matrix/matrix.js',
    'matrix-canvas': 'src/matrix-canvas.js',

    // Demos & games
    'catch-me': 'src/game/catch-me.js',
    'turtle': 'src/game/turtle.js',
    'langtons-ant': 'src/game/langtons-ant/langtons-ant.js',
    'conway': 'src/game/conway/conway.js',
    boulder: 'src/game/boulder/boulder.js',
    'boulder-level-editor': 'src/game/boulder/level-editor.js',
    minesweeper: 'src/game/minesweeper/minesweeper.js',
    snake: 'src/game/snake/snake.js',
    settler: 'src/game/settler/game.js',

    // Malkasten
    malkasten: 'src/malkasten/malkasten.js',
    'malkasten-tools': 'src/malkasten/objects/all.js',
  },
  output: {
    dir: 'public/js',
    entryFileNames: '[name].js',
    format: 'es',
    minifyInternalExports: false,
    chunkFileNames: 'lib-[name].js',
    manualChunks: {
    }
  },
  plugins: [
    // Cleanup public/ directory before each build
    del({ targets: 'public/js/*' }),

    // Copy static files to public dir
/*
    copy({
      targets: [
        { src: 'index.htm', dest: 'public' },
        {
          src: ['demo/*.htm', 'demo/** /*.htm'],
          dest: 'public/demo', 
          transform: (contents, filename) => {
            //console.log({filename, contents: contents.toString()});
            return contents.toString().replaceAll('../src/', '../js/');
          },
          rename: (name, extension, fullPath) => {
            let nameWithParentPath = path.join(path.dirname(fullPath).replace(/demo\/?/, ''), name);
            return `${nameWithParentPath}.${extension}`;
          }
        },
        { src: 'demo/matrix/boulder-level.json', dest: 'public/demo/matrix' }
      ]
    })
*/
  ]
};