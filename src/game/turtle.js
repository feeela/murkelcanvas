import { extend } from "../helper/helper.js";
import { ActionRunner } from "../helper/action-runner.js";
import { Point } from "../point.js";
import { Direction } from "../constants.js";

// Current the Lindenmayer system implementation is only used
// in combination with the turtle class; hence it is exported here.
export * from "../helper/lindenmayer.js";


// Events will be dispatched on the HTMLCanvasElement

export class TurtleActionEvent extends Event {
    constructor(turtle, action) {
        super(TurtleActionEvent.name);

        this.turtle = turtle;
        this.action = action;
    }
}

export class TurtleFinishedEvent extends Event {
    constructor(turtle, action) {
        super(TurtleFinishedEvent.name);

        this.turtle = turtle;
        this.action = action;
    }
}


export class Turtle extends ActionRunner {

    static defaultOptions = {
        angle: 90,
        grammar: {
            "M": (turtle) => turtle.move(1),
            "F": (turtle) => turtle.line(1),
            "L": (turtle) => turtle.leaf(1),
            "D": (turtle) => turtle.dot(1),
            "+": (turtle) => turtle.turnLeft(),
            "-": (turtle) => turtle.turnRight(),
            "[": (turtle) => turtle.bracketOpen(),
            "]": (turtle) => turtle.bracketClose(),
        },
        speed: 500,
        lineWidth: 5,
        pixelPerStep: 20,
        color: "darkgreen",
        direction: 0,
        limit: Infinity,
    };

    /**
     * 
     * @param {CanvasRenderingContext2D} context
     * @param {Point} position The start position
     * @param {Object|false} rules A key-value object of replacement rules: `{ A: "AB", B: "BB", … }`; Set to `false` for manual turtle control;
     * @param {Object} options Overwrite one or more of the `defaultOptions` above
     */
    constructor(context, position, rules = false, options = {}) {
        // no auto-run
        super(false);

        this.context = context;
        this.position = position;
        this.options = extend(Turtle.defaultOptions, options);

        this.direction = this.options.direction;
        // Stack to store bracket positions to restore when a bracket was closed
        this.bracketPositions = [];

        this.context.lineWidth = this.options.lineWidth;
        this.context.strokeStyle = this.options.color;

        if(rules !== false) {
            this.iteration = 0;
            this.setRulesAsActions(rules, this.options.onlyOnce);

            // Set actual speed and run
            this.speed = this.options.speed;
            this.run();

            this.context.canvas.addEventListener(TurtleFinishedEvent.name, event => {
                this.iteration++;
            });
        }
    }

    run() {
        if(this.iteration < this.options.limit) {
            super.run();
        } else {
            this.stop();
        }
    }

    setRulesAsActions(rules, onlyOnce = false) {
        // Translate `rules` into actions using `options.grammar`
        rules.split("").forEach(rule => {
            // Ignore non-implemented rules
            if(this.options.grammar[rule]) {
                this.addAction(function() {
                    this.options.grammar[rule](this);
                    this.context.canvas.dispatchEvent(new TurtleActionEvent(this, this.options.grammar[rule]));
                    return true;
                });
            }
        });

        // Notify on end of one iteration loop
        this.addAction(function() {
            this.context.canvas.dispatchEvent(new TurtleFinishedEvent(this, "end-of-loop"));
            return true;
        });
    }

    // draw actions
    begin() {
        this.context.beginPath();
        this.context.moveTo(this.position.x, this.position.y);
    }

    /**
     * @param {Number} steps
     */
    line(steps = 1) {
        let target = this.position.getOrbitPoint(this.direction, steps * this.options.pixelPerStep, steps * this.options.pixelPerStep);
        this.begin();
        this.context.lineTo(target.x, target.y);
        this.context.stroke();
        this.position = target;
    }

    /**
     * @param {Number} steps
     */
    move(steps = 1) {
        let target = this.position.getOrbitPoint(this.direction, steps * this.options.pixelPerStep, steps * this.options.pixelPerStep);
        this.context.moveTo(target.x, target.y);
        this.position = target;
    }

    /**
     * @param {Number} steps
     */
    leaf(steps = 1) {
        let target = this.position.getOrbitPoint(this.direction, steps * this.options.pixelPerStep, steps * this.options.pixelPerStep);
        this.begin();
        this.context.lineTo(target.x, target.y);
        this.context.stroke();
        this.context.moveTo(this.position.x, this.position.y);
    }

    dot(steps = 1) {
        let oldPos = this.position;

        this.move(steps);

        let radius = (this.options.pixelPerStep * steps) / 2;
        let center = oldPos.midpoint(this.position);
        this.context.beginPath(); // Do not call this.begin() here, since it also moves the cursor
        this.context.arc(center.x, center.y, radius, 0, 2 * Math.PI);
        this.context.stroke();
        this.context.moveTo(this.position.x, this.position.y);
    }

    /**
     * @param {Number} degree Turn direction by degrees (positive or negative)
     */
    turn(degree) {
        this.direction = this.direction + degree;

        if(this.direction < 0) {
            this.direction = this.direction + 360;
        } else if(this.direction > 360) {
            this.direction = this.direction - 360;
        }
    }

    turnLeft() {
        this.turn(this.options.angle * -1);
    }

    turnRight() {
        this.turn(this.options.angle);
    }

    bracketOpen() {
        this.bracketPositions.push({ pos: this.position, angle: this.direction });
    }

    bracketClose() {
        let { pos, angle } = this.bracketPositions.pop();
        this.position = pos;
        this.direction = angle;
        this.context.moveTo(this.position.x, this.position.y);
    }
}
