/**
 * A collection of pointer icons to be used with Malkasten
 */

/**
 * Pointer cursor is a crosshair
 */
export function crosshair(crosshairLength = 20, color = "#666", lineWidth = 2) {
    crosshairLength = crosshairLength / 2;

    return (context, point) => {
        context.lineWidth = lineWidth;
        context.strokeStyle = color;

        context.beginPath();
        context.moveTo(point.x - crosshairLength, point.y);
        context.lineTo(point.x + crosshairLength, point.y);
        context.stroke();

        context.beginPath();
        context.moveTo(point.x, point.y - crosshairLength);
        context.lineTo(point.x, point.y + crosshairLength);
        context.stroke();
    }
}

/**
 * Pointer cursor is a transparent circle
 */
export function transparentDot(size, color = "rgba(90, 90, 90, .7)", borderColor = "rgba(255, 255, 255, .5)") {

    return (context, point) => {
        let radius = size / 2;
        context.beginPath();
        context.fillStyle = color;
        context.strokeStyle = borderColor;
        context.lineWidth = radius / 10;
        context.arc(point.x, point.y, radius - radius / 2, 0, 2 * Math.PI);
        context.fill();
        context.stroke();
    }
}
