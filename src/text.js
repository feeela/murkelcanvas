/**
 * Helper tools to draw text
 */

import { extend } from "./helper/helper.js";

/**
 * Print a text string at the given coordinates.
 * Has basic styles and computes the line-width and x-position, where the line starts.
 * 
 * "textAlign" sets were the line of text starts according to the X coordinate.
 * That means "right" aligned text is displayed left of the X coordinate (x - lineWidth);
 * "center" aligned text is displayed with the middle of the line at the X coordinate (x - lineWidth / 2).
 * 
 * The actual line-start x-coordinate is stored in Text.realX;
 * the computed line-width is stored in Text.lineWidth.
 */
export class Text {

    static defaultStyles = {
        color: "#666",
        background: null,
        size: 16,
        textAlign: "left",
        fontFamily: "sans-serif",
    };

    constructor(context, content, x, y, styles = {}) {
        this.context = context;
        this.content = content;

        this.x = x;
        this.y = y;

        this.styles = extend(Text.defaultStyles, styles);
        this.styles.lineHeight = this.styles.size * 1.6;

        this.context.font = `${this.styles.size}px ${this.styles.fontFamily}`;
        this.lineWidth = this.context.measureText(this.content).width;

        this.realX = this.x;
        switch(this.styles.textAlign) {
            case "center":
                this.realX = this.x - this.lineWidth / 2;
                break;
            case "right":
                this.realX = this.x - this.lineWidth;
                break;
        }

    }

    draw() {
        if(this.styles.background !== null) {
            let padding = this.styles.size / 3;
            this.context.beginPath();
            this.context.fillStyle = this.styles.background;
            this.context.fillRect(this.realX - padding, this.y, this.lineWidth + padding * 2, this.styles.size + padding);
        }

        this.context.font = `${this.styles.size}px ${this.styles.fontFamily}`;
        this.context.textAlign = this.styles.textAlign;
        this.context.fillStyle = this.styles.color;
        this.context.textBaseline = "middle";
        this.context.fillText(this.content, this.x, this.y + this.styles.lineHeight / 2);
    }
}

/**
 * A text block consisting of heading and separate lines;
 * can have a border, background & padding.
 * 
 * Padding is always drawn outside the text-box, starting at "x - padding" & "y - padding".
 */
export class TextBlock {

    static defaultStyles = {
        size: Text.defaultStyles.size,
        background: null,
        padding: 0,
        border: 0,
        borderColor: "#666",
        borderRadius: 0,
    };

    constructor(context, x, y, styles = {}) {
    	this.context = context;
    	this.x = x;
    	this.y = y;
        this.currentY = y;
        this.styles = extend(TextBlock.defaultStyles, styles);

    	this.lines = [];
        this.dimensions = null;
    }

    heading(content) {
    	this.text(content, { size: this.styles.size * 1.2 });
    }

    text(content, styles = {}) {
        styles = extend(this.styles, styles);
        styles.background = null;
        let text = new Text(this.context, content, this.x, this.currentY, styles);
        this.lines.push(text);

        // increment Y by line height
        this.currentY = this.currentY + text.styles.lineHeight;

        this.dimensions = null;
    }

    getBlockDimensions() {
        if(this.dimensions) {
            return this.dimensions;
        }

        let totalWidth = 0;
        let realX = this.x;

        // loop over all lines and gather horizontal dimension infos
        this.lines.forEach(line => {


            if(line.realX < realX) {
                realX = line.realX;
            }

            if(totalWidth < line.lineWidth) {
                totalWidth = line.lineWidth;
            }
        });

        let lastLine = this.lines[this.lines.length - 1];

        this.dimensions = {
            x: realX,
            y: this.y,
            width: totalWidth,

            // calculate total height based on the last lines Y & lineHeight
            height: lastLine.y - this.y + lastLine.styles.lineHeight,
        };

        return this.dimensions;
    }

    draw() {
        // draw backgrond and/or border
        if(this.styles.background !== null || this.styles.border > 0) {
            let dim = this.getBlockDimensions();

            // clear path drawing context / start a new graphic
            this.context.beginPath();

            this.context.roundRect(
                dim.x - this.styles.padding,
                dim.y - this.styles.padding,
                dim.width + this.styles.padding * 2,
                dim.height + this.styles.padding * 2,
                this.styles.borderRadius
            );

            if(this.styles.background !== null) {
                this.context.fillStyle = this.styles.background;
                this.context.fill();
            }

            if(this.styles.border > 0) {
                this.context.lineWidth = this.styles.border;
                this.context.strokeStyle = this.styles.borderColor;
                this.context.lineJoin = "round";
                this.context.stroke();
            }
        }

        // draw each line
    	this.lines.forEach(line => line.draw());
    }
}
