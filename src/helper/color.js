
export class Color {
    constructor(color, alpha = 1) {
        // Use an HTMLOptionElement to translate color inputs to normalized rgb(a)
        const { style } = new Option();
        style.color = Color.colorNameTohex(color);
        let regexpColor = style.color.match(/rgba?\((\d+),\s?(\d+),\s?(\d+),?\s?([\.\d]+)?\)/);

        this.r = regexpColor[1];
        this.g = regexpColor[2];
        this.b = regexpColor[3];
        this.a = regexpColor[4] || alpha;
    }

    toString() {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
    }

    saturate(percent) {
        let sat = percent / 100;
        let gray = this.r * 0.3086 + this.g * 0.6094 + this.b * 0.0820;

        this.r = Math.round(this.r * sat + gray * (1 - sat));
        this.g = Math.round(this.g * sat + gray * (1 - sat));
        this.b = Math.round(this.b * sat + gray * (1 - sat));
    }

    lighten(percent) {
        let r = Math.round(this.r * (100 + percent) / 100);
        let g = Math.round(this.g * (100 + percent) / 100);
        let b = Math.round(this.b * (100 + percent) / 100);

        this.r = (r < 255) ? r : 255;
        this.g = (g < 255) ? g : 255;
        this.b = (b < 255) ? b : 255;
    }

    rotateRGB() {
        let oldRGB = {
            r: this.b,
            g: this.r,
            b: this.g,            
        };

        this.r = oldRGB.b;
        this.g = oldRGB.r;
        this.b = oldRGB.g;
    }

    static colorNameTohex(colorName) {
        let cc = document.createElement('canvas').getContext('2d');
        cc.fillStyle = colorName;
        return cc.fillStyle;
    }
}

/** @deprecated */
export function toRGB(color) {
    return new Color(color);
}

/** @deprecated */
export function saturation(color, saturation) {
    color.saturate(saturation);
    return color;
}

/** @deprecated */
export function light(color, percent) {
    color.lighten(percent);
    return color;
}

/** @deprecated */
export function rgbValuesAsColorString(color) {
    return color.toString();
}
