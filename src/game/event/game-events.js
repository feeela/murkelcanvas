
/**
 * A custom event to announce updates in the game status (e.g. play, pause, win)
 * 
 * Example usage:
 * <code>
    myCanvas.addEventListener(GameStatus.type, event => {
        switch(event.status) {
            case GameStatus.statusWon:
                alert("You've won!");
                break;

            case GameStatus.statusPause:
                myCanvasContext.fillStyle = "#333";
                myCanvasContext.font = "12px monospaced";
                myCanvasContext.textAlign = "center";
                myCanvasContext.fillText("Paused", myCanvasContext.canvas.width / 2, myCanvasContext.canvas.height / 2 - 5);
                break;
        }
    });
 * </code>
 */
export class GameStatus extends Event {
    static type = "game-status";

    static statusStart = 1;
    static statusEnd = 2;
    static statusWon = 3
    static statusLost = 4;

    static getLabel(status) {
        let label = "";
        Object.keys(GameStatus).forEach(fieldName => {
            if(fieldName.substring(0, 6) == "status" && GameStatus[fieldName] == status) {
                label = fieldName.substring(6);
            }
        });
        return label;
    }

    constructor(status) {
        super(GameStatus.type);
        this.status = status;
        this.label = GameStatus.getLabel(status);
    }
}
