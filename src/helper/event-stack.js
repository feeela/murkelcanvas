
export class EventStack {

    constructor() {
        this.handler = {};
    }

    /**
     * @param {string} eventType A class name of a GameEvent
     * @param {Function} callback The event handler; the current GameEvent is passed as only argument
     */
    on(eventType, callback) {
        if(!this.handler[eventType]) {
            this.handler[eventType] = [];
        }

        this.handler[eventType].push(callback);
    }

    /**
     * @param {GameEvent} event
     */
    dispatchEvent(event) {
        this.handler[event.constructor.name].forEach(handler => {
            handler(event);
        });
    }
}
