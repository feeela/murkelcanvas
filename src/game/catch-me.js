import { Point } from "../point.js";
import { getRandomInt } from "../helper/helper.js";
import { Text, TextBlock } from "../text.js"
import { MatrixCanvas } from "../matrix-canvas.js";
import { Direction, Specificity } from "../constants.js";
import { Cursor, Wall } from "../matrix/matrix-object.js";
import { Behavior, ObjectWithBehavior } from "../matrix/matrix-object-behavior.js";
import { WaitBehavior, ManhattanBehavior, ChaiseCursorBehavior } from "./behavior.js";
import { getMonsterTile } from "./pattern.js";
import { GameStatus } from "./event/game-events.js";

class SetColorBehavior extends Behavior {
    constructor(color) {
        super();
        this.color = color;
    }

    run(matrix, currentPoint, self) {
        self.color = this.color;
    }
}

class Monster extends ObjectWithBehavior {
    constructor(color) {
        super();

        this.color = "#393";

        this.addBehavior(new SetColorBehavior("#393"));
        this.addBehavior(new WaitBehavior(), 1000);

        this.addBehavior(new SetColorBehavior("#ba3"));
        this.addBehavior(new ManhattanBehavior(2), 6000);

        this.addBehavior(new SetColorBehavior("#c33"));
        this.addBehavior(new ChaiseCursorBehavior(), 2500);
    }
}

class Player extends Cursor {
    constructor(direction = Direction.UP) {
        super();

        this.passable = true;
    }
}

/**
 * Example usage for MatrixMoveIt
 */
export class CatchMe extends MatrixCanvas {
    constructor(canvas, options = {}) {
        options.cursor = Player;

        super(canvas, options);

        // Place walls
        let wall = new Wall();
        for(let i = 0; i < 20; i++) {
            let y = getRandomInt(0, this.rows - 1);
            let x = getRandomInt(0, this.cols - 1);

            if(Math.random() < 0.5) {
                this.matrix.setValue({x: x, y: y - 1}, wall);
                this.matrix.setValue({x: x, y: y}, wall);
                this.matrix.setValue({x: x, y: y + 1}, wall);
            } else {
                this.matrix.setValue({x: x - 1, y: y}, wall);
                this.matrix.setValue({x: x, y: y}, wall);
                this.matrix.setValue({x: x + 1, y: y}, wall);
            }
        }

        this.matrix.addValue(new Point(this.cols - 1, this.rows - 1), new Monster());

        this.status = GameStatus.statusStart;
    }

    resetRenderer() {
        super.resetRenderer();

        this.renderer.register(Player.name, (context, x, y, width, height) => {
            context.fillStyle = "#a6a";
            context.fillRect(x + (width / 8), y + (height / 8), width / 8 * 6, height / 8 * 6);
        });

        this.monsterIcons = {};
        const monsterRenderer = (context, x, y, width, height, monsterObject) => {
            if(!this.monsterIcons[monsterObject.color]) {
                this.monsterIcons[monsterObject.color] = getMonsterTile(width, height, monsterObject.color);
            }
            context.drawImage(this.monsterIcons[monsterObject.color], x, y);
        };
        this.renderer.register(Monster.name, monsterRenderer);
    }

    content() {
        super.content();

        // Monster eats player
        if(this.matrix.hasValue(this.cursor, Monster)) {
            this.matrix.removeValue(this.cursor, Cursor);
            this.matrix.getValue(this.cursor, Monster).isPaused = true;
            this.status = GameStatus.statusLost;
        }

        // Text styles
        let textBlockOptions = {
            size: this.height / 35,
            textAlign: "center",
            background: "rgba(255,255,255,.75)",
            padding: this.height / 40,
        };

        switch(this.status) {

            case GameStatus.statusLost:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You have been eaten!");
                    msg.text("");
                    msg.text("Reload this page to try again.");

                    msg.draw();
                });
                break;
        }
    }
}
