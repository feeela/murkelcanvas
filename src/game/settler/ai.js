import { extend } from "../../helper/helper.js";
import { ActionGenerator } from "../../helper/action-generator.js";
import { Mountain } from "./game-object/terrain.js";
import { Forest, Rock, Swamp, Iron } from "./game-object/terrain-feature.js";
import { Kontor, Tower, StoneCutter, WoodCutter, Forester, Farm } from "./game-object/building.js";
import { Goods } from "./constants.js";
import { ResourcesDepletedEvent } from "./events.js";

// for debugging
import { Marker } from "../../matrix/matrix-object.js";


export class AI {
    static defaultOptions = {
        speed: 5000,
    };

    /**
     * @param {Civilization} civilization
     */
    constructor(civilization, options = {}) {
        this.civilization = civilization;

        this.options = extend(this.constructor.defaultOptions, options);

        this.actions = new ActionGenerator();

        // Initial actions
        this.actions.addAction(this.wait);
        this.actions.addAction(this.buildWoodCutter);
        this.actions.addAction(this.buildStoneCutter);

        // Find Iron as direction to first tower target
        this.nextTowerTarget = this.civilization.game.matrix.findNearestWithType(this.civilization.position, Iron);
        this.actions.addAction(this.buildTower);

        this.actions.addAction(this.buildForester);
        this.actions.addAction(this.buildFarm);

        this.civilization.on(ResourcesDepletedEvent.name, event => {
            if(event.harvester == null || event.harvester instanceof StoneCutter) {
                let nearestRock = this.civilization.game.matrix.findNearestWithType(this.civilization.position, Rock);
                if(!this.civilization.isPointWithinBorder(nearestRock)) {
                    // Find Rock as direction to next tower target
                    this.nextTowerTarget = nearestRock;
                    this.actions.addAction(this.buildTower);
                }
                this.actions.addAction(this.buildStoneCutter);
            }
        });

        this.run();
    }

    wait() {
        // Does nothing; but still takes up one AI tick (i.e. `options.speed`)
        this.actions.removeAction(this.wait);
        return true;
    }

    buildWoodCutter() {
        if(!this.civilization.storage.check(WoodCutter.requires)) {
            return false;
        }

        let latestKontor = Object.values(this.civilization.buildings[Kontor.name]).pop();
        let nextTypeField = this.civilization.findTypeField(Forest, latestKontor.position);
        if(nextTypeField === false) {
            console.error("Could not find Forest in our territory", {civilization: this.civilization});
            return true;
        }

        let buildingLocation = this.civilization.findFreeField(nextTypeField);

        if(buildingLocation === false) {
            console.error("Could not find a free field for WoodCutter", {civilization: this.civilization});
            return true;
        }

        this.civilization.addBuilding(buildingLocation, WoodCutter);
        this.actions.removeAction(this.buildWoodCutter);
        return true;
    }

    buildStoneCutter() {
        if(!this.civilization.storage.check(StoneCutter.requires)) {
            return false;
        }

        let latestKontor = Object.values(this.civilization.buildings[Kontor.name]).pop();
        let nextTypeField = this.civilization.findTypeField(Rock, latestKontor.position);
        if(nextTypeField === false) {
            // Dispatch this event here to look again for rocks outside of own territory and build another tower
            this.actions.addAction(this.wait);
            this.civilization.dispatchEvent(new ResourcesDepletedEvent(null));
            return true;
        }

        let buildingLocation = this.civilization.findFreeField(nextTypeField);

        if(buildingLocation === false) {
            console.error("Could not find a free field for StoneCutter", {civilization: this.civilization});
            return true;
        }

        if(this.civilization.buildings[StoneCutter.name]) {
            // Remove inactive StoneCutters
            let pausedStoneCutter = this.civilization.buildings[StoneCutter.name].filter(sc => sc.paused == true);
            pausedStoneCutter.forEach(psc => this.civilization.removeBuilding(psc));
        }

        this.civilization.addBuilding(buildingLocation, StoneCutter);
        this.actions.removeAction(this.buildStoneCutter);
        return true;
    }

    buildForester() {
        if(!this.civilization.storage.check(Forester.requires)) {
            return false;
        }

        let foresterPosition = this.civilization.buildings[WoodCutter.name][0].position;
        let possibleBuildingLocations = this.civilization.findFreeFields(foresterPosition, 1, 3);

        if(possibleBuildingLocations.length > 0) {
            this.civilization.addBuilding(possibleBuildingLocations[0], Forester);
        } else {
            console.error("Could not find a spot for Forester", this.civilization.id);
        }

        this.actions.removeAction(this.buildForester);
        return true;
    }

    /**
     *  @uses {Point} this.nextTowerTarget Must be set to determine the direction into which to build a new tower
     */
    buildTower() {
        // Pause all foresters until this tower could be built
        if(this.civilization.buildings[Forester.name]) {
            this.civilization.buildings[Forester.name].forEach(f => f.paused = true);
        }

        if(!this.civilization.storage.check(Tower.requires)) {
            return false;
        }

        let nextCivFields = this.civilization.findFreeFields(this.nextTowerTarget);

        if(nextCivFields.length > 0) {
            this.civilization.addBuilding(nextCivFields[0], Tower, {
                borderRadius: this.civilization.options.kontorBorderRadius,
            });
        } else {
            console.error("Could not find a spot for first tower.", this.civilization.id);
        }

        // Repeat until nextTowerTarget is within territory
        if(this.civilization.isPointWithinBorder(this.nextTowerTarget)) {
            this.actions.removeAction(this.buildTower);

            // Restart all foresters
            if(this.civilization.buildings[Forester.name]) {
                this.civilization.buildings[Forester.name].forEach(f => f.paused = false);
            }
        }

        return true;
    }

    buildFarm() {
        if(!this.civilization.storage.check(Farm.requires)) {
            return false;
        }

        let possibleBuildingLocations = this.civilization.findFreeFields(this.civilization.position, 1, this.civilization.options.kontorBorderRadius);

        if(possibleBuildingLocations.length > 0) {
            this.civilization.addBuilding(possibleBuildingLocations.pop(), Farm);
        } else {
            console.error("Could not find a spot for a Farm", this.civilization.id);
        }

        this.actions.removeAction(this.buildFarm);
        return true;
    }

    // TODO: for debugging only; split and move code to better suited positions
    nextKontorDemo(from, to) {
        let path = this.astar.search(from, to);

        let lastPathTileWithinBorders;
        let lastPathTileWithinBordersWithoutFeature;

        path.forEach(pp => {
            let debugTile = new Marker();
            debugTile.passable = true;

            if(Math.floor(from.distanceTo(pp)) <= this.options.kontorBorderRadius) {
                debugTile.color = "rgba(0, 90, 0, 0.75)";

                lastPathTileWithinBorders = pp;

                if(!this.game.matrix.hasValue(pp, Rock)
                    && !this.game.matrix.hasValue(pp, Forest)
                    && !this.game.matrix.hasValue(pp, Swamp)
                    && !this.game.matrix.hasValue(pp, Mountain)) {
                    lastPathTileWithinBordersWithoutFeature = pp;
                }
            }

            //this.game.matrix.addValue(pp, debugTile);
        });
/*
        let lastFreeMarker =this.game.matrix.getValue(lastPathTileWithinBorders, Marker);
        lastFreeMarker.color = "rgba(255, 255, 0, 0.8)";

        let lastFreeMarkerWOFeature =this.game.matrix.getValue(lastPathTileWithinBordersWithoutFeature, Marker);
        lastFreeMarkerWOFeature.color = "rgba(0, 0, 0, 0.8)";
*/
        if(lastPathTileWithinBordersWithoutFeature) {
            this.addKontor(lastPathTileWithinBordersWithoutFeature);
        }

        return path;
    }

    /**
     * An AI has its own loop, which runs independently of the Civilization or SettlerGame loops.
     */
    run() {
        setTimeout(() => {
            this.run();
        }, this.options.speed);

        if(this.civilization.game.isPaused == true) {
            return;
        }

        this.actions.runAction(this);
    }
}