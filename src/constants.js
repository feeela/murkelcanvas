
/**
 * The direction constant is used for cursor movement and user-input mapping 
 */
export class Direction {
    static UP = "u";
    static RIGHT = "r";
    static DOWN = "d";
    static LEFT = "l";

    static opposite(direction) {
        switch(direction) {
            case this.UP:
                return this.DOWN;
            case this.DOWN:
                return this.UP;
            case this.LEFT:
                return this.RIGHT;
            case this.RIGHT:
                return this.LEFT;
        }
    }

    static clockwise(direction) {
        switch(direction) {
            case this.UP:
                return this.RIGHT;
            case this.RIGHT:
                return this.DOWN;
            case this.DOWN:
                return this.LEFT;
            case this.LEFT:
                return this.UP;
        }
    }   

    /**
     * @param {Number} angle in degree
     */
    static angleToDirection(angle) {
        if(angle >= 315 || angle < 45) {
            return this.RIGHT;
        } else if(angle >= 45 && angle < 135) {
            return this.DOWN;
        } else if(angle >= 135 && angle < 225) {
            return this.LEFT;
        } else {
            return this.UP;
        }
    }
};

/**
 * A class for named access to specific specificities.
 */
export class Specificity {
    static DEFAULT = 50;
    static BACKGROUND = 0;
    static CURSOR = 99; // keyboard cursor
    static FOREGROUND = 100;
    static POINTER = 101; // mouse or touch pointer
}
