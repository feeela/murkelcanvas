<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

$levelStorageFile = './boulder-level.json';

$allLevels = json_decode(file_get_contents($levelStorageFile));

if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {

	$payload = json_decode(file_get_contents('php://input'));

	if(isset($_GET["edit-level"])) {
		// overwrite existing level with new data
		$editLevel = intval($_GET["edit-level"]);
	} else {
		// append new level
		$editLevel = count($allLevels);
	}

	$allLevels[$editLevel] = $payload;

	$levelJson = json_encode($allLevels, JSON_PRETTY_PRINT);

	file_put_contents($levelStorageFile, $levelJson);

	header('Content-Type: application/json; charset=utf-8');
	echo $levelJson;

} else {

	header('Content-Type: application/json; charset=utf-8');
	echo json_encode("no input");

}
