import { Point } from "../js/lib-point.js"
import { getRandomInt } from "../js/helper.js"
import { MatrixObject } from '../js/matrix.js';
import { MatrixCanvas } from "../js/matrix-canvas.js";

export class Box extends MatrixObject {
    constructor() {
        super();
        this.movable = true;
    }
}

/**
 * Example usage for MatrixMoveIt
 */
export class MoveItAroundUntilKeyUp extends MatrixCanvas {
    constructor(canvas, options = {}) {
        super(canvas, options);

        let box = new Box();

        // Place boxes
        for(let i = 0; i < 10; i++) {
            let y = getRandomInt(0, this.rows - 1);
            let x = getRandomInt(0, this.cols - 1);
            this.matrix.addValue(new Point(x, y), box);
        }
    }

    resetRenderer() {
        super.resetRenderer();

        // Extend renderer
        this.renderer.register(Box.name, (context, x, y, width, height) => {
            context.fillStyle = "#05c";
            context.fillRect(x + (width / 10), y + (height / 10), width / 10 * 8, height / 10 * 8);
        });
    }

    /**
     * Remove direction
     */
    onKeyUp(keyCode) {
        switch(keyCode) {
            case "ArrowUp":
            case "ArrowRight":
            case "ArrowDown":
            case "ArrowLeft":
                this.direction = null;
                break;
            default:
                super.onKeyUp(keyCode);
        }
    }

    /**
     * Move "this.cursor" one field into "direction".
     * Maybe also push objects out of the way.
     * This cursor can move beyond the matrix and when doing so is placed on the opposite end.
     *
     * @var {Direction} direction
     */
    moveCursor(direction) {
        const newCursor = this.cursor.move(direction);

        // Check if cursor is outside the matrix, if yes wrap around
        if(newCursor.x < 0) {
            newCursor.x = this.cols - 1;
        } else {
            if(newCursor.y < 0) {
                newCursor.y = this.rows - 1;
            } else {
                if(newCursor.x > this.cols - 1) {
                    newCursor.x = 0;
                } else {
                    if(newCursor.y > this.rows - 1) {
                        newCursor.y = 0;
                    }
                }
            }
        }

        super.moveCursor(direction, newCursor);
    }

    content() {
        // Do not reset this.direction to NULL
        super.content(false);
    }
}
