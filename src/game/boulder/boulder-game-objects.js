import { MatrixObject, Cursor } from "../../matrix/matrix-object.js";
import { Direction } from "../../constants.js";
import { GameStatus } from "../event/game-events.js";
import { Point } from "../../point.js";
import { getRandomInt } from "../../helper/helper.js";

// export base objects to parent file
export * from "../../matrix/matrix-object.js";

export class Entrance extends MatrixObject {
    constructor() {
        super();
    }
}

export class Exit extends MatrixObject {
    constructor() {
        super();
    }
}

export class Player extends Cursor {
    constructor(direction = Direction.UP) {
        super();
        this.direction = direction;
    }
}

export class Movable extends MatrixObject {
    constructor() {
        super(false, true);
    }
}

export class Boulder extends Movable {
    constructor() {
        super();
    }
}

export class Box extends Movable {
    constructor() {
        super();
    }
}

export class Collectable extends MatrixObject {
    constructor() {
        super();
        this.collectable = true;
    }
}

export class Key extends Collectable {
    constructor() {
        super();
    }
}


export class Monster extends MatrixObject {

    static colors = ["#6c6", "#939", "#cc6", "#66c", "#c66", "#6ff"];

    constructor() {
        super(false);

        this.previousTimestamp = false;
        this.speed = 150;

        this.direction = null;
        this.steps = 3;
        this.currentStep = 1;

        this.pause = false;

        this.color = Monster.colors[getRandomInt(0, Monster.colors.length - 1)];
    }

    run(matrixCanvas, x, y) {
        let timestamp = performance.now();

        // Wait for monster tick
        if(this.pause || (this.previousTimestamp !== false && timestamp < (this.previousTimestamp + this.speed))) {
            return;
        }

        let currentPoint = new Point(x, y);

        if(this.direction !== null) {
            // move towards target
            if(this.currentStep <= this.steps) {
                let targetPoint = currentPoint.move(this.direction);
                if(matrixCanvas.matrix.isValidPoint(targetPoint)
                    && (matrixCanvas.matrix.isPassable(targetPoint) || matrixCanvas.cursor.equals(targetPoint))) {
                    matrixCanvas.matrix.moveValue(currentPoint, targetPoint, Monster);
                    this.currentStep++;
                }
            } else {
                this.currentStep = 1;
                this.direction = null;
            }
        } else {
            // find a direction
            let possibleDirections = this.findDirection(matrixCanvas, currentPoint, this.steps);

            if(possibleDirections !== false) {
                this.direction = possibleDirections[getRandomInt(0, possibleDirections.length - 1)];
            }
        }

        this.previousTimestamp = timestamp;
    }

    /**
     * Look "steps" fields ahead in all directions to determine, which directions are open to go to.
     */
    findDirection(matrixCanvas, point, steps) {
        let possibleDirections = [];

        for(let direction in Direction) {
            let currentPoint = point;
            let stepsCounted = 0;

            for(let i = 0; i < steps; i++) {
                let nextField = currentPoint.move(Direction[direction]);

                if(!matrixCanvas.matrix.isValidPoint(nextField)
                    || (nextField.x == currentPoint.x && nextField.y == currentPoint.y)
                    || (!matrixCanvas.matrix.isPassable(nextField)) && !matrixCanvas.cursor.equals(nextField)) {
                    break;
                }

                currentPoint = nextField;
                stepsCounted++;
            }

            if(stepsCounted == steps) {
                possibleDirections.push(Direction[direction]);
            }
        }

        return possibleDirections.length > 0 ? possibleDirections : false;
    }
}
