import { extend } from "../helper/helper.js";
import { Direction, Specificity } from "../constants.js";
import { Point } from "../point.js";
import { MatrixObject, Marker } from "./matrix-object.js";
import { Astar } from "../matrix/astar.js";


export class Terrain extends MatrixObject {

    static Type = {
        Void: 0,
        Water: 1,
        Land: 2,
        Mountain: 4,
        River: 8,
        Road: 16,
        Settlement: 32,
    }

    /**
     * @param {Number} height A float between 0 and 1 to indicate the terrain elevation
     * @param {Number} type One of `Terrain.Type` or own indizes to indicate a terrain type (e.g. by elevation or biotope)
     */
    constructor(height, type = Terrain.Type.Land, specificity = Specificity.BACKGROUND) {
        super(true, false, specificity);
        this.height = height;
        this.type = type;
        this.color = this.calculateColor();
        this.neighbours = false;
        this.hasDifferentNeighbour = false;


    }

    /**
     * Base version calculates color based on type and elevation.
     * May be overloaded to change coloring or when more types are added.
     *
     * @return {string} A color in a valid CSS color format
     */
    calculateColor() {
        let color;
        let rgb = Math.round(255 * this.height);
        switch(this.type) {
            case Terrain.Type.Water:
                color = `rgba(0, 0, ${rgb * 2}, 1)`;
                break;
            case Terrain.Type.Mountain:
                let mountainRGB = Math.round(rgb - (255 * 100 / rgb));
                color = `rgba(${Math.round(mountainRGB / 1.5)}, ${mountainRGB}, ${mountainRGB}, 1)`;
                break;
            default:
                color = `rgba(0, ${255 - rgb / 2}, 0, 1)`;
                break;
        }
        return color;
    }

    run(matrixCanvas, x, y) {
        if(!this.neighbours) {
            this.neighbours = this.getNeighbours(matrixCanvas.matrix, new Point(x, y));
        }
    }

    getNeighbours(matrix, position) {
        let neighbours = {
            west: new Point(position.x - 1, position.y),
            east: new Point(position.x + 1, position.y),
            north: new Point(position.x, position.y - 1),
            south: new Point(position.x, position.y + 1),
        };

        for(let direction in neighbours) {
            if(!matrix.isValidPoint(neighbours[direction]) || !matrix.isPassable(neighbours[direction])) {
                neighbours[direction] = Terrain.Type.Void;
            } else {
                neighbours[direction] = matrix.getValue(neighbours[direction], Terrain);
                if(this.type != neighbours[direction].type) {
                    this.hasDifferentNeighbour = true;
                }
            }
        }

        return neighbours;
    }
}

export class Depletable extends MatrixObject {
    /**
     * An object where somehting can be taken from.
     * 
     * @param {Number} amount The fill level in percent.
     * @param {Specificity} specificity
     */
    constructor(amount = 100, specificity = Specificity.DEFAULT - 10) {
        super(true, false, specificity);

        this.amount = amount;
    }

    take(amount = 5) {
        if(this.amount >= amount) {
            this.amount = this.amount - amount;
        } else {
            amount = this.amount;
            this.amount = 0;
        }
        return amount;
    }
}

export class Forest extends Depletable {
}

export class TreeNursery extends Depletable {
    take() {
        // Disabled here; you must not cut down trees in a nursery
    }

    grow(amount = 5) {
        if(this.amount < 100) {
            this.amount = this.amount + amount;
            // Limit at 100 percent
            if(this.amount > 100) {
                this.amount = 100;
            }
        }
        return this.amount;
    }
}

export class Rock extends Depletable {
}

export class Swamp extends MatrixObject {
    constructor(specificity = Specificity.DEFAULT - 10) {
        super(true, false, specificity);
    }
}

export class Ore extends Depletable {
}

export class Iron extends Ore {
}

export class Gold extends Ore {
}

export class Coal extends Ore {
}

// TODO: the following bjects should be moved in to a separate game-object.js file
export class GameSpecificity extends Specificity {
    static PATH = 10;
    static TERRAIN_FEATURE = 20;
    static BUILDING = 55;
}

export class Civilization extends MatrixObject {

    static defaultOptions = {
        color: "#a6a",
        kontorBorderRadius: 4,
    };

    constructor(gameObject, position, options) {
        super(false, false, options.specificity || GameSpecificity.DEFAULT);

        this.options = extend(this.constructor.defaultOptions, options);

        this.gameObject = gameObject;
        this.position = position;
        this.color = this.options.color;
        this.astar = new Astar(gameObject.matrix);

        // A list of all buildings that expand the border
        this.kontors = [];
        this.buildings = {};

        // Initial Kontor at civ-center
        this.addKontor(this.position);

        let nextIron = this.gameObject.matrix.findNearestWithType(this.position, Iron);
        let pathToIron = this.findPath(this.position, nextIron);
        //this.matrix.addValue(new Point(playerPositions[i].x, playerPositions[i].y), new Forester());

        let firstBuildPoints = this.position.midpointCircle(Math.round(this.options.kontorBorderRadius / 3 * 2))
            .filter(fbp => {
                if(!this.gameObject.matrix.isValidPoint(fbp) || !this.gameObject.matrix.isPassable(fbp)) {
                    return false;
                }

                let fbpValues = this.gameObject.matrix.getValues(fbp);
                return fbpValues.length == 1;
            })
            .forEach(fbp => {
                if(this.gameObject.matrix.getNeighboursWithType(fbp, Rock).length > 0
                    && (!this.buildings[StoneCutter.name] || this.buildings[StoneCutter.name].length == 0)) {
                    this.addBuilding(fbp, new StoneCutter(this.gameObject, fbp));

                } else if(this.gameObject.matrix.getNeighboursWithType(fbp, Forest).length > 0
                    && (!this.buildings[Forester.name] || this.buildings[Forester.name].length == 0)) {
                    this.addBuilding(fbp, new Forester(this.gameObject, fbp));
                }
            });

    }

    addBuilding(position, building) {
        if(!this.buildings[building.constructor.name]) {
            this.buildings[building.constructor.name] = [];
        }

        this.buildings[building.constructor.name].push(building);
        this.gameObject.matrix.addValue(position, building);

        // Add Path objects to the found path
        let pathToNewBuilding = this.astar.search(this.position, position);
        let previousPoint = this.position;
        for(let i = 0; i < pathToNewBuilding.length - 1; i++) {
            let pp = pathToNewBuilding[i];

            let d1 = Direction.angleToDirection(pp.angleTo(previousPoint));
            let d2 = Direction.angleToDirection(pp.angleTo(pathToNewBuilding[i + 1]));

            let existingPath = this.gameObject.matrix.getValue(pp, Path);
            if(existingPath) {
                existingPath.addDirection(d1, d2);
            } else {
                let path = new Path([d1, d2]);
                this.gameObject.matrix.addValue(pp, path);
            }

            previousPoint = pp;
        }
    }

    addKontor(position) {
        let newKontor = new Kontor(this.gameObject, position, {
            borderRadius: this.options.kontorBorderRadius,
        });
        this.kontors.push(newKontor);
        this.gameObject.matrix.addValue(position, newKontor);
    }

    findPath(from, to) {
        let path = this.astar.search(from, to);

        let lastPathTileWithinBorders;
        let lastPathTileWithinBordersWithoutFeature;

        path.forEach(pp => {
            let debugTile = new Marker();
            debugTile.passable = true;

            if(Math.floor(from.distanceTo(pp)) <= this.options.kontorBorderRadius) {
                debugTile.color = "rgba(0, 90, 0, 0.75)";

                lastPathTileWithinBorders = pp;

                if(!this.gameObject.matrix.hasValue(pp, Rock)
                    && !this.gameObject.matrix.hasValue(pp, Forest)
                    && !this.gameObject.matrix.hasValue(pp, Swamp)
                    && this.gameObject.matrix.getValue(pp, Terrain).type != Terrain.Type.Mountain) {
                    lastPathTileWithinBordersWithoutFeature = pp;
                }
            }

            //this.gameObject.matrix.addValue(pp, debugTile);
        });
/*
        let lastFreeMarker =this.gameObject.matrix.getValue(lastPathTileWithinBorders, Marker);
        lastFreeMarker.color = "rgba(255, 255, 0, 0.8)";

        let lastFreeMarkerWOFeature =this.gameObject.matrix.getValue(lastPathTileWithinBordersWithoutFeature, Marker);
        lastFreeMarkerWOFeature.color = "rgba(0, 0, 0, 0.8)";
*/
        this.addKontor(lastPathTileWithinBordersWithoutFeature);

        return path;
    }
}

export class Building extends MatrixObject {
    static defaultOptions = {
        borderRadius: 4,
    };

    constructor(gameObject, position, options, specificity = GameSpecificity.BUILDING) {
        super(true, false, specificity);
if(!position || !gameObject.matrix.isValidPoint(position)) {
    console.log("position", position);
    throw new Error("Cannot build at this position.");
}

        this.options = extend(this.constructor.defaultOptions, options);
        this.gameObject = gameObject;
        this.position = position;
    }
}

export class Path extends MatrixObject {
    constructor(directions = [Direction.LEFT, Direction.RIGHT], specificity = GameSpecificity.PATH) {
        super(true, false, specificity);

        this.directions = [... new Set(directions)]; // get unique values via Set;
    }

    addDirection(...directions) {
        directions.forEach(direction => this.directions.push(direction));

        this.directions = [... new Set(this.directions)]; // get unique values via Set
    }
}

export class Kontor extends Building {
    static defaultOptions = extend(super.defaultOptions, {
        borderRadius: 4,
    });

    constructor(gameObject, position, options, specificity = GameSpecificity.BUILDING) {
        super(gameObject, position, options, specificity);

        // Add border area
        let midpointCircle = this.position.midpointCircle(this.options.borderRadius);
        midpointCircle.forEach(areaPoint => {
            if(this.gameObject.matrix.isValidPoint(areaPoint)) {
                this.gameObject.matrix.addValue(
                    areaPoint,
                    new BorderPost(this.color, this.position.angleTo(areaPoint))
                );
            }
        });
    }
}

export class Forester extends Building {
}

export class StoneCutter extends Building {
}

export class BorderPost extends MatrixObject {
    constructor(color = "#a6a", direction = Direction.RIGHT, specificity = GameSpecificity.BUILDING) {
        super(true, false, specificity);
        this.color = color;
        this.direction = direction;
    }
}
