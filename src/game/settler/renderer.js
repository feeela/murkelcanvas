import { Direction } from "../../constants.js";
import { Point } from "../../point.js";
import { Renderer } from "../../render/renderer.js";
import { getRenderer as getDefaultRenderer } from "../../render/matrix-renderer.js";
import { toRGB, rgbValuesAsColorString } from "../../helper/color.js";

import {
    Terrain, Water, Land, Mountain, River,
    Forest, TreeNursery, Rock, Swamp, Iron, Gold, Coal,
    Wheat,
    Civilization, CivArea, Path, Kontor, Tower,
    StoneCutter, WoodCutter, Forester, Farm,
    Settler,
} from "./game-object/all.js";

import {
    getForestTile,
    getOreTile,
    getBorderPostTile,
    getCivAreaTile,
    getWoodCutterTile,
    getWheatTile,
    getTowerTile,
    // from boulder game; do not edit those patterns
    getBoulderTile,
    getPlayerTile,
} from "../pattern.js";

/**
 * @var {2DRenderContext} 
 */
export function getRenderer(canvasObject) {
    // Use th default matrix renderer as base
 	let renderer = getDefaultRenderer(canvasObject);

    let icons = {};


    const terrainRenderer = (context, x, y, width, height, terrainObject) => {
        let borderRadius = width / 2;

        if(terrainObject.hasDifferentNeighbour == true) {
            let radius = [0, 0, 0, 0];

            /* TODO:
            This selection has breaking edge case when the tiles are positioned in crossed line; e.g.:

                W L
                L W

            */

            let neighbourColor = terrainObject.color;
            let gradientFromTo = false;
            if(terrainObject.constructor.name != terrainObject.neighbours.west.constructor.name
                && terrainObject.neighbours.west.constructor.name == terrainObject.neighbours.north.constructor.name) {
                radius[0] = borderRadius;
                neighbourColor = terrainObject.neighbours.west.color;
                gradientFromTo = [x, y, x + width, y + height];
            }
            if(terrainObject.constructor.name != terrainObject.neighbours.north.constructor.name
                && terrainObject.neighbours.north.constructor.name == terrainObject.neighbours.east.constructor.name) {
                radius[1] = borderRadius;
                neighbourColor = terrainObject.neighbours.north.color;
                gradientFromTo = [x + width, y, x, y + height];
            }
            if(terrainObject.constructor.name != terrainObject.neighbours.east.constructor.name
                && terrainObject.neighbours.east.constructor.name == terrainObject.neighbours.south.constructor.name) {
                radius[2] = borderRadius;
                neighbourColor = terrainObject.neighbours.east.color;
                gradientFromTo = [x + width, y + height, x, y];
            }
            if(terrainObject.constructor.name != terrainObject.neighbours.south.constructor.name
                && terrainObject.neighbours.south.constructor.name == terrainObject.neighbours.west.constructor.name) {
                radius[3] = borderRadius;
                neighbourColor = terrainObject.neighbours.south.color;
                gradientFromTo = [x, y + height, x + width, y];
            }

            if(gradientFromTo) {
                // fill background with neighbour color
                context.fillStyle = neighbourColor;
                context.fillRect(x, y, width, height);

                // and overlay with a rounded rect in current terrain color
                context.beginPath();
                context.fillStyle = terrainObject.color;
                context.roundRect(x, y, width, height, radius);
                context.fill();
            } else {
                context.fillStyle = terrainObject.color;
                context.fillRect(x, y, width, height);
            }

        } else {
/*
            let conicGradient = context.createConicGradient(0, x + width / 2, y + height / 2);
            conicGradient.addColorStop(0, terrainObject.neighbours.east.color || terrainObject.color);
            conicGradient.addColorStop(0.25, terrainObject.neighbours.south.color || terrainObject.color);
            conicGradient.addColorStop(0.5, terrainObject.neighbours.west.color || terrainObject.color);
            conicGradient.addColorStop(0.75, terrainObject.neighbours.north.color || terrainObject.color);
            conicGradient.addColorStop(1, terrainObject.neighbours.east.color || terrainObject.color);
            context.fillStyle = conicGradient;
            //context.fillStyle = terrainObject.color;
            context.fillRect(x, y, width, height);
*/
            context.fillStyle = terrainObject.color;
            context.fillRect(x, y, width, height);
        }
    };

    // The color of terrain tiles depends on their class type and height.
    renderer.register(Terrain.name, terrainRenderer);
    renderer.register(Water.name, terrainRenderer);
    renderer.register(Land.name, terrainRenderer);
    renderer.register(Mountain.name, terrainRenderer);
    renderer.register(River.name, terrainRenderer);

    renderer.register(TreeNursery.name, (context, x, y, width, height, nurseryObject) => {
        if(!icons["forest" + nurseryObject.amount]) {
            icons["forest" + nurseryObject.amount] = getForestTile(width, height, nurseryObject.amount);
        }
        context.drawImage(icons["forest" + nurseryObject.amount], x, y, width, height);
    });

    renderer.register(Forest.name, (context, x, y, width, height, forestObject) => {
        if(!icons["forest" + forestObject.amount]) {
            icons["forest" + forestObject.amount] = getForestTile(width, height, forestObject.amount);
        }
        context.drawImage(icons["forest" + forestObject.amount], x, y, width, height);
    });

    renderer.register(Swamp.name, (context, x, y, width, height, swampObject) => {
        let featureSize = width / 10;
        context.fillStyle = "rgba(0, 139, 139, 1)";
        context.beginPath();
/*
        context.moveTo(x + featureSize * 2, y + featureSize * 4);
        context.bezierCurveTo(
            x + featureSize * 4, y + featureSize * 2,
            x + featureSize * 7, y + featureSize * 9,
            x + featureSize * 9, y + featureSize * 7
        );        
*/
        //context.arc(x + featureSize * 4, y + featureSize * 6, featureSize * 3, 0, 2 * Math.PI)
        context.ellipse(x + featureSize * 5, y + featureSize * 6, featureSize * 4, featureSize * 2.5, -35, 0, 2 * Math.PI);
        context.fill();
    });

    renderer.register(Rock.name, (context, x, y, width, height, rockObject) => {
        if(!icons.boulder) {
            icons.boulder = getBoulderTile(width, height);
        }
        let rockHeight = height * (rockObject.amount / 100);
        context.drawImage(icons.boulder, x, y + (height - rockHeight), width, rockHeight);
    });

    renderer.register(Wheat.name, (context, x, y, width, height, wheatObject) => {
        let stalkHeight = Math.round(wheatObject.amount / 10);
        if(!icons["wheat" + stalkHeight]) {
            icons["wheat" + stalkHeight] = getWheatTile(width, height, stalkHeight);
        }
        context.drawImage(icons["wheat" + stalkHeight], x, y, width, height);
    });

    const getOreRenderer = (color) => {
        return (context, x, y, width, height, oreObject) => {
            let key = "ore_" + color + "_" + oreObject.amount;
            if(!icons[key]) {
                icons[key] = getOreTile(width, height, oreObject.amount, color);
            }
            context.drawImage(icons[key], x, y, width, height);
        };
    };
    renderer.register(Iron.name, getOreRenderer("#B0C4DE"));
    renderer.register(Gold.name, getOreRenderer("#FFD700"));
    renderer.register(Coal.name, getOreRenderer("#222"));

    // TODO: the following renderers should be moved to a spearate game-renderer.js

    renderer.register(Civilization.name, (context, x, y, width, height, civilizationObject) => {
        context.beginPath();
        context.lineWidth = Math.ceil(width / 20);
        context.strokeStyle = civilizationObject.color;
        context.rect(x, y, width, height);
        context.stroke();
    });

    renderer.register(CivArea.name, (context, x, y, width, height, civAreaObject) => {
        // TODO provide possibility to render all areas

/*
        let key = "civarea_" + civAreaObject.kontor.civilization.color;
        if(!icons[key]) {
            icons[key] = getCivAreaTile(width, height, civAreaObject.kontor.civilization.color);
        }
        context.drawImage(icons[key], x, y, width, height);
*/
        if(civAreaObject.isEdge == true) {
            let iconKey = "border_post_" + civAreaObject.kontor.civilization.color + "_" + JSON.stringify(civAreaObject.directions);
            if(!icons[iconKey]) {
                icons[iconKey] = getBorderPostTile(width, height, civAreaObject.directions, civAreaObject.kontor.civilization.color);
            }
            context.drawImage(icons[iconKey], x, y);
        }
    });

    renderer.register(WoodCutter.name, (context, x, y, width, height, woodCutterObject) => {
        if(!icons.woodCutter) {
            icons.woodCutter = getWoodCutterTile(width, height);
        }
        context.drawImage(icons.woodCutter, x, y, width, height);
    });

    const pseudoBuilding = (context, x, y, width, height, color, isPaused) => {
        context.beginPath();
        context.fillStyle = color;
        context.rect(x, y, width, height);
        context.fill();

        if(isPaused) {
            context.lineWidth = Math.ceil(width / 20);
            context.strokeStyle = "#fff";
            context.moveTo(x, y);
            context.lineTo(x + width, y + height);
            context.moveTo(x + width, y);
            context.lineTo(x, y + height);
            context.stroke();
        } 
    }

    renderer.register(Forester.name, (context, x, y, width, height, foresterObject) => {
        pseudoBuilding(context, x, y, width, height, "#66cc00", foresterObject.paused);
    });

    renderer.register(StoneCutter.name, (context, x, y, width, height, stoneCutterObject) => {
        pseudoBuilding(context, x, y, width, height, "#2F4F4F", stoneCutterObject.paused);
    });

    renderer.register(Farm.name, (context, x, y, width, height, farmObject) => {
        pseudoBuilding(context, x, y, width, height, "#FF8C00", farmObject.paused);
    });

    renderer.register(Path.name, (context, x, y, width, height, pathObject) => {
        context.beginPath();
        context.strokeStyle = "#FFD700"; // gold
        context.lineWidth = Math.ceil(width / 10);
        context.lineCap = "round";
        let halfX = width / 2;
        let halfY = height / 2;

        pathObject.directions.forEach(dir => {
            context.moveTo(x + halfX, y + halfY);
            switch(dir) {
                case Direction.UP:
                    context.lineTo(x + halfX, y);
                    break;
                case Direction.RIGHT:
                    context.lineTo(x + width, y + halfY);
                    break;
                case Direction.DOWN:
                    context.lineTo(x + halfX, y + height);
                    break;
                case Direction.LEFT:
                    context.lineTo(x, y + halfY);
                    break;
            }
            context.stroke();
        });
    });

    renderer.register(Tower.name, (context, x, y, width, height, towerObject) => {
        let key = "tower_" + towerObject.civilization.id;
        if(!icons[key]) {
            icons[key] = getTowerTile(width, height, towerObject.civilization.color, false);
        }
        context.drawImage(icons[key], x, y, width, height);
    });

    renderer.register(Kontor.name, (context, x, y, width, height, kontorObject) => {
        let key = "kontor_" + kontorObject.civilization.id;
        if(!icons[key]) {
            icons[key] = getTowerTile(width, height, kontorObject.civilization.color);
        }
        context.drawImage(icons[key], x, y, width, height);
    });

    renderer.register(Settler.name, (context, x, y, width, height, settlerObject) => {
        let key = "settler" + settlerObject.direction + settlerObject.color;
        if(!icons[key]) {
            icons[key] = getPlayerTile(width, height, settlerObject.direction, settlerObject.color);
        }
        context.drawImage(icons[key], x, y, width, height);
    });

    return renderer;
}
