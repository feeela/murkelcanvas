import { extend, isTouchDevice } from "../helper/helper.js";
import { Specificity } from "../constants.js";
import { Layers } from "../layer.js";
import { Point } from "../point.js";
import { ObjectState, ObjectStack } from "./objects/basic.js";
import { CanvasBase } from "../canvas-base.js";

export class Malkasten extends CanvasBase {

    /**
     * Possible types to pass to `constructor( options.contextType )`
     */
    static contextTypes = {
        "2d": "2d",
        "webgl": "webgl",
        "webgl2": "webgl2",
        "webgpu": "webgpu",
        "bitmaprenderer": "bitmaprenderer",
    };

    static defaultOptions = {
        contextType: Malkasten.contextTypes["2d"],
        backgroundColor: false,
        speed: 5,
        debug: false,
        /**
         * @param {Boolean|Function} pointer Determines if a custom pointer cursor icon should be drawn.
         *      A callback can be set as value, which is expected to draw something.
         *      `pointer: (context, point) => { …draw onto context at point.x/point.y }` 
         */
        pointer: false,
    };

    /**
     * 
     * @param {HTMLCanvasElement|string} canvas A canvas DOM element or a CSS selector to find one in the current page
     */
    constructor(canvas, options = {}, autoRun = true) {
        options = extend(Malkasten.defaultOptions, options)
        super(canvas, options.contextType);

        this.options = options;

        // Auto-enable debug, if the URL has a hash #debug
        let hash = window.location.hash.substring(1);
        if(hash == "debug") {
            this.options.debug = true;
        }

        // Debug output on smartphones in an HTML PRE element below the canvas
        if(this.options.debug == true) {
            if(isTouchDevice()) {
                this.onPageConsole();
            }
        }

        this.context = this.canvas.getContext(this.options.contextType);
        this.layers = new Layers();
        this.objects = new ObjectStack();

        // The whole canvas is filled with this color first on each paint
        this.backgroundColor = this.options.backgroundColor || "#fff";
        this.debug = this.options.debug || false;

        // App tick / main loop speed
        this.previousTimestamp = false;
        this.speed = this.options.speed || 50; // app tick iteration speed in milliseconds

        // Pause handling
        this.isInitialPaint = true;
        this.paintBeforePause = true;
        this.lastFrameCapture = null;

        // Pointer (mouse or other pointer device)
        let canvasDim = this.canvas.getBoundingClientRect();
        this.courseCorrection = new Point(canvasDim.left, canvasDim.top);
        this.pointer = new Point();

        if(this.options.pointer !== false) {
            // Hide default cursor (from browser)
            this.canvas.style.cursor = "none";
        }

        // Update pointer position
        this.canvas.addEventListener("pointermove", event => {
            this.pointer = this.pixelPointToPercentagePoint(this.pointWithOffset(event));
        });
        // Listen to clicks / taps
        this.canvas.addEventListener("pointerdown", event => {
            this.pointer = this.pixelPointToPercentagePoint(this.pointWithOffset(event));
            this.onPointerDown(event);
        });

        // Listen for user input
        this.canvas.addEventListener("keydown", event => this.onKeyPress(event.code));
        this.canvas.addEventListener("keyup", event => this.onKeyUp(event.code));

        // Go!
        if(autoRun === true) {
            this.run();
        }
        this.canvas.focus();
    }

    /**
     * @param {Point|PointerEvent} point An object with `x` & `y` properties.
     */
    pointWithOffset(point) {
        return new Point(
            point.x - this.courseCorrection.x,
            point.y - this.courseCorrection.y
        );
    }

    /**
     * What to do, when a mouse button was pressed or the user tapped on the touch screen.
     */
    onPointerDown(event) {
/*
        // on-click demo: remove objects that were clicked
        let currentlyClickedObjects = this.objects.getObjectsAtPoint(this.pointer);
        for(let key in currentlyClickedObjects) {
            this.objects.remove(key);
        }
*/
    }

    /**
     * What to do, when a keyboard key was pressed.
     * The base version here enables pausing the main loop.
     */
    onKeyPress(keyCode) {
        switch(keyCode) {
            case "Space":
                if(this.isPaused == true) {
                    this.isPaused = false;
                } else {
                    this.isPaused = true;
                }
                break;
        }
    }
    
    onKeyUp(keyCode) {
        // void base method; overload to add keyboard features
    }

    /**
     * The canvas paint loop
     * 
     * @param {DOMHighResTimeStamp} timestamp
     */
    run(timestamp = null) {
        if(timestamp === null) {
            timestamp = performance.now();
        }

        // Wait for app tick
        if(this.previousTimestamp !== false && timestamp < (this.previousTimestamp + this.speed)) {
            requestAnimationFrame(timestamp => { this.run(timestamp); });
            return;
        }

        // Pause
        // TODO Bug on retina devices: due to scaling only a part of the canvas is stored in lastFrameCaptured
        if(this.isPaused) {
            if(this.paintBeforePause) {
                this.paintBeforePause = false;

                // gather current loop contents
                this.layers.reset();
                this.content();
                this.layers.layer(Specificity.POINTER).reset();

                // draw layers to canvas
                this.draw();

                this.lastFrameCapture = this.context.getImageData(0, 0, this.width, this.height);
            } else {
                // gather current loop contents
                this.layers.reset();
                this.context.putImageData(this.lastFrameCapture, 0, 0);
                this.pointerLayer();

                // draw cursor layer to canvas
                this.drawLayer(Specificity.POINTER);
            }
        } else {
            this.paintBeforePause = true;

            // gather current loop contents
            this.layers.reset();
            this.content();

            // draw layers to canvas
            this.draw();
        }

        // on to next paint cycle
        this.previousTimestamp = timestamp;

        requestAnimationFrame(timestamp => { this.run(timestamp); });
    }

    /**
     * Actually paint all layers on the screen.
     */
    draw() {
        this.layers.each(layer => {
            layer.contents.forEach(callback => callback());
        });
    }

    /**
     * Paint only a single layer onto the screen. Silent error mode.
     */
    drawLayer(specificity) {
        if(!this.layers.layers[specificity]) {
            return;
        }

        this.layers.layers[specificity].contents.forEach(callback => callback());
    }

    /**
     * Gather contents for current paint cycle
     */
    content() {
        this.layers.layer(Specificity.BACKGROUND).addContent(() => {
            // re-draw background
            this.context.fillStyle = this.backgroundColor;
            this.context.fillRect(0, 0, this.width, this.height);
        });

        this.objects.each(canvasObject => {
            this.layers.layer(canvasObject.specificity).addContent(() => {
                canvasObject.run();

                /* TODO: This should be moved to the objects themselves
                // Check if mouse cursor is above this object, if yes, set to active state, else remove last state
                if(canvasObject.isPointPartOfObject(this.pointer) && canvasObject.state == ObjectState.idle) {
                    canvasObject.state = ObjectState.focus;
                } else {
                    canvasObject.state = ObjectState.idle;
                }
                */
            });
        });

        if(this.debug) {
            // …
        }

        this.pointerLayer();
    }

    /**
     * Paint a custom pointer cursor, if set.
     *
     * This layer (with specificity `Specificity.POINTER`) gets special treament,
     * as it is the only one rendered when the main loop is paused.
     */
    pointerLayer() {
        if(this.options.pointer !== false && this.context.canvas.matches(':hover')) {
            this.layers.layer(Specificity.POINTER).addContent(() => {
                this.options.pointer(this.context, this.percentagePointToPixelPoint(this.pointer));
            });
        }
    }

    /**
     * Turn a percentage value into a pixel value, based on the width or height of the canvas.
     * 
     * @param {Number} value
     * @param {String} widthOrHeight Either "width" or "height"
     */
    percentageToPixel(value, widthOrHeight = false) {
        let total;
        if(!widthOrHeight) {
            total = Math.min(this.width, this.height);
        } else {
            total = this[widthOrHeight];
        }

        return total / 100 * value;
    }

    /**
     * Return a new Point instance with pixel coordinates,
     * calculated from a given Point and the canvas width and height.
     * 
     * @param {Point} point
     */
    percentagePointToPixelPoint(point) {
        return new Point(
            this.percentageToPixel(point.x, "width"),
            this.percentageToPixel(point.y, "height")
        );
    }

    /**
     * Turn a pixel value into a percentage value, based on the width or height of the canvas.
     * 
     * @param {Number} value
     * @param {String} widthOrHeight Either "width" or "height"
     */
    pixelToPercentage(value, widthOrHeight = false) {
        let total;
        if(!widthOrHeight) {
            total = Math.min(this.width, this.height);
        } else {
            total = this[widthOrHeight];
        }

        return value / (total / 100);
    }

    /**
     * Return a new Point instance with percentage coordinates,
     * calculated from a given pixel Point and the canvas width and height.
     * 
     * @param {Point} point
     */
    pixelPointToPercentagePoint(point) {
        return new Point(
            this.pixelToPercentage(point.x, "width"),
            this.pixelToPercentage(point.y, "height")
        );
    }

    /**
     * Check if the given point is still on canvas
     * 
     * @param {Point} point
     * @return {Boolean}
     */
    isValidPoint(point) {
        return point.x >= 0 && point.x <= 100 && point.y >= 0 && point.y <= 100;
    }

    onPageConsole() {
        let originalConsole = window.console;
        let overriddenConsole = {};

        // Add a PRE element below the canvas to be used as on-page-console
        let pre = document.createElement("pre");
        pre.className = "console";
        pre.style.fontSize = "1.3em";
        this.canvas.after(pre);

        Object.keys(originalConsole).forEach(key => {
            overriddenConsole[key] = (...funcArgs) => {
                originalConsole[key](...funcArgs);
                funcArgs.forEach(arg => {
                    if(typeof arg == "string" || typeof arg == "undefined" || typeof arg == "number" || typeof arg == "bigint") {
                        pre.innerText += arg + " ";
                    } else if(typeof arg == "boolean") {
                        pre.innerText += arg ? "true " : "false ";
                    } else {
                        pre.innerText += `${arg.constructor.name} ${JSON.stringify(arg, null, 2).replace(/"([^"]+)":/g, '$1:')}\n`;
                    }
                });
                pre.innerText += "\n";
            };
        })

        window.console = overriddenConsole;
    }
}
