import { Point } from "../../../point.js";
import { GameSpecificity } from "../constants.js";
import { GameObject } from "./game-object.js";

export class Terrain extends GameObject {
    /**
     * @param {Number} height A float between 0 and 1 to indicate the terrain elevation
     */
    constructor(height) {
        super(true, false, {}, GameSpecificity.BACKGROUND);
        this.height = height;
        this.color = this.calculateColor();
        this.neighbours = [];
        this.hasDifferentNeighbour = false;

        //this.actions.addAction(this.setNeighbours);
    }

    /**
     * Base version calculates color based on type and elevation.
     * May be overloaded to change coloring or when more types are added.
     *
     * @return {string} A color in a valid CSS color format
     */
    calculateColor() {
        let color;
        let rgb = Math.round(255 * this.height);

        if(this instanceof Water || this instanceof River) {
            color = `rgba(0, 0, ${rgb * 2}, 1)`;
        } else if(this instanceof Mountain) {
            let mountainRGB = Math.round(rgb - (255 * 100 / rgb));
            color = `rgba(${Math.round(mountainRGB / 1.5)}, ${mountainRGB}, ${mountainRGB}, 1)`;
        } else {
            color = `rgba(0, ${255 - rgb / 2}, 0, 1)`;
        }

        return color;
    }

    /**
     * Runs once after all Terrain tiles were set in the matrix.
     */
    setNeighbours(position, game) {
        let neighbours = {
            west: new Point(position.x - 1, position.y),
            east: new Point(position.x + 1, position.y),
            north: new Point(position.x, position.y - 1),
            south: new Point(position.x, position.y + 1),
        };

        for(let direction in neighbours) {
            if(game.matrix.isValidPoint(neighbours[direction]) && game.matrix.isPassable(neighbours[direction])) {
                this.neighbours[direction] = game.matrix.getValue(neighbours[direction], Terrain);
                if(this.constructor.name != neighbours[direction].constructor.name) {
                    this.hasDifferentNeighbour = true;
                }
            } else {
                this.neighbours[direction] = neighbours[direction];
            }
        }
    }
}


export class Void {
}

export class Water extends Terrain {
}

export class Land extends Terrain {
}

export class Mountain extends Terrain {
}

export class River extends Terrain {
}
