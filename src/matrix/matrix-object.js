import { Specificity } from "../constants.js";

/**
 * An object that can be placed on a field in the matrix.
 *
 * This is the abstract base class.
 * Extend this object to create types to be placed in the matrix.
 */
export class MatrixObject {

    /**
     * @param {Boolean} passable Can this object be stepped on (e.g. by a controllable cursor or path finding algorithm)?
     * @param {Boolean} movable Can this object be moved to another point in the Matrix or is it static?
     * @param {Number} specificity The layer specificity for this object; may be used in renderers to stack objects in a certain order.
     *                             Lower numbers are in background, higher numbers are in foreground.
     */
    constructor(passable = true, movable = false, specificity = Specificity.DEFAULT) {
        /**
         * Determine if the cursor can pass a field with this object on it.
         */
        this.passable = passable;

        /**
         * Determine if this object can be moved to another field in the matrix.
         */
        this.movable = movable;

        this.specificity = specificity;
    }

    /**
     * A method that is called once on each MurkelCanvas main loop iteration.
     * Overload to implement an objects behavior.
     * 
     * @var {Matrix} matrix An instance of the Matrix class
     * @todo change x & y to Point
     */
    run(matrix, x, y) {

    }
}

export class Space extends MatrixObject {
    constructor(specificity = Specificity.BACKGROUND) {
        super(true, false, specificity);
    }
}

export class Wall extends MatrixObject {
    constructor(color = "#c90", specificity = Specificity.DEFAULT) {
        super(false, false, specificity);
        this.color = color;
    }
}

export class Cursor extends MatrixObject {
    constructor(color = "#a6a", specificity = Specificity.CURSOR) {
        super(false, false, specificity);
        this.color = color;
    }
}

export class Collectable extends MatrixObject {
    constructor(specificity = Specificity.DEFAULT) {
        super(true, false, specificity);
        this.collectable = true;
    }
}

export class Marker extends MatrixObject {
    constructor(color = "rgba(255, 0, 255, 0.5)", size = 0.33, specificity = Specificity.DEFAULT + 10) {
        super(true, false, specificity);
        this.color = color;
        this.size = size;
    }
}
