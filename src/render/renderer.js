
export class Renderer {
    /**
     * @param {MurkelCanvas} canvasObject
     * @param {Number} canvasObject
     * @param {Number} canvasObject
     */
    constructor(canvasObject) {
        this.canvasObject = canvasObject;
        this.renderer = {};
    }

    register(contents, method) {
        this.renderer[contents] = method;
    }

    render(matrixObject, x, y) {
        let objectType = matrixObject.constructor.name;

        let pixelX = x * this.canvasObject.gridSizeX + this.canvasObject.offsetX;
        let pixelY = y * this.canvasObject.gridSizeY + this.canvasObject.offsetY;

        if(!this.renderer[objectType]) {
            this.default(pixelX, pixelY);
            //throw new Error(`Type ${objectType} is not implemented by this renderer!`);
        } else {
            this.renderer[objectType](
                this.canvasObject.context,
                pixelX, pixelY,
                this.canvasObject.gridSizeX,
                this.canvasObject.gridSizeY,
                matrixObject
            );
        }
    }

    supports(contents) {
    	if(this.renderer[contents]) {
    		return true;
    	} else {
    		return false;
    	}
    }

    default(x, y) {
        // Do nothing
    }
}
