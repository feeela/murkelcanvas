
export class ActionGeneratorAction {
    constructor(actionCallback) {
        this.callback = actionCallback;
        this.done = false;
    }
}

/**
 * Generates actions from a given list.
 *
 * An action is a callback that is invoked repeatedly as long as it returns `false`.
 * Return `true` from an action callback to indicate that is done, after which
 * the next action from the list is executed.
 * When all actions have run once, the list resets to the first action again.
 */
export class ActionGenerator {

    constructor() {
        this.actions = [];
    }

    /**
     * @param {Function} callback
     */
    addAction(callback) {
        this.actions.push(new ActionGeneratorAction(callback));
        this.generator = false;
    }

    /**
     * Remove a action from the internal list.
     *
     * @param {Function} callback
     * @return {Boolean} Returns `false` if the given action object was not added before.
     */
    removeAction(callback) {
        let index = this.actions.findIndex(data => data.callback === callback);
        if(index != -1) {
            // remove this item from the list of actions
            this.actions.splice(index, 1);
            this.generator = false;
            return true;
        } else {
            return false;
        }
    }

    removeAll() {
        this.actions = [];
        this.generator = false;
    }

    *actionGenerator() {
        let actions = [...this.actions];
        let currentAction;

        while(true) {
            if(!currentAction || currentAction.done == true) {
                currentAction = actions.shift();
            }

            if(actions.length == 0) {
                actions = [...this.actions];
            }

            yield currentAction;
        }
    }

    getAction() {
        let actions = this.actions;
        if(actions.length == 0) {
            return false;
        }

        // Exception to avoid the generator loop if there is only one action
        if(actions.length == 1) {
            return actions[0];
        }

        if(!this.generator) {
            this.generator = this.actionGenerator();
        }

        return this.generator.next().value;
    }

    runAction(thisObject, ...actionArguments) {
        let action = this.getAction();
        if(action !== false) {
            action.done = action.callback.call(thisObject, ...actionArguments);
        }
    }
}
