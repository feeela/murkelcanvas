import { Renderer } from "../../render/renderer.js";
import {
    getBricksTile,
    getDoorTile,
    getFloorTile,
    getBoxTile,
    getPlayerTile,
    getKeyTile,
    getBoulderTile,
    getMonsterTile
} from "../pattern.js";
import * as GameObjects from "./boulder-game-objects.js";

/**
 * @var {2DRenderContext} 
 */
export function getRenderer(canvasObject) {
 	let renderer = new Renderer(canvasObject);

    const icons = {};

    renderer.register(GameObjects.Space.name, (context, x, y, width, height) => {
        if(!icons.floor) {
    		icons.floor = getFloorTile(width, height);
    	}
        context.drawImage(icons.floor, x, y, width, height);
    });

    renderer.register(GameObjects.Wall.name, (context, x, y, width, height) => {
    	if(!icons.bricks) {
    		icons.bricks = getBricksTile(width, height);
    	}
        context.drawImage(icons.bricks, x, y, width, height);
    });

    renderer.register(GameObjects.Entrance.name, (context, x, y, width, height) => {
    	if(!icons.door) {
    		icons.door = getDoorTile(width, height);
    	}
        context.drawImage(icons.door, x, y, width, height);
    });

    renderer.register(GameObjects.Exit.name, (context, x, y, width, height, exitObject) => {
        if(!exitObject.passable) {
            if(!icons.doorLocked) {
                icons.doorLocked = getDoorTile(width, height, true);
            }
            context.drawImage(icons.doorLocked, x, y);

        } else {
            if(!icons.door) {
                icons.door = getDoorTile(width, height);
            }
            context.drawImage(icons.door, x, y, width, height);
        }
    });

    renderer.register(GameObjects.Box.name, (context, x, y, width, height) => {
        if(!icons.box) {
            icons.box = getBoxTile(width, height);
        }
        context.drawImage(icons.box, x, y, width, height);
    });

    renderer.register(GameObjects.Boulder.name, (context, x, y, width, height) => {
        if(!icons.boulder) {
            icons.boulder = getBoulderTile(width, height);
        }
        context.drawImage(icons.boulder, x, y, width, height);
    });

    renderer.register(GameObjects.Key.name, (context, x, y, width, height) => {
        let keyShrink = width / 4;
        if(!icons.key) {
            icons.key = getKeyTile(width - keyShrink, height - keyShrink);
        }
        context.drawImage(icons.key, x + keyShrink / 2, y + keyShrink / 2, width - keyShrink, height - keyShrink);
    });

    renderer.register(GameObjects.Cursor.name, (context, x, y, width, height) => {
        let border = width / 15;
        let gap = Math.min(width, height) / 5;

        context.lineWidth = border;
        context.strokeStyle = "rgba(90, 90, 90, 0.8)";
        context.lineCap = "round";

        context.beginPath();
        context.moveTo(x + gap, y + gap);
        context.lineTo(x + width - gap, y + height - gap);
        context.stroke();

        context.beginPath();
        context.moveTo(x + gap, y + height - gap);
        context.lineTo(x + width - gap, y + gap);
        context.stroke();

        context.strokeRect(x + gap, y + gap, width - gap * 2, height - gap * 2);
    });

    renderer.register(GameObjects.Player.name, (context, x, y, width, height, playerObject) => {
        if(!icons["player" + playerObject.direction]) {
            icons["player" + playerObject.direction] = getPlayerTile(width, height, playerObject.direction);
        }
        context.drawImage(icons["player" + playerObject.direction], x, y, width, height);
    });

    renderer.register(GameObjects.Monster.name, (context, x, y, width, height, monsterObject) => {
        if(!icons.monster) {
            icons.monster = getMonsterTile(width, height);
        }
        context.drawImage(icons.monster, x, y, width, height);
        /* differently colored monsters
        if(!icons["monster" + monsterObject.color]) {
            icons["monster" + monsterObject.color] = getMonsterTile(width, height, monsterObject.color);
        }
        context.drawImage(icons["monster" + monsterObject.color], x, y);
        */
    });

    return renderer;
}
