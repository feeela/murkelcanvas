import { getRandomInt } from "../../helper/helper.js";
import { Direction, Specificity } from "../../constants.js";
import { Text } from "../../text.js";
import { MatrixObject } from "../../matrix/matrix-object.js";
import { Behavior, ObjectWithBehavior } from "../../matrix/matrix-object-behavior.js";

/**
 * Two state square tile for Langton's ant; has states BLACK & WHITE
 */
export class AntSquare  extends MatrixObject {

    static COLOR = {
        BLACK: "#444",
        WHITE: "#f4f4f4",
    }

    /**
     * @param {string} color If set to `false` (default) then a random color from `CivTile.colors` is chosen.
     */
    constructor(color = AntSquare.COLOR.WHITE) {
        super(true, false, Specificity.BACKGROUND + 1);
        this.color = color;
    }

    flip() {
        if(this.color == AntSquare.COLOR.BLACK) {
            this.color = AntSquare.COLOR.WHITE;
        } else {
            this.color = AntSquare.COLOR.BLACK;
        }
    }
}


export class LangtonsAntBehavior extends Behavior {
    constructor() {
        super();
        this.steps = 0;
    }

    run(matrixCanvas, position, matrixObject) {
        let square = matrixCanvas.matrix.getValue(position, AntSquare);
        if(square.color == AntSquare.COLOR.WHITE) {
            // Turn ant 90° clockwise
            matrixObject.direction = Direction.clockwise(matrixObject.direction);
        } else {
            // Turn ant 90° counter-clockwise
            matrixObject.direction = Direction.clockwise(Direction.clockwise(Direction.clockwise(matrixObject.direction)));
        }

        // Flip color
        square.flip();

        // Move one step forward
        let target = position.move(matrixObject.direction);
        let hasMoved = matrixCanvas.matrix.moveValue(position, target, LangtonsAnt);

        // Could not move further because edge of screen was reached
        if(!hasMoved) {
            matrixObject.removeBehavior(this);
            return;
        }

        matrixCanvas.cursor = target;

        this.steps++;
    }
}

export class LangtonsAnt extends ObjectWithBehavior {

    /**
     * @param {string} color If set to `false` (default) then a random color from `LangtonsAnt.colors` is chosen.
     */
    constructor(color = "#930", direction = Direction.UP, speed = 250) {
        super(speed);

        this.color = color;
        this.direction = direction;

        this.antBehavior = new LangtonsAntBehavior();
        this.addBehavior(this.antBehavior);
    }

    run(matrixCanvas, x, y) {
        super.run(matrixCanvas, x, y);

        // Display current number of steps
        matrixCanvas.layers.layer(Specificity.FOREGROUND).addContent(() => {
            let text = new Text(
                matrixCanvas.context,
                this.antBehavior.steps + " steps",
                matrixCanvas.gridSizeX / 3,
                matrixCanvas.gridSizeY / 6,
                {
                    size: matrixCanvas.height / 40,
                    background: "rgba(255, 255, 255, 0.75)",
                }
            );
            text.draw();
        });
    }
}
