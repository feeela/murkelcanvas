import { Point } from "../point.js";
import { Specificity } from "../constants.js";
import { MatrixObject } from "../matrix/matrix-object.js";


export class Behavior {
    constructor() {
        this.reset();
    }

    /**
     * @var {Matrix} matrix The Matrix object
     * @var {Point} currentPoint The point passed to MatrixObject.run()
     * @var {MatrixObject} self The object that is using this behavior
     */
    run(matrix, currentPoint, self) {}

    /**
     * Used to reset a behavior to its initial state.
     * Can be used after all behaviors have run once.
     */
    reset() {}
}

export class ObjectWithBehavior extends MatrixObject {
    constructor(speed = 150, passable = true, movable = false, specificity = Specificity.DEFAULT) {
        super(passable, movable, specificity);

        // Object speed
        this.speed = speed;
        this.previousTimestamp = false;
        this.isPaused = false;

        this.behaviors = [];
    }

    addBehavior(behavior, runtime = 0) {
        this.behaviors.push({behavior, runtime});
        this.generator = false;
    }

    /**
     * Remove a behavior from the internal list of this `MatrixObject`.
     *
     * @param {Behavior} behaviorObject The behavior object to remove 
     * @return {Boolean} Returns `false` if the given behavior object was not added before.
     */
    removeBehavior(behaviorObject) {
        let index = this.behaviors.findIndex(data => data.behavior === behaviorObject);
        if(index != -1) {
            // remove this item from the list of behaviors
            this.behaviors.splice(index, 1);
            this.generator = false;
            return true;
        } else {
            return false;
        }
    }

    *behaviorGenerator() {
        let behaviors = [...this.behaviors];
        let currentBehavior;
        let behaviorStartTime = performance.now();

        while(true) {
            let timeLapsed = performance.now() - behaviorStartTime;

            if(!currentBehavior || timeLapsed > currentBehavior.runtime) {
                currentBehavior = behaviors.shift();
                behaviorStartTime = performance.now();
            }

            if(behaviors.length == 0) {
                behaviors = [...this.behaviors];
                this.behaviors.forEach(b => b.behavior.reset());
            }

            yield currentBehavior.behavior;
        }
    }

    getBehavior() {
        if(this.behaviors.length == 0) {
            return false;
        }

        // Exception to avoid the generator loop if there is only one behavior
        if(this.behaviors.length == 1) {
            return this.behaviors[0].behavior;
        }

        if(!this.generator) {
            this.generator = this.behaviorGenerator();
        }

        return this.generator.next().value;
    }

    run(matrix, x, y) {
        let timestamp = performance.now();

        // Wait for object tick
        if(this.isPaused || (this.previousTimestamp !== false && timestamp < (this.previousTimestamp + this.speed))) {
            return;
        }

        let currentlyRunningBehavior = this.getBehavior();
        if(currentlyRunningBehavior !== false) {
            currentlyRunningBehavior.run(matrix, new Point(x, y), this);
        }

        this.previousTimestamp = timestamp;
    }
}
