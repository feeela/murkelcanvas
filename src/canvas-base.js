
/**
 * Canvas base class.
 * 
 * Holds only functionality that is required on all canvas applications.
 * Currently this class merely cares about the correct scaling of the canvas
 * when displayed on a high resolution screen (i.e. retina screens).
 */
export class CanvasBase {
    /**
     * @param {HTMLCanvasElement|string} canvas A canvas DOM element or a CSS selector to find one in the current page
     */
    constructor(canvas, contextType = "2d") {

        if(canvas instanceof HTMLCanvasElement) {
            this.canvas = canvas;
        } else {
            this.canvas = document.querySelector(canvas);
            if(!(this.canvas instanceof HTMLCanvasElement)) {
                throw new Error(`Canvas element "${canvas}" not found in page.`);
            }
        }

        this.contextType = contextType;
        this.context = this.canvas.getContext(contextType);

        // Use CSS sizing for canvas
        let canvasDimensions = this.context.canvas.getBoundingClientRect();
        this.width = canvasDimensions.width;
        this.height = canvasDimensions.height;
        // …and scale the canvas according to the device pixel ratio to prevent blurry rendering on retina devices
        let scale = contextType == "2d" ? window.devicePixelRatio : 1;
        this.canvas.width = canvasDimensions.width * scale;
        this.canvas.height = canvasDimensions.height * scale;
        this.canvas.style.width = `${canvasDimensions.width}px`;
        this.canvas.style.height = `${canvasDimensions.height}px`;
        if(contextType == "2d") {
            this.context.scale(scale, scale);
        }
    }

    /**
     * Check if the given point is still on canvas
     * 
     * @param {Point} point
     * @return {Boolean}
     */
    isValidPoint(point) {
        return point.x >= 0 && point.x < this.width && point.y >= 0 && point.y < this.height;
    }

    clear() {
        if(this.contextType = "webgl") {
            // Set clear color to white, fully opaque
            this.context.clearColor(1, 1, 1, 1);
            // Clear the color buffer with specified clear color
            this.context.clear(this.context.COLOR_BUFFER_BIT);
        }
    }
}
