import { Point, Specificity } from '../js/lib-point.js';
import { Text } from '../js/text.js';
import { extend, getRandomInt, shuffle } from "../js/helper.js"
import { Space, Wall } from '../js/matrix.js';
import { MatrixCanvas } from "../js/matrix-canvas.js";


class PartitionData {
    constructor(xMin, xMax, yMin, yMax, parentDoorPositions = null) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.parentDoorPositions = parentDoorPositions;
    }
}

class DrawDoorsData {
    constructor(doorPositions) {
        this.doorPositions = doorPositions;
    }
}

/**
 * Example usage for MatrixMoveIt
 */
export class MazeVisusalization extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        paintSpeed: 500,
        cursor: false,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        // generate maze
        this.minimumGapBetweenWalls = options.minimumGap || 1;
        this.spaceObject = new Space();
        this.wallObject = new Wall();
        this.step = 0;

        // Point data to keep between distinct steps
        this.drawDoorsData = null;
        this.partitionData = [];

        // Used for display only
        this.usedRandomCoordinatesForWalls = [];
    }

    content() {
        super.content();

        if(this.step == 0) {
            /* place initial walls */
            this.addWallsToEdges();

            this.partitionData = [
                new PartitionData(1, this.cols - 2, 1, this.rows - 2),
            ];

        } else if(this.drawDoorsData instanceof DrawDoorsData) {
            this.drawDoors(this.drawDoorsData);
            this.drawDoorsData = null;
        } else if(this.partitionData.length > 0) {
            let currentPartitionData = this.partitionData.shift();
            let randomXY = this.partition(currentPartitionData);
            if(randomXY !== false) {
                this.usedRandomCoordinatesForWalls.push(randomXY);
            }
        }

        this.step++;

        this.layers.layer(Specificity.FOREGROUND).addContent(() => {
            this.context.font = `${this.gridSizeY / 8 * 4}px sans-serif`;
            this.context.textAlign = "center";
            this.context.textBaseline = "middle";
            this.context.fillStyle = "#333";
            this.usedRandomCoordinatesForWalls.forEach(point => {
                this.context.fillText(
                    `${point.x}/${point.y}`,
                    point.x * this.gridSizeX + this.gridSizeX / 2,
                    point.y * this.gridSizeY + this.gridSizeY / 1.8
                );
            });

            if(this.partitionData.length) {
                let text = new Text(this.context, `${this.partitionData.length} partition sections to process`, this.gridSizeX / 3, this.gridSizeY / 8, {
                    size: this.gridSizeY / 5 * 3,
                    background: "rgba(255, 255, 255, 0.75)",
                });
                text.draw();
            }
        });
    }

    drawHorizontalWall(y, xMin, xMax) {
        for(let x = xMin; x <= xMax; x++) {
            this.matrix.setValue(new Point(x, y), this.wallObject);
        }
    }

    drawVerticalWall(x, yMin, yMax) {
        for(let y = yMin; y <= yMax; y++) {
            this.matrix.setValue(new Point(x, y), this.wallObject);
        }
    }

    addWallsToEdges() {
        this.drawHorizontalWall(0, 0, this.cols - 1);
        this.drawHorizontalWall(this.rows - 1, 0, this.cols - 1);
        this.drawVerticalWall(0, 0, this.rows - 1);
        this.drawVerticalWall(this.cols - 1, 0, this.rows - 1);
    }

    getRandomIntExluding(min, max, exclude = []) {
        if(min > max || (exclude.includes(min) && exclude.includes(max))) {
            // Nope, not possible
            return false;
        }

        let randomInt;
        do {
            randomInt = getRandomInt(min, max);
        } while(exclude.includes(randomInt));

        return randomInt;
    }

    partition(partitionData) {
        this.layers.layer(Specificity.FOREGROUND - 3).addContent(() => {
            this.context.fillStyle = "rgba(0, 0, 255, .5)";
            this.context.fillRect(
                partitionData.xMin * this.gridSizeX,
                partitionData.yMin * this.gridSizeY,
                partitionData.xMax * this.gridSizeX - partitionData.xMin * this.gridSizeX + this.gridSizeX,
                partitionData.yMax * this.gridSizeY - partitionData.yMin * this.gridSizeY + this.gridSizeY
            );
        });

        if((partitionData.xMax - partitionData.xMin) <= this.minimumGapBetweenWalls || (partitionData.yMax - partitionData.yMin) <= this.minimumGapBetweenWalls) {
            return false;
        }

        // Generate two random walls
        let randomXY;
        if(!partitionData.parentDoorPositions) {
            randomXY = new Point(
                getRandomInt(partitionData.xMin + this.minimumGapBetweenWalls, partitionData.xMax - this.minimumGapBetweenWalls),
                getRandomInt(partitionData.yMin + this.minimumGapBetweenWalls, partitionData.yMax - this.minimumGapBetweenWalls)
            );
        } else {
            // Exclude certain x & y positions when partioning to keep doors free
            let excludeX = [], excludeY = [];
            partitionData.parentDoorPositions.forEach(doorPosition => {
                if(doorPosition.x < partitionData.xMin - 1
                    || doorPosition.x > partitionData.xMax + 1
                    || doorPosition.y < partitionData.yMin - 1
                    || doorPosition.y > partitionData.yMax + 1
                ) {
                    // Skip, this door has nothing to do with the current section
                    return;
                }

                if(doorPosition.x == partitionData.xMin - 1) {
                    // door in left wall
                    excludeY.push(doorPosition.y);
                } else if(doorPosition.x == partitionData.xMax + 1) {
                    // door in right wall
                    excludeY.push(doorPosition.y);
                } else if(doorPosition.y == partitionData.yMin - 1) {
                    // door in top wall
                    excludeX.push(doorPosition.x);
                } else if(doorPosition.y == partitionData.yMax + 1) {
                    // door in bottom wall
                    excludeX.push(doorPosition.x);
                }
            });

            this.layers.layer(Specificity.FOREGROUND - 2).addContent(() => {
                excludeX.forEach(noX => {
                    this.context.fillStyle = "rgba(255, 0, 0, .5)";
                    this.context.fillRect(
                        noX * this.gridSizeX,
                        partitionData.yMin * this.gridSizeY,
                        this.gridSizeX,
                        partitionData.yMax * this.gridSizeY - partitionData.yMin * this.gridSizeY + this.gridSizeY
                    );
                });
                excludeY.forEach(noY => {
                    this.context.fillStyle = "rgba(255, 0, 0, .5)";
                    this.context.fillRect(
                        partitionData.xMin * this.gridSizeX,
                        noY * this.gridSizeY,
                        partitionData.xMax * this.gridSizeX - partitionData.xMin * this.gridSizeX + this.gridSizeX,
                        this.gridSizeY
                    );
                });
            });

            randomXY = new Point(
                this.getRandomIntExluding(partitionData.xMin + this.minimumGapBetweenWalls, partitionData.xMax - this.minimumGapBetweenWalls, excludeX),
                this.getRandomIntExluding(partitionData.yMin + this.minimumGapBetweenWalls, partitionData.yMax - this.minimumGapBetweenWalls, excludeY)
            );

            if(randomXY.x === false || randomXY.y === false) {
                // No more walls will fit here
                return false;
            }
        }

        this.drawHorizontalWall(randomXY.y, partitionData.xMin, partitionData.xMax);
        this.drawVerticalWall(randomXY.x, partitionData.yMin, partitionData.yMax);

        let doors = shuffle([true, true, true, false]);
        let doorPositions = [];
        if(doors[0]) {
            // door left
            doorPositions.push(new Point(getRandomInt(partitionData.xMin, randomXY.x - 1), randomXY.y));
        }
        if(doors[1]) {
            // door right
            doorPositions.push(new Point(getRandomInt(randomXY.x + 1, partitionData.xMax - 1), randomXY.y));
        }
        if(doors[2]) {
            // door top
            doorPositions.push(new Point(randomXY.x, getRandomInt(partitionData.yMin, randomXY.y - 1)));
        }
        if(doors[3]) {
            // door bottom
            doorPositions.push(new Point(randomXY.x, getRandomInt(randomXY.y + 1, partitionData.yMax - 1)))
        }
        this.drawDoorsData = new DrawDoorsData(doorPositions);
        if(partitionData.parentDoorPositions) {
            doorPositions = [...doorPositions, ...partitionData.parentDoorPositions];
        }

        // Set the created four sections as new partition data for next iteration
        // upper left
        this.partitionData.push(new PartitionData(partitionData.xMin, randomXY.x - 1, partitionData.yMin, randomXY.y - 1, doorPositions));
        // upper right
        this.partitionData.push(new PartitionData(randomXY.x + 1, partitionData.xMax, partitionData.yMin, randomXY.y - 1, doorPositions));
        // lower left
        this.partitionData.push(new PartitionData(partitionData.xMin, randomXY.x - 1, randomXY.y + 1, partitionData.yMax, doorPositions));
        // lower right
        this.partitionData.push(new PartitionData(randomXY.x + 1, partitionData.xMax, randomXY.y + 1, partitionData.yMax, doorPositions));

        return randomXY;
    }

    drawDoors(drawDoorsData) {
        drawDoorsData.doorPositions.forEach(point => {
            this.matrix.setValue(point, this.spaceObject);
        });
    }
}
