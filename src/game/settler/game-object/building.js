import { extend, getRandomInt } from "../../../helper/helper.js";
import { GameSpecificity, Goods } from "../constants.js";
import { GameObjectWithActions, CivArea, Settler } from "./game-object.js";
import { TerrainFeature, Forest, TreeNursery, Rock, Wheat, Path } from "./terrain-feature.js";
import { ResourcesDepletedEvent } from "../events.js";

// for debugging
import { Marker } from "../../../matrix/matrix-object.js";
import { toRGB, rgbValuesAsColorString } from "../../../helper/color.js";


export class Building extends GameObjectWithActions {
    static defaultOptions = extend(super.defaultOptions, {
        borderRadius: 4,
        speed: 75,
    });

    /**
     * A list of Goods that are required to build this building.
     * @type {Object} requires A list Goods as keys and amounts as values; like Civilization.storage.
     */
    static requires = {};

    constructor(civilization, kontor, position, options = {}) {
        super(civilization.game, false, false, options, options.specificity || GameSpecificity.BUILDING);

// Should not occur; must be checked in Civilization.addBuilding()
if(!position || !civilization.game.matrix.isValidPoint(position)) {
    console.log("position", position);
    throw new Error("Cannot build at this position.");
}

        this.civilization = civilization;
        this.kontor = kontor; // Can be false, which usually means, this is a Kontor itself
        this.position = position;

        this.pathTo = {};

        if(this.kontor) {
            this.civilization.astar.targetPointValuesTemporaryPassable(this.kontor.position, () => {
                this.pathTo[Kontor.name] = this.civilization.findPathWithinTerritory(this.position, this.kontor.position);
            });
        }
    }

    /**
     * Removes other objects, that were created by this building
     */
    cleanup() {
        for(let i in this.pathTo[Kontor.name]) {
            let path = this.civilization.game.matrix.getValue(this.pathTo[Kontor.name][i], Path);

            // Only delete path segments, that were solely created for this building
            if(path == false || path.directions.length > 2) {
                return;
            }

            this.civilization.game.matrix.removeValue(this.pathTo[Kontor.name][i], Path)
        }
    }
}

export class Tower extends Building {
    static defaultOptions = extend(super.defaultOptions, {
        borderRadius: 4,
    });

    static requires = {
        [Goods.Wood]: 20,
        [Goods.Stone]: 10,
    };

    constructor(civilization, kontor = false, position, options = {}, specificity = GameSpecificity.BUILDING) {
        super(civilization, false, position, options, specificity);

        this.civilization.addArea(this.position.midpointCircle(this.options.borderRadius, true), this);
    }
}

export class Kontor extends Tower {
    static requires = {
        [Goods.Wood]: 30,
        [Goods.Stone]: 20,
    };

    constructor(civilization, kontor = false, position, options = {}, specificity = GameSpecificity.BUILDING) {
        super(civilization, false, position, options, specificity);
    }
}

export class Harvester extends Building {
    static defaultOptions = extend(super.defaultOptions, {
            borderRadius: 6,
            type: false,
            resource: Goods.Wood,
            resourceRetryTime: false,
            harvestTime: 1500,
        });

    constructor(civilization, kontor, position, options = {}) {
        super(civilization, kontor, position, options);

        if(!this.options.type) {
            throw new Error("A Harvester object must have `options.type` set to some TerrainFeature constructor.");
        }

        // How many collected on each run to the resource
        this.collected = 0;
        this.possibleRessources = this.getPossibleResources(this.options.type, this.options.borderRadius);

        this.addAction(this.getResource);
        this.addAction(this.walkToResource);
        this.addAction(this.harvest);
        this.addAction(this.walkHome);

        // Spawn a Settler belonging to this building; each Harvester has one.
        this.settler = new Settler(this.civilization.color);
        this.civilization.game.matrix.addValue(this.position, this.settler);
    }

    /**
     * Removes other objects, that were created by this building
     */
    cleanup() {
        this.paused = true;
        this.civilization.game.matrix.removeValue(this.currentWorkerPos, Settler);

        super.cleanup();
    }

    getPossibleResources(type, radius) {
        let possibleRessources = this.civilization.findTypeFields(type, this.position, 0, radius);

        if(this.options.debug) {
            // debug; can be reused to highlight building radius while constructing it
            let rgb = toRGB(this.civilization.color);
            rgb.a = rgb.b > 127 ? 0.4 : 0.6;
            possibleRessources.forEach((pr, i) => this.civilization.game.matrix.addValue(pr, new Marker(rgbValuesAsColorString(rgb), 0.5, GameSpecificity.AREA)))
        }

        // Reset current ressource position
        this.currentResourcePos = false;

        return possibleRessources;
    }

    getResource(x, y) {
        if(this.currentResourcePos !== false) {
            this.walkingPath = false;
            return true;
        }

        if(this.possibleRessources.length == 0) {
            // No more available ressources right now; so idle.
            this.paused = true;

            if(this.options.resourceRetryTime !== false) {
                // Try again later
                setTimeout(() => {
                    this.possibleRessources = this.getPossibleResources(this.options.type, this.options.borderRadius);
                    if(this.possibleRessources.length > 0) {
                        this.paused = false;
                    } else {
                        // Fail finally; this Harvester can't find any more ressources, even after waiting.
                        this.civilization.dispatchEvent(new ResourcesDepletedEvent(this));
                    }
                }, this.options.resourceRetryTime);
            } else {
                this.civilization.dispatchEvent(new ResourcesDepletedEvent(this));
            }

            return false;
        } else {
            this.currentResourcePos = this.possibleRessources.shift();
            this.pathTo[this.options.type.name] = this.civilization.findPathWithinTerritory(this.position, this.currentResourcePos);
            this.walkingPath = false;
            return true;
        }
    }

    walkToResource() {
        let done = this.walkPath([this.position, ...this.pathTo[this.options.type.name]]);
        if(done == true) {
            // used in  harvest()
            this.waitForHarvest = true;
            this.hasHarvestTimeOut = false;
        }
        return done;
    }

    harvest() {
        // Wait for `options.harvestTime`
        if(this.waitForHarvest == true) {
            if(!this.hasHarvestTimeOut) {
                this.hasHarvestTimeOut = true;
                setTimeout(() => {
                    this.waitForHarvest = false;
                }, this.options.harvestTime);
            }

            return false;
        }

        let resource = this.civilization.game.matrix.getValue(this.currentResourcePos, this.options.type);
        if(resource == false) {
            return true;
        }

        this.collected = resource.take();
        if(this.collected == 0) {
            this.civilization.game.matrix.removeValue(this.currentResourcePos, this.options.type);
            this.currentResourcePos = false;
        }

        return true;
    }

    walkHome() {
        let done = this.walkPath([this.position, ...this.pathTo[this.options.type.name]].toReversed());
        if(done == true) {
            this.civilization.storage.add(this.options.resource, this.collected);
        }
        return done;
    }

    walkPath(path) {
        if(!this.walkingPath) {
            this.walkingPath = [...path];
            this.currentWorkerPos = path.shift();
        }

        let nextWorkerPos = this.walkingPath.shift();

        this.civilization.game.matrix.moveValue(this.currentWorkerPos, nextWorkerPos, Settler);
        this.currentWorkerPos = nextWorkerPos;

        if(this.walkingPath.length == 0) {
            this.walkingPath = false;
            return true;
        } else {
            return false;
        }
    }
}

export class WoodCutter extends Harvester {
    static defaultOptions = extend(super.defaultOptions, {
        type: Forest,
        resource: Goods.Wood,
        // Retry to find new forests after two minutes; hopefully there is a TreeNursery nearby.
        //resourceRetryTime: 120000,
        resourceRetryTime: 10000,
    });

    static requires = {
        [Goods.Axe]: 1,
    };
}

export class StoneCutter extends Harvester {
    static defaultOptions = extend(super.defaultOptions, {
        type: Rock,
        resource: Goods.Stone,
        // Retry to find new rocks after two minutes; hopefully there was a new Kontor that expanded the territory.
        //resourceRetryTime: 120000,
        resourceRetryTime: 10000,
    });

    static requires = {
        [Goods.Wood]: 5,
        [Goods.Pickaxe]: 1,
    };
}

export class Planter extends Building {
    static defaultOptions = extend(super.defaultOptions, {
            borderRadius: 3,
            type: Forest,
            plantingTime: 1500,
            waitAtHomeTime: 3000,
            waitForFreeTileTime: 5000,
        });

    constructor(civilization, kontor, position, options = {}) {
        super(civilization, kontor, position, options);

        this.originalSpeed = this.options.speed;

        this.addAction(this.findFreeField);
        this.addAction(this.walkToFreeTile);
        this.addAction(this.plant);
        this.addAction(this.walkHome);
        this.addAction(this.waitAtHome);

        // Spawn a Settler belonging to this building; each Planter has one.
        this.currentWorkerPos = this.position;
        this.civilization.game.matrix.addValue(this.position, new Settler(this.civilization.color));
    }

    /**
     * Removes other objects, that were created by this building
     */
    cleanup() {
        this.paused = true;
        this.civilization.game.matrix.removeValue(this.currentWorkerPos, Settler);

        super.cleanup();
    }

    sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    findFreeField() {
        let freeFields = this.civilization.findFreeFields(this.position, 1, this.options.borderRadius);
        if(freeFields.length == 0) {
            this.paused = true;
            this.sleep(this.options.waitForFreeTileTime).then(() => { this.paused = false; });
            return false;
        }

        // Pick random one
        this.freeField = freeFields[getRandomInt(0, freeFields.length - 1)];

        let path = this.civilization.findPathWithinTerritory(this.position, this.freeField);
        this.walkingPath = false;

        if(path == false) {
            console.error("Planter could not find a path to free field.", this);
            /*
            this.civilization.area.forEach(ap => {
                this.civilization.game.matrix.addValue(this.freeField, new Marker("rgba(200, 200, 0, 0.5)", 0.5, GameSpecificity.FOREGROUND));
            });
            */
            this.civilization.game.matrix.addValue(this.freeField, new Marker("rgba(200, 0, 0, 0.5)", 0.5, GameSpecificity.FOREGROUND));
            this.paused = true;
            this.sleep(this.options.waitForFreeTileTime).then(() => { this.paused = false; });
            return false;
        } else {
            this.pathTo[this.options.type.name] = path;
            return true;
        }
    }

    walkToFreeTile() {
        let done = this.walkPath([this.position, ...this.pathTo[this.options.type.name]]);
        if(done == true) {
            // used in  plant()
            this.waitPlanting = true;
            this.hasPlantingTimeOut = false;
        }
        return done;
    }

    plant() {
        if(this.civilization.game.matrix.hasValue(this.freeField, TerrainFeature)) {
            return true;
        }

        // Wait for `options.plantingTime`
        if(this.waitPlanting == true) {
            if(!this.hasPlantingTimeOut) {
                this.hasPlantingTimeOut = true;
                setTimeout(() => {
                    this.waitPlanting = false;
                }, this.options.plantingTime);
            }

            return false;
        }

        this.civilization.game.matrix.addValue(this.freeField, new this.options.type(this.civilization.game, this.freeField));

        return true;
    }

    walkHome() {
        this.waitAtHome = true;
        this.waitAtHomeTimeOut = false;
        return this.walkPath([this.position, ...this.pathTo[this.options.type.name]].toReversed());;
    }

    waitAtHome() {
        if(this.waitAtHome == true) {
            if(!this.waitAtHomeTimeOut) {
                this.waitAtHomeTimeOut = true;
                setTimeout(() => {
                    this.waitAtHome = false;
                }, this.options.waitAtHomeTime);
            }

            return false;
        } else {
            this.waitAtHomeTimeOut = false;
            return true;
        }
    }

    walkPath(path) {
        if(!this.walkingPath) {
            this.walkingPath = [...path];
            this.currentWorkerPos = path.shift();
        }

        let nextWorkerPos = this.walkingPath.shift();

        this.civilization.game.matrix.moveValue(this.currentWorkerPos, nextWorkerPos, Settler);
        this.currentWorkerPos = nextWorkerPos;

        if(this.walkingPath.length == 0) {
            this.walkingPath = false;
            return true;
        } else {
            return false;
        }
    }
}

export class Forester extends Planter {
    static defaultOptions = extend(super.defaultOptions, {
        plantingTime: 2000,
        waitAtHomeTime: 6000,
        borderRadius: 5,
        type: TreeNursery,
    });

    static requires = {
        [Goods.Wood]: 5,
    };
}

export class Farm extends Planter {
    static defaultOptions = extend(super.defaultOptions, {
        borderRadius: 2,
        type: Wheat,
    });

    static requires = {
        [Goods.Stone]: 5,
        [Goods.Wood]: 15,
    };
}
