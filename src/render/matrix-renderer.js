import { getTempCanvas } from "../helper/helper.js";
import { Space, Wall, Cursor, Marker } from "../matrix/matrix-object.js";
import {
    Terrain,
    Forest, TreeNursery, Rock, Swamp, Ore, Iron, Gold, Coal,
    Civilization, Path, BorderPost, StoneCutter, Forester,
} from "../matrix/terrain.js";
import { Direction } from "../constants.js";
import { Point } from "../point.js";
import { Renderer } from "./renderer.js";
import { toRGB, rgbValuesAsColorString } from "../helper/color.js";
import {
    getForestTile,
    getBorderPostTile,
    getBoulderTile,
} from "../game/pattern.js";

export { Renderer };

/**
 * @var {2DRenderContext} 
 */
export function getRenderer(canvasObject) {
 	let defaultRenderer = new Renderer(canvasObject);

    let icons = [];

    /*
    defaultRenderer.register("Space", (context, x, y, width, height) => {
    });
    */

    defaultRenderer.register(Wall.name, (context, x, y, width, height, wallObject) => {
        context.fillStyle = wallObject.color;
        context.fillRect(x, y, width, height);
    });

    defaultRenderer.register(Cursor.name, (context, x, y, width, height, cursorObject) => {
        context.beginPath();
        context.fillStyle = cursorObject.color;
        context.lineWidth = Math.ceil(width / 20);
        context.strokeStyle = "rgba(255, 255, 255, 0.5)";
        context.rect(x + (width / 8), y + (height / 8), width / 8 * 6, height / 8 * 6);
        context.fill();
        context.stroke();
    });

    defaultRenderer.register(Marker.name, (context, x, y, width, height, markerObject) => {
        context.beginPath();
        context.fillStyle = markerObject.color;
        context.lineWidth = Math.ceil(width / 30);
        context.strokeStyle = `rgba(255, 255, 255, ${1 - markerObject.size})`;
        context.rect(
            x + (width / 2) - (width * markerObject.size / 2),
            y + (height / 2) - (height * markerObject.size / 2),
            width * markerObject.size,
            height * markerObject.size
        );
        context.fill();
        context.stroke();
    });

    return defaultRenderer;
}
