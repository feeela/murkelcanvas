import { getRandomInt } from "../js/helper.js"
import { Wall } from '../js/matrix.js';
import { MatrixCanvas } from "../js/matrix-canvas.js";

/**
 * Example usage for MatrixMoveIt
 */
export class Walls extends MatrixCanvas {
    constructor(canvas, options = {}) {
        super(canvas, options);

        let wall = new Wall();

        // Place walls
        for(let i = 0; i < 15; i++) {
            let y = getRandomInt(0, this.rows - 1);
            let x = getRandomInt(0, this.cols - 1);

            if(Math.random() < 0.5) {
                this.matrix.setValue({x: x, y: y - 1}, wall);
                this.matrix.setValue({x: x, y: y}, wall);
                this.matrix.setValue({x: x, y: y + 1}, wall);
            } else {
                this.matrix.setValue({x: x - 1, y: y}, wall);
                this.matrix.setValue({x: x, y: y}, wall);
                this.matrix.setValue({x: x + 1, y: y}, wall);
            }
        }
    }
}
