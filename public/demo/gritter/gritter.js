import { MurkelCanvas } from "../../js/murkel-canvas.js";

/**
 * An example usage of the MurkelCanvas base class
 */
export class Gritter extends MurkelCanvas {
    constructor(canvas, options = {}) {
        super(canvas, options);

        this.currentCol = 1;
        this.currentRow = 1;
        this.totalTiles = this.cols * this.rows;

        this.highlightColor = options.highlightColor || "#c00";
    }

    content() {
        super.content();

        this.layers.layer().addContent(() => {
            this.context.fillStyle = this.highlightColor;
            let x = (this.currentCol - 1) * this.gridSizeX + this.offsetX;
            let y = (this.currentRow - 1) * this.gridSizeY + this.offsetY;
            this.context.fillRect(x, y, this.gridSizeX, this.gridSizeY);
        });

        if(this.currentCol < this.cols) {
            this.currentCol++;
        } else {
            this.currentCol = 1;
            if(this.currentRow < this.rows) {
                this.currentRow++;
            } else {
                this.currentRow = 1;
            }
        }
    }
}
