import { getTempCanvas } from "../../helper/helper.js";
import { Direction } from "../../constants.js";

export function getSnakeHeadTile(width, height, direction = Direction.UP, color = "#090") {
    let tileCanvas = getTempCanvas(width, height);
    let tileContext = tileCanvas.getContext('2d');

    // Rotate the image according to direction
    let angle;
    switch(direction) {
        case Direction.UP:
            angle = 270
            break;

        case Direction.RIGHT:
            angle = 0
            break;

        case Direction.DOWN:
            angle = 90
            break;

        case Direction.LEFT:
            angle = 180
            break;
    }

    let centerX = width / 2;
    let centerY = height / 2;
    let radians = angle * Math.PI / 180;

    tileContext.translate(centerX, centerY);
    tileContext.rotate(radians);
    tileContext.translate(centerX * -1, centerY * -1);

    // Draw the head
    let drawWidth = width / 8;
    let drawHeight = height / 8;
    let border = width / 30;

    tileContext.lineWidth = border / 2;
    tileContext.lineCap = "square";

    // Tongue
    tileContext.beginPath();
    tileContext.fillStyle = "#a00";
    tileContext.moveTo(drawWidth * 7, drawHeight * 3.75);
    tileContext.bezierCurveTo(drawWidth * 7.25, drawHeight * 3.75, drawWidth * 7.8, drawHeight * 3.75, drawWidth * 8, drawHeight * 4);
    tileContext.bezierCurveTo(drawWidth * 7.8, drawHeight * 4.25, drawWidth * 7.25, drawHeight * 4.25, drawWidth * 7, drawHeight * 4.25);
    tileContext.fill();

    // Head
    tileContext.beginPath();
    // TODO fillStyle with gradient
    tileContext.fillStyle = color;
    tileContext.moveTo(0, drawHeight * 2);
    tileContext.bezierCurveTo(drawWidth * 2, drawHeight, drawWidth * 2, drawHeight, drawWidth * 7, drawHeight * 3);
    tileContext.bezierCurveTo(drawWidth * 7.5, drawHeight * 3.5, drawWidth * 7.5, drawHeight * 4.5, drawWidth * 7, drawHeight * 5);
    tileContext.bezierCurveTo(drawWidth * 2, drawHeight * 7, drawWidth * 2, drawHeight * 7, 0, drawHeight * 6);
    tileContext.fill();

    // Nose
    tileContext.fillStyle = "rgba(33, 33, 33, 0.6)";
    let noseHoleSize = border / 1.5;

    tileContext.beginPath();
    tileContext.arc(drawWidth * 6.75, drawHeight * 3.5, noseHoleSize, 0, 2 * Math.PI);
    tileContext.arc(drawWidth * 6.75, drawHeight * 4.5, noseHoleSize, 0, 2 * Math.PI);
    tileContext.fill();

    // Eyes
    tileContext.strokeStyle = "#dfb";
    let eyeSize = border * 1.5;

    // Eye shadow
    tileContext.fillStyle = "rgba(0, 0, 0, 0.25)";
    tileContext.beginPath();
    tileContext.arc(drawWidth * 4.85, drawHeight * 3.1, eyeSize * 1.5, 0, 2 * Math.PI);
    tileContext.arc(drawWidth * 4.85, drawHeight * 4.9, eyeSize * 1.5, 0, 2 * Math.PI);
    tileContext.fill();

    // Pupils
    tileContext.fillStyle = "#333";
    tileContext.beginPath();
    tileContext.arc(drawWidth * 5, drawHeight * 3, eyeSize, 0, 2 * Math.PI);
    tileContext.fill();
    tileContext.stroke();

    tileContext.beginPath();
    tileContext.arc(drawWidth * 5, drawHeight * 5, eyeSize, 0, 2 * Math.PI);
    tileContext.fill();
    tileContext.stroke();

    return tileCanvas;
}

export function getSnakeBodyTile(width, height, from = Direction.UP, to = Direction.DONW, color = "#090") {
    let tileCanvas = getTempCanvas(width, height);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = width / 8;
    let drawHeight = height / 8;
    let border = width / 30;

    tileContext.lineWidth = border / 2;
    tileContext.lineCap = "square";

    if((from == Direction.UP && to == Direction.DOWN)
        || (from == Direction.DOWN && to == Direction.UP)
        || (from == Direction.LEFT && to == Direction.RIGHT)
        || (from == Direction.RIGHT && to == Direction.LEFT)) {
        // Straight body

        if((from == Direction.UP && to == Direction.DOWN)
            || (from == Direction.DOWN && to == Direction.UP)) {
            let centerX = width / 2;
            let centerY = height / 2;
            tileContext.translate(centerX, centerY);
            tileContext.rotate(90 * Math.PI / 180);
            tileContext.translate(centerX * -1, centerY * -1);
        }

        tileContext.beginPath();
        // TODO fillStyle with gradient
        tileContext.fillStyle = color;
        tileContext.moveTo(0, drawHeight * 2);
        tileContext.bezierCurveTo(drawWidth * 3, drawHeight, drawWidth * 5, drawHeight * 3, drawWidth * 8, drawHeight * 2);
        tileContext.lineTo(drawWidth * 8, drawHeight * 6);
        tileContext.bezierCurveTo(drawWidth * 5, drawHeight * 7, drawWidth * 3, drawHeight * 5, 0, drawHeight * 6);
        tileContext.fill();
    } else {
        let angle = 0;

        if((from == Direction.UP && to == Direction.RIGHT)
            || (from == Direction.RIGHT && to == Direction.UP)) {
            angle = 90;
        } else if((from == Direction.RIGHT && to == Direction.DOWN)
            || (from == Direction.DOWN && to == Direction.RIGHT)) {
            angle = 180;
        } else if((from == Direction.LEFT && to == Direction.DOWN)
            || (from == Direction.DOWN && to == Direction.LEFT)) {
            angle = 270;
        }

        let centerX = width / 2;
        let centerY = height / 2;
        tileContext.translate(centerX, centerY);
        tileContext.rotate(angle * Math.PI / 180);
        tileContext.translate(centerX * -1, centerY * -1);

        // Draw the body as corner
        tileContext.beginPath();
        // TODO fillStyle with gradient
        tileContext.fillStyle = color;
        tileContext.moveTo(0, drawHeight * 2);
        tileContext.bezierCurveTo(drawWidth * 1.5, drawHeight * 1.5, drawWidth * 2, drawHeight * 1.5, drawWidth * 2, 0);
        tileContext.lineTo(drawWidth * 6, 0);
        tileContext.bezierCurveTo(drawWidth * 7, drawHeight * 5, drawWidth * 3, drawHeight * 5, 0, drawHeight * 6);
        tileContext.fill();
    }

    return tileCanvas;
}

export function getSnakeTailTile(width, height, direction = Direction.DOWN, color = "#090") {
    let tileCanvas = getTempCanvas(width, height);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = width / 8;
    let drawHeight = height / 8;

    // Rotate the image according to direction
    let angle;
    switch(direction) {
        case Direction.UP:
            angle = 270
            break;

        case Direction.RIGHT:
            angle = 0
            break;

        case Direction.DOWN:
            angle = 90
            break;

        case Direction.LEFT:
            angle = 180
            break;
    }

    let centerX = width / 2;
    let centerY = height / 2;
    tileContext.translate(centerX, centerY);
    tileContext.rotate(angle * Math.PI / 180);
    tileContext.translate(centerX * -1, centerY * -1);

    tileContext.beginPath();
    // TODO fillStyle with gradient
    tileContext.fillStyle = color;
    tileContext.moveTo(0, drawHeight * 2);
    tileContext.bezierCurveTo(drawWidth * 3, drawHeight * 1.5, drawWidth * 5, drawHeight * 4, drawWidth * 8, drawHeight * 4);
    tileContext.bezierCurveTo(drawWidth * 5, drawHeight * 6, drawWidth * 3, drawHeight * 4.5, 0, drawHeight * 6);
    tileContext.fill();

    return tileCanvas;
}

export function getAppleTile(width, height) {
    let tileCanvas = getTempCanvas(width, height);
    let tileContext = tileCanvas.getContext('2d');

    let drawWidth = width / 10;
    let drawHeight = height / 10;

    // Stem
    tileContext.beginPath();
    tileContext.fillStyle = "#960";
    tileContext.moveTo(drawWidth * 5, drawHeight * 5);
    tileContext.bezierCurveTo(drawWidth * 5, drawHeight * 4, drawWidth * 5, drawHeight * 2, drawWidth * 4, 0);
    tileContext.lineTo(drawWidth * 5, 0);
    tileContext.bezierCurveTo(drawWidth * 6, drawHeight * 1, drawWidth * 6, drawHeight * 5, drawWidth * 5, drawHeight * 5);
    tileContext.fill();

    // Corpse
    tileContext.beginPath();
    tileContext.fillStyle = "#e00";
    tileContext.moveTo(drawWidth * 5, drawHeight * 9);
    tileContext.bezierCurveTo(0, drawHeight * 11, drawWidth * 2 * -1, 0, drawWidth * 5, drawHeight * 3);
    tileContext.bezierCurveTo(drawWidth * 12, 0, drawWidth * 10, drawHeight * 11, drawWidth * 5, drawHeight * 9);
    tileContext.fill();

    // Leave
    tileContext.beginPath();
    tileContext.fillStyle = "#693";
    tileContext.moveTo(drawWidth * 5, drawHeight * 3);
    tileContext.bezierCurveTo(drawWidth * 4.5, 0, drawWidth * 8, drawHeight * 1.5, drawWidth * 9, 0);
    tileContext.bezierCurveTo(drawWidth * 8, drawHeight * 3, drawWidth * 6, drawHeight * 3, drawWidth * 5, drawHeight * 3);
    tileContext.fill();

    return tileCanvas;
}
