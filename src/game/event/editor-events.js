
export class EditorStatus extends Event {
    static type = "editor-status";

    static statusUpdated = 1;
    static statusSaved = 2;
    static statusLoaded = 3;

    static getLabel(status) {
        let label = "";
        Object.keys(EditorStatus).forEach(fieldName => {
            if(fieldName.substring(0, 6) == "status" && EditorStatus[fieldName] == status) {
                label = fieldName.substring(6);
            }
        });
        return label;
    }

    constructor(status) {
        super(EditorStatus.type);
        this.status = status;
        this.label = EditorStatus.getLabel(status);
    }
}
