import { Renderer } from "../../render/renderer.js";
import { ConwayCell } from "./conway-objects.js";

export function getRenderer(canvasObject) {
    let renderer = new Renderer(canvasObject);

    renderer.register(ConwayCell.name, (context, x, y, width, height, conwayTileObject) => {
        context.fillStyle = conwayTileObject.getColor();
        context.fillRect(x, y, width, height);
    });

    return renderer;
}
