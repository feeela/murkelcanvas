import { Matrix } from "../../matrix/matrix.js";
import * as GameObjects from "./boulder-game-objects.js";
import { Text, TextBlock } from "../../text.js"
import { EditorStatus } from "../../game/event/editor-events.js";
import { getRenderer } from "./boulder-renderer.js";

export { EditorStatus };

export class LevelEditor extends Matrix {
    constructor(canvas, options = {}) {
        options.skipInit = true;
        options.cols = options.cols || 10;
        options.rows = options.rows || 10;
        options.getRenderer = getRenderer;

        super(canvas, options);

        this.currentCursor = GameObjects.Wall;

        if(!options.levelData) {
            throw new Error("No level data found. Please specify JSON data for levels. Use \"new Boulder(canvas, { levelData: […] });\"");
        }
        this.allLevelData = options.levelData;

        this.status = EditorStatus.statusSaved;
        this.loadLevel("new");

        this.canvas.addEventListener("pointerdown", event => {
            if(!this.isPaused && this.level.locked !== true) {
                this.onClick();
            }
        });
    }

    getNewLevel(label = "new level", author = "unknown") {
        return {
            label,
            author,
            //data: Matrix.generateEmptyMatrix(this.rows, this.cols, ),
            data: Array.from({length: this.rows}, el => Array.from({length: this.cols}, el => ["Space"])),
        };
    }

    onClick() {
        let matrixPoint = this.cursorToMatrixPoint(this.pointer);

        if(this.pointHasValue(matrixPoint, this.currentCursor)) {
            if(this.currentCursor == GameObjects.Wall) {
                this.setPoint(matrixPoint, [new GameObjects.Space()]);
            } else {
                this.removeFromPoint(matrixPoint, this.currentCursor);
            }
        } else {
            let newObject = new this.currentCursor();
            // disable object behavior in editor
            newObject.run = () => {};

            if(this.currentCursor == GameObjects.Wall) {
                this.setPoint(matrixPoint, [newObject]);
            } else {
                this.addToPoint(matrixPoint, newObject);
            }
        }

        this.status = EditorStatus.statusUpdated;
        this.canvas.dispatchEvent(new EditorStatus(EditorStatus.statusUpdated));
    }

    cursorToMatrixPoint(cursor) {
        return { x: Math.floor(cursor.x / this.gridSizeX), y: Math.floor(cursor.y / this.gridSizeY) };
    }

    loadLevel(levelNo) {
        if(this.status != EditorStatus.statusSaved
            && !window.confirm("Unsaved changes; do you really want to load another level?")) {
            return false;
        }

        if(levelNo === "new") {
            this.level = this.getNewLevel();
            this.currentLevel = null;
        } else {
            levelNo = parseInt(levelNo);

            if(!this.allLevelData[levelNo]) {
                throw new Error(`The level ${levelNo} does not exist.`);
            }

            this.level = this.allLevelData[levelNo];
            this.currentLevel = levelNo;
            this.setRowsCols(this.level.data.length, this.level.data[0].length);
        }

        // Set matrix rows & columns to the dimensions of the loaded level.
        this.setRowsCols(this.level.data.length, this.level.data[0].length);

        // Necessary in case the level dimensions have changed, as the renderer use the current tile size.
        this.resetRenderer();

        // Load a cloned object of level data into the matrix
        this.matrix = JSON.parse(JSON.stringify(this.level["data"]));

        // Loop through each and every field in the matrix.
        this.matrix.forEach((column, y) => {
            column.forEach((field, x) => {
                field.forEach((matrixObjectName, i) => {
                    // Replace class name with object instance
                    field[i] = new GameObjects[matrixObjectName]();
                    // disable object behavior in editor
                    field[i].run = () => {};
                });
            });
        });

        this.status = EditorStatus.statusSaved;

        this.canvas.dispatchEvent(new EditorStatus(EditorStatus.statusLoaded));
    }

    // has to be empty to suppress keyboard cursor
    movePoint(direction) {
    }

    content() {
        super.content();

        this.layers.layer(this.layers.specificities.background).addContent(() => this.drawGrid());

        // Paused message
        if(this.isPaused == true) {

            this.layers.layer(this.layers.specificities.foreground).addContent(() => {

                let msg = new TextBlock(this.context, this.context.canvas.width / 2, this.context.canvas.height / 4, {
                    textAlign: "center",
                    background: "rgba(255,255,255,.8)",
                    padding: 30,
                });

                msg.heading("Boulder Editor with MurkelCanvas");

                msg.draw();
            });
        }

        this.layers.layer(this.layers.specificities.foreground).addContent(() => this.drawStatus());
    }

    pointerLayer() {
        super.pointerLayer();

        if (this.canvas.matches(':hover')) {
            this.layers.layer(this.layers.specificities.cursor - 1).addContent(() => {
                // paint the currently selected object type as cursor
                let matrixPoint = this.cursorToMatrixPoint(this.pointer);
                this.renderer.render(new this.currentCursor(), matrixPoint.x, matrixPoint.y);
            });

        }

    }

    drawStatus() {
        let text = new Text(this.context, this.status == EditorStatus.statusSaved ? "saved" : "changed", 10, 10, {
            background: "rgba(255, 255, 255, 0.75)",
        });
        text.draw();
    }

    exportLevel() {
        let level = this.level;

        level.data = [];
        this.matrix.forEach((column, y) => {
            level.data[y] = [];
            column.forEach((field, x) => {
                level.data[y][x] = [];
                field.forEach((matrixObject, i) => {
                    level.data[y][x][i] = matrixObject.constructor.name;
                });
            });
        });

        return level;
    }
}
