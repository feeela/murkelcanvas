import { Point } from '../js/lib-point.js';
import { Wall, Cursor } from '../js/matrix.js';
import { MatrixCanvas } from "../js/matrix-canvas.js";
import { extend } from "../js/helper.js";

export class CursorControl extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        paintSpeed: 25,
    });

    /**
     * Set a Wall object to the selected tile (if not already occupied by the Cursor)
     *
     * @param {Point} pointer
     */
    onPointerDown(pointer) {
        super.onPointerDown(pointer);

        if(!this.isPaused && !this.matrix.hasValue(this.lastClickedTile, Cursor)) {
            this.matrix.setValue(this.lastClickedTile, new Wall());
        }
    }

    *objectGenerator() {
        let index = 0;
        while (true) {
            if(index >= this.gameObjectsAsArray.length - 1) {
                index = 0;
            }
            let newObject = new this.gameObjectsAsArray[index]();
            index++;
            yield newObject;
        }
    }

    getObjectGenerator() {
        if(!this.generator) {
            this.gameObjectsAsArray = [];
            for(let objectName in GameObjects) {
                this.gameObjectsAsArray.push(GameObjects[objectName]);
            }

            this.generator = this.objectGenerator();
        }
        return this.generator;
    }

    getObject() {
        return this.getObjectGenerator().next().value;
    }
}
