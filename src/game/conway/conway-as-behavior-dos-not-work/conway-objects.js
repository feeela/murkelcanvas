import { Behavior, ObjectWithBehavior } from "../../matrix/matrix-object-behavior.js";
/*

export class CellBehavior extends Behavior {

    static STAGE = {
        TEST: 0,
        SET: 1,
    }

    constructor() {
        super();

        this.stage = CellBehavior.STAGE.TEST;
        this.nextState = false;
    }

    run(matrixCanvas, position, matrixObject) {
        /*
        Any live cell with fewer than two live neighbours dies, as if by underpopulation.
        Any live cell with two or three live neighbours lives on to the next generation.
        Any live cell with more than three live neighbours dies, as if by overpopulation.
        Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

        console.log("CellBehavior.run()", {matrixObject});
        if(this.stage == CellBehavior.STAGE.TEST) {
            let activeNeighbourCount = this.getActiveNeighbourCount(matrixCanvas, position);
            if(matrixObject.alife == true) {
                if(activeNeighbourCount < 2 || activeNeighbourCount > 3) {
                    this.nextState = false;
                } else {
                    this.nextState = true;
                }
            } else {
                if(activeNeighbourCount == 3) {
                    this.nextState = true;
                }
            }
            this.stage = CellBehavior.STAGE.SET;
        } else {
            this.stage = CellBehavior.STAGE.TEST;
            matrixObject.alife = this.nextState;
        }
    }

    
    getActiveNeighbourCount(matrixCanvas, position) {
        let allNeighbours = matrixCanvas.matrix.getNeighbours(position, true, true);
        let activeNeighbours = allNeighbours.filter(neighbourPoint => {
            return matrixCanvas.matrix.getValue(neighbourPoint, ConwayCell).alife == true;
        });
        return activeNeighbours.length;
    }
}
*/


export class CellBehaviorTest extends Behavior {

    constructor() {
        super();
    }

    run(matrixCanvas, position, matrixObject) {
        matrixObject.activeNeighbourCount = this.getActiveNeighbourCount(matrixCanvas, position);
    }

    getActiveNeighbourCount(matrixCanvas, position) {
        let allNeighbours = matrixCanvas.matrix.getNeighbours(position, true, true);
        let activeNeighbours = allNeighbours.filter(neighbourPoint => {
            return matrixCanvas.matrix.getValue(neighbourPoint, ConwayCell).alife == true;
        });
        return activeNeighbours.length;
    }
}

export class CellBehaviorUpdate extends Behavior {

    constructor() {
        super();
    }

    run(matrixCanvas, position, matrixObject) {
        /*
        Any live cell with fewer than two live neighbours dies, as if by underpopulation.
        Any live cell with two or three live neighbours lives on to the next generation.
        Any live cell with more than three live neighbours dies, as if by overpopulation.
        Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
        */

        if(matrixObject.alife == true) {
            if(matrixObject.activeNeighbourCount < 2 || matrixObject.activeNeighbourCount > 3) {
                matrixObject.alife = false;
            } else {
                matrixObject.alife = true;
            }
        } else {
            if(matrixObject.activeNeighbourCount == 3) {
                matrixObject.alife = true;
            }
        }
    }
}

export class ConwayCell extends ObjectWithBehavior {

    static COLOR = {
        BLACK: "#444",
        WHITE: "#f4f4f4",
    }

    /**
     * @param {Boolean} alife
     */
    constructor(alife = false, speed = 250) {
        super(speed);

        this.isPaused = true;

        this.alife = alife;
        this.activeNeighbourCount = 0;

        this.addBehavior(new CellBehaviorTest());
        this.addBehavior(new CellBehaviorUpdate());
    }

    getColor() {
        if(this.alife === true) {
            return ConwayCell.COLOR.BLACK;
        } else {
            return ConwayCell.COLOR.WHITE;
        }
    }

    flip() {
        this.alife = !this.alife;
    }
}
