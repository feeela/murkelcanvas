import { Point } from "../../point.js";

/**
 * Behaviors for canvas objects
 * 
 * These functions are executed in the context of a Malkasten instance (i.e. has `this`)
 */

export function diagonal(context) {
    this.position.x++;
    this.position.y++;
}

export function getMoveInCircleBehavior(startAngle = 0, counterClockwise = false, orbitRadius = 10) {
    var angle = counterClockwise ? 360 - startAngle : startAngle;
    var originalCenter = null;

    return function(context) {
        if(originalCenter === null) {
            originalCenter = this.getCenter();
        }

        this.setCenter(originalCenter.getOrbitPoint(angle, orbitRadius, orbitRadius, Math.PI / 32));

        if(counterClockwise) {
            angle--;
            if(angle <= 0) {
                angle = 360;
            }
        } else {
            angle++;
            if(angle > 360) {
                angle = 0;
            }
        }
    }
}

export function getRotationBehavior(counterClockwise = false) {
    let angle = counterClockwise ? 360 : 0;
    return function(context) {
        let center = this.malkasten.percentagePointToPixelPoint(this.getCenter());
        let radians = angle * Math.PI / 180;

        if(counterClockwise) {
            angle--;
            if(angle <= 0) {
                angle = 360;
            }
        } else {
            angle++;
            if(angle > 360) {
                angle = 0;
            }
        }

        return function(context) {
            context.translate(center.x, center.y);
            context.rotate(radians);
            context.translate(center.x * -1, center.y * -1);
        }
    }
}

/**
 * Applied on a Circle with `options.method = "stroke"` this gives a loading spinner.
 */
export function getCircleArcShrinkGrowBehavior(speed = Math.PI / 16) {
    let increaseBy = speed;
    let growPhase = false;

    return function(context) {
        if(growPhase === true) {
            this.options.end = this.options.end + increaseBy;

            if(this.options.end > 2 * Math.PI) {
                this.options.start = 0;
                this.options.end = 2 * Math.PI;
                growPhase = false;
            }
        } else {
            this.options.start = this.options.start + increaseBy;

            if(this.options.start > 2 * Math.PI) {
                this.options.start = 0;
                this.options.end = 0;
                growPhase = true;
            }
        }
    }
}

export function getCurveWobble(wobble = 10, counterClockwise = false) {
    let angleCp1 = counterClockwise ? 360 : 0;
    let angleCp2 = counterClockwise ? 0 : 360;
    let originalCp1 = null;
    let originalCp2 = null;
    let lastCenter = null;

    return function(context) {
        if(originalCp1 === null) {
            originalCp1 = this.cp1;
        }
        if(originalCp2 === null) {
            originalCp2 = this.cp2;
        }
        if(lastCenter === null) {
            lastCenter = this.getCenter();
        }

        let center = this.getCenter();
        if(!center.equals(lastCenter)) {
            let diffX = center.x - lastCenter.x;
            let diffY = center.y - lastCenter.y;
            originalCp1.x = originalCp1.x + diffX;
            originalCp1.y = originalCp1.y + diffY;
            originalCp2.x = originalCp2.x + diffX;
            originalCp2.y = originalCp2.y + diffY;

            lastCenter = center;
        }

        this.cp1 = originalCp1.getOrbitPoint(angleCp1, wobble, wobble, Math.PI / 32);
        this.cp2 = originalCp2.getOrbitPoint(angleCp2, wobble, wobble, Math.PI / 32);

        if(counterClockwise) {
            angleCp1++;
            if(angleCp1 > 360) {
                angleCp1 = 0;
            }

            angleCp2--;
            if(angleCp2 <= 0) {
                angleCp2 = 360;
            }
        } else {
            angleCp1--;
            if(angleCp1 <= 0) {
                angleCp1 = 360;
            }

            angleCp2++;
            if(angleCp2 > 360) {
                angleCp2 = 0;
            }
        }
    }
}

export class Direction {
    static UP = "u";
    static RIGHT = "r";
    static DOWN = "d";
    static LEFT = "l";

    static getOpposite(direction) {
        switch(direction) {
            case this.UP:
                return this.DOWN;
            case this.DOWN:
                return this.UP;
            case this.LEFT:
                return this.RIGHT;
            case this.RIGHT:
                return this.LEFT;
        }
    }
};

export function getMoveBehavior(direction, speed = 1) {
    return function(context) {
        let center = this.getCenter();
        switch(direction) {
            case Direction.UP:
                center.y = center.y - speed;
                break;

            case Direction.RIGHT:
                center.x = center.x + speed;
                break;

            case Direction.DOWN:
                center.y = center.y + speed;
                break;

            case Direction.LEFT:
                center.x = center.x - speed;
                break;
        }
        this.setCenter(center);
    }
}

export class ObjectEvent extends Event {
    static type = "malkasten-object";

    static added = 1;
    static removed = 2;

    constructor(object, status) {
        super(ObjectEvent.type);
        this.object = object;
        this.status = status;
    }
}

function isObjectOffCanvas(object) {
    let dim = object.getDimensions();
    return dim.x1 > 100 || dim.y1 > 100 || dim.x2 < 0 || dim.y2 < 0;
}

export function deleteIfOffCanvas(context) {
    if(typeof this.isOffCanvas == undefined) {
        if(isObjectOffCanvas(this)) {
            this.isOffCanvas = true;
        } else {
            this.isOffCanvas = false;
        }
    }

    let dim = this.getDimensions();
    if(dim.x1 > 100 || dim.y1 > 100
        || dim.x2 < 0 || dim.y2 < 0) {

        if(this.isOffCanvas == false) {
            // Remove this object from the object stack in Malkasten
            this.malkasten.objects.remove(this.malkasten.objects.getKeyByInstance(this));

            // Notify everyone else that this happened
            this.malkasten.canvas.dispatchEvent(new ObjectEvent(this, ObjectEvent.removed));

        }

        this.isOffCanvas = true;
    } else {
        this.isOffCanvas = false;
    }
}
