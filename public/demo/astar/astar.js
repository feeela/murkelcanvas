import { Point } from '../../js/lib-point.js';
import { extend, getRandomInt } from "../../js/helper.js"
import { MatrixObject, Cursor, Wall } from '../../js/matrix.js';
import { MatrixCanvas } from "../../js/matrix-canvas.js";
import { Astar } from "../../js/astar.js";

export class Entrance extends MatrixObject {
    constructor() {
        super();
    }
}

export class Pebble extends MatrixObject {
    constructor() {
        super();
    }
}

export class AstarMatrix extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        paintSpeed: 25,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        let wall = new Wall();

        // Place walls
        for(let i = 0; i < (this.rows * this.cols / 10); i++) {
            let y = getRandomInt(0, this.rows - 1);
            let x = getRandomInt(0, this.cols - 1);

            if(Math.random() < 0.5) {
                this.matrix.setValue({x: x, y: y - 1}, wall);
                this.matrix.setValue({x: x, y: y}, wall);
                this.matrix.setValue({x: x, y: y + 1}, wall);
            } else {
                this.matrix.setValue({x: x - 1, y: y}, wall);
                this.matrix.setValue({x: x, y: y}, wall);
                this.matrix.setValue({x: x + 1, y: y}, wall);
            }
        }

        this.entrance = new Point(0, 0);
        this.matrix.addValue(this.entrance, new Entrance());

        // Set start cursor position to a random, free field
        let cursorPosition = null;
        let attempts = 0;
        while(cursorPosition === null && attempts < 10) {
            let cursorPositionCandidate = new Point(
                getRandomInt(Math.floor(this.cols / 3), Math.floor(this.cols / 3 * 2)),
                getRandomInt(Math.floor(this.rows / 3), Math.floor(this.rows / 3 * 2))
            );

            if(this.matrix.isPassable(cursorPositionCandidate)) {
                cursorPosition = cursorPositionCandidate;
            }

            attempts++;
        }
        if(cursorPosition === null) {
            cursorPosition = new Point(this.cols - 1, this.rows - 1);
        }

        this.matrix.moveValue(this.cursor, cursorPosition, Cursor);
        let cursorObject = this.matrix.getValue(cursorPosition, Cursor);
        if(cursorObject === false) {
            cursorObject = new Cursor();
            this.matrix.addValue(cursorPosition, cursorObject);
        }
        cursorObject.passable = true;
        this.cursor = cursorPosition;

        // Initialize path searching object
        this.astar = new Astar(this.matrix);
        this.path = this.astar.search(this.entrance, this.cursor);
    }

    resetRenderer() {
        super.resetRenderer();

        this.renderer.register(Pebble.name, (context, x, y, width, height) => {
            context.beginPath();
            context.fillStyle = "#99c";
            context.arc(x + width / 2, y + height / 2, width / 3, 0, 2 * Math.PI);
            context.fill();
        });
        this.renderer.register(Entrance.name, (context, x, y, width, height) => {
            context.fillStyle = "#ff0";
            context.fillRect(x, y, width, height);
        });
    }

    moveCursor(direction, newCursor = null) {
        if(super.moveCursor(direction, newCursor) !== false) {
            this.path = this.astar.search(this.entrance, this.cursor);
console.log(this.entrance, this.cursor, this.path);

            // Remove pebbles from earlier path renderings
            this.matrix.matrix.forEach((column, y) => {
                column.forEach((field, x) => {
                    this.matrix.removeValue(new Point(x, y), Pebble);
                });
            });
        }
    }

    content(removeDirection = true) {
        super.content();

        // Draw found path, one pebble per Matrix.run() iteration
        if(this.path.length > 0) {
            let wayPoint = this.path.shift();
            this.matrix.addValue(wayPoint, new Pebble());
        }
    }

}
