import { Point, Specificity } from '../js/lib-point.js';
import { Text } from '../js/text.js';
import { getRandomInt } from "../js/helper.js"
import { MatrixObject, Cursor } from '../js/matrix.js';
import { MatrixCanvas } from "../js/matrix-canvas.js";

export class Box extends MatrixObject {
    constructor() {
        super();
        this.movable = false;
    }
}

export class Collector extends MatrixCanvas {
    constructor(canvas, options) {
        super(canvas, options);

        // Setup boxes to collect
        this.totalBoxes = 20;
        this.collectedBoxes = 0;

        // place objects to collect
        let box = new Box();
        box.movable = false;
        box.passable = true;

        for(let i = 0; i < this.totalBoxes; i++) {
            this.matrix.addValue(new Point(getRandomInt(0,this.cols - 1), getRandomInt(0,this.rows - 1)), box);
        }
    }

    resetRenderer() {
        super.resetRenderer();

        // Extend renderer
        this.renderer.register(Box.name, (context, x, y, width, height) => {
            context.fillStyle = "#05c";
            context.fillRect(x + (width / 10), y + (height / 10), width / 10 * 8, height / 10 * 8);
        });
    }

    moveCursor(direction) {
        super.moveCursor(direction);

        if(this.matrix.hasValue(this.cursor, Box)) {
            this.collectedBoxes++;
            this.matrix.removeValue(this.cursor, Box);
        }
    }

    content() {
        super.content();

        this.layers.layer(Specificity.FOREGROUND).addContent(() => {
            let text = new Text(this.context, `${this.collectedBoxes} / ${this.totalBoxes} boxes collected`, this.canvas.width - 10, 10, {
                textAlign: "right",
                background: "rgba(255, 255, 255, 0.6)",
            });
            text.draw();
        });
    }
}
