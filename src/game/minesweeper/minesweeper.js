import { Point } from "../../point.js";
import { Specificity } from "../../constants.js";
import { MatrixCanvas } from "../../matrix-canvas.js";
import { GameStatus } from "../../game/event/game-events.js";
import { Text, TextBlock } from "../../text.js"
import { extend, getRandomInt, isTouchDevice } from "../../helper/helper.js";
import * as GameObjects from "./minesweeper-game-objects.js";
import { getRenderer } from "./minesweeper-renderer.js";

export class Minesweeper extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        cursor: false,
        rows: 10,
        cols: 10,
        getRenderer: getRenderer,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        this.status = GameStatus.statusStart;

        this.openedTiles = 0;
        this.totalMineCount = this.options.howManyMines || Math.floor(Math.min(this.rows, this.cols) * 1.5);
        this.mineLocations = [];
        this.generateLevel(this.totalMineCount);

        this.canvas.oncontextmenu = () => false;

        // Show a virtual Space button to start the game
        this.isTouchDevice = isTouchDevice();
        if(this.isTouchDevice) {
            this.virtualKeyboard();
        }
    }

    onPointerDown(pointer) {
        super.onPointerDown(pointer);

        if(!this.isPaused && this.status != GameStatus.statusLost) {
            let matrixPoint = this.matrixPointFromPointer();
            if(!this.matrix.isValidPoint(matrixPoint)) {
                return;
            }

            let coverObject = this.matrix.getValue(matrixPoint, GameObjects.Cover);

            if(event.button == 2) {
                // Right click, mark cover tile
                coverObject.mark();
            } else {
                let openedCount = coverObject.open();
                if(openedCount !== false) {
                    if(this.matrix.hasValue(matrixPoint, GameObjects.Mine)) {
                        this.status = GameStatus.statusLost;
                        return;
                    }

                    this.openedTiles = this.openedTiles + openedCount;

                    if(this.cols * this.rows - this.openedTiles == this.totalMineCount) {
                        // The player has opened all tiles, that do not have a mine underneath
                        this.status = GameStatus.statusWon;

                        // Uncover whole matrix
                        this.matrix.matrix.forEach((column, y) => {
                            column.forEach((field, x) => {
                                let point = new Point(x, y);
                                let cover = this.matrx.getValue(point, GameObjects.Cover);
                                if(cover != false) {
                                    cover.isMarked = false;
                                    cover.open();
                                }
                            });
                        });
                    }
                }
            }
        }
    }

    getUniqueRandomCoordinates(howMany) {
        let coordinates = [];
        let coordinateCount = 0;
        while(coordinateCount < howMany) {
            let x = getRandomInt(0, this.cols - 1);
            let y = getRandomInt(0, this.rows - 1);
            if(!coordinates[y]) {
                coordinates[y] = [];
            }
            if(!coordinates[y][x]) {
                coordinates[y][x] = true;
                coordinateCount++;
            }
        }
        return coordinates;
    }

    /**
     * Generate a random minesweeper level in the matrix
     */
    generateLevel(howManyMines) {
        // Place mines
        let possibleMines = this.getUniqueRandomCoordinates(howManyMines);
        for(let y in possibleMines) {
            for(let x in possibleMines[y]) {
                let minePoint = new Point(x, y);
                this.matrix.addValue(minePoint, new GameObjects.Mine());
                this.mineLocations.push(minePoint);
            }
        }

        // Loop through each and every field in the matrix.
        this.matrix.matrix.forEach((column, y) => {
            column.forEach((field, x) => {
                let point = new Point(x, y);
                this.matrix.addValue(point, new GameObjects.Cover(this, point));
            });
        });

        this.isPaused = true;
    }

    content() {
        super.content();

        // Reuse text styles
        let textBlockOptions = {
            size: this.height / 35,
            textAlign: "center",
            background: "rgba(255,255,255,.75)",
            padding: this.height / 40,
        };

        // Paused message
        if(this.isPaused == true) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                let msg = new TextBlock(this.context, this.width / 2, this.height / 4, textBlockOptions);

                msg.heading("Minesweeper");
                msg.text("");
                msg.text("Left click to open a tile; right click to mark it.");
                msg.text("Marked tiles cannot be opened before removing the mark.");
                msg.text("");
                msg.heading("Paused");
                if(!this.isTouchDevice) {
                    msg.text("Press Space to pause / resume");
                }

                msg.draw();
            });
            return;
        }

        switch(this.status) {

            // Level won message
            case GameStatus.statusWon:
            case GameStatus.statusEnd:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You've won!");
                    msg.text("");
                    msg.text("Reload this page to try again.");

                    msg.draw();
                });
                break;

            case GameStatus.statusLost:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You've lost!");
                    msg.text("");
                    msg.text("Reload this page to try again.");

                    msg.draw();
                });
                break;
        }
    }

    /**
     * Do nothing. We do not need the keyboard cursor
     */
    moveCursor(direction) {}

    pointerLayer() {
        if (this.canvas.matches(':hover')) {
            this.layers.layer(Specificity.POINTER).addContent(() => {
                let radius = this.gridSizeX / 2;
                this.context.beginPath();
                this.context.fillStyle = "rgba(90, 90, 90, .7)";
                this.context.strokeStyle = "rgba(255, 255, 255, .5)";
                this.context.lineWidth = radius / 10;
                this.context.arc(this.pointer.x, this.pointer.y, radius - radius / 2, 0, 2 * Math.PI);
                this.context.fill();
                this.context.stroke();
            });
        }
    }


    /**
     * Display a virtual keyboard for touch devices.
     *
     * This is implemented using good old HTML buttons, which are inserted
     * into the DOM after the canvas.
     */
    virtualKeyboard() {
        // Add buttons to fieldset
        let button = document.createElement("input");
        button.type = "button";
        button.name = "Space"
        button.value = "Pause";

        // Setup event handler for each button
        button.addEventListener("pointerdown", event => {
            this.canvas.dispatchEvent(new KeyboardEvent("keydown", {"code": event.target.name}));
        });

        // Insert the fieldset into the DOM after the canvas
        this.canvas.after(button);

        // Inject button styles
        const style = document.createElement('style');
        style.appendChild(document.createTextNode(`
            input[type="button"] {
                font-size: 5vw;
                text-align: center;
                background: #eee;
                color: #333;
                border: .15em outset #666;
                border-radius: .5em;
                cursor: pointer;
                -webkit-tap-highlight-color: transparent;
                user-select: none;
            }
        `));
        document.head.appendChild(style);
    }

}
