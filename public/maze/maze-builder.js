import { Point } from '../js/lib-point.js';
import { getRandomInt, shuffle } from "../js/helper.js"
import { Space, Wall } from '../js/matrix.js';


class PartitionData {
    constructor(xMin, xMax, yMin, yMax, parentDoorPositions = null) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
        this.parentDoorPositions = parentDoorPositions;
    }
}

/**
 * A maze builder that works directly on a given Matrix object.
 */
export class MazeBuilder {

    constructor(matrix, options = {}) {
        this.matrix = matrix;

        this.minimumGapBetweenWalls = options.minimumGap || 1;
        this.spaceObject = new Space();
        this.wallObject = new Wall();

        // Draw initial walls all around the matrix
        this.addWallsToEdges();

        // Start partitioning the matrix in sections
        this.partition(new PartitionData(1, this.matrix.cols - 2, 1, this.matrix.rows - 2));
    }

    drawHorizontalWall(y, xMin, xMax) {
        for(let x = xMin; x <= xMax; x++) {
            this.matrix.setValue(new Point(x, y), this.wallObject);
        }
    }

    drawVerticalWall(x, yMin, yMax) {
        for(let y = yMin; y <= yMax; y++) {
            this.matrix.setValue(new Point(x, y), this.wallObject);
        }
    }

    addWallsToEdges() {
        this.drawHorizontalWall(0, 0, this.matrix.cols - 1);
        this.drawHorizontalWall(this.matrix.rows - 1, 0, this.matrix.cols - 1);
        this.drawVerticalWall(0, 0, this.matrix.rows - 1);
        this.drawVerticalWall(this.matrix.cols - 1, 0, this.matrix.rows - 1);
    }

    getRandomIntExluding(min, max, exclude = []) {
        if(min > max || (exclude.includes(min) && exclude.includes(max))) {
            // Nope, not possible
            return false;
        }

        let randomInt;
        do {
            randomInt = getRandomInt(min, max);
        } while(exclude.includes(randomInt));

        return randomInt;
    }

    partition(partitionData) {
        if((partitionData.xMax - partitionData.xMin) <= this.minimumGapBetweenWalls || (partitionData.yMax - partitionData.yMin) <= this.minimumGapBetweenWalls) {
            return false;
        }

        // Generate two random walls
        let randomXY;
        if(!partitionData.parentDoorPositions) {
            randomXY = new Point(
                getRandomInt(partitionData.xMin + this.minimumGapBetweenWalls, partitionData.xMax - this.minimumGapBetweenWalls),
                getRandomInt(partitionData.yMin + this.minimumGapBetweenWalls, partitionData.yMax - this.minimumGapBetweenWalls)
            );
        } else {
            // Exclude certain x & y positions when partioning to keep doors free
            let excludeX = [], excludeY = [];
            partitionData.parentDoorPositions.forEach(doorPosition => {
                if(doorPosition.x < partitionData.xMin - 1
                    || doorPosition.x > partitionData.xMax + 1
                    || doorPosition.y < partitionData.yMin - 1
                    || doorPosition.y > partitionData.yMax + 1
                ) {
                    // Skip, this door has nothing to do with the current section
                    return;
                }

                if(doorPosition.x == partitionData.xMin - 1) {
                    // door in left wall
                    excludeY.push(doorPosition.y);
                } else if(doorPosition.x == partitionData.xMax + 1) {
                    // door in right wall
                    excludeY.push(doorPosition.y);
                } else if(doorPosition.y == partitionData.yMin - 1) {
                    // door in top wall
                    excludeX.push(doorPosition.x);
                } else if(doorPosition.y == partitionData.yMax + 1) {
                    // door in bottom wall
                    excludeX.push(doorPosition.x);
                }
            });

            randomXY = new Point(
                this.getRandomIntExluding(partitionData.xMin + this.minimumGapBetweenWalls, partitionData.xMax - this.minimumGapBetweenWalls, excludeX),
                this.getRandomIntExluding(partitionData.yMin + this.minimumGapBetweenWalls, partitionData.yMax - this.minimumGapBetweenWalls, excludeY)
            );

            if(randomXY.x === false || randomXY.y === false) {
                // No more walls will fit here
                return false;
            }
        }

        this.drawHorizontalWall(randomXY.y, partitionData.xMin, partitionData.xMax);
        this.drawVerticalWall(randomXY.x, partitionData.yMin, partitionData.yMax);

        let doors = shuffle([true, true, true, false]);
        let doorPositions = [];
        if(doors[0]) {
            // door left
            doorPositions.push(new Point(getRandomInt(partitionData.xMin, randomXY.x - 1), randomXY.y));
        }
        if(doors[1]) {
            // door right
            doorPositions.push(new Point(getRandomInt(randomXY.x + 1, partitionData.xMax - 1), randomXY.y));
        }
        if(doors[2]) {
            // door top
            doorPositions.push(new Point(randomXY.x, getRandomInt(partitionData.yMin, randomXY.y - 1)));
        }
        if(doors[3]) {
            // door bottom
            doorPositions.push(new Point(randomXY.x, getRandomInt(randomXY.y + 1, partitionData.yMax - 1)))
        }

        // Add doors
        doorPositions.forEach(point => {
            this.matrix.setValue(point, this.spaceObject);
        });

        // Store door positions for nested partition() calls below
        if(partitionData.parentDoorPositions) {
            doorPositions = [...doorPositions, ...partitionData.parentDoorPositions];
        }

        // Set the created four sections as new partition data for next iteration
        // upper left
        this.partition(new PartitionData(partitionData.xMin, randomXY.x - 1, partitionData.yMin, randomXY.y - 1, doorPositions));
        // upper right
        this.partition(new PartitionData(randomXY.x + 1, partitionData.xMax, partitionData.yMin, randomXY.y - 1, doorPositions));
        // lower left
        this.partition(new PartitionData(partitionData.xMin, randomXY.x - 1, randomXY.y + 1, partitionData.yMax, doorPositions));
        // lower right
        this.partition(new PartitionData(randomXY.x + 1, partitionData.xMax, randomXY.y + 1, partitionData.yMax, doorPositions));

        return randomXY;
    }
}
