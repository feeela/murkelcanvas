import { Specificity } from "../../constants.js";
import { Point } from "../../point.js";
import { Text, TextBlock } from "../../text.js";
import { MatrixCanvas } from "../../matrix-canvas.js";
import { extend } from "../../helper/helper.js";
import { GameStatus } from "../../game/event/game-events.js";
import { GameOfLife, GameOfLifeCell } from "./game-of-life.js";
import { getRenderer } from "./game-of-life-renderer.js";

export { GameOfLifeCell };

/**
 * Provides a wrapper for the GameOfLife and import/export functionality
 * (GameOfLife has its own class for any game logic)
 */
export class ConwayCanvas extends MatrixCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        speed: 150,
        cols: 300,
        rows: 300,
        zoom: true,
        cursor: false,
        getRenderer: getRenderer,
        fillConstructor: GameOfLifeCell,
        fillDistinct: true,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        if (this.options.speed < this.options.paintSpeed) {
            this.options.paintSpeed = this.options.speed;
        }

        /* Zoom into grid based on numer of columns */
        let initialZoomPercent = 1 / (this.width / this.cols) * 2000;
        this.options.zoomSteps = initialZoomPercent / 10;
        /** @todo zoom does not pan to center of the matrix */
        this.zoom(Math.min(this.options.maxZoomPercent, initialZoomPercent));

        /* Initialize the GameOfLife, which has its own internal loop */
        this.gameOfLife = new GameOfLife(this.options.speed, this.matrix);
        this.pointerWasMoved = false;
        this.status = GameStatus.statusStart;
    }

    /**
     * Toggle the state of a cell b clicking on it.
     *
     * @param {Point} pointer 
     */
    onPointerDown(pointer) {
        super.onPointerDown(pointer);

        if (!this.isPaused && this.gameOfLife.paused) {
            /* this.lastClickedTile is updated in super.onPointerDown() */
            let cell = this.matrix.getValue(this.lastClickedTile, GameOfLifeCell);
            cell.flip();
        }
    }

    /**
     * This exists to prevent displaying the coordinates prior to the user having moved the cursor above the canvas.
     *
     * @param {Point} pointer 
     */
    onPointerMove(pointer) {
        super.onPointerMove(pointer);

        this.pointerWasMoved = true;
    }

    /**
     * Keep track of population and stop game if the complete population has died off.
     *
     * @param {DOMHighResTimeStamp} timestamp 
     */
    run(timestamp = null) {
        super.run(timestamp);

        if (this.gameOfLife.iteration > 0 && this.gameOfLife.population == 0) {
            /* Population has died off */
            this.status = GameStatus.statusEnd;
            this.gameOfLife.paused = true;
        }
    }

    /**
     * Display of coordinates and statistics
     */
    content() {
        super.content();

        this.layers.layer(Specificity.FOREGROUND).addContent(() => {
            let generations = new Text(
                this.context,
                `gen: ${this.gameOfLife.iteration}`,
                10,
                4,
            );
            generations.draw();

            let population = new Text(
                this.context,
                `pop: ${this.gameOfLife.population}`,
                10,
                30,
            );
            population.draw();

            let maxPopulation = new Text(
                this.context,
                `max: ${this.gameOfLife.maxPopulation}`,
                10,
                56,
            );
            maxPopulation.draw();
        });

        if (this.pointerWasMoved == true) {
            let coords = this.matrixPointFromPointer(this.pointer);

            this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                let text = new Text(
                    this.context,
                    `x: ${coords.x}, y: ${coords.y}`,
                    10,
                    this.height - 24,
                );
                text.draw();
            });
        }

        if (this.status == GameStatus.statusEnd) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                let msg = new TextBlock(
                    this.context,
                    this.width / 2,
                    this.height / 2 - (this.height / 35) * 2,
                    {
                        size: this.height / 35,
                        textAlign: "center",
                        background: "rgba(255,255,255,.75)",
                        padding: this.height / 40,
                    }
                );

                msg.heading("It's dead, Jim!");
                msg.text("");
                msg.text(`The population died off completely`);
                msg.text(`after ${this.gameOfLife.iteration} generations.`);

                msg.draw();
            });
        }
    }

    debugContent() {
        /* Do nothing – prevent display of debugging box from parent class */
    }

    /**
     * Export the contents of the matrix, trimmed to the most outer alife cells
     * in the RLE format as described here: https://conwaylife.com/wiki/Run_Length_Encoded
     * 
     * @todo replace empty lines with amount of dollar signs (i.e. instead of "41b$41b$41b$41b$" it should be "4$")
     *
     * @returns {string}
     */
    matrixToRLE() {
        let rle = "";

        let rows = [];
        let paddingLeft = this.cols;
        let paddingRight = this.cols;
        let paddingTop = this.rows;
        let paddingBottom = this.rows;

        this.gameOfLife.matrix.matrix.forEach((row, y) => {
            let rowPattern = "";
            let rowHasLivingCell = false;
            row.forEach((field, x) => {

                if (field[0].alife == true) {
                    rowHasLivingCell = true;
                    rowPattern += "o";

                    if (x < paddingLeft) {
                        paddingLeft = x;
                    }
                    if ((this.cols - (x + 1)) < paddingRight) {
                        paddingRight = this.cols - (x + 1);
                    }

                } else {
                    rowPattern += "b";
                }
            });

            if (rowHasLivingCell == true) {
                if (y < paddingTop) {
                    paddingTop = y;
                }
                if ((this.rows - (y + 1)) < paddingBottom) {
                    paddingBottom = this.rows - (y + 1);
                }
            }

            rows.push(rowPattern);
        });

        rows = rows.slice(paddingTop, this.rows - paddingBottom);
        rows = rows.map(row => {
            return row.slice(paddingLeft, this.cols - paddingRight)
        });

        let dimensions = `x = ${this.cols - paddingLeft - paddingRight}, y = ${this.rows - paddingTop - paddingBottom}\n`;
        return `${dimensions}${this.encodeRLE(rows.join("$"))}!`;
    }

    /**
     * Import RLE encoded data into the matrix;
     * The expected format is as described here: https://conwaylife.com/wiki/Run_Length_Encoded
     * 
     * @param {string} input 
     */
    rleToMatrix(input) {
        let lines = input.split("\n").filter(line => {
            return !line.startsWith("#");
        });

        let dimensionLine = lines.shift();

        let rle = lines.join("").trim();
        if(rle.endsWith("!")) {
            rle = rle.slice(0, rle.length - 1);
        }

        /* get dimensions as specified */
        let dimRegexMatch = dimensionLine.match(/x = (\d+), y = (\d+)(, rule = ([\w\d\/]+))?/);
        let width = dimRegexMatch[1];
        let height = dimRegexMatch[2];
        // let rule = typeof dimRegexMatch[4] !== "undefined" ? dimRegexMatch[4] : "B3/S23"; // Conway default
        let paddingTop = Math.floor((this.rows - height) / 2);
        let paddingLeft = Math.floor((this.cols - width) / 2);
        let pattern = this.decodeRLE(rle, width);

        /* start with an empty matrix */
        this.matrix = this.getEmptyMatrix();
        pattern.forEach((row, y) => {
            row.split("").forEach((cell, x) => {
                if(cell == "o") {
                    let position = new Point(x + paddingLeft, y + paddingTop);
                    this.matrix.getValue(position, GameOfLifeCell).alife = true;
                }
            });
        });

        /* Reload GameOfLife (as in constructor) */
        this.gameOfLife = new GameOfLife(this.options.speed, this.matrix);
        this.pointerWasMoved = false;
        this.status = GameStatus.statusStart;
    }

    decodeRLE(text, width) {
        let pattern = text.replace(/(\d+)([ \w\$])/g, (_, count, chr) => chr.repeat(count));
        return pattern.split("$").map(line => {
            return line.padEnd(width, "b");
        });
    };

    encodeRLE(text) {
        return text.replace(/([ \w])\1+/g, (group, chr) => group.length + chr);
    };
}
