import { MatrixObject, Cursor } from "../../matrix/matrix-object.js";
import { Point } from "../../point.js";

// export base objects to parent file
export * from "../../matrix/matrix-object.js";

export class Mine extends MatrixObject {
    constructor() {
        super();
    }
}

export class Cover extends MatrixObject {
    constructor(matrixCanvas, point) {
        super();

        this.matrixCanvas = matrixCanvas;
        this.point = point;

        this.isOpen = false;
        this.isMarked = false;
        this.mineCount = this.getMinesInVicinityCount();
    }

    open(openNeighbours = true) {
        if(this.isMarked == true) {
            // Cannot open marked covers
            return false;
        }

        this.isOpen = true;
        if(openNeighbours == true) {
            return 1 + this.openNeighbourCoversWithZeroMineCount();
        } else {
            return 1;
        }
    }

    mark() {
        if(this.isMarked == false) {
            this.isMarked = true;
        } else {
            this.isMarked = false;
        }
    }

    /**
     * Count the mines in the eight fields around "point"
     * @return {Number} mine count between 0 and 8
     */
    getMinesInVicinityCount() {
        let mines = 0;
        for(let x = this.point.x - 1; x <= this.point.x + 1; x++) {
            for(let y = this.point.y - 1; y <= this.point.y + 1; y++) {
                let neighbour = new Point(x, y);
                if(this.point.x == neighbour.x && this.point.y == neighbour.y) {
                    continue;
                }

                if(this.matrixCanvas.matrix.isValidPoint(neighbour) && this.matrixCanvas.matrix.hasValue(neighbour, Mine)) {
                    mines++;
                }
            }
        }
        return mines;
    }

    openNeighbourCoversWithZeroMineCount() {
        let openedTilesCount = 0;
        for(let x = this.point.x - 1; x <= this.point.x + 1; x++) {
            for(let y = this.point.y - 1; y <= this.point.y + 1; y++) {
                let neighbour = new Point(x, y);
                if((this.point.x != neighbour.x || this.point.y != neighbour.y)
                    && this.matrixCanvas.matrix.isValidPoint(neighbour)
                    && !this.matrixCanvas.matrix.hasValue(neighbour, Mine)
                ) {
                    let neighbourCover = this.matrixCanvas.matrix.getValue(neighbour, Cover);
                    if(neighbourCover != false
                        && neighbourCover.isOpen == false
                        && this.mineCount == 0) {
                        openedTilesCount = openedTilesCount + neighbourCover.open(neighbourCover.mineCount == 0);
                    }
                }
            }
        }

        return openedTilesCount;
    }
}
