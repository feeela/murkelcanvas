import { Point } from "../../point.js";
import { extend } from "../../helper/helper.js";

export const ObjectState = {
    // Idle state; the object has no special UI meaning (right now)
    idle: "idle",
    // Object has a focus (i.e. mouse over or selection via tabulator)
    focus: "focus",
    // Object was activated by user interavtion (i.e. mouse button click or pressed enter)
    active: "active",
}

export class ObjectStack {
    constructor() {
        this.nextNumericKey = 0;
        this.objects = {};
    }

    /**
     * Add an instance of a CanvasObject (or child thereof) to the stack of objects.
     * 
     * @param {CanvasObject} canvasObject
     * @param {Number|string|null} key Will use a numeric indix when set to `null` 
     */
    add(canvasObject, key = null) {
        if(key === null) {
            key = this.nextNumericKey;
            this.nextNumericKey++;
        } else if(Number.isInteger(key)) {
            this.nextNumericKey = key + 1;
        }

        this.objects[key] = canvasObject;  
    }

    get(key) {
        return this.objects[key] || false;
    }

    /**
     * Return the internal key for the given instance
     * 
     * @param {CanvasObject} canvasObject 
     */
    getKeyByInstance(canvasObject) {
        for(let key in this.objects) {
            if(this.objects[key] === canvasObject) {
                return key;
            }
        }
    }

    /**
     * Remove an object instance from the list of objects
     */
    remove(key) {
        delete this.objects[key];
    }

    each(callback) {
        for(let key in this.objects) {
            callback(this.objects[key], key);
        }
    }

    getObjectsAtPoint(point) {
        let objectsAtPoint = {};
        this.each((canvasObject, key) => {
            if(canvasObject.isPointPartOfObject(point)) {
                objectsAtPoint[key] = canvasObject;
            }
        });
        return objectsAtPoint;
    }
}

/**
 * The base class for any canvas objects; extend and implement draw();
 */
export class CanvasObject {

    static defaultOptions = {
        lineWidth: 0.25, // In percentage of canvas width
        speed: 50, // In milliseconds
        specificity: 50, // The default layer specificity; see layer.js
    };

    /**
     * @param {Malkasten} malkasten The canvas wrapper
     * @param {Point} position A Point holding percentage coordinates
     * @param {Object} options A plain object with key-value pairs used to configure any CanvasObjects
     */
    constructor(malkasten, position, options) {
        this.malkasten = malkasten;
        this.position = position;
        this.options = extend(this.constructor.defaultOptions, options);

        this.behaviors = [];
        this.behaviorDrawManipulation = [];
        this.renderedImage = null;

        // Object tick
        this.previousTimestamp = false;
        this.isPaused = false;

        // Indicates the state the current object is in
        this.state = ObjectState.idle;

        // The Malkasten layer to render this instance on
        this.specificity = this.options.specificity;
    }

    /**
     * Abstract base method to actually draw something to `context`;
     * Overload and paint something.
     */
    draw(context) {

    }


    /* ***** Object behavior ***** */

    /**
     * Add a behavior function to this object.
     * Behavior functions are executed with the current object as `this` and are used to modify the objects data.
     * Each behavior function is called once on each `CanvasObject.run()` cycle.
     *
     * @param {Function} behavior A callback that receives the canvas `context` as only argument.
     */
    addBehavior(behavior) {
        this.behaviors.push(behavior);
    }

    /**
     * Remove a behavior from the internal list of this object.
     *
     * @param {Function} behavior
     * @return {Boolean} Returns `false` if the given behavior function was not added before.
     */
    removeBehavior(behavior) {
        let index = this.behaviors.indexOf(behavior);
        if(index != -1) {
            // remove this item from the list of behaviors
            this.behaviors.splice(index, 1);
            return true;
        } else {
            return false;
        }
    }

    /**
     * The objects main loop; used to run behaviors, apply a brake (wait for this.options.speed) 
     */
    run() {
        let timestamp = performance.now();
        let context = this.malkasten.context;

        // Wait for object tick
        if(!this.isPaused && (this.previousTimestamp === false || timestamp >= (this.previousTimestamp + this.options.speed))) {
            // Run behaviors and add their returned functions to `this.behaviorDrawManipulation`
            this.behaviorDrawManipulation = [];
            this.behaviors.forEach(behavior => {
                let behaviorReturnsDrawManipulation = behavior.call(this, context);
                if(behaviorReturnsDrawManipulation) {
                    this.behaviorDrawManipulation.push(behaviorReturnsDrawManipulation);
                }
            });
            this.previousTimestamp = timestamp;
        }

        // Run functions that were returned by previous execution of the behaviors
        this.behaviorDrawManipulation.forEach(drawManipulation => {
            drawManipulation.call(this, context);
        });

        // Actually draw this object
        this.draw(context);

        // Reset any transforms made by behaviors
        context.resetTransform();
    }


    /* ***** Internal data API ***** */

    getCenter() {
        return this.position;
    }

    setCenter(point) {
        this.position = point;
    }


    /* ***** Helper methods to draw ***** */

    stroke(context, drawCallback, color) {
        context.beginPath();
        context.lineWidth = this.malkasten.percentageToPixel(this.options.lineWidth, "width");
        context.strokeStyle = color;

        drawCallback();

        context.stroke();
    }

    fill(context, drawCallback, color) {
        context.beginPath();
        context.fillStyle = color;
        context.lineWidth = 0;

        drawCallback();

        context.fill();
    }
}

/**
 * Create gradients based on their center point and an angle
 */
export class Gradient {

    /**
     * @param {Point} position The center of the gradient
     * @param {Array} colors A list of colors, will be equally distributed
     * @param {Number} angle In degree
     */
    constructor(malkasten, position, colors = [], angle = 0) {
        this.malkasten = malkasten;
        this.position = position;
        this.colors = colors;
        this.angle = angle;
    }

    getGradientPositions() {
        let angle = 180 - this.angle;

        return {
            start: this.malkasten.percentagePointToPixelPoint(this.position.getOrbitPoint(angle, 50, 50, Math.PI / 180)),
            end: this.malkasten.percentagePointToPixelPoint(this.position.getOrbitPoint(angle + 180, 50, 50, Math.PI / 180)),
        };
    }

    fillStyle() {
        // Create gradient
        let { start, end } = this.getGradientPositions(true);
        let gradient = this.malkasten.context.createLinearGradient(start.x, start.y, end.x, end.y);

        // Set color steps
        let stepSize = 1 / (this.colors.length - 1);
        let stepAt = 0;
        this.colors.forEach(color => {
            gradient.addColorStop(stepAt, color);
            stepAt = stepAt + stepSize;
        });

        return gradient;
    }
}
