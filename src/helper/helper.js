/**
 * This is common module for often used functions.
 *
 * The functions must be without further dependencies outside this module scope.
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 */

/**
 * Recursively merge properties of two objects
 *
 * @param {Object} defaults Default settings
 * @param {Object} options User options
 * @returns {Object} Merged values of defaults and options
 */
export function extend(defaults, options) {

    function copy(obj1, obj2, prop) {
        try {
            if (obj2[prop].constructor === Object) {
                obj1[prop] = extend(obj1[prop] || {}, obj2[prop]);
            }
            else {
                obj1[prop] = obj2[prop];
            }
        }
        catch (error) {
            obj1[prop] = obj2[prop];
        }
        return obj1;
    }

    var extended = {};
    var prop;
    for (prop in defaults) {
        if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
            copy(extended, defaults, prop);
        }
    }
    for (prop in options) {
        if (Object.prototype.hasOwnProperty.call(options, prop)) {
            copy(extended, options, prop);
        }
    }
    return extended;
}

/**
 * Returns a function, that, as long as it continues to be invoked, will not be triggered.
 * The function will be called after it stops being called for `wait` milliseconds.
 * If `immediate` is passed, trigger the function on the leading edge, instead of the trailing.
 *
 * This function was taken from Underscore.js as of 2014
 * @link https://davidwalsh.name/javascript-debounce-function
 *
 * @param {Function} func
 * @param {Number} wait
 * @param {Boolean} immediate
 * @returns {Function}
 */
export function debounce(func, wait = 100, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}


/**
 * Throttling Function
 */
export function throttle(func, delay = 100) {
    // Previously called time of the function
    let prev = 0;
    return (...args) => {
        // Current called time of the function
        let now = new Date().getTime();

        // If difference is greater than delay, call the function again.
        if (now - prev > delay) {
            prev = now;
            return func(...args);
        }
    }
}

export function getRandomInt(min, max) {
    const minCeiled = Math.ceil(min);
    const maxFloored = Math.floor(max);
    return Math.floor(Math.random() * (maxFloored - minCeiled + 1) + minCeiled);
}

export function getRandomIntSteps(min, max, steps) {
    const minCeiled = Math.ceil(min);
    const maxFloored = Math.floor(max);
    return steps * Math.floor(Math.random() * (maxFloored - minCeiled) / steps + minCeiled / steps);
}

/**
 * Shuffle the entries of an array
 * 
 * @author https://stackoverflow.com/a/12646864
 */
export function shuffle(array) {
    for(let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

/**
 * Detect if the current device is a touch device using temporary elements and CSS media queries.
 * Inspired by Modernizr but trimmed to essential code.
 *
 * @return {Boolean}
 */
export function isTouchDevice() {
    var bool;
    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        bool = true;
    }
    else {
        let fakeBody = document.createElement('fakebody');
        fakeBody.innerHTML += '<style>@media (touch-enabled),(-webkit-touch-enabled),(-moz-touch-enabled),(-o-touch-enabled){#touchtest{top:42px;position:absolute}}</style>';
        document.documentElement.appendChild(fakeBody);

        let touchTestNode = document.createElement('div');
        touchTestNode.id = 'touchtest';
        fakeBody.appendChild(touchTestNode);
        bool = touchTestNode.offsetTop === 42;
    }
    return bool;
}

/**
 * Get a temporary canvas with a specific size
 */
export function getTempCanvas(width, height) {
    let canvas = document.createElement("canvas");
    canvas.width = Math.ceil(width);
    canvas.height = Math.ceil(height);
    return canvas;
}

/**
 * Return an array of 0 & 1 for given integer
 * 
 * @param {Number} number
 * @return {Array}
 */
export function intToBitValues(number) {
    if (number == 0) {
        return [];
    }

    let bitValues = [];
    while (number != 0) {
        let oldNumber = number;
        number = number & (number - 1);
        bitValues.push(oldNumber - number);
    }

    return bitValues;
}

/**
 * @author https://gist.github.com/cawfree
 */
export function toCamelCase(input) {
    return input.replace(/_([a-z])/g, (word) =>  word[1].toUpperCase());
}

/**
 * @author https://gist.github.com/simongong
 */
export function toSnakeCase(input, separator = "_") {
    let split = /(?=[A-Z])/;
    return input.split(split).join(separator).toLowerCase();
}

/**
 * @author https://stackoverflow.com/a/23013726/341201
 */
export function objectFlip(object){
    var flipped = {};
    for(var key in object){
        flipped[object[key]] = key;
    }
    return flipped;
}
