import { Point } from "../../point.js";
import { Text, TextBlock } from "../../text.js"
import { extend, isTouchDevice } from "../../helper/helper.js";
import { MatrixCanvas } from "../../matrix-canvas.js";
import { GameStatus } from "../../game/event/game-events.js";
import { Direction, Specificity } from "../../constants.js";
import * as GameObjects from "./boulder-game-objects.js";
import { getRenderer } from "./boulder-renderer.js";

export class Boulder extends MatrixCanvas {
    static levelStorage = "boulder-level";

    static defaultOptions = extend(super.defaultOptions, {
        paintSpeed: 25,
        cursorConstructor: GameObjects.Player,
        getRenderer: getRenderer,
    });

    constructor(canvas, options = {}) {
        super(canvas, options);

        this.status = GameStatus.statusStart;

        if(!options.levelData) {
            throw new Error("No level data found. Please specify JSON data for levels. Use \"new Boulder(canvas, { levelData: […] });\"");
        }
        this.allLevelData = options.levelData;

        let savedLevel = window.localStorage.getItem(Boulder.levelStorage);
        if(!savedLevel) {
            savedLevel = 0;
        }

        this.loadLevel(options.level || savedLevel);

        this.isTouchDevice = isTouchDevice();
        if(this.isTouchDevice) {
            this.virtualKeyboard();
        }
    }

    /**
     * Add some more user input handling;
     * Everything we don't change here is handled in MoveIt.onKeyPress()
     */
    onKeyPress(event) {
        switch(event.key) {

            case " ":
                // prevent pause message and resume via space key when game was won
                if(this.status != GameStatus.statusWon) {
                    if(this.isPaused == true) {
                        this.isPaused = false;
                    } else {
                        this.isPaused = true;
                    }
                }
                break;

            case "Enter":
                if(this.status == GameStatus.statusWon) {
                    // Load next level
                    let nextLevel = this.currentLevel + 1;
                    if(this.allLevelData[nextLevel]) {
                        this.loadLevel(nextLevel);
                    } else {
                        this.status = GameStatus.statusEnd;
                    }
                }
                break;

            case "l":
                // Reset save game to first level
                // TODO This does not re-render the pause-screen when pressed while in pause (although reset works)
                this.loadLevel(0);
                break;

            default:
                super.onKeyPress(event);
        }
    }

    /**
     * Display a virtual keyboard for touch devices.
     *
     * This is implemented using good old HTML buttons, which are inserted
     * into the DOM after the canvas.
     */
    virtualKeyboard() {
        // Create a fieldset as wrapper for the control buttons
        let fieldset = document.createElement("fieldset");
        fieldset.className = "buttons";

        // Add buttons to fieldset
        let button = document.createElement("input");
        button.type = "button";
        [
            { name: "ArrowUp", value: "⇧" },
            { name: "Space", value: "Pause" },
            { name: "ArrowLeft", value: "⇦" },
            { name: "ArrowDown", value: "⇩" },
            { name: "ArrowRight", value: "⇨" },
            { name: "Enter", value: "Enter" },
            { name: "KeyL", value: "Reset" },
        ].forEach(btn => {
            let cb = button.cloneNode();
            cb.name = btn.name;
            cb.value = btn.value;

            // Setup event handler for each button
            cb.addEventListener("pointerdown", event => {
                if(event.target.name == "KeyL" && !window.confirm("Really reset to level one?")) {
                    return;
                }
                this.canvas.dispatchEvent(new KeyboardEvent("keydown", {"code": event.target.name}));
            });

            fieldset.appendChild(cb);
        });

        // Insert the fieldset into the DOM after the canvas
        this.canvas.after(fieldset);

        // Inject button styles
        const style = document.createElement('style');
        style.appendChild(document.createTextNode(`
            .buttons {
                display: grid;
                grid-template-areas:
                    ". up reset"
                    "left . right"
                    "space down enter";
                column-gap: 1em;
                row-gap: 1em;
                justify-items: center;
                justify-content: space-around;
                align-items: end;

                width: 80vw;
                margin: 0 auto;
                border: 0;
            }

            .buttons input[type="button"] {
                font-size: 5vw;
                text-align: center;
                background: #eee;
                color: #333;
                border: .15em outset #666;
                border-radius: .5em;
                cursor: pointer;
                -webkit-tap-highlight-color: transparent;
                user-select: none;
            }
            .buttons input[type="button"]:active {
                border-style: inset;
            }

            .buttons input[name^="Arrow"] {
                width: 3em;
                height: 3em;
            }
            .buttons input[name="ArrowUp"] {
                grid-area: up;
            }
            .buttons input[name="ArrowLeft"] {
                grid-area: left;
            }
            .buttons input[name="ArrowRight"] {
                grid-area: right;
            }
            .buttons input[name="ArrowDown"] {
                grid-area: down;
            }
            .buttons input[name="Space"] {
                grid-area: space;
            }
            .buttons input[name="Enter"] {
                grid-area: enter;
            }
            .buttons input[name="KeyL"] {
                grid-area: reset;
                align-self: start;
            }

            .buttons input[name="Space"],
            .buttons input[name="Enter"],
            .buttons input[name="KeyL"] {
                height: 2em;
                width: 4em;
            }`
        ));
        document.head.appendChild(style);
    }

    /**
     * Load a matrix with text-strings containing class names, which are replaced by GameObjects instances.
     */
    loadLevel(levelNo) {
        levelNo = parseInt(levelNo);

        if(!this.allLevelData[levelNo]) {
            throw new Error(`The level ${levelNo} does not exist.`);
        }

        // Store complete current level data for reference (to read metadate like the level-title).
        this.level = this.allLevelData[levelNo];

        // Store current level number and set game to.
        this.currentLevel = levelNo;
        this.status = GameStatus.statu;
        this.isPaused = true;
        window.localStorage.setItem(Boulder.levelStorage, this.currentLevel);

        // Set matrix rows & columns to the dimensions of the loaded level.
        this.setRowsCols(this.level.data.length, this.level.data[0].length);

        // Necessary in case the level dimensions have changed, as the renderer use the current tile size.
        this.resetRenderer();

        // Load a cloned object of level data into the matrix
        this.matrix.matrix = JSON.parse(JSON.stringify(this.level.data));

        // Initialize matrix objects and find some objects.
        this.keys = [];
        this.keyCount = 0;
        this.keysCollected = 0;
        this.exitPoint = null;
        this.player = new GameObjects.Player();

        // Loop through each and every field in the matrix.
        this.matrix.matrix.forEach((column, y) => {
            column.forEach((field, x) => {
                field.forEach((matrixObjectName, i) => {
                    let point = new Point(x, y);

                    // Replace class name with object instance
                    field[i] = new GameObjects[matrixObjectName]();

                    // The current object is the entrance, set cursor (player position) to here.
                    if(matrixObjectName == GameObjects.Entrance.name) {
                        // Set active cursor to the current point
                        this.cursor = point;
                        // Add player object to that field
                        // (The original cursor from Matrix has been overwritten abive by resetting the matrix.)
                        this.matrix.addValue(this.cursor, this.player);
                        return;
                    }

                    // The current object is the exit, store as reference to find it again.
                    if(matrixObjectName == GameObjects.Exit.name) {
                        // found player position
                        this.exitPoint = point;
                        return;
                    }

                    // The current object is a key, store as reference to find it again.
                    if(matrixObjectName == GameObjects.Key.name) {
                        // There is a key in this level
                        this.keys.push(point);
                        return;
                    }

                });
            });
        });

        if(this.exitPoint === null) {
            alert("This level has no exit; you can't win.");
            throw new Error("This level has no exit; you can't win.");
        }

        this.keyCount = this.keys.length;

        // Set exit door to non-passable, so player can't exit
        if(this.keyCount > 0) {
            let exitObject = this.matrix.getValue(this.exitPoint, GameObjects.Exit);
            exitObject.passable = false;
        }

        // Update visibleFrom & visibleTo for new rows/cols
        this.updateVisiblePortion();
    }

    /**
     * Update direction in player object
     */
    moveCursor(direction) {
        // Do not move the cursor of player has lost
        if(this.status == GameStatus.statusLost) {
            return false;
        }

        super.moveCursor(direction);

        // Set current direction to player object
        this.player.direction = direction;

        // Collect keys, if there are any
        if(this.matrix.hasValue(this.cursor, GameObjects.Key)) {
            // The player is on a field with a key; collect it
            this.keysCollected++;

            // Remove this key object
            this.matrix.removeValue(this.cursor, GameObjects.Key);

            // all keys collected
            if(this.keysCollected == this.keyCount) {
                // Open the exit door
                let exitDoor = this.matrix.getValue(this.exitPoint, GameObjects.Exit);
                exitDoor.passable = true;
            }
        }

        let nextOne = this.cursor.move(direction);
        if(this.matrix.isValidPoint(nextOne)
            && this.matrix.hasValue(nextOne, GameObjects.Box) && this.matrix.hasValue(nextOne, GameObjects.Exit)) {
            // The player has pushed a box onto a field with an exit; the player has won.
            this.status = GameStatus.statusWon;
        }
    }

    content() {
        super.content();

        // Reuse text styles
        let textBlockOptions = {
            size: this.height / 35,
            textAlign: "center",
            background: "rgba(255,255,255,.9)",
            padding: this.height / 40,
        };

        // Show number of keys, if there are any in the level
        if(this.keyCount > 0) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                let text = new Text(this.context, `${this.keysCollected} / ${this.keyCount} keys`, this.gridSizeX / 3, this.gridSizeY / 6, {
                    size: this.height / 35,
                    background: "rgba(255, 255, 255, 0.75)",
                });
                text.draw();
            });
        }

        // Player gets eaten by monster
        if(this.matrix.hasValue(this.cursor, GameObjects.Monster)) {
            this.matrix.removeValue(this.cursor, GameObjects.Player);
            this.status = GameStatus.statusLost;
        }

        // Paused message
        if(this.isPaused == true) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                let msg = new TextBlock(this.context, this.width / 2, this.height / 4, textBlockOptions);

                msg.heading("Boulder Game with MurkelCanvas");
                msg.text("Get one blue box into the other door.");
                msg.text("Control with arrow-keys.");
                msg.text("");
                msg.text("The latest level is saved, a page refresh will restart the last played level.");
                if(!this.isTouchDevice) {
                    msg.text("(Press L to delete save game and respectively reset saved level to 1.)");
                }

                msg.text(""); // empty line
                msg.heading(`Level ${this.currentLevel + 1}: ${this.level.label}`);
                msg.text(`by ${this.level.author}`);
                msg.text(""); // empty line

                msg.heading("Paused");
                if(!this.isTouchDevice) {
                    msg.text("Press Space to pause / resume");
                }

                msg.draw();
            });
            return;
        }

        switch(this.status) {

            // Level won message
            case GameStatus.statusWon:
            case GameStatus.statusEnd:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You've won!");
                    msg.text("");

                    // TODO the two events below get fired over and over again, but should only get dispatched once
                    if(this.currentLevel == this.allLevelData.length - 1) {
                        msg.text("This was the last level, you have finished the game.");
                        this.status = GameStatus.statusEnd;

                        this.canvas.dispatchEvent(new GameStatus(GameStatus.statusEnd));
                    } else {
                        msg.text("Press Enter to proceed to next level");
                        window.localStorage.setItem(Boulder.levelStorage, this.currentLevel + 1);

                        this.canvas.dispatchEvent(new GameStatus(GameStatus.statusWon));
                    }

                    msg.draw();
                });
                break;

            case GameStatus.statusLost:
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {

                    let msg = new TextBlock(
                        this.context,
                        this.width / 2,
                        this.height / 2 - textBlockOptions.size * 2,
                        textBlockOptions
                    );

                    msg.heading("You've lost!");
                    msg.text("");
                    msg.text("Reload this page to try again.");

                    msg.draw();
                });
                break;
        }
    }

    pointerLayer() {
        if (this.canvas.matches(':hover')) {
            this.layers.layer(Specificity.POINTER).addContent(() => {
                this.context.beginPath();
                this.context.fillStyle = "#666";
                this.context.arc(this.pointer.x, this.pointer.y, 5, 0, 2 * Math.PI);
                this.context.fill();
            });
        }
    }
}
