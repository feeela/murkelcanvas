import { MatrixObject, Cursor } from "./base.js";
import { Direction } from "./constants.js";
import { GameStatus } from "../../game/event/game-status.js";
import { Point } from "../../murkel-canvas.js";
import { getRandomInt } from "../../helper.js";

// export base objects to parent file
export * from './base.js';

export class Collectable extends MatrixObject {
    constructor() {
        super();
        this.collectable = true;
    }
}

export class Pebble extends Collectable {
    constructor() {
        super();
    }
}

export class SuperPebble extends Pebble {
    constructor() {
        super();
    }
}

export class PacMan extends Cursor {
    constructor(direction = Direction.RIGHT) {
        super();
        this.direction = direction;
    }
}

export class Ghost extends MatrixObject {
    constructor() {
        super();

        this.passable = false;

        this.previousTimestamp = false;
        this.speed = 150;

        this.direction = null;

        this.color = "#999";
    }

    run(matrix, x, y) {
        let timestamp = performance.now();

        // Wait for ghost tick
        if(this.previousTimestamp !== false && timestamp < (this.previousTimestamp + this.speed)) {
            return;
        }

        let currentPoint = new Point(x, y);
        let chaseDirectionPoint = new Point(matrix.cursor.x - currentPoint.x, matrix.cursor.y - currentPoint.y);
        let preferredDirections = [];
        if(matrix.cursor.x < currentPoint.x) {
            preferredDirections.push(Direction.LEFT);
        }
        if(matrix.cursor.x > currentPoint.x) {
            preferredDirections.push(Direction.RIGHT);
        }
        if(matrix.cursor.y < currentPoint.y) {
            preferredDirections.push(Direction.UP);
        }
        if(matrix.cursor.y > currentPoint.y) {
            preferredDirections.push(Direction.DOWN);
        }

console.log({
    player: matrix.cursor,
    ghost: currentPoint,
    chaseDirectionPoint,
    preferredDirections,
});

        // Find and choose a direction
        let possibleDirections = this.findDirection(matrix, currentPoint, Direction.getOpposite(this.direction));
        if(possibleDirections !== false) {
            this.direction = possibleDirections[getRandomInt(0, possibleDirections.length - 1)];
        }

        // Move towards direction, if possible – if not reset direction
        let targetPoint = matrix.getMovedPoint(currentPoint, this.direction);
        if(matrix.isValidPoint(targetPoint)
            && (matrix.isPassable(targetPoint) || matrix.cursor.equals(targetPoint))) {
            matrix.moveObject(currentPoint, targetPoint, Ghost);
        } else {
            this.direction = null;
        }

        this.previousTimestamp = timestamp;
    }

    /**
     * See if there are free fields next to the current one.
     * Check all directions, except the opposite of the current one.
     */
    findDirection(matrix, point, currentDirection) {
        let possibleDirections = [];
        for(let direction in Direction) {

            if(Direction[direction] == currentDirection) {
                continue;
            }

            let nextField = matrix.getMovedPoint(point, Direction[direction]);

            if(!matrix.isValidPoint(nextField)
                || (nextField.x == point.x && nextField.y == point.y)
                || (!matrix.isPassable(nextField)) && !matrix.cursor.equals(nextField)) {
                continue;
            }

            possibleDirections.push(Direction[direction]);
        }

        return possibleDirections.length > 0 ? possibleDirections : false;
    }
}

const Behavior = {
    HOUND: 1,
    HUNTER: 2,
    SCHIZO: 3,
    ELUSIVE: 4,
}

export class Blinky extends Ghost {
     constructor() {
        super();
        this.color = "#f33";
        this.behavior = Behavior.HOUND;
    }   
}

export class Pinky extends Ghost {
     constructor() {
        super();
        this.color = "#f3f";
        this.behavior = Behavior.HUNTER;
    }   
}

export class Inky extends Ghost {
     constructor() {
        super();
        this.color = "#3ff";
        this.behavior = Behavior.SCHIZO;
    }   
}

export class Clyde extends Ghost {
     constructor() {
        super();
        this.color = "#ffa500";
        this.behavior = Behavior.ELUSIVE;
    }   
}
