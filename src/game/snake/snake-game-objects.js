import { MatrixObject, Cursor, Collectable } from "../../matrix/matrix-object.js";
import { Direction } from "../../constants.js";
import { GameStatus } from "../event/game-events.js";
import { Point } from "../../point.js";
import { getRandomInt } from "../../helper/helper.js";

// export base objects to parent file
export * from "../../matrix/matrix-object.js";

export class SnakeHead extends Cursor {
    constructor(direction = Direction.UP) {
        super();
        this.direction = direction;
    }
}

export class SnakeBody extends MatrixObject {
    constructor(from = Direction.UP, to = Direction.DOWN) {
        super(false);
        this.from = from;
        this.to = to;
    }
}

export class SnakeTail extends MatrixObject {
    constructor(direction = Direction.DOWN) {
        super(false);
        this.direction = direction;
    }
}

export class Apple extends Collectable {

}
