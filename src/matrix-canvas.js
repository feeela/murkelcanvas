import { extend } from "./helper/helper.js";
import { Point } from "./point.js";
import { MurkelCanvas } from "./murkel-canvas.js";
import { Text, TextBlock } from "./text.js";
import { Direction, Specificity } from "./constants.js";
import { Matrix } from "./matrix/matrix.js";
import { Space, Cursor } from "./matrix/matrix-object.js";
import { getRenderer } from "./render/matrix-renderer.js";

export { getRenderer };

/*
// iterate over all available matrix/game objects
for(let objectName in MatrixObjects) {
    console.log({
        name: objectName,
        class: MatrixObjects[objectName],
    });
}
*/

/**
 * A 2D matrix with user control and renderer. 
 * 
 * Each point in the internal matrix of this object holds an array of object ID's
 * and can thus be used to render an unlimited amount of objects in total and per field.
 * 
 * Terminology:
 * 
 *  - matrix: a 2D array containing rows which contain columns which contain fields with values
 *  - field: a point in the matrix designated by X & Y coordinates
 *  - field value: any field in the matrix contains a list of objects, which are currently placed on this field
 *  - cursor: the active point in the matrix, can be controlled with keyboard (e.g. the player position)
 *  - pointer: the current mouse (or other pointer device) position
 */
export class MatrixCanvas extends MurkelCanvas {

    static defaultOptions = extend(super.defaultOptions, {
        cols: 20,
        rows: 20,
        cursor: Cursor,
        cursorInitialPos: new Point(),
        getRenderer: null,
        fillConstructor: Space,
        fillDistinct: false,
        debugDisplayRemoveAfter: 5000, // only used if options.debug = true
        debugDisplayOptions: {
            size: 14,
            textAlign: "left",
            background: "rgba(255,255,255,.8)",
            padding: 16,
        },
    });

    constructor(canvas, options = {}) {
        // The matrix requires a call to Matrix.run() to start the canvas paint cycle
        super(canvas, options, false);

        this.matrix = this.getEmptyMatrix();

        // Set active cursor
        this.cursor = this.options.cursorInitialPos;

        // Add an object representing the cursor to that field
        if(this.options.cursor !== false) {
            this.matrix.addValue(this.cursor, new this.options.cursor());
        }

        // The direction to walk into
        this.direction = null;

        // initialize renderer
        if(this.options.getRenderer !== null) {
            this.getRendererMethod = this.options.getRenderer;
        } else {
            this.getRendererMethod = getRenderer;
        }
        this.resetRenderer();

        this.visibleFrom = new Point();
        this.visibleTo = new Point(this.cols - 1, this.rows - 1);

        window.addEventListener("keydown", function(e) {
            // Disable scrolling with arrow keys
            if(["ArrowUp","ArrowDown"].indexOf(event.code) > -1) {
                event.preventDefault();
            }
        }, false);

        this.lastClickedTile = false;
        this.lastClickedTileUpdate = performance.now();

        // focus canvas to capture keyboard events
        this.context.canvas.focus();
    }

    getEmptyMatrix() {
        return new Matrix(this.cols, this.rows, this.options.fillConstructor, this.options.fillDistinct);
    }

    /**
     * Calculate a matrix grid point from the current pointer position
     */
    matrixPointFromPointer() {
        return new Point(
            Math.floor(this.pointer.x / this.gridSizeX - this.offsetX / this.gridSizeX),
            Math.floor(this.pointer.y / this.gridSizeY - this.offsetY / this.gridSizeY)
        );
    }

    onPointerDown(pointer) {
        super.onPointerDown(pointer);

        this.lastClickedTile = this.matrixPointFromPointer(pointer);
        this.lastClickedTileUpdate = performance.now();
    }

    /**
     * Offset can not be positive in MatrixCanvas and matrix can only be moved up to the right/bottom edges.
     */
    onPointerMove(pointer) {
        super.onPointerMove(pointer);

        if(this.pointerBeforeDown !== null && this.options.zoom == true) {
            this.updateVisiblePortion();
        }
    }

    /**
     * Add direction to keyboard handling
     */
    onKeyPress(event) {
        // Do not listen to keyboard controls when paused; except pause itself, which is handled in `super.onKeyPress()`
        if(this.isPaused === true) {
            super.onKeyPress(event);
            return;
        }

        switch(event.key) {
            case "w":
            case "ArrowUp":
                this.direction = Direction.UP;
                break;
            case "d":
            case "ArrowRight":
                this.direction = Direction.RIGHT;
                break;
            case "s":
            case "ArrowDown":
                this.direction = Direction.DOWN;
                break;
            case "a":
            case "ArrowLeft":
                this.direction = Direction.LEFT;
                break;
            default:
                super.onKeyPress(event);
        }
    }

    zoom(percent) {
        super.zoom(percent);

        this.resetRenderer();
        this.updateVisiblePortion();
    }

    /**
     * Calculate visible portion of matrix according to offset.
     */
    updateVisiblePortion() {
        this.visibleFrom = new Point(
            this.offsetX > (this.gridSizeX * -1) ? 0 : Math.floor(Math.abs(this.offsetX) / this.gridSizeX),
            this.offsetY > (this.gridSizeY * -1) ? 0 : Math.floor(Math.abs(this.offsetY) / this.gridSizeY)
        );

        this.visibleTo = new Point(
            Math.min(this.visibleFrom.x + Math.ceil(this.width / this.gridSizeX), this.cols - 1),
            Math.min(this.visibleFrom.y + Math.ceil(this.height / this.gridSizeY), this.rows - 1)
        );
    }

    setRowsCols(rows, cols) {
        super.setRowsCols(rows, cols);

        if(this.matrix) {
            this.matrix.rows = rows;
            this.matrix.cols = cols;
        }
    }

    /**
     * Move "this.cursor" one field into "direction".
     * Maybe also push objects out of the way.
     *
     * @var {Direction} direction
     */
    moveCursor(direction, newCursor = null) {
        if(newCursor === null) {
            newCursor = this.cursor.move(direction);
        }

        if(!this.matrix.isValidPoint(newCursor)) {
            return false;
        }

        let movable = this.matrix.findValue(newCursor, mo => mo.movable == true);
        if(!this.matrix.isPassable(newCursor) && movable === false) {
            return false;
        }

        if(movable !== false) {
            let afterNewCursor = newCursor.move(direction);

            if(!this.matrix.isValidPoint(afterNewCursor) || !this.matrix.isPassable(afterNewCursor)) {
                return false;
            }

            movable.forEach(mov => {
                this.matrix.moveValue(newCursor, afterNewCursor, mov.constructor);
            });
        }

        // move the cursor object from one field to the other
        this.matrix.moveValue(this.cursor, newCursor, Cursor);

        // update internal storage of where it is
        this.cursor = newCursor;

        return true;
    }

    resetRenderer() {
        this.renderer = this.getRendererMethod(this);
    }

    /**
     * Manipulate or add game contents and react to different statuses.
     */
    content(removeDirection = true) {
        super.content();

        // Move cursor if the user is pressing a key during this paint cycle
        if(this.direction !== null) {
            this.moveCursor(this.direction);
            if(removeDirection === true) {
                this.direction = null;
            }
        }


        for(let x = this.visibleFrom.x; x <= this.visibleTo.x; x++) {
            for(let y = this.visibleFrom.y; y <= this.visibleTo.y; y++) {
                let point = new Point(x, y);
                let values = this.matrix.getValues(point);
                // Add to layer
                values.forEach(matrixObject => {
                    this.layers.layer(matrixObject.specificity).addContent(() => {
                        // Execute the MatrixObject.run() method for each object
                        matrixObject.run(this, point.x, point.y);

                        // Render the current object
                        this.renderer.render(matrixObject, point.x, point.y);
                    });
                });
            }
        }
/*
        this.matrix.forEach((point, values) => {
            values.forEach(matrixObject => {
                // Add to layer
                this.layers.layer(matrixObject.specificity).addContent(() => {
                    // Execute the MatrixObject.run() method for each object
                    matrixObject.run(this, point.x, point.y);

                    // Render the current object
                    this.renderer.render(matrixObject, point.x, point.y);
                });
            });
        });
*/
        if(this.options.debug === true) {
            this.debugContent();
        }
    }

    debugContent() {
        // Display cursor coordinates.
        if(this.options.cursor !== false) {
            this.layers.layer(Specificity.FOREGROUND).addContent(() => this.drawPosition());
        }

        // If the user had clicked somewhere, display infos about the values from the corresponding matrix field.
        if(this.lastClickedTile) {
            // Ensure to remove (not add again) the message after some time
            if(performance.now() < (this.lastClickedTileUpdate + this.options.debugDisplayRemoveAfter)) {
                this.layers.layer(Specificity.FOREGROUND).addContent(() => {
                    let msg = new TextBlock(this.context, this.gridSizeX, this.gridSizeY, this.options.debugDisplayOptions);
                    msg.heading(`Objects at position x: ${this.lastClickedTile.x}, y: ${this.lastClickedTile.y}`);
                    msg.text(""); // empty line
                    this.matrix.getValues(this.lastClickedTile).forEach(val => {
                        msg.heading(val.constructor.name);
                        for(let key in val) {
                            msg.text(`– ${key}: ${val[key]}`);
                        }
                        msg.text(""); // empty line
                    });

                    msg.draw();
                });
            }
        }
    }

    /**
     * For debug purposes: print current cursor point coordinates
     */
    drawPosition() {
        let text = new Text(this.context, `x: ${this.cursor.x}  y: ${this.cursor.y}`, 10, this.height - 20);
        text.draw();
    }
}