
/**
 * @todo This class should be an Iterable itself (using the internal generator)
 */
export class Lindenmayer {
    constructor(variables, constants, axiom, rules, maxIterations = 10) {
        this.variables = variables;
        this.constants = constants;
        this.axiom = axiom;
        this.rules = rules;

        this.maxIterations = maxIterations;
        this.index = 0;
    }

    *axiomGenerator() {
        while(this.index < this.maxIterations) {
            // Yield first, to also return the original input axiom
            yield this.axiom;

            let newAxiom = [];
            this.axiom.split("").forEach(rule => {
                if(!this.variables.includes(rule) && !this.constants.includes(rule)) {
                    throw new Error(`Variable "${rule}" is not defined in grammar.`);
                }

                if(this.rules[rule]) {
                    newAxiom.push(this.rules[rule]);
                } else {
                    newAxiom.push(rule);
                }
            });

            this.axiom = newAxiom.join("");

            this.index++;
        }
        return false;
    }

    getAxiom() {
        if(!this.generator) {
            this.generator = this.axiomGenerator();
        }

        return this.generator.next().value;
    }
}


export const LindenmayerPresets = {
    "play": {
        variables: ["A", "B", "G"],
        // Those are the actual movement of the default turtle grammar
        constants: ["M", "F", "L", "D", "+", "-", "[", "]"],
        axiom: "A",
        rules: {
            A: "GDG+A",
            G: "GF",
/* Spiral
            A: "AG+",
            G: "GF",
*/
/* Snail spiral
            A: "AG++[++++G]",
            G: "GF",
*/
/* Feather spiral
            A: "AG[-C]+G[-C]+",
            C: "G[-C][+C]FD",
            D: "[+L]-L",
            G: "GG",
*/
/* Chainsaw spiral
            A: "AG+[C]GF+[C]GFF+",
            G: "GF",
            C: "---F+++G+F",
*/
/* Octagon spiral
            A: "AG+GF+GFF+",
            G: "GF",
*/
/* Spiral tiles
            A: "AG+GF+GFF+",
            G: "GF",
*/
/* Mondrian
            A: "AGFF+GF+G+",
            G: "GG",
*/
/*
            A: "BBBBBB+++N---A",
            B: "F+MG+F",
            C: "--M++F+FM",
            G: "GG",
            N: "NM",
*/
/*
            A: "F[+FA]--FA",
            B: "A-F+FA"
*/
/* overlapping Hexagon loop
            A: "F+F+FB",
            B: "F[--FA]F[FA]",
            C: "CF",
*/
/* swastika starfish
            A: "+FB+FB+FB+FB",
            B: "[+F-F+AF-FA]",
            F: "FF",
*/
        },
        limit: 5,
        //angle: 25,
        grammar: {
            G: (turtle) => turtle.line(1),
            E: (turtle) => turtle.line(1),
            //N: (turtle) => turtle.move(1),
        },
/*
        stepSize: (() => {
            let sz = 1;
            return (n, axiom, width, height) => {
                let oldSz = sz;
                sz = sz * 2;
                return width / oldSz;
            };
        })(),
*/
    },
    "algae": {
        variables: ["A", "B"],
        constants: ["F", "G", "+", "-", "[", "]"],
        axiom: "+++++++++A",
        rules: {
            A: "AG[-BF]",
            B: "-A+",
            G: "GF",
        },
        grammar: {
            A: (turtle) => turtle.line(1),
            B: (turtle) => turtle.line(1),
            G: (turtle) => turtle.line(1),
        },
        limit: 8,
        angle: 10,
        bottom: 0,
    },
    "fractal-binary-tree": {
        variables: ["L", "F"],
        constants: ["+", "-", "[", "]"],
        axiom: "+++L",
        rules: {
            L: "F[+L]-L",
            F: "FF",
        },
        limit: 7,
        angle: 30,
        bottom: 0,
    },
    "cantor-set": {
        variables: ["F", "M"],
        constants: [],
        axiom: "F",
        rules: {
            F: "FMF",
            M: "MMM",
        },
        limit: 5,
        left: 0,
        stepSize: (() => {
            let sz = 1;
            return (n, axiom, width, height) => {
                let oldSz = sz;
                sz = sz * 3;
                return width / oldSz;
            };
        })(),
    },
    "koch-curve": {
        variables: ["F"],
        constants: ["+", "-"],
        axiom: "F",
        rules: {
            F: "F+F-F-F+F",
        },
        limit: 6,
        left: 0,
        stepSize: (() => {
            let sz = 1;
            return (n, axiom, width, height) => {
                let oldSz = sz;
                sz = sz * 3;
                return width / oldSz;
            };
        })(),
    },
    "sierpinski-triangle": {
        variables: ["F", "G"],
        constants: ["+", "-"],
        axiom: "F-G-G",
        rules: {
            F: "F-G+F+G-F",
            G: "GG",
        },
        limit: 7,
        angle: 120,
        grammar: {
            G: (turtle) => turtle.line(1),
        },
        top: 10,
        left: 10,
        stepSize: (() => {
            let sz = 1;
            return (n, axiom, width, height) => {
                let oldSz = sz;
                sz = sz * 2.025;
                return width / oldSz;
            };
        })(),
    },
    "sierpinski-arrowhead-curve": {
        variables: ["F", "G"],
        constants: ["+", "-"],
        axiom: "F",
        rules: {
            F: "G-F-G",
            G: "F+G+F",
        },
        limit: 7,
        angle: 60,
        grammar: {
            G: (turtle) => turtle.line(1),
        },
        left: 10,
        stepSize: (() => {
            let sz = 1;
            return (n, axiom, width, height) => {
                let oldSz = sz;
                sz = sz * 2.25;
                return width / oldSz;
            };
        })(),
    },
    "dragon-curve": {
        variables: ["F", "G"],
        constants: ["+", "-"],
        axiom: "F",
        rules: {
            F: "F+G",
            G: "F-G",
        },
        limit: 12,
        grammar: {
            G: (turtle) => turtle.line(1),
        },
    },
    "fractal-plant": {
        variables: ["X", "F"],
        constants: ["+", "-", "[", "]"],
        axiom: "++++X",
        rules: {
            X: "F+[[X]-X]-F[-FX]+X",
            F: "FF",
        },
        angle: 25,
        bottom: 0,
        limit: 6,
    },
    "bush": {
        variables: ["X", "F"],
        constants: ["L", "+", "-", "[", "]"],
        axiom: "+++X",
        rules: {
            X: "F[+XL]F[X][-XL]",
            F: "FF",
        },
        bottom: 0,
        limit: 6,
        angle: 30,
    },
    "bush2": {
        variables: ["A", "B", "C", "D", "F"],
        constants: ["L", "+", "-", "[", "]"],
        axiom: "+++A",
        rules: {
            A: "F[+AB][-AB][F[CAB]]",
            B: "[+L--L]",
            C: "+D",
            D: "-C",
            F: "FF",
        },
        bottom: 0,
        limit: 6,
        angle: 30,
    },
    "plant1": {
        variables: ["X", "F"],
        constants: ["L", "+", "-", "[", "]"],
        axiom: "+++X",
        rules: {
            X: "F[+XL][FX][-XL]",
            F: "FF",
        },
        bottom: 0,
        limit: 6,
        angle: 30,
    },
    "plant2": {
        variables: ["F"],
        constants: ["+", "-", "[", "]"],
        axiom: "++++F",
        rules: {
            F: "FF-[-F+F+F]+[+F-F-F]",
        },
        bottom: 0,
        limit: 6,
        angle: 22,
    },
    "hilbert-curve": {
        variables: ["A", "B"],
        constants: ["F", "+", "-"],
        axiom: "A",
        rules: {
            A: "+BF-AFA-FB+",
            B: "-AF+BFB+FA-",
        },
        limit: 6,
        left: 10,
        bottom: 10,
        stepSize: (() => {
            let sz = 1;
            return (n, axiom, width, height) => {
                let oldSz = sz;
                sz = sz * 2;
                return width / oldSz;
            };
        })(),
    },
    "tile1": {
        variables: ["F"],
        constants: ["+", "-"],
        axiom: "F+F+F+F",
        rules: {
            F: "FF+F+F+F+F+F-F",
        },
        limit: 4,
        bottom: 10,
    },
    "tile2": {
        variables: ["F"],
        constants: ["+", "-"],
        axiom: "F+F+F+F",
        rules: {
            F: "FF+F++F+F",
        },
        limit: 4,
        left: 10,
        bottom: 10,
    },
    "tile3": {
        variables: ["F"],
        constants: ["+", "-"],
        axiom: "F+F+F+F",
        rules: {
            F: "FF+F+F+F+FF",
        },
        limit: 4,
        left: 10,
        bottom: 10,
    },
    "kolam-snake": {
        variables: ["X"],
        constants: ["F", "+", "-"],
        axiom: "F+XF+F+XF",
        rules: {
            X: "XF-F-F+XF+F+XF-F-F+X",
        },
        limit: 5,
        bottom: 10,
    },
    "kolam-krishna": {
        variables: ["X"],
        constants: ["F", "+", "-"],
        axiom: "-X--X",
        rules: {
            X: "XFX--XFX",
        },
        limit: 6,
        top: 50,
        angle: 45,
    },
};
