
export class Storage {
	constructor() {
        this.storage = {};
	}

	fillKeys(keys, fillValue = 0) {
		keys.forEach(key => this.storage[key] = 0);
	}

	add(key, amount = 1) {
		if(!this.storage[key]) {
			this.storage[key] = 0;
		}

		this.storage[key] = this.storage[key] + amount;
	}

	/**
	 * @return {Boolean} Returns `false` if the key was not set or the amonut was greater than available in the storage.
	 */
	take(key, amount = 1) {
		if(!this.storage[key] || this.storage[key] < amount) {
			return false
		}

		this.storage[key] = this.storage[key] - amount;
		return true;
	}

	has(key, minAmount) {
		if(this.storage[key] && this.storage[key] >= minAmount) {
			return true;
		} else {
			return false;
		}
	}

	check(storageLike) {
        for(let key in storageLike) {
            if(this.storage[key] < storageLike[key]) {
                return false;
            }
        }
        return true;
	}
}
